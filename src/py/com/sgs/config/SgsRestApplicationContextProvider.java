package py.com.sgs.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SgsRestApplicationContextProvider  implements ApplicationContextAware {

	private static ApplicationContext thotdnaApplicationContext;
	private Logger log = LoggerFactory.getLogger(getClass());
		
	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
		log.debug("setting spring context");
		thotdnaApplicationContext = applicationContext;		
	}

	public static ApplicationContext getContext() {
		return thotdnaApplicationContext;
	}
	
}
