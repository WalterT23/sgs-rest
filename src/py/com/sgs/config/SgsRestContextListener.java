package py.com.sgs.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;


public class SgsRestContextListener implements ServletContextListener {

	Logger log = LoggerFactory.getLogger(getClass());
	
	static String buildNumber = "unknown";
	static String vendor = "unknown";
	static String version = "unknown";
	static String artifactName = "unknown";
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		log.info("Stopped {} {} build {} by {}",new String[]{artifactName, version,buildNumber,vendor});
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		//bootstrap to mybatis logging
		org.apache.ibatis.logging.LogFactory.useSlf4jLogging();
		
		ApplicationContext ctx =  SgsRestApplicationContextProvider.getContext();

		ApplicationInfo info = (ApplicationInfo) ctx.getBean("applicationInfo");
		
		log.debug("{}",info);
		
		buildNumber = info.getBuild();
		vendor = info.getVendor();
		artifactName = info.getArtifact();
		version = info.getVersion();		
		
		log.info("Started {} {} build {} by {}",new String[]{artifactName, version,buildNumber,vendor});
		
		
	}

}
