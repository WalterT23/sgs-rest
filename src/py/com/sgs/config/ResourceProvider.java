package py.com.sgs.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

public class ResourceProvider {
	
	private static String propFileProtocol = "file://";
	private static Logger log = LoggerFactory.getLogger(ResourceProvider.class);
  
  public static String getResourcesPath() throws FileNotFoundException, IOException {
    ApplicationContext appctx = SgsRestApplicationContextProvider.getContext();
    ConfigLocator config = ((ConfigLocator) appctx.getBean("configLocator"));

    StringBuffer confPath = new StringBuffer();
    confPath.append(config.getServerPath());
    confPath.append("/apps/sgs/");

    log.debug("Returning path: {}", confPath.toString());
    return confPath.toString();
  }

	public static Properties getResource(String resource) throws FileNotFoundException, IOException {
		Properties props = null;
		ApplicationContext appctx = SgsRestApplicationContextProvider.getContext();
	  ConfigLocator config = ((ConfigLocator) appctx.getBean("configLocator"));
	    
		StringBuffer confPath = new StringBuffer();
		confPath.append(config.getServerPath());
		confPath.append("/apps/sgs/");
		confPath.append(resource);
		log.debug("Loading properties: {}", confPath.toString());
		InputStream stream = new FileInputStream(confPath.toString());

		props = new Properties();
		props.load(stream);
		
		return props;
	}

  public static URL getResourceURL(String resource) throws FileNotFoundException, IOException {
    URL propsURL = null;
    ApplicationContext appctx = SgsRestApplicationContextProvider.getContext();
    ConfigLocator config = ((ConfigLocator) appctx.getBean("configLocator"));
    
    StringBuffer confPath = new StringBuffer();
    confPath.append(propFileProtocol);
    confPath.append(config.getServerPath());
    confPath.append("/apps/sgs/");
    confPath.append(resource);
    
    String path = confPath.toString();
    path = path.replace("C:", "");
    propsURL = new URL (path);
    
    return propsURL;
  }
  
}
