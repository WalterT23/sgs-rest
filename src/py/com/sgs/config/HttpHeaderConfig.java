package py.com.sgs.config;

public class HttpHeaderConfig {

	private String userPrincipalHeader;
	private String applicationNameHeader;
	private String applicationPageHeader;
	private String contextAttributeName;
	private String userSucursalHeader;

	public String getUserPrincipalHeader() {
		return userPrincipalHeader;
	}
	public void setUserPrincipalHeader(String userPrincipalHeader) {
		this.userPrincipalHeader = userPrincipalHeader;
	}
	public String getApplicationNameHeader() {
		return applicationNameHeader;
	}
	public void setApplicationNameHeader(String applicationNameHeader) {
		this.applicationNameHeader = applicationNameHeader;
	}
	public String getApplicationPageHeader() {
		return applicationPageHeader;
	}
	public void setApplicationPageHeader(String applicationPageHeader) {
		this.applicationPageHeader = applicationPageHeader;
	}
	public String getContextAttributeName() {
		return contextAttributeName;
	}
	public void setContextAttributeName(String contextAttributeName) {
		this.contextAttributeName = contextAttributeName;
	}
	public String getUserSucursalHeader() {
		return userSucursalHeader;
	}
	public void setUserSucursalHeader(String userSucursalHeader) {
		this.userSucursalHeader = userSucursalHeader;
	}



}
