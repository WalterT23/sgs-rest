package py.com.sgs.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;


public class BeanTransmorfer {
  
	@SuppressWarnings({ "rawtypes", "unchecked" })
  public static Object toUpperCase(Object obj){
	  if (obj == null)
      return null;
	  
	  Class clase = null;
    Field fields[] = null;
    Method method = null;
    String getterName = "";
    String setterName = "";
    String fieldName = "";
    Object fieldValue = null;
    Class fieldType = null;
    
    clase = obj.getClass();
    fields = clase.getDeclaredFields();
    for (Field field: fields){
      fieldName = field.getName();
      fieldType = field.getType();
        
      getterName = "get" + fieldName.substring(0,1).toUpperCase() + fieldName.substring(1,fieldName.length());
      setterName = "set" + fieldName.substring(0,1).toUpperCase() + fieldName.substring(1,fieldName.length());
      try{
        if (fieldType.getName().equals("java.lang.String")){
          method = clase.getMethod(getterName,null);
          fieldValue = method.invoke(obj, null);
          
          fieldValue = fieldValue!=null?((String)fieldValue).toUpperCase():null;
          
          method = clase.getMethod(setterName,String.class);
          method.invoke(obj, fieldValue);
        }
        
      } catch (Exception e) {
        throw new ServiceException(ErrorCode.PARAMETER_ERROR, e, "internal.error");
      }
    }
	  
	  return obj;
	}
}
