package py.com.sgs.util;

import java.util.Comparator;

import py.com.sgs.persistence.dto.consulta.ResumenHorarioVentasDTO;


public class HorariosComparator implements Comparator<ResumenHorarioVentasDTO>{

	@Override
	public int compare(ResumenHorarioVentasDTO o1, ResumenHorarioVentasDTO o2) {
		int resultado = 0;

		resultado = o1.getMonto().compareTo(o2.getMonto());

		if(resultado > 0){
			return -1;
		}else if (resultado < 0){
			return 1;
		}
		return resultado;
	}

}
