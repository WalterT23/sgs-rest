package py.com.sgs.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;


@SuppressWarnings({"unchecked"})
public class JSONTranslator<T> {
	
	private Logger log = LoggerFactory.getLogger(getClass());
	private Class<T> clazz;
	
	public JSONTranslator(Class<T> clazz) {	
		this.clazz = clazz;
	}

	public T fromJSON(String jsondata) throws ServiceException {
		try {
			
			if (log.isTraceEnabled()){
				log.trace("converting into dto of type {} from  {}",this.clazz,jsondata);
			}
			
			ObjectMapper mapper = new ObjectMapper();
			T response = (T) mapper.readValue(jsondata, this.clazz);
			return response;
		} catch (JsonGenerationException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		} catch (JsonMappingException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		} catch (IOException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		}
	}	
	
	public String toJSON(T instance) throws ServiceException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(instance);
			return response;
		} catch (JsonGenerationException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		} catch (JsonMappingException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		} catch (IOException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		}
	}
	
	
	public List<T> toList(String jsondata) throws ServiceException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			JavaType t = mapper.getTypeFactory().constructCollectionType(List.class, this.clazz);
			return  (List<T>)mapper.readValue(jsondata, t);
		} catch (JsonGenerationException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		} catch (JsonMappingException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		} catch (IOException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		}
	}
	
	
		
	public String fromList(List<T> list) throws ServiceException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			String response = mapper.writeValueAsString(list);
			return response;
		} catch (JsonGenerationException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		} catch (JsonMappingException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		} catch (IOException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		}
	}

	public Map<String, Object> toMap(String jsonString) throws ServiceException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> response = mapper.readValue(jsonString, HashMap.class);
			return response;
		} catch (JsonGenerationException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		} catch (JsonMappingException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		} catch (IOException e) {
			throw new ServiceException(ErrorCode.JSON_ENCODING_ERROR, e);
		}
	}
}