package py.com.sgs.util.report;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.OrdenDeCompraDTO;
import py.com.sgs.web.services.impl.OrdenDeCompraServiceImpl;

public class ReportOC {
	
	private static Font helvetica6 = FontFactory.getFont(FontFactory.HELVETICA, 6F);
	private static Font helvetica7 = FontFactory.getFont(FontFactory.HELVETICA, 7F);
	private static Font helvetica8 = FontFactory.getFont(FontFactory.HELVETICA, 8F);
	private static Font helvetica9 = FontFactory.getFont(FontFactory.HELVETICA, 9F);
	//	private static Font helvetica10 = FontFactory.getFont(FontFactory.HELVETICA, 10F);
	private static Font helvetica7Bold = FontFactory.getFont(FontFactory.HELVETICA, 7F, Font.BOLD);
	private static Font helvetica8Bold = FontFactory.getFont(FontFactory.HELVETICA, 8F, Font.BOLD);
	private static Font helvetica9Bold = FontFactory.getFont(FontFactory.HELVETICA, 9F, Font.BOLD);
	private static Font helvetica10Bold = FontFactory.getFont(FontFactory.HELVETICA, 10F, Font.BOLD);
	//	private static Font helvetica12Bold = FontFactory.getFont(FontFactory.HELVETICA, 12F, Font.BOLD);

	private Properties prop = null;
	private Logger log = LoggerFactory.getLogger(getClass());
	
	private OrdenDeCompraDTO composite;
	
	public ReportOC(OrdenDeCompraDTO composite) {
		this.composite = composite;
	}

	public ReportOC(){}
	
	public byte[] generarPDF() throws ServiceException{
		PdfPTable table = null;
		try {
			Document document=new Document(PageSize.A4, 30, 30, 40, 36);
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			//for test only 
			PdfWriter writer = PdfWriter.getInstance(document, output);
//			PdfWriter.getInstance(document, output);
			document.open(); 
			
			String direccion = "1636 NW 82ND AVE. Doral, FL. 33126.";
			
//			Image img = Image.getInstance(facTemplatePath);
//			img.scaleToFit(500f, 800f);
//	        document.add(img);
//	        
//	        PdfContentByte canvas = writer.getDirectContentUnder();
//	        Image image = Image.getInstance(facTemplatePath);
//	        image.setAbsolutePosition(0, 0);
//	        canvas.saveState();
//	        PdfGState state = new PdfGState();
////	        state.fillOpacity = 0.6f;
//	        canvas.setGState(state);
//	        canvas.addImage(image);
//	        canvas.restoreState();
	    	//tabla para el detalle de las mercaderias
//			table = makeItemDetailsTable();
//			document.add(table);
			document.close();
			
			PdfReader reader = new PdfReader(output.toByteArray());
//
			ByteArrayOutputStream outputFinal = new ByteArrayOutputStream();
			PdfStamper stamper  = new PdfStamper(reader, outputFinal);
//			PdfContentByte add_watermark;   
//			// recorrer cada una de las paginas para poder agregar la cabecera a cada una
			int n = reader.getNumberOfPages();
//			for (int i = 1; i <= n; i++) {
//				getHeaderTable(i, n, formatDateWithOutTime(this.composite.getFacturas().get(0).getFecha())).writeSelectedRows(
//						0, -1, -303, 732, stamper.getOverContent(i));
//				getHeaderTable(i, n, formatDecimalNumber(this.composite.getFacturas().get(0).getMonto())).writeSelectedRows(
//						0, -1, -55, 642, stamper.getOverContent(i));
//				getHeaderTableDescription(i, n, this.composite.getMercaderia()).writeSelectedRows(
//						0, -1, -340, 642, stamper.getOverContent(i));
//				getHeaderTable(i, n, formatDecimalNumber(this.composite.getFacturas().get(0).getMonto())).writeSelectedRows(
//						0, -1, -55, 587, stamper.getOverContent(i));
//				getHeaderTableDescription2(i, n, this.composite.getConsignatario()).writeSelectedRows(
//						0, -1, -297, 581, stamper.getOverContent(i));
//				getHeaderTableDescription2(i, n, direccion).writeSelectedRows(
//						0, -1, -314, 568, stamper.getOverContent(i));
//				getHeaderTable(i, n, formatDecimalNumber(this.composite.getFacturas().get(0).getMonto())).writeSelectedRows(
//						0, -1, -55, 522, stamper.getOverContent(i));
//				getHeaderTable(i, n, formatDecimalNumber(this.composite.getFacturas().get(0).getMonto())).writeSelectedRows(
//						0, -1, -55, 457, stamper.getOverContent(i));
//				getHeaderTable(i, n, formatDecimalNumber(this.composite.getFacturas().get(0).getMonto())).writeSelectedRows(
//						0, -1, -55, 424, stamper.getOverContent(i));
//				getHeaderTableBold(i, n, formatDecimalNumber(this.composite.getFacturas().get(0).getMonto())).writeSelectedRows(
//						0, -1, -55, 392, stamper.getOverContent(i));
////				getFooterTable().writeSelectedRows(
////						0, -1, 30, 30, stamper.getOverContent(i));
////				add_watermark = stamper.getUnderContent(i);
////				add_watermark.addImage(watermark_image);
//			}
//
			stamper.close();
			reader.close();
//
//			SignatureUtils signatures = new SignatureUtils();
//			byte[] finalSignedReport = signatures.signPdf(outputFinal.toByteArray());

//			for test only 
//			File someFile = new File("/home/nsuarez/Escritorio/18002TERE045007X/" + composite.getNroGuia() + ".png");
//			FileOutputStream fos = new FileOutputStream(someFile);
//			fos.write(outputFinal.toByteArray());
//			fos.flush();
//			fos.close();

			return outputFinal.toByteArray();
		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}

	/**
	 * Crear el header con la pagina X de Y
	 * @param x el nro de pagina
	 * @param y el nro total de paginas
	 * @return a table that can be used as header
	 */
	public static PdfPTable getHeaderTable(int x, int y, String valor) {
		PdfPTable table = new PdfPTable(2);
		table.setTotalWidth(527);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(20);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell("");
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		Paragraph p = new Paragraph(valor, helvetica8);
		table.addCell(p);
		return table;
	}
	
	public static PdfPTable getHeaderTableBold(int x, int y, String valor) {
		PdfPTable table = new PdfPTable(2);
		table.setTotalWidth(527);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(20);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell("");
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		Paragraph p = new Paragraph(valor, helvetica8Bold);
		table.addCell(p);
		return table;
	}
	
	public static PdfPTable getHeaderTableDescription(int x, int y, String valor) {
		PdfPTable table = new PdfPTable(2);
		table.setTotalWidth(527);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(20);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell("");
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		Paragraph p = new Paragraph(valor, helvetica8);
		table.addCell(p);
		return table;
	}
	
	public static PdfPTable getHeaderTableDescription2(int x, int y, String valor) {
		PdfPTable table = new PdfPTable(2);
		table.setTotalWidth(527);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(20);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell("");
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		Paragraph p = new Paragraph(valor, helvetica7);
		table.addCell(p);
		return table;
	}

	/**
	 * Crear el footer con las firmas autorizadas
	 * @return a table that can be used as footer
	 */
	public static PdfPTable getFooterTable() {
		PdfPTable table = new PdfPTable(1);
		table.setTotalWidth(527);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(20);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(new Paragraph("Firmas Autorizadas: ............................"
				+ "............................................................." 
				+ "............................................................." , helvetica9));
		return table;
	}

	/**
	 * Cabecera del DS_DDT conteniendo la informacion gral del despacho simplificado,
	 * debe descomentarse para tomar los valores del dto
	 * @param composite
	 * @return
	 * @throws ServiceException
	 */
	private PdfPTable makeItemDetailsTable() throws ServiceException {
		PdfPCell cell = null;
		Paragraph p = null;

		try{
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			PdfPTable table = new PdfPTable(5);
			float[] columnWidths = {2f, 1.5f, 2f, 2f, 2f};
			table.setWidths(columnWidths);
			table.setWidthPercentage(100);
			table.setSpacingBefore(5);
			table.setSpacingAfter(5);

//			// BACKGROUND
//			InputStream is = DespachoSimplificadoServiceImpl.class.getClassLoader().getResourceAsStream("white.bmp");
//
//			is = DespachoSimplificadoServiceImpl.class.getClassLoader().getResourceAsStream("logo_aduana_bw.png");
//			Image logo = Image.getInstance(IOUtils.toByteArray(is));
//			logo.scaleAbsolute(83, 50);
//			logo.setAbsolutePosition(30, (PageSize.A4.getHeight() - logo.getScaledHeight() - 20));
//			cell = new PdfPCell();
//			p = new Paragraph("", helvetica10Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.addElement(p);
//			cell.setBorder(Rectangle.NO_BORDER);
//			cell.setColspan(12);
//			table.addCell(cell);
//			
//			cell = new PdfPCell();
//			p = new Paragraph("", helvetica10Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.addElement(p);
//			cell.setBorder(Rectangle.NO_BORDER);
//			cell.setColspan(10);
//			table.addCell(cell);
//			
//			cell = new PdfPCell();
//			p = new Paragraph("", helvetica10Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.addElement(p);
//			cell.setBorder(Rectangle.NO_BORDER);
//			cell.setColspan(10);
//			table.addCell(cell);
//			
//			cell = new PdfPCell();
//			p = new Paragraph("", helvetica10Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.addElement(p);
//			cell.setBorder(Rectangle.NO_BORDER);
//			cell.setColspan(10);
//			table.addCell(cell);

			
			//row 2
			cell = new PdfPCell();
			p = new Paragraph("", helvetica10Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.addElement(p);
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setColspan(10);
			table.addCell(cell);
//			
//			cell = new PdfPCell();
//			p = new Paragraph(formatDecimalNumber(this.composite.getFacturas().get(0).getMonto()), helvetica7);
//			cell.setIndent(3f);
//			cell.setBorder(Rectangle.NO_BORDER);
//			cell.addElement(p);
//			cell.setColspan(2);
////			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			table.addCell(cell);
//
//			cell = new PdfPCell();
//			p = new Paragraph("Procedencia Vuelo: ",helvetica7);
//			cell.addElement(p);
//			cell.setColspan(2);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			table.addCell(cell);s.com

//			//row 3
//			cell = new PdfPCell();
//			p = new Paragraph("RUC: ",helvetica7);
//			cell.addElement(p);
//			cell.setColspan(2);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			table.addCell(cell);
//
//
//			cell = new PdfPCell();
//			p = new Paragraph("Guía Madre: ", helvetica7);
//			cell.setIndent(3f);
//			cell.addElement(p);
//			cell.setColspan(2);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			table.addCell(cell);
//			
//
//			//row 4
//			cell = new PdfPCell();
//			p = new Paragraph("TERE: ",helvetica7);
//			cell.addElement(p);
//			cell.setColspan(2);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			table.addCell(cell);
//			
//			cell = new PdfPCell();
//			p = new Paragraph("Cantidad de Guías: ",helvetica7);
//			cell.addElement(p);
//			cell.setColspan(2);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			table.addCell(cell);
//
//			makeBodyDetailTable(table);

			return table;
		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}

//	/**
//	 * Columns/rows settings para el detalle de las mercaderias
//	 * @param artList
//	 * @return
//	 * @throws ServiceException
//	 */
//	private PdfPTable makeBodyDetailTable(PdfPTable table) throws ServiceException {
//		PdfPCell cell = null;
//		Paragraph p = null;
//		try{
//			//			PdfPTable table = new PdfPTable(8);
//			//			float[] columnWidths = {1.5f, 4f, 5f, 3f, 2f, 4.6f, 2.2f, 2.2f};
//			//			table.setWidths(columnWidths);
//			//			table.setWidthPercentage(100);
//			//			table.setSpacingBefore(5);
//			//			table.setSpacingAfter(5);
//
//			//row 1
//			cell = new PdfPCell();
//			cell.setColspan(12);
//			table.addCell(cell);
//
//			//row 2
//			cell = new PdfPCell();
//			p = new Paragraph("DETALLE DE LA MERCADERÍA", helvetica8Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.addElement(p);
//			cell.setColspan(12);
//			table.addCell(cell);
//
//			setDetailsHeader(table);
//
//			table.setHeaderRows(12);
//
//			return table;
//		}catch (ServiceException e){
//			log.error(e.getMessage(), e);
//			throw new ServiceException(ErrorCode.INTERNAL_ERROR, e.getMessage());
//		}catch (Exception e){
//			log.error(e.getMessage(), e);
//			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
//		}
//	}


//	/**
//	 * Cabecera/titulo del detalle para las mercaderias
//	 * @param table
//	 * @throws ServiceException
//	 */
//	private void setDetailsHeader(PdfPTable table) throws ServiceException {
//		PdfPCell cell = null;
//		Paragraph p = null;
//		try{
//
//			cell = new PdfPCell();
//			p = new Paragraph("Guía",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			table.addCell(cell);
//
//			cell = new PdfPCell();
//			p = new Paragraph("Canal",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			table.addCell(cell);
//
//			cell = new PdfPCell();
//			p = new Paragraph("Importador",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			
//			table.addCell(cell);
//
//			cell = new PdfPCell();
//			p = new Paragraph("Origen",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			table.addCell(cell);
//
//			cell = new PdfPCell();
//			p = new Paragraph("Peso (Kg)",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			table.addCell(cell);
//
//			cell = new PdfPCell();
//			p = new Paragraph("Moneda",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			table.addCell(cell);
//
//			cell = new PdfPCell();
//			p = new Paragraph("Monto Moneda",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			table.addCell(cell);
//			
//			/////////////////
//			cell = new PdfPCell();
//			p = new Paragraph("Valor Guia USD",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			table.addCell(cell);
//			
//			cell = new PdfPCell();
//			p = new Paragraph("Valor Flete USD",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			table.addCell(cell);
//			
//			cell = new PdfPCell();
//			p = new Paragraph("Valor Seguro USD",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			table.addCell(cell);
//			
//			////////////////
//
//			cell = new PdfPCell();
//			p = new Paragraph("Valor Imp. USD",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			table.addCell(cell);
//			
//			cell = new PdfPCell();
//			p = new Paragraph("IVA (Gs.)",helvetica7Bold);
//			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			cell.addElement(p);
//			table.addCell(cell);
//
//		}catch (Exception e){
//			log.error(e.getMessage(), e);
//			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
//		}
//	}

	/**
	 * to format numbers
	 * @param n
	 * @return
	 * @throws ServiceException 
	 */
	private String formatDecimalNumber2Gs(Object n) throws ServiceException {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		DecimalFormat df2 = new DecimalFormat("#0.000", symbols);
		df2.setDecimalSeparatorAlwaysShown(false);
		df2.setGroupingUsed(true);
		df2.setGroupingSize(3);
		df2.setMaximumFractionDigits(0);
		df2.setMinimumFractionDigits(0);
		String formatted = df2.format(n);
		return formatted;
	}

	/**
	 * to format numbers
	 * @param n
	 * @return
	 * @throws ServiceException 
	 */
	private String formatDecimalNumber(Object n) throws ServiceException {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		if (n instanceof Long || n instanceof Integer) {
			DecimalFormat df2 = new DecimalFormat("#0.000", symbols);
			df2.setDecimalSeparatorAlwaysShown(false);
			df2.setGroupingUsed(true);
			df2.setGroupingSize(3);
			df2.setMaximumFractionDigits(0);
			df2.setMinimumFractionDigits(0);
			String formatted = df2.format(n);
			return formatted;
		}else if (n instanceof Double || n instanceof BigDecimal) {
			symbols.setDecimalSeparator(',');
			symbols.setGroupingSeparator('.');

			DecimalFormat df = new DecimalFormat("##0.00", symbols);
			df.setDecimalSeparatorAlwaysShown(false);
			df.setGroupingUsed(true);
			df.setGroupingSize(3);
			String formatted = df.format(n);
			return formatted;
		} else {
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"number.format.type.error");
		}
	}

	/**
	 * to format numbers
	 * @param n
	 * @return
	 * @throws ServiceException 
	 */
	private String formatDecimalKgs(Object n) throws ServiceException {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		symbols.setDecimalSeparator(',');
		symbols.setGroupingSeparator('.');

		DecimalFormat df = new DecimalFormat("#0.000", symbols);
		df.setDecimalSeparatorAlwaysShown(false);
		df.setGroupingUsed(true);
		df.setGroupingSize(3);
		String formatted = df.format(n);
		return formatted;

	}

	private String getCurrentDate() {
		Date date = new Date(System.currentTimeMillis());
		return formatDate(date);
	}

	private String formatDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String dateStr = formatter.format(date);
		return dateStr;
	}

	private String formatDateWithOutTime(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String dateStr = formatter.format(date);
		return dateStr;
	}
}
