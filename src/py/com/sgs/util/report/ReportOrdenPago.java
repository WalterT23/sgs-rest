package py.com.sgs.util.report;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import py.com.sgs.config.ResourceProvider;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.CabeceraOrdenPagoDTO;
import py.com.sgs.persistence.dto.composite.CabeceraOrdenPagoCompositeDTO;
import py.com.sgs.persistence.dto.composite.CabeceraVentaCompositeDTO;
import py.com.sgs.persistence.dto.composite.DetalleOrdenPagoCompositeDTO;
import py.com.sgs.persistence.dto.composite.DetalleVentaCompositeDTO;
import py.com.sgs.persistence.dto.composite.FormaPagoCompositeDTO;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

public class ReportOrdenPago {
	
	private static Font helvetica6 = FontFactory.getFont(FontFactory.HELVETICA, 6F);
	private static Font helvetica7 = FontFactory.getFont(FontFactory.HELVETICA, 7F);
	private static Font helvetica8 = FontFactory.getFont(FontFactory.HELVETICA, 8F);
	private static Font helvetica9 = FontFactory.getFont(FontFactory.HELVETICA, 9F);
	//	private static Font helvetica10 = FontFactory.getFont(FontFactory.HELVETICA, 10F);
	private static Font helvetica7Bold = FontFactory.getFont(FontFactory.HELVETICA, 7F, Font.BOLD);
	private static Font helvetica8Bold = FontFactory.getFont(FontFactory.HELVETICA, 8F, Font.BOLD);
	private static Font helvetica9Bold = FontFactory.getFont(FontFactory.HELVETICA, 9F, Font.BOLD);
	private static Font helvetica10Bold = FontFactory.getFont(FontFactory.HELVETICA, 10F, Font.BOLD);
	private static Font helvetica12Bold = FontFactory.getFont(FontFactory.HELVETICA, 12F, Font.BOLD);

	private Logger log = LoggerFactory.getLogger(getClass());
	
	private CabeceraOrdenPagoCompositeDTO composite;
	
	public ReportOrdenPago(CabeceraOrdenPagoCompositeDTO composite) {
		this.composite = composite;
	}

	public ReportOrdenPago(){}
	
	public byte[] generarPDF() throws ServiceException{
		PdfPTable table = null;
		try {
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			//for test only 
			Document document=new Document(PageSize.A4, 30, 30, 40, 36);

			//tabla para el detalle de las mercaderias
			table = makeItemDetailsTable();
			//PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(composite.getIdDsDdt() + ".pdf"));
			PdfWriter.getInstance(document, output);
			document.open(); 

			document.add(table);

			document.close();

			PdfReader reader = new PdfReader(output.toByteArray());
//
			ByteArrayOutputStream outputFinal = new ByteArrayOutputStream();
			PdfStamper stamper  = new PdfStamper(reader, outputFinal);
//			PdfContentByte add_watermark;   
			// recorrer cada una de las paginas para poder agregar la cabecera a cada una
			int n = reader.getNumberOfPages();
			for (int i = 1; i <= n; i++) {
				getHeaderTable(i, n).writeSelectedRows(
						0, -1, 34, 803, stamper.getOverContent(i));
				getFooterTable().writeSelectedRows(
						0, -1, 30, 30, stamper.getOverContent(i));
//				add_watermark = stamper.getUnderContent(i);
//				add_watermark.addImage(watermark_image);
			}

			stamper.close();
			reader.close();

			//for test only 
			//File someFile = new File("/home/nsuarez/Escritorio/" + composite.getNroOrdenPago() + ".pdf");
			//FileOutputStream fos = new FileOutputStream(someFile);
			//fos.write(outputFinal.toByteArray());
			//fos.flush();
			//fos.close();

			return outputFinal.toByteArray();
		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}
	

	/**
	 * Crear el header con la pagina X de Y
	 * @param x el nro de pagina
	 * @param y el nro total de paginas
	 * @return a table that can be used as header
	 */
	public static PdfPTable getHeaderTable(int x, int y) {
		PdfPTable table = new PdfPTable(2);
		table.setTotalWidth(527);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(20);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell("");
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(new Paragraph(String.format("Página %d de %d", x, y), helvetica9));
		return table;
	}

	/**
	 * Crear el footer con las firmas autorizadas
	 * @return a table that can be used as footer
	 */
	public static PdfPTable getFooterTable() {
		PdfPTable table = new PdfPTable(1);
		table.setTotalWidth(527);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(20);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
//		table.addCell(new Paragraph("Firmas Autorizadas: ............................"
//				+ "............................................................." 
//				+ "............................................................." , helvetica9));
		return table;
	}

	/**
	 * @param composite
	 * @return
	 * @throws ServiceException
	 */
	private PdfPTable makeItemDetailsTable() throws ServiceException {
		PdfPCell cell = null;
		Paragraph p = null;

		try{
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			PdfPTable table = new PdfPTable(5);
			float[] columnWidths = {2f, 2f, 2f, 2f, 2f};
			table.setWidths(columnWidths);
			table.setWidthPercentage(100);
			table.setSpacingBefore(5);
			table.setSpacingAfter(5);

			//logo
			String logoPath = ResourceProvider.getResourcesPath() + "/logo.png";
			Image logo = Image.getInstance(logoPath);
			logo.scaleAbsolute(83, 50);
			logo.setAbsolutePosition(30, (PageSize.A4.getHeight() - logo.getScaledHeight() - 20));
			
			cell = new PdfPCell(logo);
			cell.setRowspan(3);
			cell.setColspan(2);
			cell.setPaddingTop(4F);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_CENTER);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(" ", helvetica10Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.addElement(p);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setColspan(3);
			table.addCell(cell);

			// row 1
			cell = new PdfPCell();
			p = new Paragraph("Orden de Pago ", helvetica10Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(composite.getNroOrdenPago(), helvetica10Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(" ", helvetica10Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(5);
			table.addCell(cell);
			

			cell = new PdfPCell();
			p = new Paragraph("Fecha: ", helvetica7Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(1);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(composite.getFechaCreacion() != null ? formatter.format(composite.getFechaCreacion()) : null, helvetica7);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(1);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Sucursal: ",helvetica7Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(1);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(composite.getSucursal() != null ? composite.getSucursal().getDescripcion() : "",helvetica7);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Concepto: ",helvetica7Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(1);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(composite.getConcepto() != null ? composite.getConcepto().getDescripcion() : "",helvetica7);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(4);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Medio: ",helvetica7Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(1);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(composite.getMedio() != null ? composite.getMedio().getDescripcion() : "",helvetica7);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(1);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Cuenta: ",helvetica7Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(1);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(composite.getCuenta() != null ? composite.getCuenta().getNroCuenta().toString() : "",helvetica7);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(" ",helvetica7Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(5);
			table.addCell(cell);
			
			makeBodyDetailTable(table);
			
			return table;
		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}

	/**
	 * Columns/rows settings para el detalle de las mercaderias
	 * @param artList
	 * @return
	 * @throws ServiceException
	 */
	private PdfPTable makeBodyDetailTable(PdfPTable table) throws ServiceException {
		PdfPCell cell = null;
		Paragraph p = null;
		try{
			//row 1
			cell = new PdfPCell();
			cell.setColspan(5);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);

			setDetailsHeader(table);
			
			cell = new PdfPCell();
			p = new Paragraph("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------",helvetica7Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(5);
			table.addCell(cell);

//			table.setHeaderRows(12);
			addDetailsRow(composite.getDetalles(), table);

			return table;
		}catch (ServiceException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, e.getMessage());
		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}

	/**
	 * iteracion sobre el detalle de las mercaderias
	 * descomentar para su uso en produccion
	 * @param list
	 * @param table
	 * @throws ServiceException 
	 */
	private void addDetailsRow(List<DetalleOrdenPagoCompositeDTO> list, PdfPTable table) throws ServiceException {
		PdfPCell cell = null;
		Paragraph p = null;

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Integer maximaLongitud = 30;
		String tipoIva = "";
			for (DetalleOrdenPagoCompositeDTO d : list) {
				
				cell = new PdfPCell();
				p = new Paragraph(d.getFactura() != null ? d.getFactura().getNroFactura() : "", helvetica7);
				p.setAlignment(Element.ALIGN_LEFT);
				cell.addElement(p);
				cell.setColspan(1);
				cell.setBorderWidthBottom(Rectangle.NO_BORDER);
				cell.setBorderWidthLeft(Rectangle.NO_BORDER);
				cell.setBorderWidthRight(Rectangle.NO_BORDER);
				cell.setBorderWidthTop(Rectangle.NO_BORDER);
				table.addCell(cell);
				
				cell = new PdfPCell();
				p = new Paragraph(d.getFactura() != null ? formatter.format(d.getFactura().getFecha()) : null, helvetica7);
				p.setAlignment(Element.ALIGN_LEFT);
				cell.addElement(p);
				cell.setColspan(1);
				cell.setBorderWidthBottom(Rectangle.NO_BORDER);
				cell.setBorderWidthLeft(Rectangle.NO_BORDER);
				cell.setBorderWidthRight(Rectangle.NO_BORDER);
				cell.setBorderWidthTop(Rectangle.NO_BORDER);
				table.addCell(cell);
				
				cell = new PdfPCell();
				p = new Paragraph(new Paragraph(d.getFactura() != null ? formatDecimalNumber2Gs(d.getFactura().getTotal()) : "0", helvetica7));
				p.setAlignment(Element.ALIGN_LEFT);
				cell.addElement(p);
				cell.setColspan(2);
				cell.setBorderWidthBottom(Rectangle.NO_BORDER);
				cell.setBorderWidthLeft(Rectangle.NO_BORDER);
				cell.setBorderWidthRight(Rectangle.NO_BORDER);
				cell.setBorderWidthTop(Rectangle.NO_BORDER);
				table.addCell(cell);
				
				cell = new PdfPCell();
				p = new Paragraph(new Paragraph(d.getMonto() != null ? formatDecimalNumber2Gs(d.getMonto()) : "0", helvetica7));
				p.setAlignment(Element.ALIGN_LEFT);
				cell.addElement(p);
				cell.setColspan(1);
				cell.setBorderWidthBottom(Rectangle.NO_BORDER);
				cell.setBorderWidthLeft(Rectangle.NO_BORDER);
				cell.setBorderWidthRight(Rectangle.NO_BORDER);
				cell.setBorderWidthTop(Rectangle.NO_BORDER);
				table.addCell(cell);

			}
			
			cell = new PdfPCell();
			p = new Paragraph(" ", helvetica7);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(5);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(" ", helvetica7);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(5);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph("TOTAL: ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setColspan(1);
			cell.setRowspan(2);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(new Paragraph(composite.getMontoTotal() != null ? formatDecimalNumber2Gs(composite.getMontoTotal()) : "0", helvetica8Bold));
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setColspan(1);
			cell.setRowspan(2);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Confeccionado ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setColspan(1);
			cell.setRowspan(2);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Firmado ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setColspan(1);
			cell.setRowspan(2);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Autorizado ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setColspan(1);
			cell.setRowspan(2);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(" ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(5);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(" ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(5);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);

			
			cell = new PdfPCell();
			p = new Paragraph(" ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(3);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Recibido ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_RIGHT);
			cell.addElement(p);
			cell.setColspan(1);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("______________________",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(1);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(" ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(3);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Aclaracion ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_RIGHT);
			cell.addElement(p);
			cell.setColspan(1);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("______________________",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(1);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(" ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(3);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Fecha ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_RIGHT);
			cell.addElement(p);
			cell.setColspan(1);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("______________________",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(1);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);


	}

	/**
	 * Cabecera/titulo del detalle para las mercaderias
	 * @param table
	 * @throws ServiceException
	 */
	private void setDetailsHeader(PdfPTable table) throws ServiceException {
		PdfPCell cell = null;
		Paragraph p = null;
		try{

			cell = new PdfPCell();
			p = new Paragraph("Factura",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph("Fecha",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph("Total",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(2);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Monto",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			table.addCell(cell);

		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}

	/**
	 * to format numbers
	 * @param n
	 * @return
	 * @throws ServiceException 
	 */
	private String formatDecimalNumber2Gs(Object n) throws ServiceException {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		DecimalFormat df2 = new DecimalFormat("#0.000", symbols);
		df2.setDecimalSeparatorAlwaysShown(false);
		df2.setGroupingUsed(true);
		df2.setGroupingSize(3);
		df2.setMaximumFractionDigits(0);
		df2.setMinimumFractionDigits(0);
		String formatted = df2.format(n);
		return formatted;
	}

	/**
	 * to format numbers
	 * @param n
	 * @return
	 * @throws ServiceException 
	 */
	private String formatDecimalKgs(Object n) throws ServiceException {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		symbols.setDecimalSeparator(',');
		symbols.setGroupingSeparator('.');

		DecimalFormat df = new DecimalFormat("#0.000", symbols);
		df.setDecimalSeparatorAlwaysShown(false);
		df.setGroupingUsed(true);
		df.setGroupingSize(3);
		String formatted = df.format(n);
		return formatted;

	}
	
//	private BigDecimal totalGuaranies(List<DsArtDTO> items) throws ServiceException{
//		BigDecimal totalGs = BigDecimal.ZERO;
//		if(items != null && !items.isEmpty()){
//			for(DsArtDTO item: items){
//				//totalGs = totalGs.add(item.getValorGuaranies());
//				totalGs = totalGs.add(item.getValorImponible());
//			}
//		}
//		return totalGs;
//	}

	private String getCurrentDate() {
		Date date = new Date(System.currentTimeMillis());
		return formatDate(date);
	}

	private String formatDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String dateStr = formatter.format(date);
		return dateStr;
	}

	private String formatDateWithOutTime(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String dateStr = formatter.format(date);
		return dateStr;
	}

}
