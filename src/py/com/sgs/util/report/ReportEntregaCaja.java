package py.com.sgs.util.report;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import py.com.sgs.config.ResourceProvider;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.composite.EntregaCajaCompositeDTO;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

public class ReportEntregaCaja {

	private static Font helvetica6 = FontFactory.getFont(FontFactory.HELVETICA, 6F);
	private static Font helvetica7 = FontFactory.getFont(FontFactory.HELVETICA, 7F);
	private static Font helvetica8 = FontFactory.getFont(FontFactory.HELVETICA, 8F);
	private static Font helvetica9 = FontFactory.getFont(FontFactory.HELVETICA, 9F);
	//	private static Font helvetica10 = FontFactory.getFont(FontFactory.HELVETICA, 10F);
	private static Font helvetica7Bold = FontFactory.getFont(FontFactory.HELVETICA, 7F, Font.BOLD);
	private static Font helvetica8Bold = FontFactory.getFont(FontFactory.HELVETICA, 8F, Font.BOLD);
	private static Font helvetica9Bold = FontFactory.getFont(FontFactory.HELVETICA, 9F, Font.BOLD);
	private static Font helvetica10Bold = FontFactory.getFont(FontFactory.HELVETICA, 10F, Font.BOLD);
	private static Font helvetica12Bold = FontFactory.getFont(FontFactory.HELVETICA, 12F, Font.BOLD);

	private Logger log = LoggerFactory.getLogger(getClass());

	private EntregaCajaCompositeDTO composite;
	private Properties prop = null;

	public ReportEntregaCaja(EntregaCajaCompositeDTO composite) {
		this.composite = composite;
	}

	public ReportEntregaCaja(){}

	public byte[] generarPDF() throws ServiceException{
		PdfPTable table = null;
		try {
			Rectangle envelope = new Rectangle(216, 250);
			
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			//for test only 
			Document documentTest = new Document(envelope, 0, 0, 0, 0);
			documentTest.open(); 

			//tabla para el detalle de las mercaderias
			table = makeItemDetailsTable();
			documentTest.add(table);
			
			
			Float y = documentTest.getPageSize().getHeight();

			documentTest.close();
			Rectangle envelopeFinal = new Rectangle(216, y);
			Document document = new Document(envelopeFinal, 0, 0, 0, 0);
			
			//PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(composite.getIdDsDdt() + ".pdf"));
			PdfWriter.getInstance(document, output);
			document.open(); 

			//tabla para el detalle de las mercaderias
//			table = makeItemDetailsTable();
			document.add(table);

			document.close();
//			PdfContentByte add_watermark;   
			PdfReader reader = new PdfReader(output.toByteArray());
			//
			ByteArrayOutputStream outputFinal = new ByteArrayOutputStream();
			PdfStamper stamper  = new PdfStamper(reader, outputFinal);
			// recorrer cada una de las paginas para poder agregar la cabecera a cada una
			int n = reader.getNumberOfPages();
			for (int i = 1; i <= n; i++) {
//				getHeaderTable(i, n).writeSelectedRows(
//						0, -1, 34, 803, stamper.getOverContent(i));
//				getFooterTable().writeSelectedRows(
//						0, -1, 30, 30, stamper.getOverContent(i));
//				add_watermark = stamper.getUnderContent(i);
//				add_watermark.addImage(watermark_image);
			}

			stamper.close();
			reader.close();

			//for test only 
//			File someFile = new File("/home/eve/Escritorio/" + composite.getId() + ".pdf");
//			FileOutputStream fos = new FileOutputStream(someFile);
//			fos.write(outputFinal.toByteArray());
//			fos.flush();
//			fos.close();

			return outputFinal.toByteArray();
		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}

	/**
	 * Crear el header con la pagina X de Y
	 * @param x el nro de pagina
	 * @param y el nro total de paginas
	 * @return a table that can be used as header
	 */
	public static PdfPTable getHeaderTable(int x, int y) {
		PdfPTable table = new PdfPTable(2);
		table.setTotalWidth(527);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(20);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell("");
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(new Paragraph(String.format("Página %d de %d", x, y), helvetica9));
		return table;
	}

	/**
	 * Crear el footer con las firmas autorizadas
	 * @return a table that can be used as footer
	 */
	public static PdfPTable getFooterTable() {
		PdfPTable table = new PdfPTable(1);
		table.setTotalWidth(527);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(20);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
//		table.addCell(new Paragraph("Firmas Autorizadas: ............................"
//				+ "............................................................." 
//				+ "............................................................." , helvetica9));
		return table;
	}

	/**
	 * Cabecera del DS_DDT conteniendo la informacion gral del despacho simplificado,
	 * debe descomentarse para tomar los valores del dto
	 * @param composite
	 * @return
	 * @throws ServiceException
	 */
	private PdfPTable makeItemDetailsTable() throws ServiceException {
		PdfPCell cell = null;
		Paragraph p = null;

		try{
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			PdfPTable table = new PdfPTable(5);
			float[] columnWidths = {2f, 2f, 2f, 2f, 2f};
			table.setWidths(columnWidths);
			table.setWidthPercentage(100);
			table.setSpacingBefore(5);
			table.setSpacingAfter(5);

			//logo
			String logoPath = ResourceProvider.getResourcesPath() + "/logo.png";
			Image logo = Image.getInstance(logoPath);
			logo.scaleAbsolute(83, 50);
			logo.setAbsolutePosition(30, (PageSize.A4.getHeight() - logo.getScaledHeight() - 20));

			// row 1
			cell = new PdfPCell();
			p = new Paragraph("Comprobante de Entrega ", helvetica10Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(5);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(" ", helvetica10Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(5);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph("Monto Efectivo", helvetica7Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(this.composite.getMontoEfectivo() != null ? formatDecimalNumber2Gs(this.composite.getMontoEfectivo()) : "0", helvetica7);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);
			
			
			
			cell = new PdfPCell();
			p = new Paragraph("Monto Tarjeta", helvetica7Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(this.composite.getMontoTarjeta() != null ? formatDecimalNumber2Gs(this.composite.getMontoTarjeta()) : "0", helvetica7);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);
			
			
			cell = new PdfPCell();
			p = new Paragraph("Total Entregado", helvetica7Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(this.composite.getTotal() != null ? formatDecimalNumber2Gs(this.composite.getTotal()) : "0", helvetica7);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);
			
			//row 2
			cell = new PdfPCell();
			p = new Paragraph("Fecha de Entrega: ",helvetica7Bold);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(formatter.format(new Date(System.currentTimeMillis())),helvetica7);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);
			
			//row 3
			cell = new PdfPCell();
			p = new Paragraph("Cajero: ",helvetica7Bold);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(this.composite.getApertura().getCajero().getUsuario(),helvetica7);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);
			
			//row 2
			cell = new PdfPCell();
			p = new Paragraph("Fecha de Apertura: ",helvetica7Bold);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(composite.getApertura().getFechaApertura() != null ? formatter.format(composite.getApertura().getFechaApertura()) : null,helvetica7);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);
			
			
			//row 2
			cell = new PdfPCell();
			p = new Paragraph("Diferencia Arqueo: ",helvetica7Bold);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(composite.getArqueo().getFaltanteSobrante()!= null ? formatDecimalNumber2Gs(composite.getArqueo().getFaltanteSobrante()) : "0,00" ,helvetica7);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);

			return table;
		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}

	/**
	 * to format numbers
	 * @param n
	 * @return
	 * @throws ServiceException 
	 */
	private String formatDecimalNumber2Gs(Object n) throws ServiceException {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		DecimalFormat df2 = new DecimalFormat("#0.000", symbols);
		df2.setDecimalSeparatorAlwaysShown(false);
		df2.setGroupingUsed(true);
		df2.setGroupingSize(3);
		df2.setMaximumFractionDigits(0);
		df2.setMinimumFractionDigits(0);
		String formatted = df2.format(n);
		return formatted;
	}

	/**
	 * to format numbers
	 * @param n
	 * @return
	 * @throws ServiceException 
	 */
	private String formatDecimalNumber(Object n) throws ServiceException {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		if (n instanceof Long || n instanceof Integer) {
			DecimalFormat df2 = new DecimalFormat("#0.000", symbols);
			df2.setDecimalSeparatorAlwaysShown(false);
			df2.setGroupingUsed(true);
			df2.setGroupingSize(3);
			df2.setMaximumFractionDigits(0);
			df2.setMinimumFractionDigits(0);
			String formatted = df2.format(n);
			return formatted;
		}else if (n instanceof Double || n instanceof BigDecimal) {
			symbols.setDecimalSeparator(',');
			symbols.setGroupingSeparator('.');

			DecimalFormat df = new DecimalFormat("##0.00", symbols);
			df.setDecimalSeparatorAlwaysShown(false);
			df.setGroupingUsed(true);
			df.setGroupingSize(3);
			String formatted = df.format(n);
			return formatted;
		} else {
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"number.format.type.error");
		}
	}

	/**
	 * to format numbers
	 * @param n
	 * @return
	 * @throws ServiceException 
	 */
	private String formatDecimalKgs(Object n) throws ServiceException {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		symbols.setDecimalSeparator(',');
		symbols.setGroupingSeparator('.');

		DecimalFormat df = new DecimalFormat("#0.000", symbols);
		df.setDecimalSeparatorAlwaysShown(false);
		df.setGroupingUsed(true);
		df.setGroupingSize(3);
		String formatted = df.format(n);
		return formatted;

	}
	
	private String getCurrentDate() {
		Date date = new Date(System.currentTimeMillis());
		return formatDate(date);
	}

	private String formatDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String dateStr = formatter.format(date);
		return dateStr;
	}

	private String formatDateWithOutTime(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String dateStr = formatter.format(date);
		return dateStr;
	}
	
}
