package py.com.sgs.util.report;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import py.com.sgs.config.ResourceProvider;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.OrdenCompraDetalleDTO;
import py.com.sgs.persistence.dto.OrdenDeCompraDTO;
import py.com.sgs.persistence.dto.composite.OrdenDeCompraCompositeDTO;
import py.com.sgs.web.services.impl.OrdenDeCompraServiceImpl;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

/**
 * 
 *
 */
public class ReportOrdenDeCompra {

	private static Font helvetica6 = FontFactory.getFont(FontFactory.HELVETICA, 6F);
	private static Font helvetica7 = FontFactory.getFont(FontFactory.HELVETICA, 7F);
	private static Font helvetica8 = FontFactory.getFont(FontFactory.HELVETICA, 8F);
	private static Font helvetica9 = FontFactory.getFont(FontFactory.HELVETICA, 9F);
	//	private static Font helvetica10 = FontFactory.getFont(FontFactory.HELVETICA, 10F);
	private static Font helvetica7Bold = FontFactory.getFont(FontFactory.HELVETICA, 7F, Font.BOLD);
	private static Font helvetica8Bold = FontFactory.getFont(FontFactory.HELVETICA, 8F, Font.BOLD);
	private static Font helvetica9Bold = FontFactory.getFont(FontFactory.HELVETICA, 9F, Font.BOLD);
	private static Font helvetica10Bold = FontFactory.getFont(FontFactory.HELVETICA, 10F, Font.BOLD);
	private static Font helvetica12Bold = FontFactory.getFont(FontFactory.HELVETICA, 12F, Font.BOLD);

	private Logger log = LoggerFactory.getLogger(getClass());

	private OrdenDeCompraCompositeDTO composite;
	private Properties prop = null;

	public ReportOrdenDeCompra(OrdenDeCompraCompositeDTO composite) {
		this.composite = composite;
	}

	public ReportOrdenDeCompra(){}

	public byte[] generarPDF() throws ServiceException{
		PdfPTable table = null;
		try {
			Document document=new Document(PageSize.A4, 30, 30, 40, 36);
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			//for test only 
			//PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(composite.getIdDsDdt() + ".pdf"));
			PdfWriter.getInstance(document, output);
			document.open(); 

			//tabla para el detalle de las mercaderias
			table = makeItemDetailsTable();
			document.add(table);

			document.close();

//			Image watermark_image = Image.getInstance(IOUtils.toByteArray(is));
//			watermark_image.setAbsolutePosition(35, 100);
			PdfReader reader = new PdfReader(output.toByteArray());
//
			ByteArrayOutputStream outputFinal = new ByteArrayOutputStream();
			PdfStamper stamper  = new PdfStamper(reader, outputFinal);
//			PdfContentByte add_watermark;   
			// recorrer cada una de las paginas para poder agregar la cabecera a cada una
			int n = reader.getNumberOfPages();
			for (int i = 1; i <= n; i++) {
				getHeaderTable(i, n).writeSelectedRows(
						0, -1, 34, 803, stamper.getOverContent(i));
				getFooterTable().writeSelectedRows(
						0, -1, 30, 30, stamper.getOverContent(i));
//				add_watermark = stamper.getUnderContent(i);
//				add_watermark.addImage(watermark_image);
			}

			stamper.close();
			reader.close();

			//for test only 
			//File someFile = new File("/home/nsuarez/Escritorio/SGS/" + composite.getNroOrdenCompra() + ".pdf");
			//FileOutputStream fos = new FileOutputStream(someFile);
			//fos.write(outputFinal.toByteArray());
			//fos.flush();
			//fos.close();

			return outputFinal.toByteArray();
		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}

	/**
	 * Crear el header con la pagina X de Y
	 * @param x el nro de pagina
	 * @param y el nro total de paginas
	 * @return a table that can be used as header
	 */
	public static PdfPTable getHeaderTable(int x, int y) {
		PdfPTable table = new PdfPTable(2);
		table.setTotalWidth(527);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(20);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell("");
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(new Paragraph(String.format("Página %d de %d", x, y), helvetica9));
		return table;
	}

	/**
	 * Crear el footer con las firmas autorizadas
	 * @return a table that can be used as footer
	 */
	public static PdfPTable getFooterTable() {
		PdfPTable table = new PdfPTable(1);
		table.setTotalWidth(527);
		table.setLockedWidth(true);
		table.getDefaultCell().setFixedHeight(20);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
//		table.addCell(new Paragraph("Firmas Autorizadas: ............................"
//				+ "............................................................." 
//				+ "............................................................." , helvetica9));
		return table;
	}

	/**
	 * Cabecera del DS_DDT conteniendo la informacion gral del despacho simplificado,
	 * debe descomentarse para tomar los valores del dto
	 * @param composite
	 * @return
	 * @throws ServiceException
	 */
	private PdfPTable makeItemDetailsTable() throws ServiceException {
		PdfPCell cell = null;
		Paragraph p = null;

		try{
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			PdfPTable table = new PdfPTable(5);
			float[] columnWidths = {0.5f, 1f, 2f, 2f, 2f};
			table.setWidths(columnWidths);
			table.setWidthPercentage(100);
			table.setSpacingBefore(5);
			table.setSpacingAfter(5);

			//logo
			String logoPath = ResourceProvider.getResourcesPath() + "/logo.png";
			Image logo = Image.getInstance(logoPath);
			logo.scaleAbsolute(83, 50);
			logo.setAbsolutePosition(30, (PageSize.A4.getHeight() - logo.getScaledHeight() - 20));

			// row 1
			cell = new PdfPCell(logo);
			cell.setRowspan(3);
			cell.setColspan(2);
			cell.setPaddingTop(4F);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(" ", helvetica10Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.addElement(p);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setColspan(3);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph("Orden de Compra", helvetica12Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(composite.getNroOrdenCompra(), helvetica10Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);

			//row 2
			cell = new PdfPCell();
			p = new Paragraph("Proveedor: ", helvetica7Bold);
			cell.setIndent(3f);
			cell.addElement(p);
			cell.setColspan(2);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(composite.getProveedor(),helvetica6);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);

			//row3
			cell = new PdfPCell();
			p = new Paragraph("Fecha de Emisión: ",helvetica7Bold);
			cell.addElement(p);
			cell.setColspan(2);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(composite.getFechaRegistro() != null ? formatter.format(composite.getFechaRegistro()) : null,helvetica7);
			cell.addElement(p);
			cell.setColspan(3);
			table.addCell(cell);

			//row 4
			cell = new PdfPCell();
			p = new Paragraph("Estado: ",helvetica7Bold);
			cell.addElement(p);
			cell.setColspan(2);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(composite.getEstado(),helvetica6);
			cell.setColspan(3);
			cell.addElement(p);
			table.addCell(cell);
			
			makeBodyDetailTable(table);

			return table;
		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}

	/**
	 * Columns/rows settings para el detalle de las mercaderias
	 * @param artList
	 * @return
	 * @throws ServiceException
	 */
	private PdfPTable makeBodyDetailTable(PdfPTable table) throws ServiceException {
		PdfPCell cell = null;
		Paragraph p = null;
		try{
			//row 1
			cell = new PdfPCell();
			cell.setColspan(5);
			table.addCell(cell);

			//row 2
			cell = new PdfPCell();
			p = new Paragraph("DETALLE DE LA MERCADERÍA", helvetica8Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.addElement(p);
			cell.setColspan(5);
			table.addCell(cell);

			setDetailsHeader(table);

//			table.setHeaderRows(12);
			addDetailsRow(composite.getDetalles(), table);

			return table;
		}catch (ServiceException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, e.getMessage());
		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}

	/**
	 * iteracion sobre el detalle de las mercaderias
	 * descomentar para su uso en produccion
	 * @param list
	 * @param table
	 * @throws ServiceException 
	 */
	private void addDetailsRow(List<OrdenCompraDetalleDTO> list, PdfPTable table) throws ServiceException {
		PdfPCell cell = null;
		Paragraph p = null;

//		if(CollectionUtils.isNotEmpty(list)){ 
			for (OrdenCompraDetalleDTO d : list) {
				cell = new PdfPCell();
				p = new Paragraph(new Paragraph(String.valueOf(d.getCantidad()), helvetica7));
				p.setAlignment(Element.ALIGN_RIGHT);
				cell.addElement(p);
				table.addCell(cell);

				cell = new PdfPCell();
				p = new Paragraph(new Paragraph(d.getCodProducto(), helvetica7));
				p.setAlignment(Element.ALIGN_CENTER);
				cell.addElement(p);
				table.addCell(cell);
				
				cell = new PdfPCell();
				p = new Paragraph(new Paragraph(d.getProducto(), helvetica7));
				p.setAlignment(Element.ALIGN_CENTER);
				cell.addElement(p);
				table.addCell(cell);

				cell = new PdfPCell();
				p = new Paragraph(d.getUltCostoCompra() != null ? formatDecimalNumber2Gs(d.getUltCostoCompra()) : "0,00", helvetica7);
				p.setAlignment(Element.ALIGN_RIGHT);
				cell.addElement(p);
				table.addCell(cell);
				
				cell = new PdfPCell();
				p = new Paragraph(d.getCostoTotal() != null ? formatDecimalNumber2Gs(d.getCostoTotal()) : "0,00", helvetica7);
				p.setAlignment(Element.ALIGN_RIGHT);
				cell.addElement(p);
				table.addCell(cell);

			}
//
//
			cell = new PdfPCell();
			p = new Paragraph("Total Gravadas 10%: ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(this.composite.getTotalIva10() != null ? formatDecimalNumber2Gs(this.composite.getTotalIva10()) : "0,00", helvetica8);
			p.setAlignment(Element.ALIGN_RIGHT);
			cell.addElement(p);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
//			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(" ", helvetica8);
			p.setAlignment(Element.ALIGN_RIGHT);
			cell.addElement(p);
			cell.setColspan(2);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
//			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			table.addCell(cell);
//
			cell = new PdfPCell();
			p = new Paragraph("Total Gravadas 5%: ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(this.composite.getTotalIva5() != null ? formatDecimalNumber2Gs(this.composite.getTotalIva5()) : "0,00", helvetica8);
			p.setAlignment(Element.ALIGN_RIGHT);
			cell.addElement(p);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
//			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Total: ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_CENTER);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
			cell.addElement(p);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(this.composite.getMontoTotal() != null ? formatDecimalNumber2Gs(this.composite.getMontoTotal()) : "0,00", helvetica8Bold);
			p.setAlignment(Element.ALIGN_RIGHT);
			cell.addElement(p);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.setBorderWidthBottom(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
//			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			cell.setVerticalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph("Total Excentas: ",helvetica8Bold);
			p.setAlignment(Element.ALIGN_LEFT);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.addElement(p);
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph(this.composite.getTotalExcenta() != null ? formatDecimalNumber2Gs(this.composite.getTotalExcenta()) : "0,00", helvetica8);
			p.setAlignment(Element.ALIGN_RIGHT);
			cell.addElement(p);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
//			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			table.addCell(cell);
			
			cell = new PdfPCell();
			p = new Paragraph(" ", helvetica8);
			p.setAlignment(Element.ALIGN_RIGHT);
			cell.addElement(p);
			cell.setColspan(2);
//			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.setBorderWidthTop(Rectangle.NO_BORDER);
			cell.setBorderWidthLeft(Rectangle.NO_BORDER);
//			cell.setBorderWidthRight(Rectangle.NO_BORDER);
			table.addCell(cell);
			
//		}
	}

	/**
	 * Cabecera/titulo del detalle para las mercaderias
	 * @param table
	 * @throws ServiceException
	 */
	private void setDetailsHeader(PdfPTable table) throws ServiceException {
		PdfPCell cell = null;
		Paragraph p = null;
		try{

			cell = new PdfPCell();
			p = new Paragraph("Cantidad",helvetica7Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.addElement(p);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph("Código",helvetica7Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.addElement(p);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph("Descripcion",helvetica7Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.addElement(p);
			
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph("Costo Unitario",helvetica7Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.addElement(p);
			table.addCell(cell);

			cell = new PdfPCell();
			p = new Paragraph("Costo Total",helvetica7Bold);
			p.setAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
			cell.addElement(p);
			table.addCell(cell);

		}catch (Exception e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"tereds.print.error");
		}
	}

	/**
	 * to format numbers
	 * @param n
	 * @return
	 * @throws ServiceException 
	 */
	private String formatDecimalNumber2Gs(Object n) throws ServiceException {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		DecimalFormat df2 = new DecimalFormat("#0.000", symbols);
		df2.setDecimalSeparatorAlwaysShown(false);
		df2.setGroupingUsed(true);
		df2.setGroupingSize(3);
		df2.setMaximumFractionDigits(0);
		df2.setMinimumFractionDigits(0);
		String formatted = df2.format(n);
		return formatted;
	}

	/**
	 * to format numbers
	 * @param n
	 * @return
	 * @throws ServiceException 
	 */
	private String formatDecimalNumber(Object n) throws ServiceException {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		if (n instanceof Long || n instanceof Integer) {
			DecimalFormat df2 = new DecimalFormat("#0.000", symbols);
			df2.setDecimalSeparatorAlwaysShown(false);
			df2.setGroupingUsed(true);
			df2.setGroupingSize(3);
			df2.setMaximumFractionDigits(0);
			df2.setMinimumFractionDigits(0);
			String formatted = df2.format(n);
			return formatted;
		}else if (n instanceof Double || n instanceof BigDecimal) {
			symbols.setDecimalSeparator(',');
			symbols.setGroupingSeparator('.');

			DecimalFormat df = new DecimalFormat("##0.00", symbols);
			df.setDecimalSeparatorAlwaysShown(false);
			df.setGroupingUsed(true);
			df.setGroupingSize(3);
			String formatted = df.format(n);
			return formatted;
		} else {
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"number.format.type.error");
		}
	}

	/**
	 * to format numbers
	 * @param n
	 * @return
	 * @throws ServiceException 
	 */
	private String formatDecimalKgs(Object n) throws ServiceException {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		symbols.setDecimalSeparator(',');
		symbols.setGroupingSeparator('.');

		DecimalFormat df = new DecimalFormat("#0.000", symbols);
		df.setDecimalSeparatorAlwaysShown(false);
		df.setGroupingUsed(true);
		df.setGroupingSize(3);
		String formatted = df.format(n);
		return formatted;

	}
	
//	private BigDecimal totalGuaranies(List<DsArtDTO> items) throws ServiceException{
//		BigDecimal totalGs = BigDecimal.ZERO;
//		if(items != null && !items.isEmpty()){
//			for(DsArtDTO item: items){
//				//totalGs = totalGs.add(item.getValorGuaranies());
//				totalGs = totalGs.add(item.getValorImponible());
//			}
//		}
//		return totalGs;
//	}

	private String getCurrentDate() {
		Date date = new Date(System.currentTimeMillis());
		return formatDate(date);
	}

	private String formatDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String dateStr = formatter.format(date);
		return dateStr;
	}

	private String formatDateWithOutTime(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String dateStr = formatter.format(date);
		return dateStr;
	}

}