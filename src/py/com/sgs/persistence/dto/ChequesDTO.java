package py.com.sgs.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChequesDTO {
	
	private Long idCuentaBanco;
	private String serie;
	private String numeroInicial;
	private String numeroFinal;
	private Long idEstado;
	private Long idTipoCheque;
	private Long id;
	private String ultNumero;
	private Long idBanco;
	private String estado;
	private String tipoCheque;
	private Integer nroCuenta;
	private String banco;
	
	

	public Long getIdCuentaBanco() {
		return idCuentaBanco;
	}
	public void setIdCuentaBanco(Long idCuentaBanco) {
		this.idCuentaBanco = idCuentaBanco;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getNumeroInicial() {
		return numeroInicial;
	}
	public void setNumeroInicial(String numeroInicial) {
		this.numeroInicial = numeroInicial;
	}
	public String getNumeroFinal() {
		return numeroFinal;
	}
	public void setNumeroFinal(String numeroFinal) {
		this.numeroFinal = numeroFinal;
	}
	public Long getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}
	public Long getIdTipoCheque() {
		return idTipoCheque;
	}
	public void setIdTipoCheque(Long idTipoCheque) {
		this.idTipoCheque = idTipoCheque;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUltNumero() {
		return ultNumero;
	}
	public void setUltNumero(String ultNumero) {
		this.ultNumero = ultNumero;
	}
	public Long getIdBanco() {
		return idBanco;
	}
	public void setIdBanco(Long idBanco) {
		this.idBanco = idBanco;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getTipoCheque() {
		return tipoCheque;
	}
	public void setTipoCheque(String tipoCheque) {
		this.tipoCheque = tipoCheque;
	}
	public Integer getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Integer nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	
	

}