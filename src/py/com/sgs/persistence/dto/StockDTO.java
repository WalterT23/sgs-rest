package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StockDTO {
	private Long id;
	private Long idProducto;
	private String codProducto;
	private BigDecimal ultCostoCompra;
	private BigDecimal promPonderado;
	private Double stock;
	private Date fechaUltModif;
	private String usuarioUltModif;
	private Long idSucursal;
	
	public StockDTO(){
		
	}
	
	public StockDTO(Long idProducto, Long idSucursal){
		super();
		this.idProducto = idProducto;
		this.idSucursal = idSucursal;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public Double getStock() {
		return stock;
	}
	public void setStock(Double stock) {
		this.stock = stock;
	}
	public BigDecimal getUltCostoCompra() {
		return ultCostoCompra;
	}
	public void setUltCostoCompra(BigDecimal ultCostoCompra) {
		this.ultCostoCompra = ultCostoCompra;
	}
	public BigDecimal getPromPonderado() {
		return promPonderado;
	}
	public void setPromPonderado(BigDecimal promPonderado) {
		this.promPonderado = promPonderado;
	}
	public Date getFechaUltModif() {
		return fechaUltModif;
	}
	public void setFechaUltModif(Date fechaUltModif) {
		this.fechaUltModif = fechaUltModif;
	}
	public String getUsuarioUltModif() {
		return usuarioUltModif;
	}
	public void setUsuarioUltModif(String usuarioUltModif) {
		this.usuarioUltModif = usuarioUltModif;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	
}