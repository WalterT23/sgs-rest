package py.com.sgs.persistence.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReporteProductosMasVendidosDTO {
	private Date fechaDesde;
	private Date fechaHasta;
	private Boolean grupo;
	private Boolean subGrupo;
	private Long idSucursal;
	private List<DetalleProMasVenDTO> detalles;
	private List<Det4GraphicProMasVenDTO> datosGraphics;
	
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public Boolean getGrupo() {
		return grupo;
	}
	public void setGrupo(Boolean grupo) {
		this.grupo = grupo;
	}
	public Boolean getSubGrupo() {
		return subGrupo;
	}
	public void setSubGrupo(Boolean subGrupo) {
		this.subGrupo = subGrupo;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	public List<DetalleProMasVenDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<DetalleProMasVenDTO> detalles) {
		this.detalles = detalles;
	}
	public List<Det4GraphicProMasVenDTO> getDatosGraphics() {
		return datosGraphics;
	}
	public void setDatosGraphics(List<Det4GraphicProMasVenDTO> datosGraphics) {
		this.datosGraphics = datosGraphics;
	}

}
