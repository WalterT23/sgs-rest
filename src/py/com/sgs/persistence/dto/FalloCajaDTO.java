package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FalloCajaDTO {
	private Long id;
	private Long idAperturaCierre;
	private String puntoExp;
	private Long idUsuario;
	private String usuario;
	private String cajero;
	BigDecimal monto;
	private Date fecha;
	private Long idSucursal;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdAperturaCierre() {
		return idAperturaCierre;
	}
	public void setIdAperturaCierre(Long idAperturaCierre) {
		this.idAperturaCierre = idAperturaCierre;
	}
	
	
	public String getPuntoExp() {
		return puntoExp;
	}
	public void setPuntoExp(String puntoExp) {
		this.puntoExp = puntoExp;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getCajero() {
		return cajero;
	}
	public void setCajero(String cajero) {
		this.cajero = cajero;
	}
	
	

}