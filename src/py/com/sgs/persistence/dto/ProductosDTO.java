package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductosDTO {
	
	private Long id;
	private String cod;
	private String nombre;
	private Long idUnidadMed;
	private String unidadMed;
	private Integer stockMin;
	private String descripcion;
	private Long idGrupo;
	private String grupo;
	private Long idMarca;
	private String marca;
	private Long idTipoTributo;
	private String tipoTributo;
	private String abvTributo;
	private BigDecimal porcRecarga;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getIdUnidadMed() {
		return idUnidadMed;
	}
	public void setIdUnidadMed(Long idUnidadMed) {
		this.idUnidadMed = idUnidadMed;
	}
	public String getUnidadMed() {
		return unidadMed;
	}
	public void setUnidadMed(String unidadMed) {
		this.unidadMed = unidadMed;
	}
	public Integer getStockMin() {
		return stockMin;
	}
	public void setStockMin(Integer stockMin) {
		this.stockMin = stockMin;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Long getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(Long idGrupo) {
		this.idGrupo = idGrupo;
	}
	
	
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public Long getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(Long idMarca) {
		this.idMarca = idMarca;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public Long getIdTipoTributo() {
		return idTipoTributo;
	}
	public void setIdTipoTributo(Long idTipoTributo) {
		this.idTipoTributo = idTipoTributo;
	}
	public String getTipoTributo() {
		return tipoTributo;
	}
	public void setTipoTributo(String tipoTributo) {
		this.tipoTributo = tipoTributo;
	}
	public String getAbvTributo() {
		return abvTributo;
	}
	public void setAbvTributo(String abvTributo) {
		this.abvTributo = abvTributo;
	}
	public BigDecimal getPorcRecarga() {
		return porcRecarga;
	}
	public void setPorcRecarga(BigDecimal porcRecarga) {
		this.porcRecarga = porcRecarga;
	}

}