package py.com.sgs.persistence.dto;

public class RolFuncionalidadDTO {
	
	private Long idRol;
	private Long idFuncionalidad;
	
	public Long getIdRol() {
		return idRol;
	}
	public void setIdRol(Long idRol) {
		this.idRol = idRol;
	}
	
	public Long getIdFuncionalidad() {
		return idFuncionalidad;
	}
	public void setIdFuncionalidad(Long idFuncionalidad) {
		this.idFuncionalidad = idFuncionalidad;
	}
	
}
