package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrdenCompraDetalleDTO {
	private Long id;
	private Long idOrdenCompra;
	private Long idProducto;
	private String codProducto;
	private String producto;
	private String tipoTributo;
	private Long idTipoTributo;
	private Double cantidad;
	private BigDecimal ultCostoCompra;
	private BigDecimal costoTotal;
	private Date fechaReg;

	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}
	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	
	public BigDecimal getUltCostoCompra() {
		return ultCostoCompra;
	}
	public void setUltCostoCompra(BigDecimal ultCostoCompra) {
		this.ultCostoCompra = ultCostoCompra;
	}
	public BigDecimal getCostoTotal() {
		return costoTotal;
	}
	public void setCostoTotal(BigDecimal costoTotal) {
		this.costoTotal = costoTotal;
	}
	public Date getFechaReg() {
		return fechaReg;
	}
	public void setFechaReg(Date fechaReg) {
		this.fechaReg = fechaReg;
	}
	public String getTipoTributo() {
		return tipoTributo;
	}
	public void setTipoTributo(String tipoTributo) {
		this.tipoTributo = tipoTributo;
	}
	public Long getIdTipoTributo() {
		return idTipoTributo;
	}
	public void setIdTipoTributo(Long idTipoTributo) {
		this.idTipoTributo = idTipoTributo;
	}
	
	

}