package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DetalleForGraphicsDTO {
	private Date dia;
	private BigDecimal totalXdia;
	
	public Date getDia() {
		return dia;
	}
	public void setDia(Date dia) {
		this.dia = dia;
	}
	public BigDecimal getTotalXdia() {
		return totalXdia;
	}
	public void setTotalXdia(BigDecimal totalXdia) {
		this.totalXdia = totalXdia;
	}
}
