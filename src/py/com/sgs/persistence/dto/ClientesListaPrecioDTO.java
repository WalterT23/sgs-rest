package py.com.sgs.persistence.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientesListaPrecioDTO {
	private Long id;
	private Long idCliente;
	private Long idListaPrecio;
	private String observacion;
	private Date fechaInicio;
	private Date fechaFin;
	private Long idSucursalListaP;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public Long getIdListaPrecio() {
		return idListaPrecio;
	}
	public void setIdListaPrecio(Long idListaPrecio) {
		this.idListaPrecio = idListaPrecio;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Long getIdSucursalListaP() {
		return idSucursalListaP;
	}
	public void setIdSucursalListaP(Long idSucursalListaP) {
		this.idSucursalListaP = idSucursalListaP;
	}

}