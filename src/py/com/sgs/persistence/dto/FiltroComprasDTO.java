package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class FiltroComprasDTO{
	private Date fechaDesde;
	private Date fechaHasta;
	private Long idProveedor;
	private String proveedor;
	private Long idSucursal;
	private BigDecimal total;
	private List<CabeceraCompraDTO> detalles;
	private List<DetalleForGraphicsDTO> totalXfecha;

	
	
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	
	public Long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}

	public List<CabeceraCompraDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<CabeceraCompraDTO> detalles) {
		this.detalles = detalles;
	}
	public List<DetalleForGraphicsDTO> getTotalXfecha() {
		return totalXfecha;
	}
	public void setTotalXfecha(List<DetalleForGraphicsDTO> totalXfecha) {
		this.totalXfecha = totalXfecha;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
