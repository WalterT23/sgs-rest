package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DetalleCierreDTO {
	private Long id;
	private Long idAperturaCierre;
	private BigDecimal montoEfectivo;
	private BigDecimal montoTarjetas;
	private BigDecimal montoTotal;
	private String usuarioCreacion;
	private Date fechaCreacion;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdAperturaCierre() {
		return idAperturaCierre;
	}
	public void setIdAperturaCierre(Long idAperturaCierre) {
		this.idAperturaCierre = idAperturaCierre;
	}
	public BigDecimal getMontoEfectivo() {
		return montoEfectivo;
	}
	public void setMontoEfectivo(BigDecimal montoEfectivo) {
		this.montoEfectivo = montoEfectivo;
	}
	public BigDecimal getMontoTarjetas() {
		return montoTarjetas;
	}
	public void setMontoTarjetas(BigDecimal montoTarjetas) {
		this.montoTarjetas = montoTarjetas;
	}
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}