package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListaPreciosDetalleDTO {
	private Long id;
	private Long idListaPrecios;
	private Long idProducto;
	private BigDecimal costoBase;
	private Long idTipoRecarga;
	private BigDecimal montoRecarga;
	private BigDecimal precioVenta;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdListaPrecios() {
		return idListaPrecios;
	}
	public void setIdListaPrecios(Long idListaPrecios) {
		this.idListaPrecios = idListaPrecios;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public BigDecimal getCostoBase() {
		return costoBase;
	}
	public void setCostoBase(BigDecimal costoBase) {
		this.costoBase = costoBase;
	}
	public Long getIdTipoRecarga() {
		return idTipoRecarga;
	}
	public void setIdTipoRecarga(Long idTipoRecarga) {
		this.idTipoRecarga = idTipoRecarga;
	}
	public BigDecimal getMontoRecarga() {
		return montoRecarga;
	}
	public void setMontoRecarga(BigDecimal montoRecarga) {
		this.montoRecarga = montoRecarga;
	}
	public BigDecimal getPrecioVenta() {
		return precioVenta;
	}
	public void setPrecioVenta(BigDecimal precioVenta) {
		this.precioVenta = precioVenta;
	}

}