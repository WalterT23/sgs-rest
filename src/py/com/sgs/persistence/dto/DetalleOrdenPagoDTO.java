package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DetalleOrdenPagoDTO {
	private Long id;
	private Long idCabeceraOp;
	private Long idCabeceraCompra;
	private BigDecimal monto;
	private Integer nroRecibo;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCabeceraOp() {
		return idCabeceraOp;
	}
	public void setIdCabeceraOp(Long idCabeceraOp) {
		this.idCabeceraOp = idCabeceraOp;
	}
	public Long getIdCabeceraCompra() {
		return idCabeceraCompra;
	}
	public void setIdCabeceraCompra(Long idCabeceraCompra) {
		this.idCabeceraCompra = idCabeceraCompra;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public Integer getNroRecibo() {
		return nroRecibo;
	}
	public void setNroRecibo(Integer nroRecibo) {
		this.nroRecibo = nroRecibo;
	}
	

}