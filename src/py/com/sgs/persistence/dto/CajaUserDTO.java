package py.com.sgs.persistence.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)

public class CajaUserDTO {
	private Long id;
	private Long idPuntoExpedicion;
	private Long idUsuario;
	private Date inicio;
	private Date fin;


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdPuntoExpedicion() {
		return idPuntoExpedicion;
	}
	public void setIdPuntoExpedicion(Long idPuntoExpedicion) {
		this.idPuntoExpedicion = idPuntoExpedicion;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Date getInicio() {
		return inicio;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	public Date getFin() {
		return fin;
	}
	public void setFin(Date fin) {
		this.fin = fin;
	}
}