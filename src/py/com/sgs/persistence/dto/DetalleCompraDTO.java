package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DetalleCompraDTO {
	private Long id;
	private Long idCabeceraCompra;
	private Long idProducto;
	private Double cantidad;
	private BigDecimal precioUnitario;
	private BigDecimal precioTotal;
	private Long impuestoFiscal;
	private String codProducto;
	private String producto;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCabeceraCompra() {
		return idCabeceraCompra;
	}
	public void setIdCabeceraCompra(Long idCabeceraCompra) {
		this.idCabeceraCompra = idCabeceraCompra;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	public BigDecimal getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(BigDecimal precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public BigDecimal getPrecioTotal() {
		return precioTotal;
	}
	public void setPrecioTotal(BigDecimal precioTotal) {
		this.precioTotal = precioTotal;
	}
	public Long getImpuestoFiscal() {
		return impuestoFiscal;
	}
	public void setImpuestoFiscal(Long impuestoFiscal) {
		this.impuestoFiscal = impuestoFiscal;
	}
	public String getCodProducto() {
		return codProducto;
	}
	public void setCodProducto(String codProducto) {
		this.codProducto = codProducto;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	
}