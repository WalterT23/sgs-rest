package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CabeceraVentaDTO {
	private Long id;
	private Long idTimbradoPtoExp;
	private String nroFactura;
	private Date fechaCreacion;
	private String cajero;
	private BigDecimal gravada10;
	private BigDecimal gravada5;
	private BigDecimal exenta;
	private BigDecimal iva10;
	private BigDecimal iva5;
	private BigDecimal descuento;
	private BigDecimal neto;
	private Long idCliente;
	private Long idAperturaCierre;
	private Long idSucursal;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdTimbradoPtoExp() {
		return idTimbradoPtoExp;
	}
	public void setIdTimbradoPtoExp(Long idTimbradoPtoExp) {
		this.idTimbradoPtoExp = idTimbradoPtoExp;
	}
	public String getNroFactura() {
		return nroFactura;
	}
	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getCajero() {
		return cajero;
	}
	public void setCajero(String cajero) {
		this.cajero = cajero;
	}
	public BigDecimal getGravada10() {
		return gravada10;
	}
	public void setGravada10(BigDecimal gravada10) {
		this.gravada10 = gravada10;
	}
	public BigDecimal getGravada5() {
		return gravada5;
	}
	public void setGravada5(BigDecimal gravada5) {
		this.gravada5 = gravada5;
	}
	public BigDecimal getExenta() {
		return exenta;
	}
	public void setExenta(BigDecimal exenta) {
		this.exenta = exenta;
	}
	public BigDecimal getIva10() {
		return iva10;
	}
	public void setIva10(BigDecimal iva10) {
		this.iva10 = iva10;
	}
	public BigDecimal getIva5() {
		return iva5;
	}
	public void setIva5(BigDecimal iva5) {
		this.iva5 = iva5;
	}
	public BigDecimal getDescuento() {
		return descuento;
	}
	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}
	public BigDecimal getNeto() {
		return neto;
	}
	public void setNeto(BigDecimal neto) {
		this.neto = neto;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public Long getIdAperturaCierre() {
		return idAperturaCierre;
	}
	public void setIdAperturaCierre(Long idAperturaCierre) {
		this.idAperturaCierre = idAperturaCierre;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}

}