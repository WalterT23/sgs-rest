package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NotaCreditoVentaDTO {
	private Long id;
	private Long idCabeceraVenta;
	private String nroNotaCredito;
	private BigDecimal montoTotal;
	private BigDecimal iva5;
	private BigDecimal iva10;
	private BigDecimal exentas;
	private Date fechaCreacion;
	private String usuarioCreacion;
	private Long idTimbradoPtoExp;
	private Long idCliente;
	private Long idSucursal;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCabeceraVenta() {
		return idCabeceraVenta;
	}
	public void setIdCabeceraVenta(Long idCabeceraVenta) {
		this.idCabeceraVenta = idCabeceraVenta;
	}
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	public BigDecimal getIva5() {
		return iva5;
	}
	public void setIva5(BigDecimal iva5) {
		this.iva5 = iva5;
	}
	public BigDecimal getIva10() {
		return iva10;
	}
	public void setIva10(BigDecimal iva10) {
		this.iva10 = iva10;
	}
	public BigDecimal getExentas() {
		return exentas;
	}
	public void setExentas(BigDecimal exentas) {
		this.exentas = exentas;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Long getIdTimbradoPtoExp() {
		return idTimbradoPtoExp;
	}
	public void setIdTimbradoPtoExp(Long idTimbradoPtoExp) {
		this.idTimbradoPtoExp = idTimbradoPtoExp;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public String getNroNotaCredito() {
		return nroNotaCredito;
	}
	public void setNroNotaCredito(String nroNotaCredito) {
		this.nroNotaCredito = nroNotaCredito;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}

}