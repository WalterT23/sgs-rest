package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DetalleVentaDTO {
	private Long id;
	private Long idCabVenta;
	private Long idProducto;
	private Double cantidad;
	private BigDecimal precioUnitario;
	private BigDecimal precioTotal;
	private String tipoIdentificadorFiscal;
	private Integer porcentajeDescuento;
	private BigDecimal montoDescuento;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCabVenta() {
		return idCabVenta;
	}
	public void setIdCabVenta(Long idCabVenta) {
		this.idCabVenta = idCabVenta;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	public BigDecimal getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(BigDecimal precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public BigDecimal getPrecioTotal() {
		return precioTotal;
	}
	public void setPrecioTotal(BigDecimal precioTotal) {
		this.precioTotal = precioTotal;
	}
	public String getTipoIdentificadorFiscal() {
		return tipoIdentificadorFiscal;
	}
	public void setTipoIdentificadorFiscal(String tipoIdentificadorFiscal) {
		this.tipoIdentificadorFiscal = tipoIdentificadorFiscal;
	}
	public Integer getPorcentajeDescuento() {
		return porcentajeDescuento;
	}
	public void setPorcentajeDescuento(Integer porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}
	public BigDecimal getMontoDescuento() {
		return montoDescuento;
	}
	public void setMontoDescuento(BigDecimal montoDescuento) {
		this.montoDescuento = montoDescuento;
	}

}