package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.ProveedoresContactoDTO;
import py.com.sgs.persistence.dto.ProveedoresDTO;

public class ProveedoresCompositeDTO extends ProveedoresDTO{
	
	private List<ProveedoresContactoDTO> contactos; //para mandar los contactos del proveedor
	

	public List<ProveedoresContactoDTO> getContactos() {
		return contactos;
	}

	public void setContactos(List<ProveedoresContactoDTO> contactos) {
		this.contactos = contactos;
	}

}
