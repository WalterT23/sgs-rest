package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.NotaCreditoCompraDTO;
import py.com.sgs.persistence.dto.NotaCreditoCompraDetalleDTO;

public class NotaCreditoCompraCompositeDTO extends NotaCreditoCompraDTO{
	
	private List<NotaCreditoCompraDetalleDTO> detalles;
	private String proveedor;
	private String estado;
	

	public List<NotaCreditoCompraDetalleDTO> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<NotaCreditoCompraDetalleDTO> detalles) {
		this.detalles = detalles;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	
}
