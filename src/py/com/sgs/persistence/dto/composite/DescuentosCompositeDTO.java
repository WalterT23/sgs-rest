package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.DescuentosDTO;
import py.com.sgs.persistence.dto.DescuentosDetallesDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DescuentosCompositeDTO extends DescuentosDTO{
	
	private InfoRefOpcDTO tipoDescuento;
	private List<DescuentosDetallesDTO> detalles;

	public List<DescuentosDetallesDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<DescuentosDetallesDTO> detalles) {
		this.detalles = detalles;
	}
	public InfoRefOpcDTO getTipoDescuento() {
		return tipoDescuento;
	}
	public void setTipoDescuento(InfoRefOpcDTO tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}

}
