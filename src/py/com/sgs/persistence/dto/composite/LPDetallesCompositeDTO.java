package py.com.sgs.persistence.dto.composite;

import py.com.sgs.persistence.dto.ListaPreciosDetalleDTO;
import py.com.sgs.persistence.dto.composite.StockCompositeDTO;

public class LPDetallesCompositeDTO extends ListaPreciosDetalleDTO {
	
	private StockCompositeDTO stock;

	public StockCompositeDTO getStock() {
		return stock;
	}

	public void setStock(StockCompositeDTO stock) {
		this.stock = stock;
	}
	

	
}
