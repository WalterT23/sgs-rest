package py.com.sgs.persistence.dto.composite;

import py.com.sgs.persistence.dto.CuotaDetalleDTO;


public class CuotasDetalleCompositeDTO extends CuotaDetalleDTO{
	
	private String nro;
	private String nroFactura;
	private Long idProveedor;
	private String proveedor;
	private Long idCabecera;
	
	
	public String getNro() {
		return nro;
	}
	public void setNro(String nro) {
		this.nro = nro;
	}
	public String getNroFactura() {
		return nroFactura;
	}
	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}
	public Long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public Long getIdCabecera() {
		return idCabecera;
	}
	public void setIdCabecera(Long idCabecera) {
		this.idCabecera = idCabecera;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	
	
	
}
