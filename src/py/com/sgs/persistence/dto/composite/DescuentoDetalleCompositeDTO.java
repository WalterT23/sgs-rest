package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.DescuentosDetallesDTO;
import py.com.sgs.persistence.dto.ProductosDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DescuentoDetalleCompositeDTO  extends DescuentosDetallesDTO {

	private ProductosDTO producto;

	public ProductosDTO getProducto() {
		return producto;
	}

	public void setProducto(ProductosDTO producto) {
		this.producto = producto;
	}

	

	
	
}
