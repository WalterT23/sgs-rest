package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.CabeceraCompraDTO;
import py.com.sgs.persistence.dto.DetalleCompraDTO;


public class CabeceraCompraCompositeDTO extends CabeceraCompraDTO{
	
	private List<DetalleCompraDTO> detalles;
	
	private List<DetalleCompraDTO> detallesAEliminar;
	private List<DetalleCompraDTO> detallesAAgregar;
	private CuotasCompositeDTO cuotas;
	private PagoProveedoresCompositeDTO pagos;
	private String estado;
	
	private String proveedor;
	
	
	
	public List<DetalleCompraDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<DetalleCompraDTO> detalles) {
		this.detalles = detalles;
	}
	
	
	
	
	public List<DetalleCompraDTO> getDetallesAEliminar() {
		return detallesAEliminar;
	}
	public void setDetallesAEliminar(List<DetalleCompraDTO> detallesAEliminar) {
		this.detallesAEliminar = detallesAEliminar;
	}
	public List<DetalleCompraDTO> getDetallesAAgregar() {
		return detallesAAgregar;
	}
	public void setDetallesAAgregar(List<DetalleCompraDTO> detallesAAgregar) {
		this.detallesAAgregar = detallesAAgregar;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public CuotasCompositeDTO getCuotas() {
		return cuotas;
	}
	public void setCuotas(CuotasCompositeDTO cuotas) {
		this.cuotas = cuotas;
	}
	public PagoProveedoresCompositeDTO getPagos() {
		return pagos;
	}
	public void setPagos(PagoProveedoresCompositeDTO pagos) {
		this.pagos = pagos;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

}
