package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.DetallePagoDTO;
import py.com.sgs.persistence.dto.FormaPagoDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;

public class FormaPagoCompositeDTO extends FormaPagoDTO{
	
	private InfoRefOpcDTO medioPago;
	private List<DetallePagoDTO> detallesPagos;

	public List<DetallePagoDTO> getDetallesPagos() {
		return detallesPagos;
	}

	public void setDetallesPagos(List<DetallePagoDTO> detallesPagos) {
		this.detallesPagos = detallesPagos;
	}

	public InfoRefOpcDTO getMedioPago() {
		return medioPago;
	}

	public void setMedioPago(InfoRefOpcDTO medioPago) {
		this.medioPago = medioPago;
	}

}
