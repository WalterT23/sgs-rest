package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.CabeceraOrdenPagoDTO;
import py.com.sgs.persistence.dto.CuentaBancoDTO;
import py.com.sgs.persistence.dto.DetalleOrdenPagoDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.ProveedoresDTO;
import py.com.sgs.persistence.dto.SucursalDTO;



public class CabeceraOrdenPagoCompositeDTO extends CabeceraOrdenPagoDTO{
	
	private List<DetalleOrdenPagoCompositeDTO> detalles;
	private ProveedoresDTO proveedor;
	private InfoRefOpcDTO concepto;
	private InfoRefOpcDTO medio;
	private CuentaBancoDTO cuenta;
	private SucursalDTO sucursal;
	
	public List<DetalleOrdenPagoCompositeDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<DetalleOrdenPagoCompositeDTO> detalles) {
		this.detalles = detalles;
	}
	public ProveedoresDTO getProveedor() {
		return proveedor;
	}
	public void setProveedor(ProveedoresDTO proveedor) {
		this.proveedor = proveedor;
	}
	public InfoRefOpcDTO getConcepto() {
		return concepto;
	}
	public void setConcepto(InfoRefOpcDTO concepto) {
		this.concepto = concepto;
	}
	public InfoRefOpcDTO getMedio() {
		return medio;
	}
	public void setMedio(InfoRefOpcDTO medio) {
		this.medio = medio;
	}
	public CuentaBancoDTO getCuenta() {
		return cuenta;
	}
	public void setCuenta(CuentaBancoDTO cuenta) {
		this.cuenta = cuenta;
	}
	public SucursalDTO getSucursal() {
		return sucursal;
	}
	public void setSucursal(SucursalDTO sucursal) {
		this.sucursal = sucursal;
	}

	
	
	
}
