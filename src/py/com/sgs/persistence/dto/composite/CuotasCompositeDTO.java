package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.CuotaDetalleDTO;
import py.com.sgs.persistence.dto.CuotasDTO;


public class CuotasCompositeDTO extends CuotasDTO{
	
	private List<CuotaDetalleDTO> detalles;
	private String proveedor;
	private String nroFactura;

	public List<CuotaDetalleDTO> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<CuotaDetalleDTO> detalles) {
		this.detalles = detalles;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}
	
}
