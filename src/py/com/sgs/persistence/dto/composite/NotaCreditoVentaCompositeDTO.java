package py.com.sgs.persistence.dto.composite;

import java.util.Date;
import java.util.List;

import py.com.sgs.persistence.dto.ClientesDTO;
import py.com.sgs.persistence.dto.EmpresaDTO;
import py.com.sgs.persistence.dto.NotaCreditoVentaDTO;
import py.com.sgs.persistence.dto.NotaCreditoVentaDetalleDTO;
import py.com.sgs.persistence.dto.TimbradoPtoExpedicionDTO;

public class NotaCreditoVentaCompositeDTO extends NotaCreditoVentaDTO{
	
	private List<NotaCreditoVentaDetalleCompositeDTO> detalles;
	private EmpresaDTO empresa;
	private TimbradoPuntoExpedicionCompositeDTO timbradoPtoExpedicion;
	private ClientesDTO cliente;
	private Date fechaFactura;

	public List<NotaCreditoVentaDetalleCompositeDTO> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<NotaCreditoVentaDetalleCompositeDTO> detalles) {
		this.detalles = detalles;
	}

	public EmpresaDTO getEmpresa() {
		return empresa;
	}

	public void setEmpresa(EmpresaDTO empresa) {
		this.empresa = empresa;
	}

	public TimbradoPuntoExpedicionCompositeDTO getTimbradoPtoExpedicion() {
		return timbradoPtoExpedicion;
	}

	public void setTimbradoPtoExpedicion(
			TimbradoPuntoExpedicionCompositeDTO timbradoPtoExpedicion) {
		this.timbradoPtoExpedicion = timbradoPtoExpedicion;
	}

	public ClientesDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClientesDTO cliente) {
		this.cliente = cliente;
	}

	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}


}
