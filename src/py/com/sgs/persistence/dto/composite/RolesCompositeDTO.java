package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.FuncionalidadesDTO;
import py.com.sgs.persistence.dto.RolesDTO;

public class RolesCompositeDTO extends RolesDTO {
	private List<FuncionalidadesDTO> funcionalidades;

	public List<FuncionalidadesDTO> getFuncionalidades() {
		return funcionalidades;
	}

	public void setFuncionalidades(List<FuncionalidadesDTO> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}
}
