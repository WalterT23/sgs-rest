package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.GrupoProductosDTO;
import py.com.sgs.persistence.dto.ProductosDTO;

public class ProductosGroupCompositeDTO  {
	
	private Boolean incSubgroups;
	private List<GrupoProductosDTO>  grupos;
	private List<ProductosDTO>  productos;
	
	public Boolean getIncSubgroups() {
		return incSubgroups;
	}
	public void setIncSubgroups(Boolean incSubgroups) {
		this.incSubgroups = incSubgroups;
	}
	public List<ProductosDTO> getProductos() {
		return productos;
	}
	public void setProductos(List<ProductosDTO> productos) {
		this.productos = productos;
	}
	public List<GrupoProductosDTO> getGrupos() {
		return grupos;
	}
	public void setGrupos(List<GrupoProductosDTO> grupos) {
		this.grupos = grupos;
	}
	

}
