package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.OrdenCompraDetalleDTO;
import py.com.sgs.persistence.dto.OrdenDeCompraDTO;
import py.com.sgs.persistence.dto.ProveedoresDTO;

public class OrdenDeCompraCompositeDTO extends OrdenDeCompraDTO{
	private List<OrdenCompraDetalleDTO> detalles;
	private List<OrdenCompraDetalleDTO> detallesAEliminar;
	private List<OrdenCompraDetalleDTO> detallesAAgregar;
	private ProveedoresDTO proveedorDto;
	private InfoRefOpcDTO estadoDto;
	
	public List<OrdenCompraDetalleDTO> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<OrdenCompraDetalleDTO> detalles) {
		this.detalles = detalles;
	}

	public List<OrdenCompraDetalleDTO> getDetallesAEliminar() {
		return detallesAEliminar;
	}

	public void setDetallesAEliminar(List<OrdenCompraDetalleDTO> detallesAEliminar) {
		this.detallesAEliminar = detallesAEliminar;
	}

	public List<OrdenCompraDetalleDTO> getDetallesAAgregar() {
		return detallesAAgregar;
	}

	public void setDetallesAAgregar(List<OrdenCompraDetalleDTO> detallesAAgregar) {
		this.detallesAAgregar = detallesAAgregar;
	}

	public ProveedoresDTO getProveedorDto() {
		return proveedorDto;
	}

	public void setProveedorDto(ProveedoresDTO proveedorDto) {
		this.proveedorDto = proveedorDto;
	}

	public InfoRefOpcDTO getEstadoDto() {
		return estadoDto;
	}

	public void setEstadoDto(InfoRefOpcDTO estadoDto) {
		this.estadoDto = estadoDto;
	}
	
	
}
