package py.com.sgs.persistence.dto.composite;

import java.math.BigDecimal;

import py.com.sgs.persistence.dto.NotaCreditoVentaDetalleDTO;
import py.com.sgs.persistence.dto.ProductosDTO;

public class NotaCreditoVentaDetalleCompositeDTO extends NotaCreditoVentaDetalleDTO{
	
	private ProductosDTO producto;
	private BigDecimal precioUni;

	public ProductosDTO getProducto() {
		return producto;
	}

	public void setProducto(ProductosDTO producto) {
		this.producto = producto;
	}
	
	public BigDecimal getPrecioUni() {
		return precioUni;
	}
	
	public void setPrecioUni(BigDecimal precioUni) {
		this.precioUni = precioUni;
	}

}
