package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.ConsultaPagosDTO;


public class PagoFacturasCompositeDTO {
	
	private List<ConsultaPagosDTO> facturas;
	private PagoProveedoresCompositeDTO pago;

	public List<ConsultaPagosDTO> getFacturas() {
		return facturas;
	}

	public void setFacturas(List<ConsultaPagosDTO> facturas) {
		this.facturas = facturas;
	}

	public PagoProveedoresCompositeDTO getPago() {
		return pago;
	}

	public void setPago(PagoProveedoresCompositeDTO pago) {
		this.pago = pago;
	}
	
	
		
	
}
