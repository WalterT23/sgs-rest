package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.RolesDTO;
import py.com.sgs.persistence.dto.UsuariosDTO;
import py.com.sgs.persistence.dto.UsuarioSucursalDTO;

public class UsuariosCompositeDTO extends UsuariosDTO{
	private List<UsuarioSucursalDTO> sucursales;
	private List<String> funcionalidades; //para mandar las funcionalidades del user en el login
	private List<RolesDTO> roles;
	private String accesstoken;
	
	public List<String> getFuncionalidades() {
		return funcionalidades;
	}

	public void setFuncionalidades(List<String> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}

	public List<RolesDTO> getRoles() {
		return roles;
	}

	public void setRoles(List<RolesDTO> roles) {
		this.roles = roles;
	}

	public String getAccesstoken() {
		return accesstoken;
	}

	public void setAccesstoken(String accesstoken) {
		this.accesstoken = accesstoken;
	}

	public List<UsuarioSucursalDTO> getSucursales() {
		return sucursales;
	}

	public void setSucursales(List<UsuarioSucursalDTO> sucursales) {
		this.sucursales = sucursales;
	}
	

}
