package py.com.sgs.persistence.dto.composite;

import py.com.sgs.persistence.dto.PuntoExpedicionDTO;
import py.com.sgs.persistence.dto.TimbradoDTO;
import py.com.sgs.persistence.dto.TimbradoPtoExpedicionDTO;

public class TimbradoPuntoExpedicionCompositeDTO extends TimbradoPtoExpedicionDTO{
	
	private TimbradoDTO timbrado;
	private PuntoExpedicionDTO puntoExpedicion;
	
	public TimbradoDTO getTimbrado() {
		return timbrado;
	}
	public void setTimbrado(TimbradoDTO timbrado) {
		this.timbrado = timbrado;
	}
	public PuntoExpedicionDTO getPuntoExpedicion() {
		return puntoExpedicion;
	}
	public void setPuntoExpedicion(PuntoExpedicionDTO puntoExpedicion) {
		this.puntoExpedicion = puntoExpedicion;
	}

}
