package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.AccesoDTO;

public class AccesoCompositeDTO extends AccesoDTO{
	
	private List<String> funcionalidades; //para mandar las funcionalidades del user
	
	public List<String> getFuncionalidades() {
		return funcionalidades;
	}

	public void setFuncionalidades(List<String> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}

}
