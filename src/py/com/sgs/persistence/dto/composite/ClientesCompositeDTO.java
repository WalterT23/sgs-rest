package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.ClientesDTO;
import py.com.sgs.persistence.dto.ClientesListaPrecioDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientesCompositeDTO  extends ClientesDTO{
	
	private List<ClientesListaPrecioDTO> cliListaPrecio;

	public List<ClientesListaPrecioDTO> getCliListaPrecio() {
		return cliListaPrecio;
	}

	public void setCliListaPrecio(List<ClientesListaPrecioDTO> cliListaPrecio) {
		this.cliListaPrecio = cliListaPrecio;
	}

}
