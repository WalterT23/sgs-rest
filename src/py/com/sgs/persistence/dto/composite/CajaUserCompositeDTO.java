package py.com.sgs.persistence.dto.composite;

import java.math.BigDecimal;

import py.com.sgs.persistence.dto.AperturaCierreDTO;
import py.com.sgs.persistence.dto.CajaUserDTO;
import py.com.sgs.persistence.dto.IngresosEgresosCajaDTO;
import py.com.sgs.persistence.dto.PuntoExpedicionDTO;
import py.com.sgs.persistence.dto.UsuariosDTO;
import sun.nio.cs.ext.Big5;

public class CajaUserCompositeDTO extends CajaUserDTO{
	
	private String usuario;
	private AperturaCierreDTO apertura;
	private BigDecimal ingresos;
	private BigDecimal egresos;
	private BigDecimal saldo;
	private IngresosEgresosCajaDTO ingresoEgreso;
	private Long puntoExp;
	



	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public AperturaCierreDTO getApertura() {
		return apertura;
	}

	public void setApertura(AperturaCierreDTO apertura) {
		this.apertura = apertura;
	}

	public IngresosEgresosCajaDTO getIngresoEgreso() {
		return ingresoEgreso;
	}

	public void setIngresoEgreso(IngresosEgresosCajaDTO ingresoEgreso) {
		this.ingresoEgreso = ingresoEgreso;
	}

	public Long getPuntoExp() {
		return puntoExp;
	}

	public void setPuntoExp(Long puntoExp) {
		this.puntoExp = puntoExp;
	}

	public BigDecimal getIngresos() {
		return ingresos;
	}

	public void setIngresos(BigDecimal ingresos) {
		this.ingresos = ingresos;
	}

	public BigDecimal getEgresos() {
		return egresos;
	}

	public void setEgresos(BigDecimal egresos) {
		this.egresos = egresos;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}	
	
	

}
