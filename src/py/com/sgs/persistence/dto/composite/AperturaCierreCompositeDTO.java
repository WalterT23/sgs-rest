package py.com.sgs.persistence.dto.composite;

import java.math.BigDecimal;

import py.com.sgs.persistence.dto.AperturaCierreDTO;
import py.com.sgs.persistence.dto.ArqueoCajaDTO;
import py.com.sgs.persistence.dto.DetalleCierreDTO;
import py.com.sgs.persistence.dto.UsuariosDTO;

public class AperturaCierreCompositeDTO extends AperturaCierreDTO{

	private BigDecimal ventaTarjeta;
	private BigDecimal ventaEfectivo;
	private BigDecimal otrosIngresos;
	private BigDecimal egresos;
	private String usuario;
	private UsuariosDTO cajero;	
	private ArqueoCajaDTO arqueo;
	private DetalleCierreDTO detalle;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public BigDecimal getVentaTarjeta() {
		return ventaTarjeta;
	}
	public void setVentaTarjeta(BigDecimal ventaTarjeta) {
		this.ventaTarjeta = ventaTarjeta;
	}
	public BigDecimal getVentaEfectivo() {
		return ventaEfectivo;
	}
	public void setVentaEfectivo(BigDecimal ventaEfectivo) {
		this.ventaEfectivo = ventaEfectivo;
	}
	public BigDecimal getOtrosIngresos() {
		return otrosIngresos;
	}
	public void setOtrosIngresos(BigDecimal otrosIngresos) {
		this.otrosIngresos = otrosIngresos;
	}
	public BigDecimal getEgresos() {
		return egresos;
	}
	public void setEgresos(BigDecimal egresos) {
		this.egresos = egresos;
	}
	public UsuariosDTO getCajero() {
		return cajero;
	}
	public void setCajero(UsuariosDTO cajero) {
		this.cajero = cajero;
	}
	public ArqueoCajaDTO getArqueo() {
		return arqueo;
	}
	public void setArqueo(ArqueoCajaDTO arqueo) {
		this.arqueo = arqueo;
	}
	public DetalleCierreDTO getDetalle() {
		return detalle;
	}
	public void setDetalle(DetalleCierreDTO detalle) {
		this.detalle = detalle;
	}	

}
