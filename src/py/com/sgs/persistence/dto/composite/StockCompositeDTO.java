package py.com.sgs.persistence.dto.composite;

import py.com.sgs.persistence.dto.ProductosDTO;
import py.com.sgs.persistence.dto.StockDTO;

public class StockCompositeDTO extends StockDTO{
	
	private ProductosDTO productoDTO;


	public ProductosDTO getProductoDTO() {
		return productoDTO;
	}

	public void setProductoDTO(ProductosDTO productoDTO) {
		this.productoDTO = productoDTO;
	}

}
