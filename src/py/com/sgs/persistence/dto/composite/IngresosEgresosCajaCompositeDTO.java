package py.com.sgs.persistence.dto.composite;

import py.com.sgs.persistence.dto.IngresosEgresosCajaDTO;

public class IngresosEgresosCajaCompositeDTO extends IngresosEgresosCajaDTO{
	
	private String puntoExp;
	private String tipoMovimiento;
	private String movimiento;
	private Long idUsuario;
	private String Cajero;
	 
	
	
	public String getPuntoExp() {
		return puntoExp;
	}
	public void setPuntoExp(String puntoExp) {
		this.puntoExp = puntoExp;
	}
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}
	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}
	public String getMovimiento() {
		return movimiento;
	}
	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getCajero() {
		return Cajero;
	}
	public void setCajero(String cajero) {
		Cajero = cajero;
	}
	
	
	

}
