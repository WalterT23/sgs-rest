package py.com.sgs.persistence.dto.composite;

import java.util.List;
import py.com.sgs.persistence.dto.PagoProvDetalleDTO;
import py.com.sgs.persistence.dto.PagoProveedoresDTO;


public class PagoProveedoresCompositeDTO extends PagoProveedoresDTO{
	
	private List<PagoProvDetalleDTO> detalles;
	private List<PagoProvDetalleDTO> detallesAEliminar;
	private List<PagoProvDetalleDTO> detallesAAgregar;
	private String proveedor;
	private String nroOrdenPago;
	private Long idOrdenPago;
	
	public List<PagoProvDetalleDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<PagoProvDetalleDTO> detalles) {
		this.detalles = detalles;
	}
	public List<PagoProvDetalleDTO> getDetallesAEliminar() {
		return detallesAEliminar;
	}
	public void setDetallesAEliminar(List<PagoProvDetalleDTO> detallesAEliminar) {
		this.detallesAEliminar = detallesAEliminar;
	}
	public List<PagoProvDetalleDTO> getDetallesAAgregar() {
		return detallesAAgregar;
	}
	public void setDetallesAAgregar(List<PagoProvDetalleDTO> detallesAAgregar) {
		this.detallesAAgregar = detallesAAgregar;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public String getNroOrdenPago() {
		return nroOrdenPago;
	}
	public void setNroOrdenPago(String nroOrdenPago) {
		this.nroOrdenPago = nroOrdenPago;
	}
	public Long getIdOrdenPago() {
		return idOrdenPago;
	}
	public void setIdOrdenPago(Long idOrdenPago) {
		this.idOrdenPago = idOrdenPago;
	}
	

	
	
}
