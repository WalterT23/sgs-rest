package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.CabeceraVentaDTO;
import py.com.sgs.persistence.dto.ClientesDTO;
import py.com.sgs.persistence.dto.EmpresaDTO;
import py.com.sgs.persistence.dto.consulta.InfoVentasDTO;


public class CabeceraVentaCompositeDTO extends CabeceraVentaDTO{
	
	private List<DetalleVentaCompositeDTO> detalles;
	private List<FormaPagoCompositeDTO> formasPago;
	private ClientesDTO cliente;
	private TimbradoPuntoExpedicionCompositeDTO timbradoPtoExpedicion;
	private EmpresaDTO empresa; //aqui se tienen los datos del super para imprimir en el encabezado de la factura
	private InfoVentasDTO infoVenta;
	
	public List<DetalleVentaCompositeDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<DetalleVentaCompositeDTO> detalles) {
		this.detalles = detalles;
	}
	public List<FormaPagoCompositeDTO> getFormasPago() {
		return formasPago;
	}
	public void setFormasPago(List<FormaPagoCompositeDTO> formasPago) {
		this.formasPago = formasPago;
	}
	public ClientesDTO getCliente() {
		return cliente;
	}
	public void setCliente(ClientesDTO cliente) {
		this.cliente = cliente;
	}
	public TimbradoPuntoExpedicionCompositeDTO getTimbradoPtoExpedicion() {
		return timbradoPtoExpedicion;
	}
	public void setTimbradoPtoExpedicion(
			TimbradoPuntoExpedicionCompositeDTO timbradoPtoExpedicion) {
		this.timbradoPtoExpedicion = timbradoPtoExpedicion;
	}
	public EmpresaDTO getEmpresa() {
		return empresa;
	}
	public void setEmpresa(EmpresaDTO empresa) {
		this.empresa = empresa;
	}
	public InfoVentasDTO getInfoVenta() {
		return infoVenta;
	}
	public void setInfoVenta(InfoVentasDTO infoVenta) {
		this.infoVenta = infoVenta;
	}

}
