package py.com.sgs.persistence.dto.composite;

import py.com.sgs.persistence.dto.DetalleOrdenPagoDTO;

public class DetalleOrdenPagoCompositeDTO extends DetalleOrdenPagoDTO{
	
	private CabeceraCompraCompositeDTO factura;

	public CabeceraCompraCompositeDTO getFactura() {
		return factura;
	}

	public void setFactura(CabeceraCompraCompositeDTO factura) {
		this.factura = factura;
	}

}
