package py.com.sgs.persistence.dto.composite;

import java.math.BigDecimal;

import py.com.sgs.persistence.dto.DescuentosDetallesDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.ProductosDTO;


public class ProductosCompositeDTO extends ProductosDTO{

	private InfoRefOpcDTO tipoTributoDTO;
	//private ProductosUniMedDTO unidadMedida;

	private BigDecimal ultCostoCompra;
	private Long idSucursal;
	private Double cantidad;
	private BigDecimal precioProducto;
	private BigDecimal total;
	private BigDecimal totalDescuento;
	private DescuentosDetallesDTO detalleDescuento;

	public InfoRefOpcDTO getTipoTributoDTO() {
		return tipoTributoDTO;
	}
	public void setTipoTributoDTO(InfoRefOpcDTO tipoTributoDTO) {
		this.tipoTributoDTO = tipoTributoDTO;
	}

	public BigDecimal getUltCostoCompra() {
		return ultCostoCompra;
	}
	public void setUltCostoCompra(BigDecimal ultCostoCompra) {
		this.ultCostoCompra = ultCostoCompra;
	}

	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getPrecioProducto() {
		return precioProducto;
	}
	public void setPrecioProducto(BigDecimal precioProducto) {
		this.precioProducto = precioProducto;
	}
	public DescuentosDetallesDTO getDetalleDescuento() {
		return detalleDescuento;
	}
	public void setDetalleDescuento(DescuentosDetallesDTO detalleDescuento) {
		this.detalleDescuento = detalleDescuento;
	}
	public BigDecimal getTotalDescuento() {
		return totalDescuento;
	}
	public void setTotalDescuento(BigDecimal totalDescuento) {
		this.totalDescuento = totalDescuento;
	}
	
}
