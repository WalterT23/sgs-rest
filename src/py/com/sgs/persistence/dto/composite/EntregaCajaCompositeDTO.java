package py.com.sgs.persistence.dto.composite;

import py.com.sgs.persistence.dto.ArqueoCajaDTO;
import py.com.sgs.persistence.dto.EntregaCajaDTO;

public class EntregaCajaCompositeDTO extends EntregaCajaDTO{
	
	AperturaCierreCompositeDTO apertura;
	
	ArqueoCajaDTO arqueo;

	public AperturaCierreCompositeDTO getApertura() {
		return apertura;
	}

	public void setApertura(AperturaCierreCompositeDTO apertura) {
		this.apertura = apertura;
	}

	public ArqueoCajaDTO getArqueo() {
		return arqueo;
	}

	public void setArqueo(ArqueoCajaDTO arqueo) {
		this.arqueo = arqueo;
	}

	

}
