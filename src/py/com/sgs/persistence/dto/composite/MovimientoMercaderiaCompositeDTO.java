package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.MovMercDetalleDTO;
import py.com.sgs.persistence.dto.MovimientoMercaderiaDTO;


public class MovimientoMercaderiaCompositeDTO extends MovimientoMercaderiaDTO{
	
	private List<MovMercDetalleDTO> detalles;

	public List<MovMercDetalleDTO> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<MovMercDetalleDTO> detalles) {
		this.detalles = detalles;
	}
	

}
