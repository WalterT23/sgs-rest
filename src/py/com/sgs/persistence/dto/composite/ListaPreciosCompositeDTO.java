package py.com.sgs.persistence.dto.composite;

import java.util.List;

import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.ListaPreciosDTO;



public class ListaPreciosCompositeDTO extends ListaPreciosDTO {
	
	private InfoRefOpcDTO tipoListaDTO; 
	private List<LPDetallesCompositeDTO> detalles ;

	
	public InfoRefOpcDTO getTipoListaDTO() {
		return tipoListaDTO;
	}

	public void setTipoListaDTO(InfoRefOpcDTO tipoListaDTO) {
		this.tipoListaDTO = tipoListaDTO;
	}


	public List<LPDetallesCompositeDTO> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<LPDetallesCompositeDTO> detalles) {
		this.detalles = detalles;
	}

}
