package py.com.sgs.persistence.dto.composite;

import py.com.sgs.persistence.dto.DetalleVentaDTO;
import py.com.sgs.persistence.dto.ProductosDTO;

public class DetalleVentaCompositeDTO extends DetalleVentaDTO{
	
	private ProductosDTO producto;

	public ProductosDTO getProducto() {
		return producto;
	}

	public void setProducto(ProductosDTO producto) {
		this.producto = producto;
	}

}
