package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EntregaCajaDTO {
	private Long id;
	private Long idAperturaCierre;
	private BigDecimal montoEfectivo;
	private BigDecimal montoTarjeta;
	private BigDecimal total;
	private Date fechaCierre;

	public Date getFechaCierre() {
		return fechaCierre;
	}
	public void setFechaCierre(Date fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdAperturaCierre() {
		return idAperturaCierre;
	}
	public void setIdAperturaCierre(Long idAperturaCierre) {
		this.idAperturaCierre = idAperturaCierre;
	}
	public BigDecimal getMontoEfectivo() {
		return montoEfectivo;
	}
	public void setMontoEfectivo(BigDecimal montoEfectivo) {
		this.montoEfectivo = montoEfectivo;
	}
	public BigDecimal getMontoTarjeta() {
		return montoTarjeta;
	}
	public void setMontoTarjeta(BigDecimal montoTarjeta) {
		this.montoTarjeta = montoTarjeta;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}