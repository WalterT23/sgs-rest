package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FiltroBalanceDTO {
	private Double periodo;
	private BigDecimal totalIngreso;
	private BigDecimal totalEgreso;
	private BigDecimal saldo;
	private Long idSucursal;
	private List <BalanceDTO> meses;
	
	
	
	
	public Double getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Double periodo) {
		this.periodo = periodo;
	}
	public BigDecimal getTotalIngreso() {
		return totalIngreso;
	}
	public void setTotalIngreso(BigDecimal totalIngreso) {
		this.totalIngreso = totalIngreso;
	}
	public BigDecimal getTotalEgreso() {
		return totalEgreso;
	}
	public void setTotalEgreso(BigDecimal totalEgreso) {
		this.totalEgreso = totalEgreso;
	}
	
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	public List<BalanceDTO> getMeses() {
		return meses;
	}
	public void setMeses(List<BalanceDTO> meses) {
		this.meses = meses;
	}
	
	
	
	
	
	
	
	
}