package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CuotasPenDTO {
	private Long idProveedor;
	private String proveedor;
	private String ruc;
	private String nombreFantasia;
	private Long idSucursal;
	private BigDecimal montoTotal;
	private Integer cuotaspendientes;
	
	
	
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getNombre_fantasia() {
		return nombreFantasia;
	}
	public void setNombre_fantasia(String nombre_fantasia) {
		this.nombreFantasia = nombre_fantasia;
	}
	public Long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	public Integer getCuotaspendientes() {
		return cuotaspendientes;
	}
	public void setCuotaspendientes(Integer cuotaspendientes) {
		this.cuotaspendientes = cuotaspendientes;
	}
	
	

	
}