package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NotaCreditoVentaDetalleDTO {
	private Long id;
	private Long idNotaCreditoVenta;
	private Long idProducto;
	private BigDecimal iva5;
	private BigDecimal iva10;
	private BigDecimal total;
	private String observaciones;
	private Double cantidad;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdNotaCreditoVenta() {
		return idNotaCreditoVenta;
	}
	public void setIdNotaCreditoVenta(Long idNotaCreditoVenta) {
		this.idNotaCreditoVenta = idNotaCreditoVenta;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public BigDecimal getIva5() {
		return iva5;
	}
	public void setIva5(BigDecimal iva5) {
		this.iva5 = iva5;
	}
	public BigDecimal getIva10() {
		return iva10;
	}
	public void setIva10(BigDecimal iva10) {
		this.iva10 = iva10;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	
	
}