package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class NotaCreditoCompraDTO {
	private Long id;
	private Long idCabeceraCompra;
	private Long idProveedor;
	private String nroNotaCredito;
	private BigDecimal montoTotal;
	private BigDecimal iva5;
	private BigDecimal iva10;
	private BigDecimal exentas;
	private Date fecha;
	private Date fechaCreacion;
	private String usuarioCreacion;
	private Long idEstado;
	private Long idSucursal;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCabeceraCompra() {
		return idCabeceraCompra;
	}
	public void setIdCabeceraCompra(Long idCabeceraCompra) {
		this.idCabeceraCompra = idCabeceraCompra;
	}
	public String getNroNotaCredito() {
		return nroNotaCredito;
	}
	public void setNroNotaCredito(String nroNotaCredito) {
		this.nroNotaCredito = nroNotaCredito;
	}
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	public BigDecimal getIva5() {
		return iva5;
	}
	public void setIva5(BigDecimal iva5) {
		this.iva5 = iva5;
	}
	public BigDecimal getIva10() {
		return iva10;
	}
	public void setIva10(BigDecimal iva10) {
		this.iva10 = iva10;
	}
	public BigDecimal getExentas() {
		return exentas;
	}
	public void setExentas(BigDecimal exentas) {
		this.exentas = exentas;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Long getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}	

}