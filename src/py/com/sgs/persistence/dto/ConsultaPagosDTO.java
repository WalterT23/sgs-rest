package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsultaPagosDTO {
	private Long idCompra;
	private Long idProveedor;
	private String proveedor;
	private Long idSucursal;
	private Long idCuotas;
	private Long idCuotaDetalle;
	private BigDecimal montoaPagar;
	private BigDecimal saldo;
	private String  tipoFactura;
	private Long  idTipoFactura;
	private String  nroFactura;
	private String nro;
	private Date fechaVencimiento;
	
	

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public Long getIdCompra() {
		return idCompra;
	}
	public void setIdCompra(Long idCompra) {
		this.idCompra = idCompra;
	}
	public Long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}
	
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	public Long getIdCuotas() {
		return idCuotas;
	}
	public void setIdCuotas(Long idCuotas) {
		this.idCuotas = idCuotas;
	}
	public Long getIdCuotaDetalle() {
		return idCuotaDetalle;
	}
	public void setIdCuotaDetalle(Long idCuotaDetalle) {
		this.idCuotaDetalle = idCuotaDetalle;
	}
	public BigDecimal getMontoaPagar() {
		return montoaPagar;
	}
	public void setMontoaPagar(BigDecimal montoaPagar) {
		this.montoaPagar = montoaPagar;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
	public String getTipoFactura() {
		return tipoFactura;
	}
	public void setTipoFactura(String tipoFactura) {
		this.tipoFactura = tipoFactura;
	}
	public String getNroFactura() {
		return nroFactura;
	}
	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}
	public String getNro() {
		return nro;
	}
	public void setNro(String nro) {
		this.nro = nro;
	}
	public Long getIdTipoFactura() {
		return idTipoFactura;
	}
	public void setIdTipoFactura(Long idTipoFactura) {
		this.idTipoFactura = idTipoFactura;
	}
	
	


}