package py.com.sgs.persistence.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListaPreciosDTO {
	private Long id;
	private Long idTipoLista;
	private Date inicioVigencia;
	private Date finVigencia;
	private Date fechaUltimaActualizacion;
	private String usuarioUltActualizacion;
	private Long idSucursal;
	private String costoBase; //con este parametro se define que costo del stock se va a utilizar para el costo base de los productos (en el detalle)
	private String descripcion;
	private Boolean calcPrecioVenta; //con esta bandera se indica si se crea la lista de precios con el costo de venta de cada producto calculado en base a su porcentaje de recarga
	private String listaBase; //indicador de si la lista es la lista base del negocio o no, solo puede haber una por sucursal
	private Boolean lpIncompleta;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdTipoLista() {
		return idTipoLista;
	}
	public void setIdTipoLista(Long idTipoLista) {
		this.idTipoLista = idTipoLista;
	}
	public Date getInicioVigencia() {
		return inicioVigencia;
	}
	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}
	public Date getFinVigencia() {
		return finVigencia;
	}
	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}
	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}
	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}
	public String getUsuarioUltActualizacion() {
		return usuarioUltActualizacion;
	}
	public void setUsuarioUltActualizacion(String usuarioUltActualizacion) {
		this.usuarioUltActualizacion = usuarioUltActualizacion;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	public String getCostoBase() {
		return costoBase;
	}
	public void setCostoBase(String costoBase) {
		this.costoBase = costoBase;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Boolean getCalcPrecioVenta() {
		return calcPrecioVenta;
	}
	public void setCalcPrecioVenta(Boolean calcPrecioVenta) {
		this.calcPrecioVenta = calcPrecioVenta;
	}
	public String getListaBase() {
		return listaBase;
	}
	public void setListaBase(String listaBase) {
		this.listaBase = listaBase;
	}
	public Boolean getLpIncompleta() {
		return lpIncompleta;
	}
	public void setLpIncompleta(Boolean lpIncompleta) {
		this.lpIncompleta = lpIncompleta;
	}

}