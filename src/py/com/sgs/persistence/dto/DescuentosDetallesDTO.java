package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DescuentosDetallesDTO {
	private Long id;
	private Long idDescuento;
	private Long idProducto;
	private Integer cantACobrar;
	private Integer cantALlevar;
	private Double porcentaje;
	private BigDecimal monto;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdDescuento() {
		return idDescuento;
	}
	public void setIdDescuento(Long idDescuento) {
		this.idDescuento = idDescuento;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public Integer getCantACobrar() {
		return cantACobrar;
	}
	public void setCantACobrar(Integer cantACobrar) {
		this.cantACobrar = cantACobrar;
	}
	public Integer getCantALlevar() {
		return cantALlevar;
	}
	public void setCantALlevar(Integer cantALlevar) {
		this.cantALlevar = cantALlevar;
	}
	public Double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

}