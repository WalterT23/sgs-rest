package py.com.sgs.persistence.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TimbradoPtoExpedicionDTO {
	private Long id;
	private Long idTimbrado;
	private Long idPtoExpedicion;
	private String numeroDesde;
	private String numeroHasta;
	private String ultimoNumero;
	private Long idEstado;
	private String usuarioModificacion;
	private Date fechaModificacion;
	private String tipoComprobante;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdTimbrado() {
		return idTimbrado;
	}
	public void setIdTimbrado(Long idTimbrado) {
		this.idTimbrado = idTimbrado;
	}
	public Long getIdPtoExpedicion() {
		return idPtoExpedicion;
	}
	public void setIdPtoExpedicion(Long idPtoExpedicion) {
		this.idPtoExpedicion = idPtoExpedicion;
	}
	public String getUltimoNumero() {
		return ultimoNumero;
	}
	public void setUltimoNumero(String ultimoNumero) {
		this.ultimoNumero = ultimoNumero;
	}
	public Long getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}
	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}
	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public String getNumeroDesde() {
		return numeroDesde;
	}
	public void setNumeroDesde(String numeroDesde) {
		this.numeroDesde = numeroDesde;
	}
	public String getNumeroHasta() {
		return numeroHasta;
	}
	public void setNumeroHasta(String numeroHasta) {
		this.numeroHasta = numeroHasta;
	}
	public String getTipoComprobante() {
		return tipoComprobante;
	}
	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

}