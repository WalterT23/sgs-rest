package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FormaPagoDTO {
	private Long id;
	private Long idCabeceraVenta;
	private Long idMedioPago;
	private BigDecimal montoPago;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCabeceraVenta() {
		return idCabeceraVenta;
	}
	public void setIdCabeceraVenta(Long idCabeceraVenta) {
		this.idCabeceraVenta = idCabeceraVenta;
	}
	public Long getIdMedioPago() {
		return idMedioPago;
	}
	public void setIdMedioPago(Long idMedioPago) {
		this.idMedioPago = idMedioPago;
	}
	public BigDecimal getMontoPago() {
		return montoPago;
	}
	public void setMontoPago(BigDecimal montoPago) {
		this.montoPago = montoPago;
	}

}