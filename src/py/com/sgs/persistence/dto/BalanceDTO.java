package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BalanceDTO {
	
	
	private Integer mes;
	private String descripcionMes;
	private BigDecimal ingresos;
	private BigDecimal egresos;
	private BigDecimal saldo;
	
	
	
	public String getDescripcionMes() {
		return descripcionMes;
	}
	public void setDescripcionMes(String descripcionMes) {
		this.descripcionMes = descripcionMes;
	}
	public Integer getMes() {
		return mes;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	public BigDecimal getIngresos() {
		return ingresos;
	}
	public void setIngresos(BigDecimal ingresos) {
		this.ingresos = ingresos;
	}
	public BigDecimal getEgresos() {
		return egresos;
	}
	public void setEgresos(BigDecimal egresos) {
		this.egresos = egresos;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
	
	
	
}