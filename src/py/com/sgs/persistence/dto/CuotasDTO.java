package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CuotasDTO {
	private Long id;
	private Long idCabeceraCompra;
	private Long idProveedor;
	private Long idSucursal;
	private BigDecimal montoEntregado;
	private Integer cantidadCuotas;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCabeceraCompra() {
		return idCabeceraCompra;
	}
	public void setIdCabeceraCompra(Long idCabeceraCompra) {
		this.idCabeceraCompra = idCabeceraCompra;
	}
	
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	public BigDecimal getMontoEntregado() {
		return montoEntregado;
	}
	public void setMontoEntregado(BigDecimal montoEntregado) {
		this.montoEntregado = montoEntregado;
	}
	public Integer getCantidadCuotas() {
		return cantidadCuotas;
	}
	public void setCantidadCuotas(Integer cantidadCuotas) {
		this.cantidadCuotas = cantidadCuotas;
	}
	public Long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}

}