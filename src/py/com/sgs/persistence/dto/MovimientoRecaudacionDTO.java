package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MovimientoRecaudacionDTO {
	private Long id;
	private Long idTipoMovimiento;
	private Long idMovimiento;
	private BigDecimal monto;
	private Date fechaHora;
	private Long idAperturaCierre;
	private Long idDepDinero;
	private String obs;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdTipoMovimiento() {
		return idTipoMovimiento;
	}
	public void setIdTipoMovimiento(Long idTipoMovimiento) {
		this.idTipoMovimiento = idTipoMovimiento;
	}
	public Long getIdMovimiento() {
		return idMovimiento;
	}
	public void setIdMovimiento(Long idMovimiento) {
		this.idMovimiento = idMovimiento;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public Long getIdAperturaCierre() {
		return idAperturaCierre;
	}
	public void setIdAperturaCierre(Long idAperturaCierre) {
		this.idAperturaCierre = idAperturaCierre;
	}
	public Date getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}
	
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	public Long getIdDepDinero() {
		return idDepDinero;
	}
	public void setIdDepDinero(Long idDepDinero) {
		this.idDepDinero = idDepDinero;
	}

}