package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CabeceraOrdenPagoDTO {
	private Long id;
	private Long idProveedor;
	private String nroOrdenPago;
	private Date fechaOrdenPago;
	private Date fechaImpresion;
	private String usuarioImpresion;
	private BigDecimal montoTotal;
	private Long idEstado;
	private String usuarioCreacion;
	private Date fechaCreacion;
	private Long idSucursal;
	private Long idMedio;
	private Long idFuente;
	private String cheque;
	private Long idPago;
	private Long idconcepto;


	
	public String getNroOrdenPago() {
		return nroOrdenPago;
	}
	public void setNroOrdenPago(String nroOrdenPago) {
		this.nroOrdenPago = nroOrdenPago;
	}
	public Date getFechaOrdenPago() {
		return fechaOrdenPago;
	}
	public void setFechaOrdenPago(Date fechaOrdenPago) {
		this.fechaOrdenPago = fechaOrdenPago;
	}
	public Date getFechaImpresion() {
		return fechaImpresion;
	}
	public void setFechaImpresion(Date fechaImpresion) {
		this.fechaImpresion = fechaImpresion;
	}
	public String getUsuarioImpresion() {
		return usuarioImpresion;
	}
	public void setUsuarioImpresion(String usuarioImpresion) {
		this.usuarioImpresion = usuarioImpresion;
	}
	
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	public Long getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	public Long getIdMedio() {
		return idMedio;
	}
	public void setIdMedio(Long idMedio) {
		this.idMedio = idMedio;
	}
	public Long getIdFuente() {
		return idFuente;
	}
	public void setIdFuente(Long idFuente) {
		this.idFuente = idFuente;
	}
	public String getCheque() {
		return cheque;
	}
	public void setCheque(String cheque) {
		this.cheque = cheque;
	}

	public Long getIdPago() {
		return idPago;
	}
	public void setIdPago(Long idPago) {
		this.idPago = idPago;
	}
	

	public Long getIdconcepto() {
		return idconcepto;
	}
	public void setIdconcepto(Long idconcepto) {
		this.idconcepto = idconcepto;
	}

	
	

}