package py.com.sgs.persistence.dto;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PrintDTO {
	private String type;
	private String mimeType;
	private String fileName;
	private byte[] bytes;
	private String hash;

	public PrintDTO(){
		super();
	}

	public PrintDTO(String type, byte[] bytes) {
		super();
		this.type = type;
		this.bytes = bytes;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public byte[] getBytes() {
		return bytes;
	}
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }

  @Override
  public String toString() {
    return String.format("PrintDTO [type=%s, mimeType=%s, fileName=%s, bytes=%s, hash=%s]", type, mimeType, fileName,
        Arrays.toString(bytes), hash);
  }

}
