package py.com.sgs.persistence.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RepoMovimientoProductosDTO {
	private Date fechaDesde;
	private Date fechaHasta;
	private Long idSucursal;
	
	List<RepoMovimientoProductosDetDTO> detalles;
	
	
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public List<RepoMovimientoProductosDetDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<RepoMovimientoProductosDetDTO> detalles) {
		this.detalles = detalles;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
}
