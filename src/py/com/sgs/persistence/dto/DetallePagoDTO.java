package py.com.sgs.persistence.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DetallePagoDTO {
	private Long id;
	private Long idFormaPago;
	private Long idBanco;
	private String numeroTarjeta;
	private String marca;
	private Long codautorizacionNrocheque;
	private Long nroCupon;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdFormaPago() {
		return idFormaPago;
	}
	public void setIdFormaPago(Long idFormaPago) {
		this.idFormaPago = idFormaPago;
	}
	public Long getIdBanco() {
		return idBanco;
	}
	public void setIdBanco(Long idBanco) {
		this.idBanco = idBanco;
	}
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public Long getCodautorizacionNrocheque() {
		return codautorizacionNrocheque;
	}
	public void setCodautorizacionNrocheque(Long codautorizacionNrocheque) {
		this.codautorizacionNrocheque = codautorizacionNrocheque;
	}
	public Long getNroCupon() {
		return nroCupon;
	}
	public void setNroCupon(Long nroCupon) {
		this.nroCupon = nroCupon;
	}
	
}