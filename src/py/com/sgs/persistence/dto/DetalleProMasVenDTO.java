package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

public class DetalleProMasVenDTO {
	
	private Double cantVendida;
	private BigDecimal montoRecaudado;
	private String grupo;
	private Long idProducto;
	private ProductosDTO producto;
	
	public Double getCantVendida() {
		return cantVendida;
	}
	public void setCantVendida(Double cantVendida) {
		this.cantVendida = cantVendida;
	}
	public BigDecimal getMontoRecaudado() {
		return montoRecaudado;
	}
	public void setMontoRecaudado(BigDecimal montoRecaudado) {
		this.montoRecaudado = montoRecaudado;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public ProductosDTO getProducto() {
		return producto;
	}
	public void setProducto(ProductosDTO producto) {
		this.producto = producto;
	}
	
}
