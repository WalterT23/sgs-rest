package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CabeceraCompraDTO {
	private Long id;
	private Long timbrado;
	private String nroFactura;
	private Long tipoFactura;
	private Boolean ocAsociada;
	private Long idOrden;
	private String motivo;
	private Long idProveedor;
	private String proveedor;
	private BigDecimal gravada10;
	private BigDecimal gravada5;
	private BigDecimal exenta;
	private BigDecimal iva10;
	private BigDecimal iva5;
	private BigDecimal total;
	private Long idEstado;
	private BigDecimal saldo;
	private String usuarioCreacion;
	private Date fecha;
	private Date fechaCreacion;
	private Long idSucursal;
	private Long tipoDocumento;
	

	public Long getTimbrado() {
		return timbrado;
	}
	public void setTimbrado(Long timbrado) {
		this.timbrado = timbrado;
	}
	public String getNroFactura() {
		return nroFactura;
	}
	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}
	 public Long getTipoFactura() {
		return tipoFactura;
	}
	public void setTipoFactura(Long tipoFactura) {
		this.tipoFactura = tipoFactura;
	}
	public Long getIdOrden() {
		return idOrden;
	}
	public void setIdOrden(Long idOrden) {
		this.idOrden = idOrden;
	}
	public Long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public Boolean getOcAsociada() {
		return ocAsociada;
	}
	public void setOcAsociada(Boolean ocAsociada) {
		this.ocAsociada = ocAsociada;
	}
	
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	

	
	public BigDecimal getGravada10() {
		return gravada10;
	}
	public void setGravada10(BigDecimal gravada10) {
		this.gravada10 = gravada10;
	}
	public BigDecimal getGravada5() {
		return gravada5;
	}
	public void setGravada5(BigDecimal gravada5) {
		this.gravada5 = gravada5;
	}
	public BigDecimal getExenta() {
		return exenta;
	}
	public void setExenta(BigDecimal exenta) {
		this.exenta = exenta;
	}
	public BigDecimal getIva10() {
		return iva10;
	}
	public void setIva10(BigDecimal iva10) {
		this.iva10 = iva10;
	}
	public BigDecimal getIva5() {
		return iva5;
	}
	public void setIva5(BigDecimal iva5) {
		this.iva5 = iva5;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public Long getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
  
	
	public Long getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(Long tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

}