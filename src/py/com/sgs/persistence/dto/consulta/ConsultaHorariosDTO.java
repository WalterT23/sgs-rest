package py.com.sgs.persistence.dto.consulta;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsultaHorariosDTO {
	
	private Date fechaDesde;
	private Date fechaHasta;
	private List<ResumenHorarioVentasDTO> detalles;
	private Long idSucursal;
	
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public List<ResumenHorarioVentasDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<ResumenHorarioVentasDTO> detalles) {
		this.detalles = detalles;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}

}
