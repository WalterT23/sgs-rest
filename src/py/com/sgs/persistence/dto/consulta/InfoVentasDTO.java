package py.com.sgs.persistence.dto.consulta;

import java.math.BigDecimal;
import java.util.List;

import py.com.sgs.persistence.dto.composite.ProductosCompositeDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InfoVentasDTO {
	
	private BigDecimal montoTotal;
	private BigDecimal iva5;
	private BigDecimal iva10;
	private BigDecimal exenta;
	private BigDecimal gravada5;
	private BigDecimal gravada10;
	private BigDecimal descuentos;
	private List<ProductosCompositeDTO> productos;
	
	public BigDecimal getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	public BigDecimal getIva5() {
		return iva5;
	}
	public void setIva5(BigDecimal iva5) {
		this.iva5 = iva5;
	}
	public BigDecimal getIva10() {
		return iva10;
	}
	public void setIva10(BigDecimal iva10) {
		this.iva10 = iva10;
	}
	public BigDecimal getExenta() {
		return exenta;
	}
	public void setExenta(BigDecimal exenta) {
		this.exenta = exenta;
	}
	public BigDecimal getGravada5() {
		return gravada5;
	}
	public void setGravada5(BigDecimal gravada5) {
		this.gravada5 = gravada5;
	}
	public BigDecimal getGravada10() {
		return gravada10;
	}
	public void setGravada10(BigDecimal gravada10) {
		this.gravada10 = gravada10;
	}
	public BigDecimal getDescuentos() {
		return descuentos;
	}
	public void setDescuentos(BigDecimal descuentos) {
		this.descuentos = descuentos;
	}
	public List<ProductosCompositeDTO> getProductos() {
		return productos;
	}
	public void setProductos(List<ProductosCompositeDTO> productos) {
		this.productos = productos;
	}

}
