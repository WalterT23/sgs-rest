package py.com.sgs.persistence.dto.consulta;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HorarioVentasDTO {
	
	private Double cantidad7a8;
	private BigDecimal monto7a8;
	private Double cantidad8a9;
	private BigDecimal monto8a9;
	private Double cantidad9a10;
	private BigDecimal monto9a10;
	private Double cantidad10a11;
	private BigDecimal monto10a11;
	private Double cantidad11a12;
	private BigDecimal monto11a12;
	private Double cantidad12a13;
	private BigDecimal monto12a13;
	private Double cantidad13a14;
	private BigDecimal monto13a14;
	private Double cantidad14a15;
	private BigDecimal monto14a15;
	private Double cantidad15a16;
	private BigDecimal monto15a16;
	private Double cantidad16a17;
	private BigDecimal monto16a17;
	private Double cantidad17a18;
	private BigDecimal monto17a18;
	private Double cantidad18a19;
	private BigDecimal monto18a19;
	private Double cantidad19a20;
	private BigDecimal monto19a20;
	private Double cantidad20a21;
	private BigDecimal monto20a21;
	private Double cantidad21a22;
	private BigDecimal monto21a22;
	
	public HorarioVentasDTO(){
		this.cantidad7a8 = 0D;
		this.cantidad8a9 = 0D;
		this.cantidad9a10 = 0D;
		this.cantidad10a11 = 0D;
		this.cantidad11a12 = 0D;
		this.cantidad12a13 = 0D;
		this.cantidad13a14 = 0D;
		this.cantidad14a15 = 0D;
		this.cantidad15a16 = 0D;
		this.cantidad16a17 = 0D;
		this.cantidad17a18 = 0D;
		this.cantidad18a19 = 0D;
		this.cantidad19a20 = 0D;
		this.cantidad20a21 = 0D;
		this.cantidad21a22 = 0D;
		
		this.monto7a8 = BigDecimal.ZERO;
		this.monto8a9 = BigDecimal.ZERO;
		this.monto9a10 = BigDecimal.ZERO;
		this.monto10a11 = BigDecimal.ZERO;
		this.monto11a12 = BigDecimal.ZERO;
		this.monto12a13 = BigDecimal.ZERO;
		this.monto13a14 = BigDecimal.ZERO;
		this.monto14a15 = BigDecimal.ZERO;
		this.monto15a16 = BigDecimal.ZERO;
		this.monto16a17 = BigDecimal.ZERO;
		this.monto17a18 = BigDecimal.ZERO;
		this.monto18a19 = BigDecimal.ZERO;
		this.monto19a20 = BigDecimal.ZERO;
		this.monto20a21 = BigDecimal.ZERO;
		this.monto21a22 = BigDecimal.ZERO;
	}
	
	public Double getCantidad7a8() {
		return cantidad7a8;
	}
	public void setCantidad7a8(Double cantidad7a8) {
		this.cantidad7a8 = cantidad7a8;
	}
	public BigDecimal getMonto7a8() {
		return monto7a8;
	}
	public void setMonto7a8(BigDecimal monto7a8) {
		this.monto7a8 = monto7a8;
	}
	public Double getCantidad8a9() {
		return cantidad8a9;
	}
	public void setCantidad8a9(Double cantidad8a9) {
		this.cantidad8a9 = cantidad8a9;
	}
	public BigDecimal getMonto8a9() {
		return monto8a9;
	}
	public void setMonto8a9(BigDecimal monto8a9) {
		this.monto8a9 = monto8a9;
	}
	public Double getCantidad9a10() {
		return cantidad9a10;
	}
	public void setCantidad9a10(Double cantidad9a10) {
		this.cantidad9a10 = cantidad9a10;
	}
	public BigDecimal getMonto9a10() {
		return monto9a10;
	}
	public void setMonto9a10(BigDecimal monto9a10) {
		this.monto9a10 = monto9a10;
	}
	public Double getCantidad10a11() {
		return cantidad10a11;
	}
	public void setCantidad10a11(Double cantidad10a11) {
		this.cantidad10a11 = cantidad10a11;
	}
	public BigDecimal getMonto10a11() {
		return monto10a11;
	}
	public void setMonto10a11(BigDecimal monto10a11) {
		this.monto10a11 = monto10a11;
	}
	public Double getCantidad11a12() {
		return cantidad11a12;
	}
	public void setCantidad11a12(Double cantidad11a12) {
		this.cantidad11a12 = cantidad11a12;
	}
	public BigDecimal getMonto11a12() {
		return monto11a12;
	}
	public void setMonto11a12(BigDecimal monto11a12) {
		this.monto11a12 = monto11a12;
	}
	public Double getCantidad12a13() {
		return cantidad12a13;
	}
	public void setCantidad12a13(Double cantidad12a13) {
		this.cantidad12a13 = cantidad12a13;
	}
	public BigDecimal getMonto12a13() {
		return monto12a13;
	}
	public void setMonto12a13(BigDecimal monto12a13) {
		this.monto12a13 = monto12a13;
	}
	public Double getCantidad13a14() {
		return cantidad13a14;
	}
	public void setCantidad13a14(Double cantidad13a14) {
		this.cantidad13a14 = cantidad13a14;
	}
	public BigDecimal getMonto13a14() {
		return monto13a14;
	}
	public void setMonto13a14(BigDecimal monto13a14) {
		this.monto13a14 = monto13a14;
	}
	public Double getCantidad14a15() {
		return cantidad14a15;
	}
	public void setCantidad14a15(Double cantidad14a15) {
		this.cantidad14a15 = cantidad14a15;
	}
	public BigDecimal getMonto14a15() {
		return monto14a15;
	}
	public void setMonto14a15(BigDecimal monto14a15) {
		this.monto14a15 = monto14a15;
	}
	public Double getCantidad15a16() {
		return cantidad15a16;
	}
	public void setCantidad15a16(Double cantidad15a16) {
		this.cantidad15a16 = cantidad15a16;
	}
	public BigDecimal getMonto15a16() {
		return monto15a16;
	}
	public void setMonto15a16(BigDecimal monto15a16) {
		this.monto15a16 = monto15a16;
	}
	public Double getCantidad16a17() {
		return cantidad16a17;
	}
	public void setCantidad16a17(Double cantidad16a17) {
		this.cantidad16a17 = cantidad16a17;
	}
	public BigDecimal getMonto16a17() {
		return monto16a17;
	}
	public void setMonto16a17(BigDecimal monto16a17) {
		this.monto16a17 = monto16a17;
	}
	public Double getCantidad17a18() {
		return cantidad17a18;
	}
	public void setCantidad17a18(Double cantidad17a18) {
		this.cantidad17a18 = cantidad17a18;
	}
	public BigDecimal getMonto17a18() {
		return monto17a18;
	}
	public void setMonto17a18(BigDecimal monto17a18) {
		this.monto17a18 = monto17a18;
	}
	public Double getCantidad18a19() {
		return cantidad18a19;
	}
	public void setCantidad18a19(Double cantidad18a19) {
		this.cantidad18a19 = cantidad18a19;
	}
	public BigDecimal getMonto18a19() {
		return monto18a19;
	}
	public void setMonto18a19(BigDecimal monto18a19) {
		this.monto18a19 = monto18a19;
	}
	public Double getCantidad19a20() {
		return cantidad19a20;
	}
	public void setCantidad19a20(Double cantidad19a20) {
		this.cantidad19a20 = cantidad19a20;
	}
	public BigDecimal getMonto19a20() {
		return monto19a20;
	}
	public void setMonto19a20(BigDecimal monto19a20) {
		this.monto19a20 = monto19a20;
	}
	public Double getCantidad20a21() {
		return cantidad20a21;
	}
	public void setCantidad20a21(Double cantidad20a21) {
		this.cantidad20a21 = cantidad20a21;
	}
	public BigDecimal getMonto20a21() {
		return monto20a21;
	}
	public void setMonto20a21(BigDecimal monto20a21) {
		this.monto20a21 = monto20a21;
	}
	public Double getCantidad21a22() {
		return cantidad21a22;
	}
	public void setCantidad21a22(Double cantidad21a22) {
		this.cantidad21a22 = cantidad21a22;
	}
	public BigDecimal getMonto21a22() {
		return monto21a22;
	}
	public void setMonto21a22(BigDecimal monto21a22) {
		this.monto21a22 = monto21a22;
	}

}
