package py.com.sgs.persistence.dto.consulta;

import java.util.Date;
import java.util.List;

import py.com.sgs.persistence.dto.composite.ProductosCompositeDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsultaVentaDTO {
	
	private Long idCliente;
	private List<ProductosCompositeDTO> productos;
	private Date fechaDesde;
	private Date fechaHasta;
	private Long idGrupo;
	
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public List<ProductosCompositeDTO> getProductos() {
		return productos;
	}
	public void setProductos(List<ProductosCompositeDTO> productos) {
		this.productos = productos;
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public Long getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(Long idGrupo) {
		this.idGrupo = idGrupo;
	}
	
}
