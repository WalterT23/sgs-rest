package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ArqueoCajaDTO {
	private Long id;
	private Long idAperturaCierre;
	private BigDecimal saldoAlCierre;
	private BigDecimal totalEntregado;
	private String diferencias;
	private BigDecimal faltanteSobrante;


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdAperturaCierre() {
		return idAperturaCierre;
	}
	public void setIdAperturaCierre(Long idAperturaCierre) {
		this.idAperturaCierre = idAperturaCierre;
	}
	public BigDecimal getSaldoAlCierre() {
		return saldoAlCierre;
	}
	public void setSaldoAlCierre(BigDecimal saldoAlCierre) {
		this.saldoAlCierre = saldoAlCierre;
	}
	public BigDecimal getTotalEntregado() {
		return totalEntregado;
	}
	public void setTotalEntregado(BigDecimal totalEntregado) {
		this.totalEntregado = totalEntregado;
	}
	public BigDecimal getFaltanteSobrante() {
		return faltanteSobrante;
	}
	public void setFaltanteSobrante(BigDecimal faltanteSobrante) {
		this.faltanteSobrante = faltanteSobrante;
	}
	public String getDiferencias() {
		return diferencias;
	}
	public void setDiferencias(String diferencias) {
		this.diferencias = diferencias;
	}


}