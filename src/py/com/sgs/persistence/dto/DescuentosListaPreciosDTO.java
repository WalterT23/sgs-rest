package py.com.sgs.persistence.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DescuentosListaPreciosDTO {
	private Long id;
	private Long idDescuento;
	private Long idListaPrecio;
	private Date inicioVigencia;
	private Date finVigencia;
	private Boolean vigente;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdDescuento() {
		return idDescuento;
	}
	public void setIdDescuento(Long idDescuento) {
		this.idDescuento = idDescuento;
	}
	public Long getIdListaPrecio() {
		return idListaPrecio;
	}
	public void setIdListaPrecio(Long idListaPrecio) {
		this.idListaPrecio = idListaPrecio;
	}
	public Date getInicioVigencia() {
		return inicioVigencia;
	}
	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}
	public Date getFinVigencia() {
		return finVigencia;
	}
	public void setFinVigencia(Date finVigencia) {
		this.finVigencia = finVigencia;
	}
	public Boolean getVigente() {
		return vigente;
	}
	public void setVigente(Boolean vigente) {
		this.vigente = vigente;
	}

}