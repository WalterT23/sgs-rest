package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AperturaCierreDTO {
	private Long id;
	private Long idPuntoExpedicion;
	private Long idUsuarioCaja;
	private Date fechaApertura;
	private BigDecimal saldoInicial;
	private BigDecimal saldoCierre;
	private Date fechaCierre;
	private String usuarioCreacion;
	private String usuarioCierre;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdPuntoExpedicion() {
		return idPuntoExpedicion;
	}
	public void setIdPuntoExpedicion(Long idPuntoExpedicion) {
		this.idPuntoExpedicion = idPuntoExpedicion;
	}
	
	public Long getIdUsuarioCaja() {
		return idUsuarioCaja;
	}
	public void setIdUsuarioCaja(Long idUsuarioCaja) {
		this.idUsuarioCaja = idUsuarioCaja;
	}
	public Date getFechaApertura() {
		return fechaApertura;
	}
	public void setFechaApertura(Date fechaApertura) {
		this.fechaApertura = fechaApertura;
	}
	public BigDecimal getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(BigDecimal saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public BigDecimal getSaldoCierre() {
		return saldoCierre;
	}
	public void setSaldoCierre(BigDecimal saldoCierre) {
		this.saldoCierre = saldoCierre;
	}
	public Date getFechaCierre() {
		return fechaCierre;
	}
	public void setFechaCierre(Date fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public String getUsuarioCierre() {
		return usuarioCierre;
	}
	public void setUsuarioCierre(String usuarioCierre) {
		this.usuarioCierre = usuarioCierre;
	}

}