package py.com.sgs.persistence.dto;

import java.math.BigDecimal;

public class Det4GraphicProMasVenDTO {
	
	private String nombre;
	private Double cantidad;
	private BigDecimal monto;
	private Double totCant;
	private BigDecimal totMonto;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Double getCantidad() {
		return cantidad;
	}
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public Double getTotCant() {
		return totCant;
	}
	public void setTotCant(Double totCant) {
		this.totCant = totCant;
	}
	public BigDecimal getTotMonto() {
		return totMonto;
	}
	public void setTotMonto(BigDecimal totMonto) {
		this.totMonto = totMonto;
	}
	
	
}
