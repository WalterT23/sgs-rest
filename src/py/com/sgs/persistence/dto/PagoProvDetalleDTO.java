package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PagoProvDetalleDTO {
	private Long id;
	private Long idPago;
	private Long idCabecera;
	private BigDecimal monto;
	private Long idMedio;
	private String medio;
	private String abreviaturaMedio;
	private Long idFuente;
	private String nroCheque;
	private String nroFactura;
	private Date fechaEmision;
	private Date fechaPago;
	private Long idCuota;
	private Integer cuota;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdPago() {
		return idPago;
	}
	public void setIdPago(Long idPago) {
		this.idPago = idPago;
	}
	public Long getIdCabecera() {
		return idCabecera;
	}
	public void setIdCabecera(Long idCabecera) {
		this.idCabecera = idCabecera;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public Long getIdMedio() {
		return idMedio;
	}
	public void setIdMedio(Long idMedio) {
		this.idMedio = idMedio;
	}
	public Long getIdFuente() {
		return idFuente;
	}
	public void setIdFuente(Long idFuente) {
		this.idFuente = idFuente;
	}
	public String getNroCheque() {
		return nroCheque;
	}
	public void setNroCheque(String nroCheque) {
		this.nroCheque = nroCheque;
	}
	public Date getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public Date getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	public Long getIdCuota() {
		return idCuota;
	}
	public void setIdCuota(Long idCuota) {
		this.idCuota = idCuota;
	}
	public String getAbreviaturaMedio() {
		return abreviaturaMedio;
	}
	public void setAbreviaturaMedio(String abreviaturaMedio) {
		this.abreviaturaMedio = abreviaturaMedio;
	}
	public String getNroFactura() {
		return nroFactura;
	}
	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}
	public String getMedio() {
		return medio;
	}
	public void setMedio(String medio) {
		this.medio = medio;
	}
	public Integer getCuota() {
		return cuota;
	}
	public void setCuota(Integer cuota) {
		this.cuota = cuota;
	}

	
	
}
	
