package py.com.sgs.persistence.dto;

import java.util.List;

import py.com.sgs.persistence.dto.composite.StockCompositeDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ReporteStockDTO {
	
	private Boolean stockMin;
	private Boolean stock20;
	private Boolean stock40;
	private Long idSucursal;
	private List<StockCompositeDTO>  detalles;
	
	
	public Boolean getStockMin() {
		return stockMin;
	}
	public void setStockMin(Boolean stockMin) {
		this.stockMin = stockMin;
	}
	public Boolean getStock40() {
		return stock40;
	}
	public void setStock40(Boolean stock40) {
		this.stock40 = stock40;
	}
	public Boolean getStock20() {
		return stock20;
	}
	public void setStock20(Boolean stock20) {
		this.stock20 = stock20;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	public List<StockCompositeDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<StockCompositeDTO> detalles) {
		this.detalles = detalles;
	}
	
	
	

}
