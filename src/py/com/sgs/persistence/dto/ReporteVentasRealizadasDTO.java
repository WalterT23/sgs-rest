package py.com.sgs.persistence.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ReporteVentasRealizadasDTO{
	private Date fechaDesde;
	private Date fechaHasta;
	private BigDecimal totalVendido;
	private Long idSucursal;
	private List<CabeceraVentaDTO> detalles;
	private List<DetalleForGraphicsDTO> totalXfecha;
	
	public Date getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public BigDecimal getTotalVendido() {
		return totalVendido;
	}
	public void setTotalVendido(BigDecimal totalVendido) {
		this.totalVendido = totalVendido;
	}
	public Long getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}
	public List<CabeceraVentaDTO> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<CabeceraVentaDTO> detalles) {
		this.detalles = detalles;
	}
	public List<DetalleForGraphicsDTO> getTotalXfecha() {
		return totalXfecha;
	}
	public void setTotalXfecha(List<DetalleForGraphicsDTO> totalXfecha) {
		this.totalXfecha = totalXfecha;
	}
	
}
