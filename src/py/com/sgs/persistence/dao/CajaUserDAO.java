package py.com.sgs.persistence.dao;

import java.util.List;

import py.com.sgs.persistence.dto.CajaUserDTO;
import py.com.sgs.persistence.dto.UsuariosDTO;
import py.com.sgs.persistence.dto.composite.CajaUserCompositeDTO;

public interface CajaUserDAO {
  List<UsuariosDTO> listCajeros(Long idSucursal);
  List<CajaUserCompositeDTO> listCajaUser(Long idSucursal);
  CajaUserCompositeDTO getCajaUserById(Long id);
  CajaUserCompositeDTO getCajaUserByIdPtoExp(Long idPuntoExpedicion);
  CajaUserDTO getAsociacionVigenteByCajero(Long idUsuario);
  CajaUserDTO getAsociacionVigenteByPtoExp(Long idPuntoExpedicion);
  int createCajaUser(CajaUserDTO dto);
  int deleteCajaUser(CajaUserDTO dto);
  int updateCajaUser(CajaUserDTO dto);
}