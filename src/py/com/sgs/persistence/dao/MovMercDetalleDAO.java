package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.MovMercDetalleDTO;

import java.util.List;

public interface MovMercDetalleDAO {
  List<MovMercDetalleDTO> listMovMercDetalle();
  MovMercDetalleDTO getMovMercDetalleById(Long id);
  List<MovMercDetalleDTO> getMovMercDetByIdMov(Long idMov);
  int createMovMercDetalle(MovMercDetalleDTO dto);
  int deleteMovMercDetalle(MovMercDetalleDTO dto);
  int updateMovMercDetalle(MovMercDetalleDTO dto);
}