package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.TimbradoPtoExpedicionDTO;

import java.util.List;

public interface TimbradoPtoExpedicionDAO {

	List<TimbradoPtoExpedicionDTO> listTimbradoPtoExpedicion();
	TimbradoPtoExpedicionDTO getTimbradoPtoExpedicionById(Long id);
	TimbradoPtoExpedicionDTO getTimbradoPtoExpedicionByIdPtoExpedicionFact(Long idPtoExpedicion);
	TimbradoPtoExpedicionDTO getTimbradoPtoExpedicionByIdPtoExpedicionNota(Long idPtoExpedicion);
	List<TimbradoPtoExpedicionDTO>  getTimbradoPtoExpedicionByDTO(TimbradoPtoExpedicionDTO dto);
	int createTimbradoPtoExpedicion(TimbradoPtoExpedicionDTO dto);
	int deleteTimbradoPtoExpedicion(TimbradoPtoExpedicionDTO dto);
	int updateTimbradoPtoExpedicion(TimbradoPtoExpedicionDTO dto);
	int actualizarUltNro(TimbradoPtoExpedicionDTO dto);

}