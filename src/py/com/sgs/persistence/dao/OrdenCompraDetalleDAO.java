package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.OrdenCompraDetalleDTO;

import java.util.List;

public interface OrdenCompraDetalleDAO {
  List<OrdenCompraDetalleDTO> listOrdenCompraDetalle();
  List<OrdenCompraDetalleDTO> listDetalleByIdOC(Long idOC);
  OrdenCompraDetalleDTO getOrdenCompraDetalleById(Long id);
  OrdenCompraDetalleDTO getOrdenCompraDetalleByOC(Long id);
  int createOrdenCompraDetalle(OrdenCompraDetalleDTO dto);
  int deleteOrdenCompraDetalle(OrdenCompraDetalleDTO dto);
  int updateOrdenCompraDetalle(OrdenCompraDetalleDTO dto);
}