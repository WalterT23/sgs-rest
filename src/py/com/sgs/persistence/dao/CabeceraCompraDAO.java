package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.CabeceraCompraDTO;
import py.com.sgs.persistence.dto.ConsultaPagosDTO;
import py.com.sgs.persistence.dto.DetalleForGraphicsDTO;
import py.com.sgs.persistence.dto.FiltroComprasDTO;
import py.com.sgs.persistence.dto.FiltroCuotasDTO;
import py.com.sgs.persistence.dto.composite.CabeceraCompraCompositeDTO;

import java.util.List;

public interface CabeceraCompraDAO {
  List<CabeceraCompraCompositeDTO> listCabeceraCompra(Long idSucursal);
  List<CabeceraCompraCompositeDTO> getCabeceraCompraByIdProveedor(CabeceraCompraDTO cc);
  CabeceraCompraCompositeDTO getCabeceraCompraById(Long id);
  List<CabeceraCompraDTO> getReporCompras(FiltroComprasDTO dto);
  List<DetalleForGraphicsDTO> getComprasGraphic(FiltroComprasDTO dto);
  
  int createCabeceraCompra(CabeceraCompraCompositeDTO dto);
  int deleteCabeceraCompra(CabeceraCompraDTO dto);
  int updateCabeceraCompra(CabeceraCompraCompositeDTO dto);
  int updateCabeceraCompraSingle(CabeceraCompraDTO dto);
}