package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.CuotaDetalleDTO;

import java.util.List;

public interface CuotaDetalleDAO {
  List<CuotaDetalleDTO> listCuotaDetalle();
  List<CuotaDetalleDTO> cuotasPenXcabCompra(Long idCabCompra);
  CuotaDetalleDTO getCuotaDetalleById(Long id);
  int createCuotaDetalle(CuotaDetalleDTO dto);
  int deleteCuotaDetalle(CuotaDetalleDTO dto);
  int updateCuotaDetalle(CuotaDetalleDTO dto);
}