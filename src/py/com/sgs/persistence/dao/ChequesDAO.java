package py.com.sgs.persistence.dao;

import java.util.List;

import py.com.sgs.persistence.dto.ChequesDTO;

public interface ChequesDAO {
  List<ChequesDTO> listCheques();
  List<ChequesDTO> listChequesActivos();
  List<ChequesDTO> listChequesByIdBanco(Integer idBanco);
  List<ChequesDTO> listChequesByIdCuenta(Integer idCuentaBanco);
  
  ChequesDTO getChequesById(Long id);
  int createCheques(ChequesDTO dto);
  int deleteCheques(ChequesDTO dto);
  int updateCheques(ChequesDTO dto);
}