package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.CabeceraVentaDTO;
import py.com.sgs.persistence.dto.EmpresaDTO;
import py.com.sgs.persistence.dto.composite.CabeceraVentaCompositeDTO;
import py.com.sgs.persistence.dto.consulta.ConsultaHorariosDTO;

import java.math.BigDecimal;
import java.util.List;

public interface CabeceraVentaDAO {

	List<CabeceraVentaDTO> listCabeceraVenta(Long idSucursal);
	List<CabeceraVentaDTO> listByParams(ConsultaHorariosDTO params);
	CabeceraVentaCompositeDTO getCabeceraVentaById(Long id);
	CabeceraVentaCompositeDTO getCabeceraVentaByFactura(String nroFactura);
	int createCabeceraVenta(CabeceraVentaDTO dto);
	int deleteCabeceraVenta(CabeceraVentaDTO dto);
	int updateCabeceraVenta(CabeceraVentaDTO dto);

	BigDecimal getMontoByIdAperturaCierre(Long idAperturaCierre);
	List<CabeceraVentaCompositeDTO> listCabeceraVentaByApertura(Long idAperturaCierre);

	EmpresaDTO getEmpresaById(Long id);
	
}