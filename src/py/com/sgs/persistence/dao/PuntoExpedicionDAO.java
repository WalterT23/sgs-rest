package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.PuntoExpedicionDTO;

import java.util.List;

public interface PuntoExpedicionDAO {
  List<PuntoExpedicionDTO> listPuntoExpedicion(Long idSucursal);
  PuntoExpedicionDTO getPuntoExpedicionById(Long id);
  int createPuntoExpedicion(PuntoExpedicionDTO dto);
  int updatePuntoExpedicion(PuntoExpedicionDTO dto);
  void updateEstado(PuntoExpedicionDTO dto);
}