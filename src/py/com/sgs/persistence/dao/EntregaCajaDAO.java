package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.EntregaCajaDTO;
import py.com.sgs.persistence.dto.composite.EntregaCajaCompositeDTO;

import java.util.List;

public interface EntregaCajaDAO {

	List<EntregaCajaDTO> listEntregaCaja();
	EntregaCajaCompositeDTO getEntregaCajaById(Long id);
	int createEntregaCaja(EntregaCajaDTO dto);
	int deleteEntregaCaja(EntregaCajaDTO dto);
	int updateEntregaCaja(EntregaCajaDTO dto);

}