package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.ProveedoresContactoDTO;

import java.util.List;

public interface ProveedoresContactoDAO {
  List<ProveedoresContactoDTO> listContactosByProveedor(Long idProveedor); //recupera todos los contactos de un proveedor
  ProveedoresContactoDTO getProveedoresContactoById(Long id); //recupera un contacto por id_contacto
  int createProveedoresContacto(ProveedoresContactoDTO dto);
  int deleteProveedoresContacto(ProveedoresContactoDTO dto);
  int updateProveedoresContacto(ProveedoresContactoDTO dto);
}