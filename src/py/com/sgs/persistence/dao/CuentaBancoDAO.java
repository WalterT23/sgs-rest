package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.CuentaBancoDTO;

import java.util.List;

public interface CuentaBancoDAO {
  List<CuentaBancoDTO> listCuentaBanco();
  List<CuentaBancoDTO> listCuentaBancoByIdBanco(Long idBanco);
  CuentaBancoDTO getCuentaBancoById(Long id);
  int createCuentaBanco(CuentaBancoDTO dto);
  int deleteCuentaBanco(CuentaBancoDTO dto);
  int updateCuentaBanco(CuentaBancoDTO dto);
}