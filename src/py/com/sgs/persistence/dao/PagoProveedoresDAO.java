package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.PagoProveedoresDTO;
import py.com.sgs.persistence.dto.composite.PagoFacturasCompositeDTO;

import java.util.List;

public interface PagoProveedoresDAO {
  List<PagoProveedoresDTO> listPagoProveedores(Long idSucursal);
  PagoProveedoresDTO getPagoProveedoresById(Long id);
  int createPagoFacturas(PagoFacturasCompositeDTO dto);
  int createPagoProveedores(PagoProveedoresDTO dto);
  int deletePagoProveedores(PagoProveedoresDTO dto);
  int updatePagoProveedores(PagoProveedoresDTO dto);
}