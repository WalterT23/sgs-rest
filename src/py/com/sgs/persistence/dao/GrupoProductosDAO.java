package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.GrupoProductosDTO;

import java.util.List;

public interface GrupoProductosDAO {
  List<GrupoProductosDTO> listGrupoProductos();
  List<GrupoProductosDTO> subgrupos(Long idGrupoPadre);
  GrupoProductosDTO getGrupoProductosById(Long id);
  int createGrupoProductos(GrupoProductosDTO dto);
  int deleteGrupoProductos(GrupoProductosDTO dto);
  int updateGrupoProductos(GrupoProductosDTO dto);
}