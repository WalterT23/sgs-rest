package py.com.sgs.persistence.dao;

import java.util.List;

import py.com.sgs.persistence.dto.RolesDTO;
import py.com.sgs.persistence.dto.UsuarioRolDTO;
import py.com.sgs.persistence.dto.composite.RolesCompositeDTO;

public interface RolesDAO {
  List<RolesCompositeDTO> listRoles();
  RolesDTO getRolesById(Long id);
  int createRoles(RolesDTO dto);
  int deleteRoles(RolesDTO dto);
  int updateRoles(RolesDTO dto);
  List<RolesCompositeDTO> getByUsuario(Long idUsuario);
  List<RolesCompositeDTO> getDispByUsuario(Long idUsuario);
  int createUsuarioRol(UsuarioRolDTO dto);
  int deleteUsuarioRol(UsuarioRolDTO dto);
}