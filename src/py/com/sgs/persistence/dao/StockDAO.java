package py.com.sgs.persistence.dao;

import java.util.List;

import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.composite.StockCompositeDTO;

public interface StockDAO {

	List<StockCompositeDTO> listStock(Long idSucursal);
	StockCompositeDTO getStockById(Long id);
	StockCompositeDTO stockByIdProducto(StockDTO dto);
	List<StockDTO>getStockByIdProducto(Long idProducto); //devuelve todos los registros de stock que encuentra para un producto (max un reg por producto)
	StockDTO getStockByCodProducto(StockDTO dto);
	int insertStock (StockDTO dto);
	int deleteStock(StockDTO dto);
	int updateStock(StockDTO dto);
	int updateCodStock (StockDTO dto);
	int updateStockOc (StockDTO dto);
	int updateStockVenta (StockDTO dto);

}