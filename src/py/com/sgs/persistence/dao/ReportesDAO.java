package py.com.sgs.persistence.dao;

import java.util.List;

import py.com.sgs.persistence.dto.CabeceraVentaDTO;
import py.com.sgs.persistence.dto.DetalleForGraphicsDTO;
import py.com.sgs.persistence.dto.EstadoCuentaProveedorDTO;
import py.com.sgs.persistence.dto.Det4GraphicProMasVenDTO;
import py.com.sgs.persistence.dto.DetalleProMasVenDTO;
import py.com.sgs.persistence.dto.RepoMovimientoProductosDTO;
import py.com.sgs.persistence.dto.RepoMovimientoProductosDetDTO;
import py.com.sgs.persistence.dto.ReporteProductosMasVendidosDTO;
import py.com.sgs.persistence.dto.ReporteStockDTO;
import py.com.sgs.persistence.dto.ReporteVentasRealizadasDTO;
import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.composite.StockCompositeDTO;


public interface ReportesDAO {
	
	List<CabeceraVentaDTO> getRepoVentas(ReporteVentasRealizadasDTO dto);
	List<DetalleForGraphicsDTO> getVentasGraphic(ReporteVentasRealizadasDTO dto);
	List<DetalleProMasVenDTO> getRepoProMasVendidosFecha(ReporteProductosMasVendidosDTO dto);
	List<DetalleProMasVenDTO> getRepoProMasVendidosGrupo(ReporteProductosMasVendidosDTO dto);
	List<Det4GraphicProMasVenDTO> getProMasVendidosGraphic(ReporteProductosMasVendidosDTO dto);
	List<StockCompositeDTO> getListaProductos(ReporteStockDTO dto);
	List<RepoMovimientoProductosDetDTO> getLstMovProductos(RepoMovimientoProductosDTO dto);
	
}