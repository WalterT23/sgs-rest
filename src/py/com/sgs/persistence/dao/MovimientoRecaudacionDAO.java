package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.MovimientoRecaudacionDTO;

import java.util.List;

public interface MovimientoRecaudacionDAO {
  List<MovimientoRecaudacionDTO> listMovimientoRecaudacion();
  MovimientoRecaudacionDTO getMovimientoRecaudacionById(Long id);
  MovimientoRecaudacionDTO getMovimientoRecaudacionByIdDeposito(Long idDepDinero);
  int createMovimientoRecaudacion(MovimientoRecaudacionDTO dto);
  int deleteMovimientoRecaudacion(MovimientoRecaudacionDTO dto);
  int updateMovimientoRecaudacion(MovimientoRecaudacionDTO dto);
}