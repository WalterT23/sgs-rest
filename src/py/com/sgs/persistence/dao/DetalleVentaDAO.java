package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.DetalleVentaDTO;

import java.util.List;

public interface DetalleVentaDAO {
  List<DetalleVentaDTO> listDetalleVenta();
  DetalleVentaDTO getDetalleVentaById(Long id);
  int createDetalleVenta(DetalleVentaDTO dto);
  int deleteDetalleVenta(DetalleVentaDTO dto);
  int updateDetalleVenta(DetalleVentaDTO dto);
}