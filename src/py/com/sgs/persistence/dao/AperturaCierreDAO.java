package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.AperturaCierreDTO;
import py.com.sgs.persistence.dto.composite.AperturaCierreCompositeDTO;

import java.util.List;

public interface AperturaCierreDAO {
	List<AperturaCierreDTO> listAperturaCierre();
	AperturaCierreCompositeDTO getAperturaCierreById(Long id);
	AperturaCierreDTO getMaxCierreByCajero(String cajero);  
	AperturaCierreCompositeDTO getAperturaCierreVigenteByIdPuntoExp(Long idPuntoExpedicion);
	AperturaCierreDTO getAperturaCierreVigenteByCajero(Long idUsuarioCaja);
	int createAperturaCierre(AperturaCierreDTO dto);
	int deleteAperturaCierre(AperturaCierreDTO dto);
	int updateAperturaCierre(AperturaCierreDTO dto);
	int cerrarAperturaCierre(AperturaCierreDTO dto);
}