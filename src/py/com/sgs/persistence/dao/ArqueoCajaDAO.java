package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.ArqueoCajaDTO;

import java.util.List;

public interface ArqueoCajaDAO {
  List<ArqueoCajaDTO> listArqueoCaja();
  ArqueoCajaDTO getArqueoCajaById(Long id);
  int createArqueoCaja(ArqueoCajaDTO dto);
  int deleteArqueoCaja(ArqueoCajaDTO dto);
  int updateArqueoCaja(ArqueoCajaDTO dto);
}