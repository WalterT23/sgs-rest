package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.ListaPreciosDTO;
import py.com.sgs.persistence.dto.composite.ListaPreciosCompositeDTO;

import java.util.List;

import org.apache.ibatis.annotations.Param;



public interface ListaPreciosDAO {
  List<ListaPreciosCompositeDTO> listListaPrecios(Long idSucursal);
  List<ListaPreciosCompositeDTO> listListaPreciosAllSucs();
  List<ListaPreciosCompositeDTO> listaPreciosVigentes(Long idSucursal);
  List<ListaPreciosCompositeDTO> listaPreciosBaseVigentes(Long idSucursal);
  ListaPreciosCompositeDTO getListaPreciosById(Long id);
  ListaPreciosDTO listExist (@Param("idSucursal") Long idSucursal, @Param("idTipoLista") Long idTipoLista);
  ListaPreciosDTO validarNewLPbase (ListaPreciosDTO dto);
  int createListaPrecios(ListaPreciosDTO dto);
  int deleteListaPrecios(ListaPreciosDTO dto);
  int updateListaPrecios(ListaPreciosDTO dto);
}