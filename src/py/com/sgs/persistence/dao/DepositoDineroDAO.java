package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.DepositoDineroDTO;

import java.util.List;

public interface DepositoDineroDAO {
  List<DepositoDineroDTO> listDepositoDinero();
  DepositoDineroDTO getDepositoDineroById(Long id);
  int createDepositoDinero(DepositoDineroDTO dto);
  int deleteDepositoDinero(DepositoDineroDTO dto);
  int updateDepositoDinero(DepositoDineroDTO dto);
}