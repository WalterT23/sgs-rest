package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.CabeceraOrdenPagoDTO;
import py.com.sgs.persistence.dto.composite.CabeceraOrdenPagoCompositeDTO;

import java.util.List;

public interface CabeceraOrdenPagoDAO {
  List<CabeceraOrdenPagoDTO> listCabeceraOrdenPago();
  CabeceraOrdenPagoCompositeDTO getCabeceraOrdenPagoById(Long id);
  Long  getNroOrdenPago(Long IdSucursal);
  int createCabeceraOrdenPago(CabeceraOrdenPagoDTO dto);
  int deleteCabeceraOrdenPago(CabeceraOrdenPagoDTO dto);
  int updateCabeceraOrdenPago(CabeceraOrdenPagoDTO dto);
}