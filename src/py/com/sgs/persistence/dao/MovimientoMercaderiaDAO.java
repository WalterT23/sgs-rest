package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.MovimientoMercaderiaDTO;
import py.com.sgs.persistence.dto.composite.MovimientoMercaderiaCompositeDTO;

import java.util.List;

public interface MovimientoMercaderiaDAO {
  List<MovimientoMercaderiaDTO> listMovimientoMercaderia(Long id);
  MovimientoMercaderiaDTO getMovimientoMercaderiaById(Long id);
  int createMovimientoMercaderia(MovimientoMercaderiaCompositeDTO dto);
  int deleteMovimientoMercaderia(MovimientoMercaderiaDTO dto);
  int updateMovimientoMercaderia(MovimientoMercaderiaDTO dto);
}