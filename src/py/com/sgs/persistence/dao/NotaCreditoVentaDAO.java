package py.com.sgs.persistence.dao;

import java.math.BigDecimal;
import java.util.List;

import py.com.sgs.persistence.dto.NotaCreditoVentaDTO;
import py.com.sgs.persistence.dto.composite.NotaCreditoVentaCompositeDTO;
import py.com.sgs.persistence.dto.consulta.ConsultaHorariosDTO;

public interface NotaCreditoVentaDAO {
	
  List<NotaCreditoVentaCompositeDTO> listNotaCreditoVenta(Long idSucursal);
  List<NotaCreditoVentaCompositeDTO> listByParams(ConsultaHorariosDTO params);
  NotaCreditoVentaCompositeDTO getNotaCreditoVentaById(Long id);
  BigDecimal getNcByIdAperturaCierre (Long idAperturaCierre);
  int createNotaCreditoVenta(NotaCreditoVentaCompositeDTO dto);
  int deleteNotaCreditoVenta(NotaCreditoVentaDTO dto);
  int updateNotaCreditoVenta(NotaCreditoVentaDTO dto);
  
}