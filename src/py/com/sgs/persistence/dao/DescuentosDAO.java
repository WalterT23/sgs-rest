package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.DescuentosDTO;
import py.com.sgs.persistence.dto.composite.DescuentosCompositeDTO;

import java.util.List;

public interface DescuentosDAO {
  List<DescuentosCompositeDTO> listDescuentos();
  DescuentosCompositeDTO getDescuentosById(Long id);
  int createDescuentos(DescuentosDTO dto);
  int deleteDescuentos(DescuentosDTO dto);
  int updateDescuentos(DescuentosDTO dto);
}