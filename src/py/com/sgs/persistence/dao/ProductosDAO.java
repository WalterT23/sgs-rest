package py.com.sgs.persistence.dao;

import java.util.List;

import py.com.sgs.persistence.dto.ProductosDTO;
import py.com.sgs.persistence.dto.composite.ProductosCompositeDTO;
import py.com.sgs.persistence.dto.composite.ProductosGroupCompositeDTO;

public interface ProductosDAO {

	List<ProductosDTO> listProductos();
	ProductosDTO getProductosById(Long id);
	ProductosCompositeDTO getProductosByIdNew(ProductosCompositeDTO dto);
	ProductosCompositeDTO getProductosByCod(ProductosCompositeDTO dto);
	List<ProductosDTO> getProductosLikeCod(String cod);
	List<ProductosDTO> listProductosByGroups(ProductosGroupCompositeDTO dto);
	int createProductos(ProductosDTO dto);
	int deleteProductos(ProductosDTO dto);
	int updateProductos(ProductosDTO dto);
	Long codProGen();

}