package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.DetalleOrdenPagoDTO;

import java.util.List;

public interface DetalleOrdenPagoDAO {
  List<DetalleOrdenPagoDTO> listDetalleOrdenPago();
  DetalleOrdenPagoDTO getDetalleOrdenPagoById(Long id);
  int createDetalleOrdenPago(DetalleOrdenPagoDTO dto);
  int deleteDetalleOrdenPago(DetalleOrdenPagoDTO dto);
  int updateDetalleOrdenPago(DetalleOrdenPagoDTO dto);
}