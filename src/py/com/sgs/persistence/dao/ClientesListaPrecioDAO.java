package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.ClientesListaPrecioDTO;
import java.util.List;

public interface ClientesListaPrecioDAO {
  List<ClientesListaPrecioDTO> listClientesListaPrecio(Long idSucursal);
  ClientesListaPrecioDTO getClientesListaPrecioById(Long id);
  List<ClientesListaPrecioDTO> getClientesLPbyCliente(ClientesListaPrecioDTO dto);
  List<ClientesListaPrecioDTO> getClientesLPbyClienteVigentes(ClientesListaPrecioDTO dto);
  ClientesListaPrecioDTO getClientesListaPrecioByDatos(ClientesListaPrecioDTO dto);
  List<ClientesListaPrecioDTO> validarNewAso(ClientesListaPrecioDTO dto);
  int createClientesListaPrecio(ClientesListaPrecioDTO dto);
  int deleteClientesListaPrecio(ClientesListaPrecioDTO dto);
  int updateClientesListaPrecio(ClientesListaPrecioDTO dto);
}