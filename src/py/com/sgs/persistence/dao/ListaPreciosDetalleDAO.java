package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.ListaPreciosDetalleDTO;
import py.com.sgs.persistence.dto.composite.LPDetallesCompositeDTO;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface ListaPreciosDetalleDAO {
  List<LPDetallesCompositeDTO> listListaPreciosDetalle();
  LPDetallesCompositeDTO getListaPreciosDetalleById(Long id); 
  LPDetallesCompositeDTO precioByLPcodPro(@Param("idLP") Long idLP, @Param("codPro") String codPro);
  List<LPDetallesCompositeDTO>  getListaPreciosDetalleByIdLP(Long idLP);
  int createListaPreciosDetalle(ListaPreciosDetalleDTO dto);
  int deleteListaPreciosDetalle(ListaPreciosDetalleDTO dto);
  int updateListaPreciosDetalle(ListaPreciosDetalleDTO dto);
}