package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.OrdenDeCompraDTO;
import py.com.sgs.persistence.dto.composite.OrdenDeCompraCompositeDTO;

import java.util.List;

public interface OrdenDeCompraDAO {

	List<OrdenDeCompraCompositeDTO> listOrdenDeCompra(Long idSucursal);
	List<OrdenDeCompraCompositeDTO> listConfirmadas(Long idSucursal);
	OrdenDeCompraCompositeDTO getOrdenDeCompraById(Long id);
	OrdenDeCompraCompositeDTO getOrdenDeCompraByNroOC(String nroOrdenCompra);
	Long getNroOrdenCompra(Long idSucursal);
	int createOrdenDeCompra(OrdenDeCompraCompositeDTO dto);
	int deleteOrdenDeCompra(OrdenDeCompraDTO dto);
	int updateOrdenDeCompra(OrdenDeCompraDTO dto);


}