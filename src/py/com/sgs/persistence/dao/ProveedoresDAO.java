package py.com.sgs.persistence.dao;

import java.math.BigDecimal;
import java.util.List;

import py.com.sgs.persistence.dto.ConsultaPagosDTO;
import py.com.sgs.persistence.dto.CuotasPenDTO;
import py.com.sgs.persistence.dto.EstadoCuentaProveedorDTO;
import py.com.sgs.persistence.dto.ProveedoresDTO;
import py.com.sgs.persistence.dto.TransaccionesCompraDTO;
import py.com.sgs.persistence.dto.composite.ProveedoresCompositeDTO;

public interface ProveedoresDAO {
  List<ProveedoresCompositeDTO> listProveedores();
  ProveedoresCompositeDTO getProveedoresById(Long id);
  ProveedoresDTO getProveedorSById (Long id);
  List<ProveedoresCompositeDTO> getProveedorById(Long id);
  List<ProveedoresDTO> getProveedoresByRuc(String ruc);
  List<CuotasPenDTO> listSaldoProveedor(Long idSucursal);
  List<TransaccionesCompraDTO> getEstadoCuenta(EstadoCuentaProveedorDTO dto);
 // List<EstadoCuentaProveedorDTO>  getProveedoresSaldo (EstadoCuentaProveedorDTO dto);
  EstadoCuentaProveedorDTO getSaldoAnterior (EstadoCuentaProveedorDTO dto);
  int createProveedores(ProveedoresDTO dto);
  int deleteProveedores(ProveedoresDTO dto);
  int updateProveedores(ProveedoresDTO dto);

}