package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.SucursalDTO;
import py.com.sgs.persistence.dto.composite.RolesCompositeDTO;

import java.util.List;

public interface SucursalDAO {
  List<SucursalDTO> listSucursal();
  SucursalDTO getSucursalById(Long id);
  List<SucursalDTO> sucDispByUsuario(Long idUsuario);
  Long getCodSucursal();
  String checkSucursal(SucursalDTO codSucursal);
  int createSucursal(SucursalDTO dto);
  int deleteSucursal(SucursalDTO dto);
  int updateSucursal(SucursalDTO dto);
}