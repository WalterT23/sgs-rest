package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.UsuarioSucursalDTO;

import java.util.List;

public interface UsuarioSucursalDAO {
  List<UsuarioSucursalDTO> listUsuarioSucursal();
  List<UsuarioSucursalDTO> listSucursalesByUser(Long idUsuario);
  List<UsuarioSucursalDTO> listSucsVigentesByUser(Long idUsuario);
  UsuarioSucursalDTO getUsuarioSucursalById(Long id);
  UsuarioSucursalDTO controlUsrSuc(UsuarioSucursalDTO dto);
  int createUsuarioSucursal(UsuarioSucursalDTO dto);
  int cerrarVigenciaUsuarioSucursal(Long id);
  int updateUsuarioSucursal(UsuarioSucursalDTO dto);
}