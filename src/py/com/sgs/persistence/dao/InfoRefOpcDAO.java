package py.com.sgs.persistence.dao;

import java.util.List;

import py.com.sgs.persistence.dto.InfoRefOpcDTO;

public interface InfoRefOpcDAO {
  List<InfoRefOpcDTO> listInfoRefOpc();
  List<InfoRefOpcDTO> getInfoRefOpcByIdRef(Integer idInfoRef);
  List<InfoRefOpcDTO> getInfoRefOpcByCodRef(String codInfoRef);
  InfoRefOpcDTO getInfoRefOpcByAbv(String abv);
  InfoRefOpcDTO getInfoRefOpcById(Long id);
  int createInfoRefOpc(InfoRefOpcDTO dto);
  int updateInfoRefOpc(InfoRefOpcDTO dto);
  int deleteInfoRefOpc (InfoRefOpcDTO dto);
}
