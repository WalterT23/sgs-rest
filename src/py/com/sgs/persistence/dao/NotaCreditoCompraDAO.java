package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.NotaCreditoCompraDTO;
import py.com.sgs.persistence.dto.composite.NotaCreditoCompraCompositeDTO;

import java.util.List;

public interface NotaCreditoCompraDAO {
  List<NotaCreditoCompraCompositeDTO> listNotaCreditoCompra(Long idSucursal);
  NotaCreditoCompraCompositeDTO getNotaCreditoCompraById(Long id);
  int createNotaCreditoCompra(NotaCreditoCompraDTO dto);
  int deleteNotaCreditoCompra(NotaCreditoCompraDTO dto);
  int updateNotaCreditoCompra(NotaCreditoCompraDTO dto);
}