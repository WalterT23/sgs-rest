package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.NotaCreditoVentaDetalleDTO;

import java.util.List;

public interface NotaCreditoVentaDetalleDAO {
  List<NotaCreditoVentaDetalleDTO> listNotaCreditoVentaDetalle();
  NotaCreditoVentaDetalleDTO getNotaCreditoVentaDetalleById(Long id);
  int createNotaCreditoVentaDetalle(NotaCreditoVentaDetalleDTO dto);
  int deleteNotaCreditoVentaDetalle(NotaCreditoVentaDetalleDTO dto);
  int updateNotaCreditoVentaDetalle(NotaCreditoVentaDetalleDTO dto);
}