package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.IngresosEgresosCajaDTO;
import py.com.sgs.persistence.dto.composite.IngresosEgresosCajaCompositeDTO;

import java.math.BigDecimal;
import java.util.List;

public interface IngresosEgresosCajaDAO {
  List<IngresosEgresosCajaDTO> listIngresosEgresosCaja();
  List<IngresosEgresosCajaDTO> getByAperturaCierre(Long idAperturaCierre);
  IngresosEgresosCajaDTO getIngresosEgresosCajaById(Long id);
  BigDecimal getIngresosByIdAperturaCierre(Long idAperturaCierre);
  BigDecimal getEgresosByIdAperturaCierre(Long idAperturaCierre);
  IngresosEgresosCajaCompositeDTO getIngresosEgresosCompositeById(Long id);
  int createIngresosEgresosCaja(IngresosEgresosCajaDTO dto);
  int deleteIngresosEgresosCaja(IngresosEgresosCajaDTO dto);
  int updateIngresosEgresosCaja(IngresosEgresosCajaDTO dto);
}