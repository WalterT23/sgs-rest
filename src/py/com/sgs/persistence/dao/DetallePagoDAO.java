package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.DetallePagoDTO;

import java.util.List;

public interface DetallePagoDAO {
  List<DetallePagoDTO> listDetallePago();
  DetallePagoDTO getDetallePagoById(Long id);
  int createDetallePago(DetallePagoDTO dto);
  int deleteDetallePago(DetallePagoDTO dto);
  int updateDetallePago(DetallePagoDTO dto);
}