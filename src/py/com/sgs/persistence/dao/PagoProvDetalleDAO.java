package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.PagoProvDetalleDTO;

import java.util.List;

public interface PagoProvDetalleDAO {
  List<PagoProvDetalleDTO> listPagoProvDetalle();
  PagoProvDetalleDTO getPagoProvDetalleById(Long id);
  int createPagoProvDetalle(PagoProvDetalleDTO dto);
  int deletePagoProvDetalle(PagoProvDetalleDTO dto);
  int updatePagoProvDetalle(PagoProvDetalleDTO dto);
}