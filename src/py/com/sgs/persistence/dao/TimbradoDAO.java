package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.TimbradoDTO;

import java.util.List;

public interface TimbradoDAO {
  List<TimbradoDTO> listTimbrado(Long idSucursal);
  TimbradoDTO getTimbradoById(Long id);
  TimbradoDTO getTimbradoByNro(Integer nroTimbrado);
  int createTimbrado(TimbradoDTO dto);
  int updateTimbrado(TimbradoDTO dto);
  void vencerTimbrado(Long id);
}