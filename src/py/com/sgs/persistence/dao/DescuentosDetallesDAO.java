package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.DescuentosDetallesDTO;

import java.util.List;

public interface DescuentosDetallesDAO {
  List<DescuentosDetallesDTO> listDescuentosDetalles();
  DescuentosDetallesDTO getDescuentosDetallesById(Long id);
  List<DescuentosDetallesDTO> getDetallesByIdDescuento(Long idDescuento);
  int createDescuentosDetalles(DescuentosDetallesDTO dto);
  int deleteDescuentosDetalles(DescuentosDetallesDTO dto);
  int updateDescuentosDetalles(DescuentosDetallesDTO dto);
}