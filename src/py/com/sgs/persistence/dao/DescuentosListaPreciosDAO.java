package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.DescuentosListaPreciosDTO;

import java.util.List;

public interface DescuentosListaPreciosDAO {
  List<DescuentosListaPreciosDTO> listDescuentosListaPrecios();
  List<DescuentosListaPreciosDTO> descuentosLPbyLista(Long idLP);
  List<DescuentosListaPreciosDTO> descuentosLPvigYprox(Long idLP);
  DescuentosListaPreciosDTO getDescuentosListaPreciosById(Long id);
  DescuentosListaPreciosDTO getDescuentosListaPreciosByLista(Long idListaPrecios);
  int createDescuentosListaPrecios(DescuentosListaPreciosDTO dto);
  int deleteDescuentosListaPrecios(DescuentosListaPreciosDTO dto);
  int updateDescuentosListaPrecios(DescuentosListaPreciosDTO dto);
  void vencerAsociacion(Long id);
}