package py.com.sgs.persistence.dao;

import java.util.List;

import py.com.sgs.persistence.dto.BalanceDTO;
import py.com.sgs.persistence.dto.FiltroBalanceDTO;
import py.com.sgs.persistence.dto.RecaudacionDTO;

public interface RecaudacionDAO {
  List<RecaudacionDTO> listRecaudacion(Long idSucursal);
  RecaudacionDTO getRecaudacionById(Long id);
  RecaudacionDTO getRecaudacionVigente(Long idSucursal);
  RecaudacionDTO getRecaudacionAnterior(Long idSucursal);
  List<BalanceDTO> getBalanceMensual (FiltroBalanceDTO dto);
  FiltroBalanceDTO getBalanceAnual (FiltroBalanceDTO dto);
  int createRecaudacion(RecaudacionDTO dto);
  int deleteRecaudacion(RecaudacionDTO dto);
  int updateRecaudacion(RecaudacionDTO dto);
}