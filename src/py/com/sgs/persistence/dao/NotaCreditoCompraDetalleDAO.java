package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.NotaCreditoCompraDetalleDTO;

import java.util.List;

public interface NotaCreditoCompraDetalleDAO {
  List<NotaCreditoCompraDetalleDTO> listNotaCreditoCompraDetalle();
  NotaCreditoCompraDetalleDTO getNotaCreditoCompraDetalleById(Long id);
  int createNotaCreditoCompraDetalle(NotaCreditoCompraDetalleDTO dto);
  int deleteNotaCreditoCompraDetalle(NotaCreditoCompraDetalleDTO dto);
  int updateNotaCreditoCompraDetalle(NotaCreditoCompraDetalleDTO dto);
}