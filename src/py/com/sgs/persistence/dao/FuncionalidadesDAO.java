package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.FuncionalidadesDTO;
import py.com.sgs.persistence.dto.RolFuncionalidadDTO;
 

import java.util.List;

public interface FuncionalidadesDAO {
  List<FuncionalidadesDTO> listFuncionalidades();
  FuncionalidadesDTO getFuncionalidadesById(Long id);
  int createFuncionalidades(FuncionalidadesDTO dto);
  int deleteFuncionalidades(FuncionalidadesDTO dto);
  int updateFuncionalidades(FuncionalidadesDTO dto);
  List<String> listFuncionalidadesByToken(String token);
  List<String> listFuncionalidadesByUsr(Long idUsuario);
  List<FuncionalidadesDTO> listFuncionalidadesByRol(Long idRol);
  int createRolFuncionalidad(RolFuncionalidadDTO dto);
  int deleteRolFuncionalidad(RolFuncionalidadDTO dto);
}