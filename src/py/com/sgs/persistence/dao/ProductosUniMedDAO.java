package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.ProductosUniMedDTO;

import java.util.List;

public interface ProductosUniMedDAO {
  List<ProductosUniMedDTO> listProductosUniMed();
  ProductosUniMedDTO getProductosUniMedById(Long id);
  ProductosUniMedDTO getProductosUniMedByName(String name);
  int createProductosUniMed(ProductosUniMedDTO dto);
  int deleteProductosUniMed(ProductosUniMedDTO dto);
  int updateProductosUniMed(ProductosUniMedDTO dto);
}