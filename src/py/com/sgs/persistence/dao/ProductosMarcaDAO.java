package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.ProductosMarcaDTO;

import java.util.List;

public interface ProductosMarcaDAO {
  List<ProductosMarcaDTO> listProductosMarca();
  ProductosMarcaDTO getProductosMarcaById(Long id);
  int createProductosMarca(ProductosMarcaDTO dto);
  int deleteProductosMarca(ProductosMarcaDTO dto);
  int updateProductosMarca(ProductosMarcaDTO dto);
}