package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.DetalleCierreDTO;

import java.util.List;

public interface DetalleCierreDAO {
  List<DetalleCierreDTO> listDetalleCierre();
  DetalleCierreDTO getDetalleCierreById(Long id);
  int createDetalleCierre(DetalleCierreDTO dto);
  int deleteDetalleCierre(DetalleCierreDTO dto);
  int updateDetalleCierre(DetalleCierreDTO dto);
}