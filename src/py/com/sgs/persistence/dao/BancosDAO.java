package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.BancosDTO;

import java.util.List;

public interface BancosDAO {
  List<BancosDTO> listBancos();
  BancosDTO getBancosById(Long id);
  int createBancos(BancosDTO dto);
  int deleteBancos(BancosDTO dto);
  int updateBancos(BancosDTO dto);
}