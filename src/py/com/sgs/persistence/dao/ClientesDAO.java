package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.ClientesDTO;
import py.com.sgs.persistence.dto.composite.ClientesCompositeDTO;

import java.util.List;

public interface ClientesDAO {
  List<ClientesCompositeDTO> listClientes();
  ClientesCompositeDTO getClientesById(Long id);
  List<ClientesCompositeDTO> getClientesByRuc(String ruc);
  int createClientes(ClientesDTO dto);
  int deleteClientes(ClientesDTO dto);
  int updateClientes(ClientesDTO dto);
}