package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.DetalleCompraDTO;

import java.util.List;

public interface DetalleCompraDAO {
  List<DetalleCompraDTO> listDetalleCompra();
  DetalleCompraDTO getDetalleCompraById(Long id);
  int createDetalleCompra(DetalleCompraDTO dto);
  int deleteDetalleCompra(DetalleCompraDTO dto);
  int updateDetalleCompra(DetalleCompraDTO dto);
}