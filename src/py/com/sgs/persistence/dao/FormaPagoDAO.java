package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.FormaPagoDTO;

import java.util.List;

public interface FormaPagoDAO {
  List<FormaPagoDTO> listFormaPago();
  FormaPagoDTO getFormaPagoById(Long id);
  int createFormaPago(FormaPagoDTO dto);
  int deleteFormaPago(FormaPagoDTO dto);
  int updateFormaPago(FormaPagoDTO dto);
}