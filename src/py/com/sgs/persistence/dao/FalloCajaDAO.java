package py.com.sgs.persistence.dao;

import py.com.sgs.persistence.dto.FalloCajaDTO;
import py.com.sgs.persistence.dto.FalloCajaGenDTO;
import py.com.sgs.persistence.dto.FiltroFalloDTO;

import java.util.List;

public interface FalloCajaDAO {
  List<FalloCajaDTO> listFalloCaja(FiltroFalloDTO dto);
  FalloCajaDTO getFalloCajaById(Long id);
  List<FalloCajaGenDTO> getRepoFallo (FiltroFalloDTO dto);
  int createFalloCaja(FalloCajaDTO dto);
  int deleteFalloCaja(FalloCajaDTO dto);
  int updateFalloCaja(FalloCajaDTO dto);
}