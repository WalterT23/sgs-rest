package py.com.sgs.persistence.dao;

import java.util.List;


import py.com.sgs.persistence.dto.UsuariosDTO;
import py.com.sgs.persistence.dto.composite.UsuariosCompositeDTO;

public interface UsuariosDAO {
//  List<UsuariosCompositeDTO> listUsuarios(@Param("pagina") Integer pagina, @Param("cantidad") Integer cantidad);
  List<UsuariosCompositeDTO> listUsuarios();
  UsuariosCompositeDTO getUsuariosById(Long id);
  int createUsuarios(UsuariosDTO dto);
  int updateUsuarios(UsuariosDTO dto);
  int inactivarUsuario(UsuariosDTO dto);
  UsuariosCompositeDTO getUsuariosByUser(UsuariosDTO dto);
  List<UsuariosCompositeDTO> listRolesUsuario();
}