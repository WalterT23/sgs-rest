package py.com.sgs.persistence.dao;

import java.util.List;

import py.com.sgs.persistence.dto.ConsultaPagosDTO;
import py.com.sgs.persistence.dto.CuotasDTO;
import py.com.sgs.persistence.dto.DetalleForGraphicsDTO;
import py.com.sgs.persistence.dto.FiltroCuotasDTO;
import py.com.sgs.persistence.dto.ReporteVentasRealizadasDTO;
import py.com.sgs.persistence.dto.composite.CuotasCompositeDTO;
import py.com.sgs.persistence.dto.composite.CuotasDetalleCompositeDTO;

public interface CuotasDAO {
  List<CuotasDTO> listCuotas( Long idSucursal);
  List<ConsultaPagosDTO> listCuotasPendientes( Long idSucursal);
  List<CuotasDetalleCompositeDTO> listCuotasporProv( CuotasDTO cd);
  List<ConsultaPagosDTO> getReporCuotas(FiltroCuotasDTO dto);
  List<DetalleForGraphicsDTO> getCuotasGraphic(FiltroCuotasDTO dto);
  CuotasDTO getCuotasById(Long id);
  int createCuotas(CuotasCompositeDTO dto);
  int deleteCuotas(CuotasDTO dto);
  int updateCuotas(CuotasDTO dto);
}