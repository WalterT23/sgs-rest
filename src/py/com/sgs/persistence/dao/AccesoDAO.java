package py.com.sgs.persistence.dao;

import java.util.List;

import py.com.sgs.persistence.dto.AccesoDTO;
import py.com.sgs.persistence.dto.composite.AccesoCompositeDTO;

public interface AccesoDAO {
  List<AccesoDTO> listAcceso();
  AccesoDTO getAccesoById(Long id);
  AccesoCompositeDTO getAccesoByToken (String token);
  int createAcceso(AccesoDTO dto);
  int updateAcceso(AccesoDTO dto);
}