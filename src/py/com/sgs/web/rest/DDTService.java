package py.com.sgs.web.rest;

public class DDTService {
  public static final String PARTIDA_SERVICE = "partidaService";
  public static final String UNSECURE_PATH = "/sgs";
  public static final String SECURE_PATH = "/secure/ddt";
  public static final String INFOREFOPC_SERVICE = "infoRefOpcService";
  public static final String ACCESO_SERVICE = "accesoService";
  public static final String CLIENTES_SERVICE = "clientesService";
  public static final String PRODUCTOS_SERVICE = "productosService";
  public static final String CABECERAVENTA_SERVICE = "cabeceraVentaService";
  public static final String USUARIOS_SERVICE = "usuariosService";
  public static final String FUNCIONALIDADES_SERVICE = "funcionalidadesService";
  public static final String ROLES_SERVICE = "rolesService";
  public static final String PROVEEDORES_SERVICE = "proveedoresService";
  public static final String PRODUCTOSUNIMED_SERVICE = "productosUniMedService";
  public static final String PRODUCTOSMARCA_SERVICE = "productosMarcaService";
  public static final String PROVEEDORESCONTACTO_SERVICE = "proveedoresContactoService";
  public static final String ORDENDECOMPRA_SERVICE = "ordenDeCompraService";
  public static final String ORDENCOMPRADETALLE_SERVICE = "ordenCompraDetalleService";
}
