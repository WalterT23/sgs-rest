package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.FalloCajaDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.FiltroComprasDTO;
import py.com.sgs.persistence.dto.FiltroFalloDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.FalloCajaService;

@RestController
@RequestMapping(value = "/falloCaja")
@CrossOrigin()
public class FalloCajaSecureRESTServiceImpl extends  BaseSecureRESTService {

  @Autowired private FalloCajaService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());
  
  
  @RequestMapping(value="/listFallo",
	      method = RequestMethod.POST,
	      consumes = MediaType.APPLICATION_JSON_VALUE,
	      produces = MediaType.APPLICATION_JSON_VALUE)
		  public ResponseEntity<List<FalloCajaDTO>> listFalloCaja(@RequestBody FiltroFalloDTO dto, HttpServletRequest request) throws ServiceException {
			  try {
			      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			      List<FalloCajaDTO> result = dao.listFalloCaja(dto,ic);
			      return new ResponseEntity<List<FalloCajaDTO>>(result, HttpStatus.OK);
			    }
			    catch (ServiceException e){
			      logger.error(e.getMessage(), e);
			      throw e;
			    }
			    catch (Exception e){
			      logger.error(e.getMessage(), e);
			      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
			    }
			}
  
//  @RequestMapping(value="",
//      method = RequestMethod.GET,
//      produces = MediaType.APPLICATION_JSON_VALUE)
//  public ResponseEntity<List<FalloCajaDTO>> listFalloCaja(HttpServletRequest request) throws ServiceException { 
//    try {
//      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
//      List<FalloCajaDTO> result = dao.listFalloCaja(ic);
//      return new ResponseEntity<List<FalloCajaDTO>>(result, HttpStatus.OK);
//    }
//    catch (ServiceException e){
//      logger.error(e.getMessage(), e);
//      throw e;
//    }
//    catch (Exception e){
//      logger.error(e.getMessage(), e);
//      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
//    }
//  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<FalloCajaDTO> showFalloCaja(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      FalloCajaDTO result = dao.getFalloCaja(id, ic);
      return new ResponseEntity<FalloCajaDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<FalloCajaDTO> saveFalloCaja(@PathVariable("id") Long id, @RequestBody FalloCajaDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      FalloCajaDTO result = dao.updateFalloCaja(id, dto, ic);
      return new ResponseEntity<FalloCajaDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  @RequestMapping(value="/reporteFallo",
	      method = RequestMethod.POST,
	      consumes = MediaType.APPLICATION_JSON_VALUE,
	      produces = MediaType.APPLICATION_JSON_VALUE)
		  public ResponseEntity<FiltroFalloDTO> repoFallo(@RequestBody FiltroFalloDTO dto, HttpServletRequest request) throws ServiceException {
			    try {
			      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			      FiltroFalloDTO result = dao.getReportFallo(dto, ic);
			      return new ResponseEntity<FiltroFalloDTO>(result, HttpStatus.OK);
			    }
			   catch (ServiceException e){
			      logger.error(e.getMessage(), e);
			      throw e;
			    }
			   catch (Exception e){
			      logger.error(e.getMessage(), e);
			     throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
			    }
			  }
  
  
  @RequestMapping(value = "/reporteFallo/impresion", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FileReportDTO> repoFallos (@RequestBody FiltroFalloDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			FileReportDTO result = dao.getFallosImpresion(dto, ic);
			return new ResponseEntity<FileReportDTO>(result,
					HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"internal.dataAccessError");
		}
	}
  
  

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<FalloCajaDTO> createFalloCaja(@RequestBody FalloCajaDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      FalloCajaDTO result = dao.createFalloCaja(dto, ic);
      return new ResponseEntity<FalloCajaDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteFalloCaja(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteFalloCaja(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

}