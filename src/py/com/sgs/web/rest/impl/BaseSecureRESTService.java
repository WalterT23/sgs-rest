package py.com.sgs.web.rest.impl;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import py.com.sgs.exception.ServiceException;


public abstract class BaseSecureRESTService {

	  @ExceptionHandler(ServiceException.class)
	  @ResponseBody
	  public String handleException(HttpServletResponse response, ServiceException e) {
			switch (e.getErrorCode()) {
				case PARAMETER_ERROR:
				case PARSING_ERROR:
				case JSON_DECODING_ERROR:
				case INVALID_REQUEST:
				{
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				}
				case NO_DATA_FOUND: {
					response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				}
				case UNAUTHORIZED: {
					response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				}
				default: {
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				}
			}
		  
	      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	      return e.toJSON();
	  }  
	
}
