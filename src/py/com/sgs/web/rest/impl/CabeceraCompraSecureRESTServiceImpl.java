package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.CabeceraCompraDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.FiltroComprasDTO;
import py.com.sgs.persistence.dto.FiltroCuotasDTO;
import py.com.sgs.persistence.dto.composite.CabeceraCompraCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.CabeceraCompraService;


@CrossOrigin
@RestController
@RequestMapping(value ="/cabeceraCompra")
public class CabeceraCompraSecureRESTServiceImpl  extends BaseSecureRESTService  {

  @Autowired private CabeceraCompraService dao;
  private InvocationContext ic;
  
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<CabeceraCompraCompositeDTO>> listCabeceraCompra(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<CabeceraCompraCompositeDTO> result = dao.listCabeceraCompra(ic);
      return new ResponseEntity<List<CabeceraCompraCompositeDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CabeceraCompraDTO> showCabeceraCompra(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      CabeceraCompraDTO result = dao.getCabeceraCompra(id, ic);
      return new ResponseEntity<CabeceraCompraDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  @RequestMapping(value="/ByIdProveedor/{idProveedor}",
	      method = RequestMethod.GET,
	      produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<List<CabeceraCompraCompositeDTO>> getCabeceraCompraByIdProveedor(@PathVariable("idProveedor") Long idProveedor,HttpServletRequest request) throws ServiceException { 
	    try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      CabeceraCompraDTO cc = new CabeceraCompraDTO();
	      cc.setIdProveedor(idProveedor);
	      cc.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
	      List<CabeceraCompraCompositeDTO> result = dao.getCabeceraCompraByIdProveedor(cc);
	      return new ResponseEntity<List<CabeceraCompraCompositeDTO>>(result, HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CabeceraCompraCompositeDTO> saveCabeceraCompra(@PathVariable("id") Long id, @RequestBody CabeceraCompraCompositeDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      CabeceraCompraCompositeDTO result = dao.updateCabeceraCompra(id, dto, ic);
      return new ResponseEntity<CabeceraCompraCompositeDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CabeceraCompraCompositeDTO> createCabeceraCompra(@RequestBody CabeceraCompraCompositeDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      CabeceraCompraCompositeDTO result = dao.createCabeceraCompra(dto, ic);
      return new ResponseEntity<CabeceraCompraCompositeDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  
  @RequestMapping(value="/reporteCompras",
	      method = RequestMethod.POST,
	      consumes = MediaType.APPLICATION_JSON_VALUE,
	      produces = MediaType.APPLICATION_JSON_VALUE)
		  public ResponseEntity<FiltroComprasDTO> repoCuotas(@RequestBody FiltroComprasDTO dto, HttpServletRequest request) throws ServiceException {
			    try {
			      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			      FiltroComprasDTO result = dao.getReporCompras(dto, ic);
			      return new ResponseEntity<FiltroComprasDTO>(result, HttpStatus.OK);
			    }
			   catch (ServiceException e){
			      logger.error(e.getMessage(), e);
			      throw e;
			    }
			   catch (Exception e){
			      logger.error(e.getMessage(), e);
			     throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
			    }
			  }
  
  @RequestMapping(value = "/reporteCompras/impresion", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
 	public ResponseEntity<FileReportDTO> repoComprasImpresion (@RequestBody FiltroComprasDTO dto, HttpServletRequest request) throws ServiceException {
 		try {
 			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
 			FileReportDTO result = dao.getComprasImpresion(dto, ic);
 			return new ResponseEntity<FileReportDTO>(result,
 					HttpStatus.OK);
 		} catch (ServiceException e) {
 			logger.error(e.getMessage(), e);
 			throw e;
 		} catch (Exception e) {
 			logger.error(e.getMessage(), e);
 			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"internal.dataAccessError");
 		}
 	}

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteCabeceraCompra(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteCabeceraCompra(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

}