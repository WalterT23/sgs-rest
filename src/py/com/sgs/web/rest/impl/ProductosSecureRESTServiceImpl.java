package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.ProductosDTO;
import py.com.sgs.persistence.dto.composite.ClientesCompositeDTO;
import py.com.sgs.persistence.dto.composite.ProductosCompositeDTO;
import py.com.sgs.persistence.dto.composite.ProductosGroupCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ProductosService;

@RestController
@RequestMapping(value = "/productos")
@CrossOrigin()
public class ProductosSecureRESTServiceImpl extends BaseSecureRESTService{

  @Autowired private ProductosService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<ProductosDTO>> listProductos(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<ProductosDTO> result = dao.listProductos(ic);
      return new ResponseEntity<List<ProductosDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  @RequestMapping(value="/byGroup",
		  method = RequestMethod.POST,
		  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<ProductosDTO> > listProductosByGroups(@RequestBody ProductosGroupCompositeDTO dto, HttpServletRequest request) throws ServiceException { 
	  try {
		  ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		  List<ProductosDTO>  result = dao.listProductosByGroups(dto, ic);
		  return new ResponseEntity<List<ProductosDTO> >(result, HttpStatus.OK);
	  }
	  catch (ServiceException e){
		  logger.error(e.getMessage(), e);
		  throw e;
	  }
	  catch (Exception e){
		  logger.error(e.getMessage(), e);
		  throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	  }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ProductosDTO> showProductos(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      ProductosDTO result = dao.getProductosById(id, ic);
      return new ResponseEntity<ProductosDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  @RequestMapping(value = "/byCodigo/{cod}",
	      method = RequestMethod.GET,
	      produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<ProductosCompositeDTO> showProductos(@PathVariable("cod") String cod, HttpServletRequest request) throws ServiceException { 
	    try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      ProductosCompositeDTO result = dao.getProductosByCod(cod, ic);
	      return new ResponseEntity<ProductosCompositeDTO>(result, HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	  }
 
  @RequestMapping(value = "/likeCod/{cod}",
	      method = RequestMethod.GET,
	      produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<List<ProductosDTO>> showClientesByRuc(@PathVariable("cod") String cod, HttpServletRequest request) throws ServiceException { 
	    try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      List<ProductosDTO> result = dao.getProductosLikeCod(cod, ic);
	      return new ResponseEntity<List<ProductosDTO>>(result, HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ProductosDTO> saveProductos(@PathVariable("id") Long id, @RequestBody ProductosDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      ProductosDTO result = dao.updateProductos(id, dto, ic);
      return new ResponseEntity<ProductosDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ProductosDTO> createProductos(@RequestBody ProductosDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      ProductosDTO result = dao.createProductos(dto, ic);
      return new ResponseEntity<ProductosDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteProductos(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteProductos(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  @RequestMapping(value="/genCodigo",
	      method = RequestMethod.GET,
	      produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<String> codProGen(HttpServletRequest request) throws ServiceException { 
	    try {
	      String result = dao.codProGen();
	      return new ResponseEntity<String>(result, HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	  }

}