package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.NotaCreditoVentaDetalleDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.NotaCreditoVentaDetalleService;

@RestController
@RequestMapping(value = "/notaCreditoVentaDetalle")
public class NotaCreditoVentaDetalleSecureRESTServiceImpl {

  @Autowired private NotaCreditoVentaDetalleService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<NotaCreditoVentaDetalleDTO>> listNotaCreditoVentaDetalle(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<NotaCreditoVentaDetalleDTO> result = dao.listNotaCreditoVentaDetalle(ic);
      return new ResponseEntity<List<NotaCreditoVentaDetalleDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<NotaCreditoVentaDetalleDTO> showNotaCreditoVentaDetalle(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      NotaCreditoVentaDetalleDTO result = dao.getNotaCreditoVentaDetalle(id, ic);
      return new ResponseEntity<NotaCreditoVentaDetalleDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<NotaCreditoVentaDetalleDTO> saveNotaCreditoVentaDetalle(@PathVariable("id") Long id, @RequestBody NotaCreditoVentaDetalleDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      NotaCreditoVentaDetalleDTO result = dao.updateNotaCreditoVentaDetalle(id, dto, ic);
      return new ResponseEntity<NotaCreditoVentaDetalleDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<NotaCreditoVentaDetalleDTO> createNotaCreditoVentaDetalle(@RequestBody NotaCreditoVentaDetalleDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      NotaCreditoVentaDetalleDTO result = dao.createNotaCreditoVentaDetalle(dto, ic);
      return new ResponseEntity<NotaCreditoVentaDetalleDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteNotaCreditoVentaDetalle(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteNotaCreditoVentaDetalle(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

}