package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.DescuentosListaPreciosDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.DescuentosListaPreciosService;

@RestController
@CrossOrigin()
@RequestMapping(value = "/descuentosListaPrecios")
public class DescuentosListaPreciosSecureRESTServiceImpl extends BaseSecureRESTService{

	@Autowired
	private DescuentosListaPreciosService dao;
	private InvocationContext ic;
	@Autowired
	private HttpHeaderConfig config;
	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DescuentosListaPreciosDTO>> listDescuentosListaPrecios(
			HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config
					.getContextAttributeName());
			List<DescuentosListaPreciosDTO> result = dao
					.listDescuentosListaPrecios(ic);
			return new ResponseEntity<List<DescuentosListaPreciosDTO>>(result,
					HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}
	
	@RequestMapping(value = "/byIdLp/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DescuentosListaPreciosDTO>> listDescuentosListaPreciosByLP(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			List<DescuentosListaPreciosDTO> result = dao.descuentosListaPreciosByLista(id, ic);
			return new ResponseEntity<List<DescuentosListaPreciosDTO>>(result,HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DescuentosListaPreciosDTO> showDescuentosListaPrecios(@PathVariable("id") Long id, HttpServletRequest request)
			throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config
					.getContextAttributeName());
			DescuentosListaPreciosDTO result = dao.getDescuentosListaPrecios(
					id, ic);
			return new ResponseEntity<DescuentosListaPreciosDTO>(result,
					HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DescuentosListaPreciosDTO> saveDescuentosListaPrecios(
			@PathVariable("id") Long id,
			@RequestBody DescuentosListaPreciosDTO dto,
			HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config
					.getContextAttributeName());
			DescuentosListaPreciosDTO result = dao
					.updateDescuentosListaPrecios(id, dto, ic);
			return new ResponseEntity<DescuentosListaPreciosDTO>(result,
					HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}

	@RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DescuentosListaPreciosDTO> createDescuentosListaPrecios(
			@RequestBody DescuentosListaPreciosDTO dto,
			HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config
					.getContextAttributeName());
			DescuentosListaPreciosDTO result = dao
					.createDescuentosListaPrecios(dto, ic);
			return new ResponseEntity<DescuentosListaPreciosDTO>(result,
					HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/cerrarVigencia/{id}", method = RequestMethod.PUT, 
    consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> cerrarVigencia(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			dao.vencerAso(id, ic);
			return new ResponseEntity<>(null, HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity deleteDescuentosListaPrecios(
			@PathVariable("id") Long id, HttpServletRequest request)
			throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config
					.getContextAttributeName());
			dao.deleteDescuentosListaPrecios(id, ic);
			return new ResponseEntity(HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}

}