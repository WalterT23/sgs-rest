package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.MovMercDetalleDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.MovMercDetalleService;

@RestController
@RequestMapping(value ="/movMercDetalle")
@CrossOrigin()
public class MovMercDetalleSecureRESTServiceImpl {

  @Autowired private MovMercDetalleService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<MovMercDetalleDTO>> listMovMercDetalle(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<MovMercDetalleDTO> result = dao.listMovMercDetalle(ic);
      return new ResponseEntity<List<MovMercDetalleDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<MovMercDetalleDTO> showMovMercDetalle(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      MovMercDetalleDTO result = dao.getMovMercDetalle(id, ic);
      return new ResponseEntity<MovMercDetalleDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/byIdMov/{idMov}",
		  method = RequestMethod.GET,
		  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<MovMercDetalleDTO>>getMovMercDetbyIdMov(@PathVariable("idMov") Long idMov, HttpServletRequest request) throws ServiceException { 
	  try {
		  ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		  List<MovMercDetalleDTO> result = dao.getMovMercDetByIdMov(idMov, ic);
		  return new ResponseEntity<List<MovMercDetalleDTO>>(result, HttpStatus.OK);
	  }
	  catch (ServiceException e){
		  logger.error(e.getMessage(), e);
		  throw e;
	  }
	  catch (Exception e){
		  logger.error(e.getMessage(), e);
		  throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	  }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<MovMercDetalleDTO> saveMovMercDetalle(@PathVariable("id") Long id, @RequestBody MovMercDetalleDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      MovMercDetalleDTO result = dao.updateMovMercDetalle(id, dto, ic);
      return new ResponseEntity<MovMercDetalleDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<MovMercDetalleDTO> createMovMercDetalle(@RequestBody MovMercDetalleDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      MovMercDetalleDTO result = dao.createMovMercDetalle(dto, ic);
      return new ResponseEntity<MovMercDetalleDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteMovMercDetalle(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteMovMercDetalle(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

}