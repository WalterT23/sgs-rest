package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.UsuarioSucursalService;
import py.com.sgs.persistence.dto.UsuarioSucursalDTO;


@RestController
@RequestMapping(value = "/usuarioSucursal")
@CrossOrigin()

public class UsuarioSucursalSecureRESTServiceImpl {

  @Autowired private UsuarioSucursalService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<UsuarioSucursalDTO>> listUsuarioSucursal(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<UsuarioSucursalDTO> result = dao.listUsuarioSucursal(ic);
      return new ResponseEntity<List<UsuarioSucursalDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<UsuarioSucursalDTO> showUsuarioSucursal(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      UsuarioSucursalDTO result = dao.getUsuarioSucursal(id, ic);
      return new ResponseEntity<UsuarioSucursalDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  @RequestMapping(value="/sucursalesByUser/{idUsuario}",
	   method = RequestMethod.GET,
	   produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<UsuarioSucursalDTO>> listSucursalesByUser(@PathVariable("idUsuario") Long idUsuario, HttpServletRequest request) throws ServiceException { 
	  try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      List<UsuarioSucursalDTO> result = dao.listSucursalesByUser(idUsuario, ic);
	      return new ResponseEntity<List<UsuarioSucursalDTO>>(result, HttpStatus.OK);
	  }
	  catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	  }
	  catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	  }
  }
  
  @RequestMapping(value="/sucsVigentesByUser/{idUsuario}",
		  method = RequestMethod.GET,
		  produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<List<UsuarioSucursalDTO>> listSucsVigentesByUser(@PathVariable("idUsuario") Long idUsuario, HttpServletRequest request) throws ServiceException { 
		  try {
		      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		      List<UsuarioSucursalDTO> result = dao.listSucsVigentesByUser(idUsuario, ic);
		      return new ResponseEntity<List<UsuarioSucursalDTO>>(result, HttpStatus.OK);
		  }
		  catch (ServiceException e){
		      logger.error(e.getMessage(), e);
		      throw e;
		  }
		  catch (Exception e){
		      logger.error(e.getMessage(), e);
		      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		  }
  }
  
  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<UsuarioSucursalDTO> saveUsuarioSucursal(@PathVariable("id") Long id, @RequestBody UsuarioSucursalDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      UsuarioSucursalDTO result = dao.updateUsuarioSucursal(id, dto, ic);
      return new ResponseEntity<UsuarioSucursalDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<UsuarioSucursalDTO> createUsuarioSucursal(@RequestBody UsuarioSucursalDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      UsuarioSucursalDTO result = dao.createUsuarioSucursal(dto, ic);
      return new ResponseEntity<UsuarioSucursalDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteUsuarioSucursal(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteUsuarioSucursal(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

}