package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ListaPreciosDetalleService;
import py.com.sgs.persistence.dto.ListaPreciosDetalleDTO;
import py.com.sgs.persistence.dto.composite.LPDetallesCompositeDTO;

@RestController
@RequestMapping(value = "/listaPreciosDetalle")
@CrossOrigin()
public class ListaPreciosDetalleSecureRESTServiceImpl extends BaseSecureRESTService {

  @Autowired private ListaPreciosDetalleService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<LPDetallesCompositeDTO>> listListaPreciosDetalle(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<LPDetallesCompositeDTO> result = dao.listListaPreciosDetalle(ic);
      return new ResponseEntity<List<LPDetallesCompositeDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<LPDetallesCompositeDTO> showListaPreciosDetalle(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      LPDetallesCompositeDTO result = dao.getListaPreciosDetalle(id, ic);
      return new ResponseEntity<LPDetallesCompositeDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  @RequestMapping(value = "/byLP/{idLP}",
	  method = RequestMethod.GET,
	  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<LPDetallesCompositeDTO>> listaPreciosDetalleByIdLP(@PathVariable("idLP") Long idLP, HttpServletRequest request) throws ServiceException { 
	  try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      List<LPDetallesCompositeDTO> result = dao.getListaPreciosDetalleByIdLP(idLP, ic);
	      return new ResponseEntity<List<LPDetallesCompositeDTO>>(result, HttpStatus.OK);
	  }
	  catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	  }
	  catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	  }
  }
  
  @RequestMapping(value = "/pVenta/{codPro}",
		  method = RequestMethod.GET,
		  produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<LPDetallesCompositeDTO> listaPreciosDetalleByIdLP(@PathVariable("codPro") String codPro, HttpServletRequest request) throws ServiceException { 
		  try {
		      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		      LPDetallesCompositeDTO result = dao.precioByLPcodPro(codPro, ic);
		      return new ResponseEntity<LPDetallesCompositeDTO>(result, HttpStatus.OK);
		  }
		  catch (ServiceException e){
		      logger.error(e.getMessage(), e);
		      throw e;
		  }
		  catch (Exception e){
		      logger.error(e.getMessage(), e);
		      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		  }
	  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ListaPreciosDetalleDTO> saveListaPreciosDetalle(@PathVariable("id") Long id, @RequestBody ListaPreciosDetalleDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      ListaPreciosDetalleDTO result = dao.updateListaPreciosDetalle(id, dto, ic);
      return new ResponseEntity<ListaPreciosDetalleDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ListaPreciosDetalleDTO> createListaPreciosDetalle(@RequestBody ListaPreciosDetalleDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      ListaPreciosDetalleDTO result = dao.createListaPreciosDetalle(dto, ic);
      return new ResponseEntity<ListaPreciosDetalleDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteListaPreciosDetalle(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteListaPreciosDetalle(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

}