package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.AccesoDTO;
import py.com.sgs.persistence.dto.composite.AccesoCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.AccesoService;

@RestController
@RequestMapping(value = "/acceso")
public class AccesoSecureRESTServiceImpl {

  @Autowired private AccesoService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<AccesoDTO>> listAcceso(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<AccesoDTO> result = dao.listAcceso(ic);
      return new ResponseEntity<List<AccesoDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE) 
  public ResponseEntity<AccesoDTO> showAcceso(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      AccesoDTO result = dao.getAcceso(id, ic);
      return new ResponseEntity<AccesoDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<AccesoDTO> saveAcceso(@PathVariable("id") Long id, @RequestBody AccesoDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      AccesoDTO result = dao.updateAcceso(id, dto, ic);
      return new ResponseEntity<AccesoDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<AccesoDTO> createAcceso(@RequestBody AccesoDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      AccesoDTO result = dao.createAcceso(dto, ic);
      return new ResponseEntity<AccesoDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  @RequestMapping(value="/funcionalidadesByToken",
	      method = RequestMethod.POST,
	      consumes = MediaType.APPLICATION_JSON_VALUE,
	      produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<AccesoCompositeDTO> getFuncionalidadesByToken(@RequestBody AccesoDTO dto, HttpServletRequest request) throws ServiceException {
	    try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      AccesoCompositeDTO result = dao.getFuncionalidadesByToken(dto, ic);
	      return new ResponseEntity<AccesoCompositeDTO>(result, HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	  }
  
}