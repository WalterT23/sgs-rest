package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.AperturaCierreDTO;
import py.com.sgs.persistence.dto.composite.AperturaCierreCompositeDTO;
import py.com.sgs.persistence.dto.composite.CajaUserCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.AperturaCierreService;

@RestController
@RequestMapping(value = "/aperturaCierre")
@CrossOrigin()

public class AperturaCierreSecureRESTServiceImpl extends BaseSecureRESTService  {

	@Autowired private AperturaCierreService dao;
	private InvocationContext ic;
	@Autowired private HttpHeaderConfig config;
	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value="",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AperturaCierreDTO>> listAperturaCierre(HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			List<AperturaCierreDTO> result = dao.listAperturaCierre(ic);
			return new ResponseEntity<List<AperturaCierreDTO>>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AperturaCierreCompositeDTO> showAperturaCierre(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			AperturaCierreCompositeDTO result = dao.getAperturaCierre(id, ic);
			return new ResponseEntity<AperturaCierreCompositeDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/{id}",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AperturaCierreDTO> saveAperturaCierre(@PathVariable("id") Long id, @RequestBody AperturaCierreDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			AperturaCierreDTO result = dao.updateAperturaCierre(id, dto, ic);
			return new ResponseEntity<AperturaCierreDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}
	
	  @RequestMapping(value = "/ByIdPtoExp/{idPuntoExpedicion}",
		      method = RequestMethod.GET,
		      produces = MediaType.APPLICATION_JSON_VALUE)
		  public ResponseEntity<AperturaCierreCompositeDTO> showAperturaCierreVigenteByIdPuntoExp(@PathVariable("idPuntoExpedicion") Long idPuntoExpedicion, HttpServletRequest request) throws ServiceException { 
		    try {
		      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		      AperturaCierreCompositeDTO result = dao.getAperturaCierreVigenteByIdPuntoExp(idPuntoExpedicion, ic);
		      return new ResponseEntity<AperturaCierreCompositeDTO>(result, HttpStatus.OK);
		    }
		    catch (ServiceException e){
		      logger.error(e.getMessage(), e);
		      throw e;
		    }
		    catch (Exception e){
		      logger.error(e.getMessage(), e);
		      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		    }
		  }

	@RequestMapping(value="",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AperturaCierreDTO> createAperturaCierre(@RequestBody AperturaCierreDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			AperturaCierreDTO result = dao.createAperturaCierre(dto, ic);
			return new ResponseEntity<AperturaCierreDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}",
	method = RequestMethod.DELETE)
	public ResponseEntity deleteAperturaCierre(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			dao.deleteAperturaCierre(id, ic);
			return new ResponseEntity(HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/cerrar/{id}",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AperturaCierreDTO> cerrarAperturaCierre(@PathVariable("id") Long id, @RequestBody AperturaCierreDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			AperturaCierreDTO result = dao.cerrarAperturaCierre(id, dto, ic);
			return new ResponseEntity<AperturaCierreDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}
	
	@RequestMapping(value="/generar",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AperturaCierreDTO> generarAperturaCierre(@RequestBody AperturaCierreDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			dto.setFechaCierre(null);
			dto.setSaldoCierre(null);
			dto.setUsuarioCierre(null);
			AperturaCierreDTO result = dao.generarAperturaCierre(dto, ic);
			return new ResponseEntity<AperturaCierreDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

}