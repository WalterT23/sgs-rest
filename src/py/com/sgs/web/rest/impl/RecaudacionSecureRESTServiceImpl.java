package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.EstadoCuentaProveedorDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.FiltroBalanceDTO;
import py.com.sgs.persistence.dto.FiltroFalloDTO;
import py.com.sgs.persistence.dto.RecaudacionDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.RecaudacionService;

@RestController
@RequestMapping(value ="/recaudacion")
@CrossOrigin()
public class RecaudacionSecureRESTServiceImpl {

  @Autowired private RecaudacionService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<RecaudacionDTO>> listRecaudacion(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<RecaudacionDTO> result = dao.listRecaudacion(ic);
      return new ResponseEntity<List<RecaudacionDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<RecaudacionDTO> showRecaudacion(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      RecaudacionDTO result = dao.getRecaudacion(id, ic);
      return new ResponseEntity<RecaudacionDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<RecaudacionDTO> saveRecaudacion(@PathVariable("id") Long id, @RequestBody RecaudacionDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      RecaudacionDTO result = dao.updateRecaudacion(id, dto, ic);
      return new ResponseEntity<RecaudacionDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<RecaudacionDTO> createRecaudacion(@RequestBody RecaudacionDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      RecaudacionDTO result = dao.createRecaudacion(dto, ic);
      return new ResponseEntity<RecaudacionDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteRecaudacion(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteRecaudacion(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  @RequestMapping(value="/reporteRecaudacion",
	      method = RequestMethod.POST,
	      consumes = MediaType.APPLICATION_JSON_VALUE,
	      produces = MediaType.APPLICATION_JSON_VALUE)
		  public ResponseEntity<FiltroBalanceDTO> repoBalance(@RequestBody FiltroBalanceDTO dto, HttpServletRequest request) throws ServiceException {
			    try {
			      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			      FiltroBalanceDTO result = dao.getReportBalance(dto, ic);
			      return new ResponseEntity<FiltroBalanceDTO>(result, HttpStatus.OK);
			    }
			   catch (ServiceException e){
			      logger.error(e.getMessage(), e);
			      throw e;
			    }
			   catch (Exception e){
			      logger.error(e.getMessage(), e);
			     throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
			    }
			  }

  
  @RequestMapping(value = "/reporteRecaudacion/impresion", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FileReportDTO> repoVentasRealizadasImpresion (@RequestBody FiltroBalanceDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			FileReportDTO result = dao.getReporteRecaImpresion(dto, ic);
			return new ResponseEntity<FileReportDTO>(result,
					HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"internal.dataAccessError");
		}
	}
}