package py.com.sgs.web.rest.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ReportesService;
import py.com.sgs.persistence.dto.EstadoCuentaProveedorDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.RepoMovimientoProductosDTO;
import py.com.sgs.persistence.dto.ReporteProductosMasVendidosDTO;
import py.com.sgs.persistence.dto.ReporteStockDTO;
import py.com.sgs.persistence.dto.ReporteVentasRealizadasDTO;

@RestController
@CrossOrigin()
@RequestMapping(value = "/reportes")
public class ReportesSecureRESTServiceImpl {

	@Autowired
	private ReportesService dao;
	private InvocationContext ic;
	@Autowired
	private HttpHeaderConfig config;
	private Logger logger = LoggerFactory.getLogger(getClass());

	
	@RequestMapping(value = "/ventasRealizadas", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ReporteVentasRealizadasDTO> repoVentasRealizadas(
			@RequestBody ReporteVentasRealizadasDTO dto,
			HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config
					.getContextAttributeName());
			ReporteVentasRealizadasDTO result = dao.getReporteVentasRealizadas(
					dto, ic);
			return new ResponseEntity<ReporteVentasRealizadasDTO>(result,
					HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}
	
	@RequestMapping(value = "/ventasRealizadas/impresion", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FileReportDTO> repoVentasRealizadasImpresion (@RequestBody ReporteVentasRealizadasDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			FileReportDTO result = dao.getReporteVentasRealizadasImpresion(dto, ic);
			return new ResponseEntity<FileReportDTO>(result,
					HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,"internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/productosVendidos", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ReporteProductosMasVendidosDTO> repoProductosVendidos(@RequestBody ReporteProductosMasVendidosDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config
					.getContextAttributeName());
			ReporteProductosMasVendidosDTO result = dao
					.getReporteProMasVendidos(dto, ic);
			return new ResponseEntity<ReporteProductosMasVendidosDTO>(result,
					HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}
	
	@RequestMapping(value = "/productosVendidos/impresion", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FileReportDTO> repoProductosVendidosImpresion(@RequestBody ReporteProductosMasVendidosDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			FileReportDTO result = dao.getReporteProMasVendidosImpresion(dto, ic);
			return new ResponseEntity<FileReportDTO>(result, HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/productosAagotarse", 
	method = RequestMethod.POST, 
	consumes = MediaType.APPLICATION_JSON_VALUE, 
	produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ReporteStockDTO> repoProductosAgotarse(@RequestBody ReporteStockDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			ReporteStockDTO result = dao.getReporteStock(dto, ic);
			return new ResponseEntity<ReporteStockDTO>(result, HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}
	
	@RequestMapping(value = "/productosAagotarse/impresion", 
	method = RequestMethod.POST, 
	consumes = MediaType.APPLICATION_JSON_VALUE, 
	produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FileReportDTO> repoProductosAgotarseImpresion(@RequestBody ReporteStockDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			FileReportDTO result = dao.getReporteStockImpresion(dto, ic);
			return new ResponseEntity<FileReportDTO>(result, HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}
	

	@RequestMapping(value = "/movimientoProductos", 
	method = RequestMethod.POST, 
	consumes = MediaType.APPLICATION_JSON_VALUE, 
	produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RepoMovimientoProductosDTO> repoMovProductos(@RequestBody RepoMovimientoProductosDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			RepoMovimientoProductosDTO result = dao.getReporteMovProductos(dto, ic);
			return new ResponseEntity<RepoMovimientoProductosDTO>(result, HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}
	
	@RequestMapping(value = "/movimientoProductos/impresion", 
	method = RequestMethod.POST, 
	consumes = MediaType.APPLICATION_JSON_VALUE, 
	produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FileReportDTO> repoMovProductosImpresion(@RequestBody RepoMovimientoProductosDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			FileReportDTO result = dao.getReporteMovProductosImpresion(dto, ic);
			return new ResponseEntity<FileReportDTO>(result, HttpStatus.OK);
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR,
					"internal.dataAccessError");
		}
	}

}