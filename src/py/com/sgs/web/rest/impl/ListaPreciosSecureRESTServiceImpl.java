package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ListaPreciosService;
import py.com.sgs.persistence.dto.ListaPreciosDTO;
import py.com.sgs.persistence.dto.composite.ListaPreciosCompositeDTO;


@RestController
@RequestMapping(value = "/listaPrecios")
@CrossOrigin()
public class ListaPreciosSecureRESTServiceImpl extends BaseSecureRESTService {

  @Autowired private ListaPreciosService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<ListaPreciosCompositeDTO>> listListaPrecios(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<ListaPreciosCompositeDTO> result = dao.listListaPrecios(ic);
      return new ResponseEntity<List<ListaPreciosCompositeDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="/vigentes",
		  method = RequestMethod.GET,
		  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<ListaPreciosCompositeDTO>> listaPreciosVigentes(HttpServletRequest request) throws ServiceException { 
	  try {
		  ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		  List<ListaPreciosCompositeDTO> result = dao.listaPreciosVigentes(ic);
		  return new ResponseEntity<List<ListaPreciosCompositeDTO>>(result, HttpStatus.OK);
	  }
	  catch (ServiceException e){
		  logger.error(e.getMessage(), e);
		  throw e;
	  }
	  catch (Exception e){
		  logger.error(e.getMessage(), e);
		  throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	  }
  }	

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ListaPreciosCompositeDTO> showListaPrecios(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      ListaPreciosCompositeDTO result = dao.getListaPrecios(id, ic);
      return new ResponseEntity<ListaPreciosCompositeDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ListaPreciosDTO> saveListaPrecios(@PathVariable("id") Long id, @RequestBody ListaPreciosDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      ListaPreciosDTO result = dao.updateListaPrecios(id, dto, ic);
      return new ResponseEntity<ListaPreciosDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ListaPreciosDTO> createListaPrecios(@RequestBody ListaPreciosDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      ListaPreciosDTO result = dao.createListaPrecios(dto, ic);
      return new ResponseEntity<ListaPreciosDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteListaPrecios(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteListaPrecios(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

}