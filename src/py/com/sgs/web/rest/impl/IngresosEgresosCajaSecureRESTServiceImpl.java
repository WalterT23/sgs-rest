package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.IngresosEgresosCajaDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.composite.IngresosEgresosCajaCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.IngresosEgresosCajaService;

@RestController
@RequestMapping(value = "/ingresosEgresosCaja")
@CrossOrigin()
public class IngresosEgresosCajaSecureRESTServiceImpl extends BaseSecureRESTService {

	@Autowired private IngresosEgresosCajaService dao;
	private InvocationContext ic;
	@Autowired private HttpHeaderConfig config;
	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value="",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<IngresosEgresosCajaDTO>> listIngresosEgresosCaja(HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			List<IngresosEgresosCajaDTO> result = dao.listIngresosEgresosCaja(ic);
			return new ResponseEntity<List<IngresosEgresosCajaDTO>>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<IngresosEgresosCajaDTO> showIngresosEgresosCaja(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			IngresosEgresosCajaDTO result = dao.getIngresosEgresosCaja(id, ic);
			return new ResponseEntity<IngresosEgresosCajaDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/{id}",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<IngresosEgresosCajaDTO> saveIngresosEgresosCaja(@PathVariable("id") Long id, @RequestBody IngresosEgresosCajaDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			IngresosEgresosCajaDTO result = dao.updateIngresosEgresosCaja(id, dto, ic);
			return new ResponseEntity<IngresosEgresosCajaDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@RequestMapping(value="",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<IngresosEgresosCajaDTO> createIngresosEgresosCaja(@RequestBody IngresosEgresosCajaDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			IngresosEgresosCajaDTO result = dao.createIngresosEgresosCaja(dto, ic);
			return new ResponseEntity<IngresosEgresosCajaDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}",
	method = RequestMethod.DELETE)
	public ResponseEntity deleteIngresosEgresosCaja(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			dao.deleteIngresosEgresosCaja(id, ic);
			return new ResponseEntity(HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	/**
	 * REPORTES
	 * */
	@RequestMapping(value = "/imprimir/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrintDTO> imprimir(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			PrintDTO result = dao.imprimir(id, ic);
			return new ResponseEntity<PrintDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

  @RequestMapping(value = "byid/{id}",
	      method = RequestMethod.GET,
	      produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<IngresosEgresosCajaCompositeDTO> showIngresosEgresosCajaComposite(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
	    try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      IngresosEgresosCajaCompositeDTO result = dao.getIngresosEgresosCajaCompositeDTO(id, ic);
	      return new ResponseEntity<IngresosEgresosCajaCompositeDTO>(result, HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	  }

}