package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.PuntoExpedicionDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.PuntoExpedicionService;

@RestController
@CrossOrigin()
@RequestMapping(value = "/puntoExpedicion")
public class PuntoExpedicionSecureRESTServiceImpl extends BaseSecureRESTService {

  @Autowired private PuntoExpedicionService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<PuntoExpedicionDTO>> listPuntoExpedicion(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<PuntoExpedicionDTO> result = dao.listPuntoExpedicion(ic);
      return new ResponseEntity<List<PuntoExpedicionDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<PuntoExpedicionDTO> showPuntoExpedicion(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      PuntoExpedicionDTO result = dao.getPuntoExpedicion(id, ic);
      return new ResponseEntity<PuntoExpedicionDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<PuntoExpedicionDTO> savePuntoExpedicion(@PathVariable("id") Long id, @RequestBody PuntoExpedicionDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      PuntoExpedicionDTO result = dao.updatePuntoExpedicion(id, dto, ic);
      return new ResponseEntity<PuntoExpedicionDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<PuntoExpedicionDTO> createPuntoExpedicion(@RequestBody PuntoExpedicionDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      PuntoExpedicionDTO result = dao.createPuntoExpedicion(dto, ic);
      return new ResponseEntity<PuntoExpedicionDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }


  @RequestMapping(value = "/inactivarPto/{id}",
      method = RequestMethod.PUT,
  	  consumes = MediaType.APPLICATION_JSON_VALUE,
  	  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> inactivarPtoExp(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.inactivarPtoExp(id, ic);
      return new ResponseEntity<>(null,HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  @RequestMapping(value = "/activarPto/{id}",
	      method = RequestMethod.PUT,
	  	  consumes = MediaType.APPLICATION_JSON_VALUE,
	  	  produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<?> activarPtoExp(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
	    try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      dao.activarPtoExp(id, ic);
	      return new ResponseEntity<>(null,HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	  }

}