package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.RolesDTO;
import py.com.sgs.persistence.dto.UsuarioRolDTO;
import py.com.sgs.persistence.dto.composite.RolesCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.RolesService;

@RestController
@RequestMapping(value = "/roles")
@CrossOrigin()
public class RolesSecureRESTServiceImpl extends BaseSecureRESTService {

  @Autowired private RolesService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<RolesCompositeDTO>> listRoles(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<RolesCompositeDTO> result = dao.listRoles(ic);
      return new ResponseEntity<List<RolesCompositeDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  @RequestMapping(value="/byUsuario/{idUsuario}",
	      method = RequestMethod.GET,
	      produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<List<RolesCompositeDTO>> getByUsuario(@PathVariable("idUsuario") Long idUsuario, HttpServletRequest request) throws ServiceException { 
	    try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      List<RolesCompositeDTO> result = dao.getByUsuario(idUsuario, ic);
	      return new ResponseEntity<List<RolesCompositeDTO>>(result, HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	  }
  
  @RequestMapping(value="/dispByUsuario/{idUsuario}",
	      method = RequestMethod.GET,
	      produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<List<RolesCompositeDTO>> getDispByUsuario(@PathVariable("idUsuario") Long idUsuario, HttpServletRequest request) throws ServiceException { 
	    try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      List<RolesCompositeDTO> result = dao.getDispByUsuario(idUsuario, ic);
	      return new ResponseEntity<List<RolesCompositeDTO>>(result, HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	  }
  
  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<RolesDTO> showRoles(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      RolesDTO result = dao.getRoles(id, ic);
      return new ResponseEntity<RolesDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<RolesDTO> saveRoles(@PathVariable("id") Long id, @RequestBody RolesDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      RolesDTO result = dao.updateRoles(id, dto, ic);
      return new ResponseEntity<RolesDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<RolesDTO> createRoles(@RequestBody RolesDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      RolesDTO result = dao.createRoles(dto, ic);
      return new ResponseEntity<RolesDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteRoles(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteRoles(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  /**
   * USUARIO - ROL
   * */
  
  @RequestMapping(value="/usuarioRol",
	      method = RequestMethod.POST,
	      consumes = MediaType.APPLICATION_JSON_VALUE,
	      produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<UsuarioRolDTO> createUsuarioRol(@RequestBody UsuarioRolDTO dto, HttpServletRequest request) throws ServiceException {
	    try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      UsuarioRolDTO result = dao.createUsuarioRol(dto, ic);
	      return new ResponseEntity<UsuarioRolDTO>(result, HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	  }
  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/usuarioRol/{idUsuario}/{idRol}",
      method = RequestMethod.DELETE)
	  public ResponseEntity deleteUsuarioRol(@PathVariable("idUsuario") Long idUsuario, @PathVariable("idRol") Long idRol, HttpServletRequest request) throws ServiceException {
	    try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      UsuarioRolDTO dto = new UsuarioRolDTO();
	      dto.setIdUsuario(idUsuario);
	      dto.setIdRol(idRol);
	      dao.deleteUsuarioRol(dto, ic);
	      return new ResponseEntity(HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
  	}
}