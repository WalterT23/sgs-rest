package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.composite.OrdenDeCompraCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.OrdenDeCompraService;

@CrossOrigin()
@RestController
@RequestMapping(value = "/ordenDeCompra")

public class OrdenDeCompraSecureRESTServiceImpl extends BaseSecureRESTService {

	@Autowired private OrdenDeCompraService dao;
	private InvocationContext ic;
	@Autowired private HttpHeaderConfig config;
	private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value="",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrdenDeCompraCompositeDTO>> listOrdenDeCompra(HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			List<OrdenDeCompraCompositeDTO> result = dao.listOrdenDeCompra(ic);
			return new ResponseEntity<List<OrdenDeCompraCompositeDTO>>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}
	
	@RequestMapping(value="/confirmadas",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrdenDeCompraCompositeDTO>> listConfirmadas(HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			List<OrdenDeCompraCompositeDTO> result = dao.listConfirmadas(ic);
			return new ResponseEntity<List<OrdenDeCompraCompositeDTO>>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrdenDeCompraCompositeDTO> showOrdenDeCompra(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			OrdenDeCompraCompositeDTO result = dao.getOrdenDeCompraById(id, ic);
			return new ResponseEntity<OrdenDeCompraCompositeDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}
	
	
	 @RequestMapping(value = "/byNroOc/{nroOrdenCompra}",
		      method = RequestMethod.GET,
		      produces = MediaType.APPLICATION_JSON_VALUE)
		  public ResponseEntity<OrdenDeCompraCompositeDTO> showOrdenDeCompra(@PathVariable("nroOrdenCompra") String  nroOrdenCompra, HttpServletRequest request) throws ServiceException { 
		    try {
		      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		      OrdenDeCompraCompositeDTO result = dao.getOrdenDeCompraByNroOC(nroOrdenCompra, ic);
		      return new ResponseEntity<OrdenDeCompraCompositeDTO>(result, HttpStatus.OK);
		    }
		    catch (ServiceException e){
		      logger.error(e.getMessage(), e);
		      throw e;
		    }
		    catch (Exception e){
		      logger.error(e.getMessage(), e);
		      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		    }
		  }

	@RequestMapping(value = "/{id}",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrdenDeCompraCompositeDTO> saveOrdenDeCompra(@PathVariable("id") Long id, @RequestBody OrdenDeCompraCompositeDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			OrdenDeCompraCompositeDTO result = dao.updateOrdenDeCompra(id, dto, ic);
			return new ResponseEntity<OrdenDeCompraCompositeDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@RequestMapping(value="",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrdenDeCompraCompositeDTO> createOrdenDeCompra(@RequestBody OrdenDeCompraCompositeDTO dto, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			OrdenDeCompraCompositeDTO result = dao.createOrdenDeCompra(dto, ic);
			return new ResponseEntity<OrdenDeCompraCompositeDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}",
	method = RequestMethod.DELETE)
	public ResponseEntity deleteOrdenDeCompra(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			dao.deleteOrdenDeCompra(id, ic);
			return new ResponseEntity(HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

	@RequestMapping(value = "/confirmarOC/{id}",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrdenDeCompraCompositeDTO> confirmOrdenDeCompra(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			OrdenDeCompraCompositeDTO result = dao.confirmarOrdenDeCompra(id, ic);
			return new ResponseEntity<OrdenDeCompraCompositeDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}
	
	@RequestMapping(value = "/anularOC/{id}",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrdenDeCompraCompositeDTO> anularOrdenDeCompra(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			OrdenDeCompraCompositeDTO result = dao.anularOrdenDeCompra(id, ic);
			return new ResponseEntity<OrdenDeCompraCompositeDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}
	
	/**
	 * REPORTES
	 * */
	@RequestMapping(value = "/imprimir/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrintDTO> imprimirDeCompra(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			PrintDTO result = dao.imprimirOrdenDeCompra(id, ic);
			return new ResponseEntity<PrintDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}

}