package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.InfoRefOpcService;

@CrossOrigin
@RestController
@RequestMapping(value = "/inforefopc")
public class InfoRefOpcSecureRESTServiceImpl extends BaseSecureRESTService {
  
	@Autowired private InfoRefOpcService dao;
	private InvocationContext ic;
	@Autowired private HttpHeaderConfig config;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(value="",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<InfoRefOpcDTO>> listInfoRefOpc(HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		      List<InfoRefOpcDTO> result = dao.listInfoRefOpc(ic);
		      return new ResponseEntity<List<InfoRefOpcDTO>>(result, HttpStatus.OK);
		}
	    catch (ServiceException e){
		      logger.error(e.getMessage(), e);
		      throw e;
	    }
		catch (Exception e){
		      logger.error(e.getMessage(), e);
		      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	}

	@RequestMapping(value = "/{id}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<InfoRefOpcDTO> showInfoRefOpc(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
		try {
		    ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		    InfoRefOpcDTO result = dao.getInfoRefOpcById(id, ic);
		      return new ResponseEntity<InfoRefOpcDTO>(result, HttpStatus.OK);
		    }
		    catch (ServiceException e){
		      logger.error(e.getMessage(), e);
		      throw e;
		    }
		    catch (Exception e){
		      logger.error(e.getMessage(), e);
		      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		    }
		  }
	@RequestMapping(value="/byIdRef/{idInfoRef}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<InfoRefOpcDTO>> getInfoRefOpcByIdRef(@PathVariable("idInfoRef") Integer idInfoRef, HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			List<InfoRefOpcDTO> result = dao.getInfoRefOpcByIdRef(idInfoRef, ic);
			return new ResponseEntity<List<InfoRefOpcDTO>>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}
	
	@RequestMapping(value="/byCodRef/{codInfoRef}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<InfoRefOpcDTO>> getInfoRefOpcByCodRef(@PathVariable("codInfoRef") String codInfoRef, HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			List<InfoRefOpcDTO> result = dao.getInfoRefOpcByCodRef(codInfoRef, ic);
			return new ResponseEntity<List<InfoRefOpcDTO>>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}
	
	@RequestMapping(value = "/byAbv/{abv}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<InfoRefOpcDTO> getInfoRefOpcByAbv(@PathVariable("abv") String abv, HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			InfoRefOpcDTO result = dao.getInfoRefOpcByAbv(abv, ic);
			return new ResponseEntity<InfoRefOpcDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}
	
	@RequestMapping(value = "/{id}",
	    method = RequestMethod.PUT,
        consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<InfoRefOpcDTO> saveInfoRefOpc(@PathVariable("id") Long id, @RequestBody InfoRefOpcDTO dto, HttpServletRequest request) throws ServiceException {
		    try {
		      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		      InfoRefOpcDTO result = dao.updateInfoRefOpc(id, dto, ic);
		      return new ResponseEntity<InfoRefOpcDTO>(result, HttpStatus.OK);
		    }
		    catch (ServiceException e){
		      logger.error(e.getMessage(), e);
		      throw e;
		    }
		    catch (Exception e){
		      logger.error(e.getMessage(), e);
		      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		    }
	}
	
  @RequestMapping(value = "",  
      method = RequestMethod.POST, 
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<InfoRefOpcDTO> createInfoRefOpc(@RequestBody InfoRefOpcDTO parameterDTO, HttpServletRequest request) {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      InfoRefOpcDTO result = dao.createInfoRefOpc(parameterDTO, ic);
      return new ResponseEntity<InfoRefOpcDTO>(result, HttpStatus.OK);
    } 
    catch (ServiceException e) {
      logger.error(e.getMessage(), e);
      throw e;
    } 
    catch (Exception e) {
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.error");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}", 
      method = RequestMethod.DELETE)
  public ResponseEntity deleteInfoRefOpc(@PathVariable("id") Long id, HttpServletRequest request) {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteInfoRefOpc(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    } 
    catch (ServiceException e) {
      logger.error(e.getMessage(), e);
      throw e;
    } 
    catch (Exception e) {
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.error");
    }
  }
  
}
