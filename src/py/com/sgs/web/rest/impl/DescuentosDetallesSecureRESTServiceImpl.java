package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.DescuentosDetallesDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.DescuentosDetallesService;

@RestController
@CrossOrigin()
@RequestMapping(value = "/descuentosDetalles")
public class DescuentosDetallesSecureRESTServiceImpl {

  @Autowired private DescuentosDetallesService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<DescuentosDetallesDTO>> listDescuentosDetalles(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<DescuentosDetallesDTO> result = dao.listDescuentosDetalles(ic);
      return new ResponseEntity<List<DescuentosDetallesDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<DescuentosDetallesDTO> showDescuentosDetalles(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      DescuentosDetallesDTO result = dao.getDescuentosDetalles(id, ic);
      return new ResponseEntity<DescuentosDetallesDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/byIdDescuento/{id}",
		  method = RequestMethod.GET,
		  produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<DescuentosDetallesDTO>> detallesByIdDescuento(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
	  try {
		  ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		  List<DescuentosDetallesDTO> result = dao.getDetallesByIdDescuento(id, ic);
		  return new ResponseEntity<List<DescuentosDetallesDTO>>(result, HttpStatus.OK);
	  }
	  catch (ServiceException e){
		  logger.error(e.getMessage(), e);
		  throw e;
	  }
	  catch (Exception e){
		  logger.error(e.getMessage(), e);
		  throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	  }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<DescuentosDetallesDTO> saveDescuentosDetalles(@PathVariable("id") Long id, @RequestBody DescuentosDetallesDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      DescuentosDetallesDTO result = dao.updateDescuentosDetalles(id, dto, ic);
      return new ResponseEntity<DescuentosDetallesDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<DescuentosDetallesDTO> createDescuentosDetalles(@RequestBody DescuentosDetallesDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      DescuentosDetallesDTO result = dao.createDescuentosDetalles(dto, ic);
      return new ResponseEntity<DescuentosDetallesDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteDescuentosDetalles(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteDescuentosDetalles(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

}