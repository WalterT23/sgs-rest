package py.com.sgs.web.rest.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.deser.Deserializers.Base;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.CabeceraVentaDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.composite.CabeceraVentaCompositeDTO;
import py.com.sgs.persistence.dto.consulta.ConsultaHorariosDTO;
import py.com.sgs.persistence.dto.consulta.ConsultaVentaDTO;
import py.com.sgs.persistence.dto.consulta.InfoVentasDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.CabeceraVentaService;

@RestController
@CrossOrigin
@RequestMapping(value = "/cabeceraVenta")
public class CabeceraVentaSecureRESTServiceImpl extends BaseSecureRESTService{

  @Autowired private CabeceraVentaService dao;
  private InvocationContext ic;
  @Autowired private HttpHeaderConfig config;
  private Logger logger = LoggerFactory.getLogger(getClass());

  @RequestMapping(value="",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<CabeceraVentaDTO>> listCabeceraVenta(HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      List<CabeceraVentaDTO> result = dao.listCabeceraVenta(ic);
      return new ResponseEntity<List<CabeceraVentaDTO>>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CabeceraVentaCompositeDTO> showCabeceraVenta(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      CabeceraVentaCompositeDTO result = dao.getCabeceraVenta(id, ic);
      return new ResponseEntity<CabeceraVentaCompositeDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value = "/{id}",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CabeceraVentaDTO> saveCabeceraVenta(@PathVariable("id") Long id, @RequestBody CabeceraVentaDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      CabeceraVentaDTO result = dao.updateCabeceraVenta(id, dto, ic);
      return new ResponseEntity<CabeceraVentaDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @RequestMapping(value="/generarVenta",
      method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CabeceraVentaCompositeDTO> createCabeceraVenta(@RequestBody CabeceraVentaCompositeDTO dto, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      CabeceraVentaCompositeDTO result = dao.createCabeceraVenta(dto, ic);
      return new ResponseEntity<CabeceraVentaCompositeDTO>(result, HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }

  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/{id}",
      method = RequestMethod.DELETE)
  public ResponseEntity deleteCabeceraVenta(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException {
    try {
      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
      dao.deleteCabeceraVenta(id, ic);
      return new ResponseEntity(HttpStatus.OK);
    }
    catch (ServiceException e){
      logger.error(e.getMessage(), e);
      throw e;
    }
    catch (Exception e){
      logger.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
    }
  }
  
  /**
	 * REPORTES
	 * */
	@RequestMapping(value = "/imprimir/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrintDTO> imprimirFactura(@PathVariable("id") Long id, HttpServletRequest request) throws ServiceException { 
		try {
			ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
			PrintDTO result = dao.imprimirFacturaVenta(id, ic);
			return new ResponseEntity<PrintDTO>(result, HttpStatus.OK);
		}
		catch (ServiceException e){
			logger.error(e.getMessage(), e);
			throw e;
		}
		catch (Exception e){
			logger.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		}
	}
	
	@RequestMapping(value="/getResumenVenta",
		      method = RequestMethod.POST,
		      consumes = MediaType.APPLICATION_JSON_VALUE,
		      produces = MediaType.APPLICATION_JSON_VALUE)
	  public ResponseEntity<InfoVentasDTO> getResumenVenta(@RequestBody ConsultaVentaDTO dto, HttpServletRequest request) throws ServiceException {
	    try {
	      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
	      InfoVentasDTO result = dao.getResumenVenta(dto, ic);
	      return new ResponseEntity<InfoVentasDTO>(result, HttpStatus.OK);
	    }
	    catch (ServiceException e){
	      logger.error(e.getMessage(), e);
	      throw e;
	    }
	    catch (Exception e){
	      logger.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
	    }
	  }
	
	@RequestMapping(value = "/getByFactura/{nroFactura}",
		      method = RequestMethod.GET,
		      produces = MediaType.APPLICATION_JSON_VALUE)
		  public ResponseEntity<CabeceraVentaCompositeDTO> showCabeceraVenta(@PathVariable("nroFactura") String nroFactura, HttpServletRequest request) throws ServiceException { 
		    try {
		      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		      CabeceraVentaCompositeDTO result = dao.getCabeceraVentaByFactura(nroFactura, ic);
		      return new ResponseEntity<CabeceraVentaCompositeDTO>(result, HttpStatus.OK);
		    }
		    catch (ServiceException e){
		      logger.error(e.getMessage(), e);
		      throw e;
		    }
		    catch (Exception e){
		      logger.error(e.getMessage(), e);
		      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		    }
		  }

	@RequestMapping(value="/ventasPorHorarios",
		      method = RequestMethod.POST,
		      consumes = MediaType.APPLICATION_JSON_VALUE,
		      produces = MediaType.APPLICATION_JSON_VALUE)
		  public ResponseEntity<ConsultaHorariosDTO> ventasPorHorarios(@RequestBody ConsultaHorariosDTO dto, HttpServletRequest request) throws ServiceException {
		    try {
		      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		      ConsultaHorariosDTO result = dao.ventasPorHorarios(dto, ic);
		      return new ResponseEntity<ConsultaHorariosDTO>(result, HttpStatus.OK);
		    }
		    catch (ServiceException e){
		      logger.error(e.getMessage(), e);
		      throw e;
		    }
		    catch (Exception e){
		      logger.error(e.getMessage(), e);
		      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		    }
		  }
	
	@RequestMapping(value="/ventasPorHorarios/impresion",
		      method = RequestMethod.POST,
		      consumes = MediaType.APPLICATION_JSON_VALUE,
		      produces = MediaType.APPLICATION_JSON_VALUE)
		  public ResponseEntity<FileReportDTO> ventasPorHorariosImpresion(@RequestBody ConsultaHorariosDTO dto, HttpServletRequest request) throws ServiceException {
		    try {
		      ic = (InvocationContext) request.getAttribute(this.config.getContextAttributeName());
		      FileReportDTO result = dao.ventasPorHorariosImpresion(dto, ic);
		      return new ResponseEntity<FileReportDTO>(result, HttpStatus.OK);
		    }
		    catch (ServiceException e){
		      logger.error(e.getMessage(), e);
		      throw e;
		    }
		    catch (Exception e){
		      logger.error(e.getMessage(), e);
		      throw new ServiceException(ErrorCode.INTERNAL_ERROR, "internal.dataAccessError");
		    }
		  }
	
}