package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.ProveedoresContactoDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ProveedoresContactoService {
  List<ProveedoresContactoDTO> listContactosByProveedor(Long idProveedor, InvocationContext ic) throws ServiceException;

  ProveedoresContactoDTO getProveedoresContacto(Long id, InvocationContext ic) throws ServiceException;

  void deleteProveedoresContacto(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ProveedoresContactoDTO updateProveedoresContacto(Long id, ProveedoresContactoDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ProveedoresContactoDTO createProveedoresContacto(ProveedoresContactoDTO dto, InvocationContext ic) throws ServiceException;


}