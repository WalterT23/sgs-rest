package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.DetalleVentaDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface DetalleVentaService {
  List<DetalleVentaDTO> listDetalleVenta(InvocationContext ic) throws ServiceException;

  DetalleVentaDTO getDetalleVenta(Long id, InvocationContext ic) throws ServiceException;

  void deleteDetalleVenta(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DetalleVentaDTO updateDetalleVenta(Long id, DetalleVentaDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DetalleVentaDTO createDetalleVenta(DetalleVentaDTO dto, InvocationContext ic) throws ServiceException;

}