package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.NotaCreditoVentaDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.composite.NotaCreditoVentaCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface NotaCreditoVentaService {
  List<NotaCreditoVentaCompositeDTO> listNotaCreditoVenta(InvocationContext ic) throws ServiceException;

  NotaCreditoVentaDTO getNotaCreditoVenta(Long id, InvocationContext ic) throws ServiceException;

  void deleteNotaCreditoVenta(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  NotaCreditoVentaDTO updateNotaCreditoVenta(Long id, NotaCreditoVentaDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  NotaCreditoVentaCompositeDTO createNotaCreditoVenta(NotaCreditoVentaCompositeDTO dto, InvocationContext ic) throws ServiceException;
  
  PrintDTO imprimirNotaCredito(Long id, InvocationContext ic) throws ServiceException;

}