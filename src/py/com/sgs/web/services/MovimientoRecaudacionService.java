package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.MovimientoRecaudacionDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface MovimientoRecaudacionService {
  List<MovimientoRecaudacionDTO> listMovimientoRecaudacion(InvocationContext ic) throws ServiceException;

  MovimientoRecaudacionDTO getMovimientoRecaudacion(Long id, InvocationContext ic) throws ServiceException;

  void deleteMovimientoRecaudacion(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  MovimientoRecaudacionDTO updateMovimientoRecaudacion(Long id, MovimientoRecaudacionDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  MovimientoRecaudacionDTO createMovimientoRecaudacion(MovimientoRecaudacionDTO dto, InvocationContext ic) throws ServiceException;

}