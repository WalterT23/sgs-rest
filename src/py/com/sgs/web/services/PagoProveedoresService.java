package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.EntregaCajaDTO;
import py.com.sgs.persistence.dto.PagoProveedoresDTO;
import py.com.sgs.persistence.dto.composite.PagoFacturasCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface PagoProveedoresService {
  List<PagoProveedoresDTO> listPagoProveedores(InvocationContext ic) throws ServiceException;

  PagoProveedoresDTO getPagoProveedores(Long id, InvocationContext ic) throws ServiceException;

  void deletePagoProveedores(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  PagoProveedoresDTO updatePagoProveedores(Long id, PagoProveedoresDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  PagoProveedoresDTO createPagoProveedores(PagoProveedoresDTO dto, InvocationContext ic) throws ServiceException;

  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  PagoFacturasCompositeDTO pagarFactura(PagoFacturasCompositeDTO dto, InvocationContext ic) throws ServiceException;
}