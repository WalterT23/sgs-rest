package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.DescuentosDTO;
import py.com.sgs.persistence.dto.composite.DescuentosCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface DescuentosService {
  List<DescuentosCompositeDTO> listDescuentos(InvocationContext ic) throws ServiceException;

  DescuentosDTO getDescuentos(Long id, InvocationContext ic) throws ServiceException;

  void deleteDescuentos(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DescuentosDTO updateDescuentos(Long id, DescuentosCompositeDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DescuentosDTO createDescuentos(DescuentosCompositeDTO dto, InvocationContext ic) throws ServiceException;

}