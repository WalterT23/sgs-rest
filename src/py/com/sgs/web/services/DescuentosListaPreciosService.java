package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.DescuentosListaPreciosDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface DescuentosListaPreciosService {
  List<DescuentosListaPreciosDTO> listDescuentosListaPrecios(InvocationContext ic) throws ServiceException;
  
  List<DescuentosListaPreciosDTO> descuentosListaPreciosByLista(Long id, InvocationContext ic) throws ServiceException;

  DescuentosListaPreciosDTO getDescuentosListaPrecios(Long id, InvocationContext ic) throws ServiceException;

  void deleteDescuentosListaPrecios(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  void vencerAso(Long id, InvocationContext ic) throws ServiceException;

  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DescuentosListaPreciosDTO updateDescuentosListaPrecios(Long id, DescuentosListaPreciosDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DescuentosListaPreciosDTO createDescuentosListaPrecios(DescuentosListaPreciosDTO dto, InvocationContext ic) throws ServiceException;

}