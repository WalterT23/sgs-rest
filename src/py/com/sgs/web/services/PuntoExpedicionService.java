package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.PuntoExpedicionDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface PuntoExpedicionService {
  List<PuntoExpedicionDTO> listPuntoExpedicion(InvocationContext ic) throws ServiceException;

  PuntoExpedicionDTO getPuntoExpedicion(Long id, InvocationContext ic) throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  void inactivarPtoExp(Long id, InvocationContext ic) throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  void activarPtoExp(Long id, InvocationContext ic) throws ServiceException;

  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  PuntoExpedicionDTO updatePuntoExpedicion(Long id, PuntoExpedicionDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  PuntoExpedicionDTO createPuntoExpedicion(PuntoExpedicionDTO dto, InvocationContext ic) throws ServiceException;

}