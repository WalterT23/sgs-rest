package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.EntregaCajaDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface EntregaCajaService {
  List<EntregaCajaDTO> listEntregaCaja(InvocationContext ic) throws ServiceException;

  EntregaCajaDTO getEntregaCaja(Long id, InvocationContext ic) throws ServiceException;

  void deleteEntregaCaja(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  EntregaCajaDTO updateEntregaCaja(Long id, EntregaCajaDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  EntregaCajaDTO createEntregaCaja(EntregaCajaDTO dto, InvocationContext ic) throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  EntregaCajaDTO generarEntregaCaja(EntregaCajaDTO dto, InvocationContext ic) throws ServiceException;
  
  PrintDTO imprimirEntrega(Long id, InvocationContext ic) throws ServiceException;

}