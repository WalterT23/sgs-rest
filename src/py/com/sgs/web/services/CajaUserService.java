package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.CajaUserDTO;
import py.com.sgs.persistence.dto.UsuariosDTO;
import py.com.sgs.persistence.dto.composite.CajaUserCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface CajaUserService {

  List <UsuariosDTO> listCajeros(InvocationContext ic) throws ServiceException;	
  List<CajaUserCompositeDTO> listCajaUser(InvocationContext ic) throws ServiceException;
  CajaUserCompositeDTO getCajaUser(Long id, InvocationContext ic) throws ServiceException;
  CajaUserCompositeDTO getCajaUserByIdPtoExp(Long idPuntoExpedicion, InvocationContext ic) throws ServiceException;
  void deleteCajaUser(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  CajaUserDTO updateCajaUser(Long id, CajaUserDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  CajaUserDTO createCajaUser(CajaUserDTO dto, InvocationContext ic) throws ServiceException;

}