package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.AperturaCierreDTO;
import py.com.sgs.persistence.dto.composite.AperturaCierreCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface AperturaCierreService {
  List<AperturaCierreDTO> listAperturaCierre(InvocationContext ic) throws ServiceException;

  AperturaCierreCompositeDTO getAperturaCierre(Long id, InvocationContext ic) throws ServiceException;

  AperturaCierreCompositeDTO getAperturaCierreVigenteByIdPuntoExp(Long idPuntoExpedicion, InvocationContext ic) throws ServiceException;
  void deleteAperturaCierre(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  AperturaCierreDTO updateAperturaCierre(Long id, AperturaCierreDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  AperturaCierreDTO createAperturaCierre(AperturaCierreDTO dto, InvocationContext ic) throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  AperturaCierreDTO cerrarAperturaCierre(Long id, AperturaCierreDTO dto, InvocationContext ic) throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  AperturaCierreDTO generarAperturaCierre(AperturaCierreDTO dto, InvocationContext ic) throws ServiceException;
  
}