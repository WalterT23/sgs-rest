package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.EstadoCuentaProveedorDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.FiltroBalanceDTO;
import py.com.sgs.persistence.dto.FiltroFalloDTO;
import py.com.sgs.persistence.dto.RecaudacionDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface RecaudacionService {
  List<RecaudacionDTO> listRecaudacion(InvocationContext ic) throws ServiceException;

  RecaudacionDTO getRecaudacion(Long id, InvocationContext ic) throws ServiceException;
  
  FiltroBalanceDTO getReportBalance (FiltroBalanceDTO dto, InvocationContext ic)throws ServiceException;
  
  FileReportDTO getReporteRecaImpresion(FiltroBalanceDTO dto, InvocationContext ic) throws ServiceException, Exception;
  

  void deleteRecaudacion(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  RecaudacionDTO updateRecaudacion(Long id, RecaudacionDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  RecaudacionDTO createRecaudacion(RecaudacionDTO dto, InvocationContext ic) throws ServiceException;

}