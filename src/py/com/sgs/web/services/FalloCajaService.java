package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.FalloCajaDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.FiltroComprasDTO;
import py.com.sgs.persistence.dto.FiltroFalloDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface FalloCajaService {
  List<FalloCajaDTO> listFalloCaja(FiltroFalloDTO dto, InvocationContext ic) throws ServiceException;

  FalloCajaDTO getFalloCaja(Long id, InvocationContext ic) throws ServiceException;
  
  FiltroFalloDTO getReportFallo (FiltroFalloDTO dto, InvocationContext ic)throws ServiceException;
  
  FileReportDTO getFallosImpresion (FiltroFalloDTO dto, InvocationContext ic) throws ServiceException, Exception;
  

  void deleteFalloCaja(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  FalloCajaDTO updateFalloCaja(Long id, FalloCajaDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  FalloCajaDTO createFalloCaja(FalloCajaDTO dto, InvocationContext ic) throws ServiceException;

}