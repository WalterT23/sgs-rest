package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.NotaCreditoCompraDTO;
import py.com.sgs.persistence.dto.composite.NotaCreditoCompraCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface NotaCreditoCompraService {
  List<NotaCreditoCompraCompositeDTO> listNotaCreditoCompra(InvocationContext ic) throws ServiceException;

  NotaCreditoCompraCompositeDTO getNotaCreditoCompra(Long id, InvocationContext ic) throws ServiceException;

  void deleteNotaCreditoCompra(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  NotaCreditoCompraDTO updateNotaCreditoCompra(Long id, NotaCreditoCompraDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  NotaCreditoCompraCompositeDTO createNotaCreditoCompra(NotaCreditoCompraCompositeDTO dto, InvocationContext ic) throws ServiceException;

}