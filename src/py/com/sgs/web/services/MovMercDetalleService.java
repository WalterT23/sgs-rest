package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.MovMercDetalleDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface MovMercDetalleService {
  List<MovMercDetalleDTO> listMovMercDetalle(InvocationContext ic) throws ServiceException;

  MovMercDetalleDTO getMovMercDetalle(Long id, InvocationContext ic) throws ServiceException;

  List<MovMercDetalleDTO> getMovMercDetByIdMov(Long idMov, InvocationContext ic) throws ServiceException;
  
  void deleteMovMercDetalle(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  MovMercDetalleDTO updateMovMercDetalle(Long id, MovMercDetalleDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  MovMercDetalleDTO createMovMercDetalle(MovMercDetalleDTO dto, InvocationContext ic) throws ServiceException;

}