package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.DetallePagoDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface DetallePagoService {
  List<DetallePagoDTO> listDetallePago(InvocationContext ic) throws ServiceException;

  DetallePagoDTO getDetallePago(Long id, InvocationContext ic) throws ServiceException;

  void deleteDetallePago(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DetallePagoDTO updateDetallePago(Long id, DetallePagoDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DetallePagoDTO createDetallePago(DetallePagoDTO dto, InvocationContext ic) throws ServiceException;

}