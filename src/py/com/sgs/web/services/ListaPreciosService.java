package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.ListaPreciosDTO;
import py.com.sgs.persistence.dto.composite.ListaPreciosCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ListaPreciosService {
  List<ListaPreciosCompositeDTO> listListaPrecios(InvocationContext ic) throws ServiceException;
  
  List<ListaPreciosCompositeDTO> listaPreciosVigentes(InvocationContext ic) throws ServiceException;
  
  ListaPreciosCompositeDTO getListaPrecios(Long id, InvocationContext ic) throws ServiceException;

  void deleteListaPrecios(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ListaPreciosDTO updateListaPrecios(Long id, ListaPreciosDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ListaPreciosDTO createListaPrecios(ListaPreciosDTO dto, InvocationContext ic) throws ServiceException;

}