package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.DescuentosDetallesDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface DescuentosDetallesService {
  List<DescuentosDetallesDTO> listDescuentosDetalles(InvocationContext ic) throws ServiceException;

  DescuentosDetallesDTO getDescuentosDetalles(Long id, InvocationContext ic) throws ServiceException;
  
  List<DescuentosDetallesDTO> getDetallesByIdDescuento(Long id, InvocationContext ic) throws ServiceException;

  void deleteDescuentosDetalles(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DescuentosDetallesDTO updateDescuentosDetalles(Long id, DescuentosDetallesDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DescuentosDetallesDTO createDescuentosDetalles(DescuentosDetallesDTO dto, InvocationContext ic) throws ServiceException;

}