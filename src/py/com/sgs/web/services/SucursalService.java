package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.SucursalDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface SucursalService {
  List<SucursalDTO> listSucursal(InvocationContext ic) throws ServiceException;

  SucursalDTO getSucursal(Long id, InvocationContext ic) throws ServiceException;
  
  List<SucursalDTO> sucDispByUsuario(Long id, InvocationContext ic) throws ServiceException;

  void deleteSucursal(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  SucursalDTO updateSucursal(Long id, SucursalDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  SucursalDTO createSucursal(SucursalDTO dto, InvocationContext ic) throws ServiceException;

}