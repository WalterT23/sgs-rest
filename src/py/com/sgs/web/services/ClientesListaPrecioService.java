package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.ClientesListaPrecioDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ClientesListaPrecioService {
  List<ClientesListaPrecioDTO> listClientesListaPrecio(InvocationContext ic) throws ServiceException;

  ClientesListaPrecioDTO getClientesListaPrecio(Long id, InvocationContext ic) throws ServiceException;
  
  List<ClientesListaPrecioDTO> getClientesLPbyCliente(Long idCliente, InvocationContext ic) throws ServiceException;
  
  void deleteClientesListaPrecio(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ClientesListaPrecioDTO updateClientesListaPrecio(Long id, ClientesListaPrecioDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ClientesListaPrecioDTO createClientesListaPrecio(ClientesListaPrecioDTO dto, InvocationContext ic) throws ServiceException;

}