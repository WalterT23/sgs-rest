package py.com.sgs.web.services;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface InfoRefOpcService {
 
  List<InfoRefOpcDTO> listInfoRefOpc(InvocationContext ic);
  
  List<InfoRefOpcDTO> getInfoRefOpcByIdRef(Integer idInfoRef, InvocationContext ic);
  
  List<InfoRefOpcDTO> getInfoRefOpcByCodRef(String codInfoRef, InvocationContext ic);
  
  InfoRefOpcDTO getInfoRefOpcById(Long id, InvocationContext ic);
  
  InfoRefOpcDTO getInfoRefOpcByAbv(String abv, InvocationContext ic);
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  InfoRefOpcDTO createInfoRefOpc(InfoRefOpcDTO dto, InvocationContext ic)throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  InfoRefOpcDTO updateInfoRefOpc(Long id, InfoRefOpcDTO dto, InvocationContext ic)throws ServiceException;
  
  void deleteInfoRefOpc(Long id, InvocationContext ic)throws ServiceException;
 
}
