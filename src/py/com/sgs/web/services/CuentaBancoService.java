package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.CuentaBancoDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface CuentaBancoService {
  List<CuentaBancoDTO> listCuentaBanco(InvocationContext ic) throws ServiceException;
  List<CuentaBancoDTO> listCuentaBancoByIdBanco(Long idBanco, InvocationContext ic) throws ServiceException;

  CuentaBancoDTO getCuentaBanco(Long id, InvocationContext ic) throws ServiceException;

  void deleteCuentaBanco(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  CuentaBancoDTO updateCuentaBanco(Long id, CuentaBancoDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  CuentaBancoDTO createCuentaBanco(CuentaBancoDTO dto, InvocationContext ic) throws ServiceException;

}