package py.com.sgs.web.services;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.UsuariosDTO;
import py.com.sgs.persistence.dto.composite.UsuariosCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface UsuariosService {
  List<UsuariosCompositeDTO> listUsuarios(InvocationContext ic) throws ServiceException;
  
  List<UsuariosCompositeDTO> listRolesUsuario(InvocationContext ic) throws ServiceException;

  UsuariosCompositeDTO getUsuarios(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  UsuariosDTO updateUsuarios(Long id, UsuariosDTO dto, InvocationContext ic) throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  UsuariosDTO inactivarUsuario(UsuariosDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  UsuariosDTO createUsuarios(UsuariosDTO dto, InvocationContext ic) throws ServiceException;
  
  UsuariosCompositeDTO validarUsuario(UsuariosDTO dto, InvocationContext ic) throws ServiceException;
}