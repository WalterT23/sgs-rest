package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.CabeceraVentaDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.RepoMovimientoProductosDTO;
import py.com.sgs.persistence.dto.composite.CabeceraVentaCompositeDTO;
import py.com.sgs.persistence.dto.consulta.ConsultaHorariosDTO;
import py.com.sgs.persistence.dto.consulta.ConsultaVentaDTO;
import py.com.sgs.persistence.dto.consulta.InfoVentasDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface CabeceraVentaService {

	List<CabeceraVentaDTO> listCabeceraVenta(InvocationContext ic) throws ServiceException;

	CabeceraVentaCompositeDTO getCabeceraVenta(Long id, InvocationContext ic) throws ServiceException;

	CabeceraVentaCompositeDTO getCabeceraVentaByFactura(String nroFactura, InvocationContext ic) throws ServiceException;

	void deleteCabeceraVenta(Long id, InvocationContext ic) throws ServiceException;

	@Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
	CabeceraVentaDTO updateCabeceraVenta(Long id, CabeceraVentaDTO dto, InvocationContext ic) throws ServiceException;

	@Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
	CabeceraVentaCompositeDTO createCabeceraVenta(CabeceraVentaCompositeDTO dto, InvocationContext ic) throws ServiceException;

	PrintDTO imprimirFacturaVenta(Long id, InvocationContext ic) throws ServiceException;

	InfoVentasDTO getResumenVenta(ConsultaVentaDTO dto, InvocationContext ic) throws ServiceException;
	
	ConsultaHorariosDTO ventasPorHorarios(ConsultaHorariosDTO param, InvocationContext ic) throws ServiceException;
	
	FileReportDTO ventasPorHorariosImpresion(ConsultaHorariosDTO dto, InvocationContext ic) throws ServiceException, Exception;

}