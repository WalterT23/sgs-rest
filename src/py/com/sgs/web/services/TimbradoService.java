package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.TimbradoDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface TimbradoService {
  List<TimbradoDTO> listTimbrado(InvocationContext ic) throws ServiceException;

  TimbradoDTO getTimbrado(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  TimbradoDTO updateTimbrado(Long id, TimbradoDTO dto, InvocationContext ic) throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  void vencerTimbrado(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  TimbradoDTO createTimbrado(TimbradoDTO dto, InvocationContext ic) throws ServiceException;

}