package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.NotaCreditoVentaDetalleDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface NotaCreditoVentaDetalleService {
  List<NotaCreditoVentaDetalleDTO> listNotaCreditoVentaDetalle(InvocationContext ic) throws ServiceException;

  NotaCreditoVentaDetalleDTO getNotaCreditoVentaDetalle(Long id, InvocationContext ic) throws ServiceException;

  void deleteNotaCreditoVentaDetalle(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  NotaCreditoVentaDetalleDTO updateNotaCreditoVentaDetalle(Long id, NotaCreditoVentaDetalleDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  NotaCreditoVentaDetalleDTO createNotaCreditoVentaDetalle(NotaCreditoVentaDetalleDTO dto, InvocationContext ic) throws ServiceException;

}