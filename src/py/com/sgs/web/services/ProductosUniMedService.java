package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.ProductosUniMedDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ProductosUniMedService {
  List<ProductosUniMedDTO> listProductosUniMed(InvocationContext ic) throws ServiceException;

  ProductosUniMedDTO getProductosUniMed(Long id, InvocationContext ic) throws ServiceException;

  void deleteProductosUniMed(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ProductosUniMedDTO updateProductosUniMed(Long id, ProductosUniMedDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ProductosUniMedDTO createProductosUniMed(ProductosUniMedDTO dto, InvocationContext ic) throws ServiceException;

}