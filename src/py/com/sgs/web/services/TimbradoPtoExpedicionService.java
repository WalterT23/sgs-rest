package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.TimbradoPtoExpedicionDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface TimbradoPtoExpedicionService {
  List<TimbradoPtoExpedicionDTO> listTimbradoPtoExpedicion(InvocationContext ic) throws ServiceException;

  TimbradoPtoExpedicionDTO getTimbradoPtoExpedicion(Long id, InvocationContext ic) throws ServiceException;

  void deleteTimbradoPtoExpedicion(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  TimbradoPtoExpedicionDTO updateTimbradoPtoExpedicion(Long id, TimbradoPtoExpedicionDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  TimbradoPtoExpedicionDTO createTimbradoPtoExpedicion(TimbradoPtoExpedicionDTO dto, InvocationContext ic) throws ServiceException;

}