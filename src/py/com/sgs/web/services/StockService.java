package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.composite.StockCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface StockService {

	List<StockCompositeDTO> listStock(InvocationContext ic) throws ServiceException;

	StockCompositeDTO getStock(Long id, InvocationContext ic) throws ServiceException;

	StockDTO getStockByCodProducto(String codProducto, InvocationContext ic) throws ServiceException;

	void deleteStock(Long id, InvocationContext ic) throws ServiceException;

	@Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
	StockDTO updateStock(Long id, StockDTO dto, InvocationContext ic) throws ServiceException;
	//
	@Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
	StockDTO updateCodStock(StockDTO dto, InvocationContext ic) throws ServiceException;

	@Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
	StockDTO insertStock(StockDTO dto, InvocationContext ic) throws ServiceException;

}