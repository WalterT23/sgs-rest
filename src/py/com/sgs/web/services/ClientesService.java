package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.ClientesDTO;
import py.com.sgs.persistence.dto.composite.ClientesCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ClientesService {
  List<ClientesCompositeDTO> listClientes(InvocationContext ic) throws ServiceException;

  ClientesCompositeDTO getClientes(Long id, InvocationContext ic) throws ServiceException;
  
  List<ClientesCompositeDTO> getClientesByRuc(String ruc, InvocationContext ic) throws ServiceException;
  
  void deleteClientes(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ClientesDTO updateClientes(Long id, ClientesDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ClientesDTO createClientes(ClientesDTO dto, InvocationContext ic) throws ServiceException;

}