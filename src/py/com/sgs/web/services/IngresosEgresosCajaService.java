package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.IngresosEgresosCajaDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.composite.IngresosEgresosCajaCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface IngresosEgresosCajaService {
  List<IngresosEgresosCajaDTO> listIngresosEgresosCaja(InvocationContext ic) throws ServiceException;

  IngresosEgresosCajaDTO getIngresosEgresosCaja(Long id, InvocationContext ic) throws ServiceException;
  
  IngresosEgresosCajaCompositeDTO getIngresosEgresosCajaCompositeDTO(Long id, InvocationContext ic) throws ServiceException;

  void deleteIngresosEgresosCaja(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  IngresosEgresosCajaDTO updateIngresosEgresosCaja(Long id, IngresosEgresosCajaDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  IngresosEgresosCajaDTO createIngresosEgresosCaja(IngresosEgresosCajaDTO dto, InvocationContext ic) throws ServiceException;
  
  PrintDTO imprimir(Long id, InvocationContext ic) throws ServiceException;

}