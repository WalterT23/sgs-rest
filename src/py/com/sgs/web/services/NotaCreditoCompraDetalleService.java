package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.NotaCreditoCompraDetalleDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface NotaCreditoCompraDetalleService {
  List<NotaCreditoCompraDetalleDTO> listNotaCreditoCompraDetalle(InvocationContext ic) throws ServiceException;

  NotaCreditoCompraDetalleDTO getNotaCreditoCompraDetalle(Long id, InvocationContext ic) throws ServiceException;

  void deleteNotaCreditoCompraDetalle(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  NotaCreditoCompraDetalleDTO updateNotaCreditoCompraDetalle(Long id, NotaCreditoCompraDetalleDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  NotaCreditoCompraDetalleDTO createNotaCreditoCompraDetalle(NotaCreditoCompraDetalleDTO dto, InvocationContext ic) throws ServiceException;

}