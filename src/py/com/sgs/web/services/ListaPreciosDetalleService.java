package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.ListaPreciosDetalleDTO;
import py.com.sgs.persistence.dto.composite.LPDetallesCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ListaPreciosDetalleService {
  List<LPDetallesCompositeDTO> listListaPreciosDetalle(InvocationContext ic) throws ServiceException;

  LPDetallesCompositeDTO getListaPreciosDetalle(Long id, InvocationContext ic) throws ServiceException;
  
  LPDetallesCompositeDTO precioByLPcodPro(String codPro, InvocationContext ic) throws ServiceException;
  
  List<LPDetallesCompositeDTO> getListaPreciosDetalleByIdLP(Long idLP, InvocationContext ic)throws ServiceException;

  void deleteListaPreciosDetalle(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ListaPreciosDetalleDTO updateListaPreciosDetalle(Long id, ListaPreciosDetalleDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ListaPreciosDetalleDTO createListaPreciosDetalle(ListaPreciosDetalleDTO dto, InvocationContext ic) throws ServiceException;

}