package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.DetalleCierreDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface DetalleCierreService {
  List<DetalleCierreDTO> listDetalleCierre(InvocationContext ic) throws ServiceException;

  DetalleCierreDTO getDetalleCierre(Long id, InvocationContext ic) throws ServiceException;

  void deleteDetalleCierre(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DetalleCierreDTO updateDetalleCierre(Long id, DetalleCierreDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DetalleCierreDTO createDetalleCierre(DetalleCierreDTO dto, InvocationContext ic) throws ServiceException;

}