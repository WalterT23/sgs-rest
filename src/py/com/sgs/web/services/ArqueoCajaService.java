package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.ArqueoCajaDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ArqueoCajaService {
  List<ArqueoCajaDTO> listArqueoCaja(InvocationContext ic) throws ServiceException;

  ArqueoCajaDTO getArqueoCaja(Long id, InvocationContext ic) throws ServiceException;

  void deleteArqueoCaja(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ArqueoCajaDTO updateArqueoCaja(Long id, ArqueoCajaDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ArqueoCajaDTO createArqueoCaja(ArqueoCajaDTO dto, InvocationContext ic) throws ServiceException;

}