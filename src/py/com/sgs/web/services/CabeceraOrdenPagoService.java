package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.CabeceraOrdenPagoDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.composite.CabeceraOrdenPagoCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface CabeceraOrdenPagoService {

	List<CabeceraOrdenPagoDTO> listCabeceraOrdenPago(InvocationContext ic) throws ServiceException;

	CabeceraOrdenPagoCompositeDTO getCabeceraOrdenPago(Long id, InvocationContext ic) throws ServiceException;

	void deleteCabeceraOrdenPago(Long id, InvocationContext ic) throws ServiceException;

	@Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
	CabeceraOrdenPagoDTO updateCabeceraOrdenPago(Long id, CabeceraOrdenPagoDTO dto, InvocationContext ic) throws ServiceException;

	@Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
	CabeceraOrdenPagoDTO createCabeceraOrdenPago(CabeceraOrdenPagoDTO dto, InvocationContext ic) throws ServiceException;
	
	PrintDTO imprimir(Long id, InvocationContext ic) throws ServiceException;

}