package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.BancosDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface BancosService {
  List<BancosDTO> listBancos(InvocationContext ic) throws ServiceException;

  BancosDTO getBancos(Long id, InvocationContext ic) throws ServiceException;

  void deleteBancos(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  BancosDTO updateBancos(Long id, BancosDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  BancosDTO createBancos(BancosDTO dto, InvocationContext ic) throws ServiceException;

}