package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.FuncionalidadesDTO;
import py.com.sgs.persistence.dto.RolFuncionalidadDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface FuncionalidadesService {
  List<FuncionalidadesDTO> listFuncionalidades(InvocationContext ic) throws ServiceException;

  FuncionalidadesDTO getFuncionalidades(Long id, InvocationContext ic) throws ServiceException;

  void deleteFuncionalidades(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  FuncionalidadesDTO updateFuncionalidades(Long id, FuncionalidadesDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  FuncionalidadesDTO createFuncionalidades(FuncionalidadesDTO dto, InvocationContext ic) throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  RolFuncionalidadDTO createRolFuncionalidad(RolFuncionalidadDTO dto, InvocationContext ic) throws ServiceException;

  void deleteRolFuncionalidad(RolFuncionalidadDTO dto, InvocationContext ic) throws ServiceException;
  
}