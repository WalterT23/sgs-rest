package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.GrupoProductosDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface GrupoProductosService {
  List<GrupoProductosDTO> listGrupoProductos(InvocationContext ic) throws ServiceException;

  GrupoProductosDTO getGrupoProductos(Long id, InvocationContext ic) throws ServiceException;

  void deleteGrupoProductos(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  GrupoProductosDTO updateGrupoProductos(Long id, GrupoProductosDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  GrupoProductosDTO createGrupoProductos(GrupoProductosDTO dto, InvocationContext ic) throws ServiceException;

}