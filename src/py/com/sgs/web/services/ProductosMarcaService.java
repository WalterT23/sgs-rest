package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.ProductosMarcaDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ProductosMarcaService {
  List<ProductosMarcaDTO> listProductosMarca(InvocationContext ic) throws ServiceException;

  ProductosMarcaDTO getProductosMarca(Long id, InvocationContext ic) throws ServiceException;

  void deleteProductosMarca(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ProductosMarcaDTO updateProductosMarca(Long id, ProductosMarcaDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ProductosMarcaDTO createProductosMarca(ProductosMarcaDTO dto, InvocationContext ic) throws ServiceException;

}