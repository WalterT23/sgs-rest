package py.com.sgs.web.services;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.AccesoDTO;
import py.com.sgs.persistence.dto.composite.AccesoCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface AccesoService {
  List<AccesoDTO> listAcceso(InvocationContext ic) throws ServiceException;

  AccesoDTO getAcceso(Long id, InvocationContext ic) throws ServiceException;
  
  //AccesoDTO getAccesoByToken(String token, InvocationContext ic) throws ServiceException;
  
  AccesoCompositeDTO getFuncionalidadesByToken(AccesoDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  AccesoDTO updateAcceso(Long id, AccesoDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  AccesoDTO createAcceso(AccesoDTO dto, InvocationContext ic) throws ServiceException;



}