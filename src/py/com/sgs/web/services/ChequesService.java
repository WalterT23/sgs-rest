package py.com.sgs.web.services;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.ChequesDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ChequesService {
  List<ChequesDTO> listCheques(InvocationContext ic) throws ServiceException; 
  List<ChequesDTO> listChequesActivos(InvocationContext ic) throws ServiceException;
  List<ChequesDTO> listChequesByIdBanco(Integer idBanco,InvocationContext ic) throws ServiceException;
  List<ChequesDTO> listChequesByIdCuenta(Integer idCuentaBanco,InvocationContext ic) throws ServiceException;


  ChequesDTO getCheques(Long id, InvocationContext ic) throws ServiceException;

  void deleteCheques(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ChequesDTO updateCheques(Long id, ChequesDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ChequesDTO createCheques(ChequesDTO dto, InvocationContext ic) throws ServiceException;

}