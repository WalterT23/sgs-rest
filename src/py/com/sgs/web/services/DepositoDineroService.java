package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.DepositoDineroDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface DepositoDineroService {
  List<DepositoDineroDTO> listDepositoDinero(InvocationContext ic) throws ServiceException;

  DepositoDineroDTO getDepositoDinero(Long id, InvocationContext ic) throws ServiceException;

  void deleteDepositoDinero(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DepositoDineroDTO updateDepositoDinero(Long id, DepositoDineroDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DepositoDineroDTO createDepositoDinero(DepositoDineroDTO dto, InvocationContext ic) throws ServiceException;

}