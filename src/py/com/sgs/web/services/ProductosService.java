package py.com.sgs.web.services;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.ProductosDTO;
import py.com.sgs.persistence.dto.composite.ProductosCompositeDTO;
import py.com.sgs.persistence.dto.composite.ProductosGroupCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ProductosService {
  List<ProductosDTO> listProductos(InvocationContext ic) throws ServiceException;
  
  List<ProductosDTO>  listProductosByGroups(ProductosGroupCompositeDTO dto, InvocationContext ic) throws ServiceException;
  
  ProductosDTO getProductosById(Long id, InvocationContext ic) throws ServiceException;
  
  ProductosCompositeDTO getProductosByCod(String cod, InvocationContext ic) throws ServiceException;
  
  List<ProductosDTO> getProductosLikeCod(String cod, InvocationContext ic) throws ServiceException;

  void deleteProductos(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ProductosDTO updateProductos(Long id, ProductosDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ProductosDTO createProductos(ProductosDTO dto, InvocationContext ic) throws ServiceException;
  
  String codProGen();    

}