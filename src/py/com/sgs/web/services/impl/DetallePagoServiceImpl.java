package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.DetallePagoDAO;
import py.com.sgs.persistence.dto.DetallePagoDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.DetallePagoService;

public class DetallePagoServiceImpl implements DetallePagoService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private DetallePagoDAO detallePagoDAO;

  public void setDetallePagoDAO(DetallePagoDAO detallePagoDAO) {
    this.detallePagoDAO = detallePagoDAO;
  }

  @Override
  public List<DetallePagoDTO> listDetallePago(InvocationContext ic) throws ServiceException {
    List<DetallePagoDTO> detallePagos =  detallePagoDAO.listDetallePago();

    return detallePagos;
  }

  @Override
  public DetallePagoDTO getDetallePago(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    DetallePagoDTO detallePago =  detallePagoDAO.getDetallePagoById(id);
    if (detallePago == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detallepago.notfound", id);

    return detallePago;
  }

  @Override
  public void deleteDetallePago(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    DetallePagoDTO detallePago =  detallePagoDAO.getDetallePagoById(id);
    if(detallePago == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detallepago.notfound", id);

    try {
      detallePagoDAO.deleteDetallePago(detallePago);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "detallepago.constraint.violation", id);
    }

  }

  @Override
  public DetallePagoDTO updateDetallePago(Long id, DetallePagoDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    DetallePagoDTO detallePago = detallePagoDAO.getDetallePagoById(id);
    if (detallePago == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detallepago.notfound", id);

    detallePago.setIdFormaPago(dto.getIdFormaPago());
    detallePago.setIdBanco(dto.getIdBanco());
    detallePago.setNumeroTarjeta(dto.getNumeroTarjeta());
 
    detallePagoDAO.updateDetallePago(detallePago);

    return detallePago;
  }

  @Override
  public DetallePagoDTO createDetallePago(DetallePagoDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
      detallePagoDAO.createDetallePago(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "detallepago.duplicate.key");
    }

    return dto;
  }

}