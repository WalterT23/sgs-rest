package py.com.sgs.web.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.CabeceraOrdenPagoDAO;
import py.com.sgs.persistence.dao.DetalleOrdenPagoDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.PagoProveedoresDAO;
import py.com.sgs.persistence.dto.ConsultaPagosDTO;
import py.com.sgs.persistence.dto.DetalleOrdenPagoDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.MovimientoRecaudacionDTO;
import py.com.sgs.persistence.dto.PagoProvDetalleDTO;
import py.com.sgs.persistence.dto.PagoProveedoresDTO;
import py.com.sgs.persistence.dto.composite.CabeceraOrdenPagoCompositeDTO;
import py.com.sgs.persistence.dto.composite.PagoFacturasCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.MovimientoRecaudacionService;
import py.com.sgs.web.services.PagoProvDetalleService;
import py.com.sgs.web.services.PagoProveedoresService;

public class PagoProveedoresServiceImpl implements PagoProveedoresService{

private Logger log = LoggerFactory.getLogger(getClass()); 

  @Autowired
  private PagoProveedoresDAO pagoProveedoresDAO;
  
  @Autowired
  private InfoRefOpcDAO infoDao;
  
  @Autowired
  private CabeceraOrdenPagoDAO ordenPagoDao;
  
  @Autowired
  private DetalleOrdenPagoDAO ordenDetDao;
  
  @Autowired
  private PagoProveedoresService pagoProvDao;

  @Autowired
  private PagoProvDetalleService pagoProvdDao;
  
  @Autowired
  private MovimientoRecaudacionService moviRecaDao;

  public void setPagoProveedoresDAO(PagoProveedoresDAO pagoProveedoresDAO) {
    this.pagoProveedoresDAO = pagoProveedoresDAO;
  }

  @Override
  public List<PagoProveedoresDTO> listPagoProveedores(InvocationContext ic) throws ServiceException {
    List<PagoProveedoresDTO> pagoProveedoress =  pagoProveedoresDAO.listPagoProveedores(Long.parseLong(ic.getUserSucursal()));

    return pagoProveedoress;
  }

  @Override
  public PagoProveedoresDTO getPagoProveedores(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    PagoProveedoresDTO pagoProveedores =  pagoProveedoresDAO.getPagoProveedoresById(id);
    if (pagoProveedores == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"pagoproveedores.notfound", id);

    return pagoProveedores;
  }

  @Override
  public void deletePagoProveedores(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    PagoProveedoresDTO pagoProveedores =  pagoProveedoresDAO.getPagoProveedoresById(id);
    if(pagoProveedores == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"pagoproveedores.notfound", id);

    try {
      pagoProveedoresDAO.deletePagoProveedores(pagoProveedores);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "pagoproveedores.constraint.violation", id);
    }

  }

  @Override
  public PagoProveedoresDTO updatePagoProveedores(Long id, PagoProveedoresDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    PagoProveedoresDTO pagoProveedores = pagoProveedoresDAO.getPagoProveedoresById(id);
    if (pagoProveedores == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"pagoproveedores.notfound", id);

		    pagoProveedores.setIdProveedor(dto.getIdProveedor());
		    pagoProveedores.setFecha(dto.getFecha());
    
 
  
		    pagoProveedoresDAO.updatePagoProveedores(pagoProveedores);


    return pagoProveedores;
  }

  @Override
  public PagoProveedoresDTO createPagoProveedores(PagoProveedoresDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
     pagoProveedoresDAO.createPagoProveedores(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "pagoproveedores.duplicate.key");
    }

    return dto;
  }



@Override
public PagoFacturasCompositeDTO pagarFactura(PagoFacturasCompositeDTO dto, InvocationContext ic) throws ServiceException {
  if (dto == null)
    throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

  try {
	  //obtener fecha pago
	  Date fecha= new Date(System.currentTimeMillis());
	  Long idusu=Long.parseLong(ic.getUserSucursal());
	  //debo realizar el insert de pago primero 
	  // borrar id_op y agregar id_pago en op
	  if(dto.getPago() != null ) {
		   dto.getPago().setFecha(fecha);
		   dto.getPago().setIdSucursal(idusu);
		   pagoProveedoresDAO.createPagoProveedores(dto.getPago());
	  }

	  if (dto.getFacturas() != null && !dto.getFacturas().isEmpty()) {
		  
		  ArrayList<PagoProvDetalleDTO> listadeta = new ArrayList<PagoProvDetalleDTO>();

		  InfoRefOpcDTO cq= infoDao.getInfoRefOpcByAbv("CQUE");
		  InfoRefOpcDTO cta= infoDao.getInfoRefOpcByAbv("CTA_CRIA");
		  InfoRefOpcDTO efe= infoDao.getInfoRefOpcByAbv("EFE");

			  for (PagoProvDetalleDTO ppd :dto.getPago().getDetalles()) {
				  
				  InfoRefOpcDTO medio= infoDao.getInfoRefOpcByAbv(ppd.getAbreviaturaMedio());
				  Long nroOrdenPago = ordenPagoDao.getNroOrdenPago(Long.parseLong(ic.getUserSucursal()));

				  if(medio.getId().equals(cq.getId()) || medio.getId().equals(cta.getId())) {
					  PagoProvDetalleDTO pd= new PagoProvDetalleDTO();
					  
					  for (ConsultaPagosDTO cp: dto.getFacturas()) {
						  
						  	 pd.setIdCabecera(cp.getIdCompra());
							 pd.setIdMedio(medio.getId());
							 pd.setIdPago(dto.getPago().getId());
							 pd.setIdFuente(ppd.getIdFuente());
							 pd.setMonto(cp.getMontoaPagar());
							 
							 if(ppd.getNroCheque()!=null && !ppd.getNroCheque().isEmpty()) 
								 pd.setNroCheque(ppd.getNroCheque());
							 if(ppd.getFechaEmision()!=null)
								 pd.setFechaEmision(ppd.getFechaEmision());
							 if(ppd.getFechaPago()!=null)
								 pd.setFechaPago(ppd.getFechaPago());
							 if(cp.getIdCuotaDetalle()!=null)
								 pd.setIdCuota(cp.getIdCuotaDetalle());
							 
							 pagoProvdDao.createPagoProvDetalle(pd, ic);
							 listadeta.add(pd);
							 
							 pd= new PagoProvDetalleDTO();

					}
						if (nroOrdenPago == null){
							nroOrdenPago = 1L;
						}else {
							nroOrdenPago = nroOrdenPago + 1L;
						}
						CabeceraOrdenPagoCompositeDTO op = new CabeceraOrdenPagoCompositeDTO();
						op.setNroOrdenPago(String.format("%010d", nroOrdenPago));
						op.setIdProveedor(dto.getPago().getIdProveedor());
						op.setFechaCreacion(fecha);
						op.setFechaOrdenPago(fecha);
						op.setMontoTotal(dto.getPago().getTotalPagado());
						op.setUsuarioCreacion(ic.getUserPrincipal()); 
						op.setIdFuente(ppd.getIdFuente());
						op.setIdMedio(medio.getId());
						op.setIdSucursal(idusu);
						op.setIdPago( dto.getPago().getId());
						if (ppd.getNroCheque()!=null && !ppd.getNroCheque().isEmpty())
							op.setCheque(ppd.getNroCheque());
						
						ordenPagoDao.createCabeceraOrdenPago(op);
						
						if(listadeta!=null && !listadeta.isEmpty()) {
							DetalleOrdenPagoDTO dop = new DetalleOrdenPagoDTO();
							for(PagoProvDetalleDTO d: listadeta) {
								dop.setIdCabeceraOp (op.getId());
								dop.setIdCabeceraCompra(d.getIdCabecera());
								dop.setMonto(d.getMonto());
								ordenDetDao.createDetalleOrdenPago(dop);
								dop = new DetalleOrdenPagoDTO();
							}
						}
						

				  }
				  else{
					  InfoRefOpcDTO tmovi = infoDao.getInfoRefOpcByAbv("EGR");
					  InfoRefOpcDTO movi = infoDao.getInfoRefOpcByAbv("PGPR");
					  PagoProvDetalleDTO pd= new PagoProvDetalleDTO();
					  
					  for (ConsultaPagosDTO cp: dto.getFacturas()) {
						
						//registrar el movimiento de recaudacion
						 
						      MovimientoRecaudacionDTO movidto= new MovimientoRecaudacionDTO();
						      movidto.setFechaHora(fecha);
						      movidto.setIdMovimiento(movi.getId());
						      movidto.setIdTipoMovimiento(tmovi.getId());
						      movidto.setMonto(cp.getMontoaPagar());
						    	
						      moviRecaDao.createMovimientoRecaudacion(movidto, ic);
						 pd.setIdPago(dto.getPago().getId());
						 pd.setIdCabecera(cp.getIdCompra());
						 pd.setIdMedio(medio.getId());
						 pd.setIdFuente(ppd.getIdFuente());
						 pd.setMonto(cp.getMontoaPagar());
						 
						 if(cp.getIdCuotaDetalle()!=null)
							 pd.setIdCuota(cp.getIdCuotaDetalle());
						 
						 pagoProvdDao.createPagoProvDetalle(pd, ic);
						 
						 pd= new PagoProvDetalleDTO();
					 }
				 }
		  }
	  }
  }
  catch (DuplicateKeyException e){
    log.error(e.getMessage(), e);
    throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "pagoproveedores.duplicate.key");
  }

  return dto;
}

}