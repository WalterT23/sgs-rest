package py.com.sgs.web.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.ClientesDAO;
import py.com.sgs.persistence.dao.ClientesListaPrecioDAO;
import py.com.sgs.persistence.dto.ClientesDTO;
import py.com.sgs.persistence.dto.ClientesListaPrecioDTO;
import py.com.sgs.persistence.dto.composite.ListaPreciosCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ClientesListaPrecioService;
import py.com.sgs.web.services.ListaPreciosService;

public class ClientesListaPrecioServiceImpl implements ClientesListaPrecioService{

private Logger log = LoggerFactory.getLogger(getClass());  
  @Autowired
  private ClientesListaPrecioDAO clientesListaPrecioDAO;

  @Autowired
  private ListaPreciosService listaPreciosSRV;
  
  @Autowired
  private ClientesDAO clientesDAO;

  public void setClientesListaPrecioDAO(ClientesListaPrecioDAO clientesListaPrecioDAO) {
    this.clientesListaPrecioDAO = clientesListaPrecioDAO;
  }

  @Override
  public List<ClientesListaPrecioDTO> listClientesListaPrecio(InvocationContext ic) throws ServiceException {
    
	  List<ClientesListaPrecioDTO> clientesListaPrecios =  clientesListaPrecioDAO.listClientesListaPrecio(Long.parseLong(ic.getUserSucursal()));

    return clientesListaPrecios;
  }

  @Override
  public ClientesListaPrecioDTO getClientesListaPrecio(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ClientesListaPrecioDTO clientesListaPrecio =  clientesListaPrecioDAO.getClientesListaPrecioById(id);
    if (clientesListaPrecio == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"clienteslistaprecio.notfound", id);

    return clientesListaPrecio;
  }
  
  @Override
  public List<ClientesListaPrecioDTO> getClientesLPbyCliente(Long idCliente, InvocationContext ic) throws ServiceException {
	  if (idCliente == null || idCliente <= 0)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");
	  
	  ClientesListaPrecioDTO dto = new ClientesListaPrecioDTO();
	  dto.setIdCliente(idCliente);
	  dto.setIdSucursalListaP(Long.parseLong(ic.getUserSucursal()));
	  
	  List<ClientesListaPrecioDTO> clientesListaPrecios =  clientesListaPrecioDAO.getClientesLPbyCliente(dto);
		  
    return clientesListaPrecios;
  }
  
  @Override
  public void deleteClientesListaPrecio(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ClientesListaPrecioDTO clientesListaPrecio =  clientesListaPrecioDAO.getClientesListaPrecioById(id);
    if(clientesListaPrecio == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"clienteslistaprecio.notfound", id);

    try {
      clientesListaPrecioDAO.deleteClientesListaPrecio(clientesListaPrecio);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "clienteslistaprecio.constraint.violation", id);
    }

  }

  @Override
  public ClientesListaPrecioDTO updateClientesListaPrecio(Long id, ClientesListaPrecioDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    ClientesListaPrecioDTO clientesListaPrecio = clientesListaPrecioDAO.getClientesListaPrecioById(id);
    if (clientesListaPrecio == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"clienteslistaprecio.notfound", id);
    
    if (dto.getIdCliente() != null)
    	clientesListaPrecio.setIdCliente(dto.getIdCliente());
    
    if (dto.getIdListaPrecio() != null)
    	clientesListaPrecio.setIdListaPrecio(dto.getIdListaPrecio());
    
    if (dto.getObservacion() != null || !dto.getObservacion().isEmpty())
    	clientesListaPrecio.setObservacion(dto.getObservacion());
    
    if (dto.getFechaInicio() != null)
    	clientesListaPrecio.setFechaInicio(dto.getFechaInicio());
    
    if (dto.getFechaFin() != null)
    	clientesListaPrecio.setFechaFin(dto.getFechaFin());

    ClientesListaPrecioDTO control = clientesListaPrecioDAO.getClientesListaPrecioByDatos(clientesListaPrecio);
    if (control != null)
    	throw new ServiceException(ErrorCode.CLIENTE_ALREADY_HAVE_LP,"lp_client_already_exist", dto.getIdCliente(), dto.getIdListaPrecio());
    
    ListaPreciosCompositeDTO lpExist = listaPreciosSRV.getListaPrecios(dto.getIdListaPrecio(), ic);
    if (lpExist == null)
    	throw new ServiceException(ErrorCode.NO_DATA_FOUND,"listaprecios.notfound", dto.getIdListaPrecio());
    
    ClientesDTO cliExist = clientesDAO.getClientesById(dto.getIdCliente());
    if (cliExist == null)
    	throw new ServiceException(ErrorCode.NO_DATA_FOUND,"clientes.notfound", dto.getIdCliente());
    
    List<ClientesListaPrecioDTO> validar = clientesListaPrecioDAO.validarNewAso(dto);
    if (validar != null)
    	throw new ServiceException(ErrorCode.CLIENTE_ALREADY_HAVE_LP,"client_already_have_LP");
    
    clientesListaPrecioDAO.updateClientesListaPrecio(clientesListaPrecio);
    

    return clientesListaPrecio;
  }

  @Override
  public ClientesListaPrecioDTO createClientesListaPrecio(ClientesListaPrecioDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    ClientesDTO cliExist = clientesDAO.getClientesById(dto.getIdCliente());
    if (cliExist == null)
    	throw new ServiceException(ErrorCode.NO_DATA_FOUND,"clientes.notfound", dto.getIdCliente());
    
    ListaPreciosCompositeDTO lpExist = listaPreciosSRV.getListaPrecios(dto.getIdListaPrecio(), ic);
    if (lpExist == null)
    	throw new ServiceException(ErrorCode.NO_DATA_FOUND,"listaprecios.notfound", dto.getIdListaPrecio());
    
    ClientesListaPrecioDTO control = clientesListaPrecioDAO.getClientesListaPrecioByDatos(dto);
    if (control != null)
    	throw new ServiceException(ErrorCode.CLIENTE_ALREADY_HAVE_LP,"lp_client_already_exist", dto.getIdListaPrecio());
    
    if(dto.getFechaInicio() == null)
    	dto.setFechaInicio(new Date(System.currentTimeMillis()));
    
    if(dto.getFechaFin() == null) {
    	Calendar fecha = Calendar.getInstance();
    	fecha.set(Calendar.YEAR, 3000);
    	dto.setFechaFin(fecha.getTime());
    }
    
    dto.setIdSucursalListaP(Long.parseLong(ic.getUserSucursal()));
    
    List<ClientesListaPrecioDTO> validar = clientesListaPrecioDAO.validarNewAso(dto);
    if (validar != null)
    	throw new ServiceException(ErrorCode.CLIENTE_ALREADY_HAVE_LP,"client_already_have_LP");
    
    try {

      clientesListaPrecioDAO.createClientesListaPrecio(dto);
      
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "clienteslistaprecio.duplicate.key");
    }

    return dto;
  }

}