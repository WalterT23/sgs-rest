package py.com.sgs.web.services.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.AperturaCierreDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.PuntoExpedicionDAO;
import py.com.sgs.persistence.dto.AperturaCierreDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.PuntoExpedicionDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.PuntoExpedicionService;

public class PuntoExpedicionServiceImpl implements PuntoExpedicionService{

private Logger log = LoggerFactory.getLogger(getClass());  
  
  @Autowired
  	private PuntoExpedicionDAO puntoExpedicionDAO;
  
  @Autowired
	private InfoRefOpcDAO infoRefOpcDAO;
  
  @Autowired
	private AperturaCierreDAO aperturaCierreDAO;

  public void setPuntoExpedicionDAO(PuntoExpedicionDAO puntoExpedicionDAO) {
    this.puntoExpedicionDAO = puntoExpedicionDAO;
  }

  @Override
  public List<PuntoExpedicionDTO> listPuntoExpedicion(InvocationContext ic) throws ServiceException {
    List<PuntoExpedicionDTO> puntoExpedicions =  puntoExpedicionDAO.listPuntoExpedicion(Long.parseLong(ic.getUserSucursal()));

    return puntoExpedicions;
  }

  @Override
  public PuntoExpedicionDTO getPuntoExpedicion(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    PuntoExpedicionDTO puntoExpedicion =  puntoExpedicionDAO.getPuntoExpedicionById(id);
    if (puntoExpedicion == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"puntoexpedicion.notfound", id);

    return puntoExpedicion;
  }


  @Override
  public PuntoExpedicionDTO updatePuntoExpedicion(Long id, PuntoExpedicionDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    PuntoExpedicionDTO puntoExpedicion = puntoExpedicionDAO.getPuntoExpedicionById(id);
    if (puntoExpedicion == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"puntoexpedicion.notfound", id);
    
    if (!dto.getPuntoExp().isEmpty())
    	puntoExpedicion.setPuntoExp(dto.getPuntoExp());
    
    if (dto.getIdEstado() != null)
    	puntoExpedicion.setIdEstado(dto.getIdEstado());
 
    puntoExpedicionDAO.updatePuntoExpedicion(puntoExpedicion);

    return puntoExpedicion;
  }

  @Override
  public PuntoExpedicionDTO createPuntoExpedicion(PuntoExpedicionDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
      dto.setUsuarioCreacion(ic.getUserPrincipal());
      dto.setFechaCreacion(new Date(System.currentTimeMillis()));
      dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
      puntoExpedicionDAO.createPuntoExpedicion(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "puntoexpedicion.duplicate.key");
    }

    return dto;
  }

  @Override
  public void inactivarPtoExp(Long id, InvocationContext ic) throws ServiceException {
	  if (id == null || id <= 0)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

	  PuntoExpedicionDTO puntoExp = puntoExpedicionDAO.getPuntoExpedicionById(id);
	  AperturaCierreDTO apertura = aperturaCierreDAO.getAperturaCierreVigenteByIdPuntoExp(id);
	    
	  if (puntoExp == null)
	      throw new ServiceException(ErrorCode.PTO_EXPEDICION_NO_ENCONTRADO,"timbrado.notfound", id);
	  
	  if (apertura != null)
		  throw new ServiceException(ErrorCode.APERTURA_ACTIVA,"apertura.cierre.existe.vigente", puntoExp.getPuntoExp());
	  
	  InfoRefOpcDTO estado = infoRefOpcDAO.getInfoRefOpcByAbv("PEINA");
	  
	  puntoExp.setIdEstado(estado.getId());
	  
	  puntoExpedicionDAO.updateEstado(puntoExp);
	
  }

  @Override
  public void activarPtoExp(Long id, InvocationContext ic) throws ServiceException {
	  if (id == null || id <= 0)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

	  PuntoExpedicionDTO puntoExp = puntoExpedicionDAO.getPuntoExpedicionById(id);
	    
	  if (puntoExp == null)
	      throw new ServiceException(ErrorCode.PTO_EXPEDICION_NO_ENCONTRADO,"timbrado.notfound", id);
	  
	  InfoRefOpcDTO estado = infoRefOpcDAO.getInfoRefOpcByAbv("PEACT");
	  
	  puntoExp.setIdEstado(estado.getId());
	  
	  puntoExpedicionDAO.updateEstado(puntoExp);
	
  }

}