package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.ProductosUniMedDAO;
import py.com.sgs.persistence.dto.ProductosUniMedDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ProductosUniMedService;

public class ProductosUniMedServiceImpl implements ProductosUniMedService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private ProductosUniMedDAO productosUniMedDAO;

  public void setProductosUniMedDAO(ProductosUniMedDAO productosUniMedDAO) {
    this.productosUniMedDAO = productosUniMedDAO;
  }

  @Override
  public List<ProductosUniMedDTO> listProductosUniMed(InvocationContext ic) throws ServiceException {
    List<ProductosUniMedDTO> productosUniMeds =  productosUniMedDAO.listProductosUniMed();

    return productosUniMeds;
  }

  @Override
  public ProductosUniMedDTO getProductosUniMed(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null",id);

    ProductosUniMedDTO productosUniMed =  productosUniMedDAO.getProductosUniMedById(id);
    if (productosUniMed == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"productosunimed.notfound", id);

    return productosUniMed;
  }

  @Override
  public void deleteProductosUniMed(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null",id);

    ProductosUniMedDTO productosUniMed =  productosUniMedDAO.getProductosUniMedById(id);
    if(productosUniMed == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"productosunimed.notfound", id);

    try {
      productosUniMedDAO.deleteProductosUniMed(productosUniMed);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "productosunimed.constraint.violation", id);
    }

  }

  @Override
  public ProductosUniMedDTO updateProductosUniMed(Long id, ProductosUniMedDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null",id);

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null",dto);

    ProductosUniMedDTO productosUniMed = productosUniMedDAO.getProductosUniMedById(id);
    if (productosUniMed == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"productosunimed.notfound", id);
    
    if (dto.getDescripcion() != null && !dto.getDescripcion().isEmpty()) 
    	productosUniMed.setDescripcion(dto.getDescripcion().trim());
    
    if (dto.getAbreviatura() != null && !dto.getAbreviatura().isEmpty())
    	productosUniMed.setAbreviatura(dto.getAbreviatura().trim());
 
    productosUniMedDAO.updateProductosUniMed(productosUniMed);

    return productosUniMed;
  }

  @Override
  public ProductosUniMedDTO createProductosUniMed(ProductosUniMedDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null",dto);

    try {
    	dto.setDescripcion(dto.getDescripcion().trim());
    	
    	ProductosUniMedDTO uniMed = productosUniMedDAO.getProductosUniMedByName(dto.getDescripcion());
    	
    	if(dto.getAbreviatura() != null && !dto.getAbreviatura().isEmpty())
    		dto.setAbreviatura(dto.getAbreviatura().trim());
    	
    	if( uniMed != null)
    		throw new ServiceException(ErrorCode.UNI_MED_YA_EXISTE,"unimed.already.exist",dto.getDescripcion());
    	
    	productosUniMedDAO.createProductosUniMed(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "productosunimed.duplicate.key");
    }

    return dto;
  }

}