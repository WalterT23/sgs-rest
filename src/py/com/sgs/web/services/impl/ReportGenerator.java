package py.com.sgs.web.services.impl;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.web.interceptor.InvocationContext;

/**
 * <b><i>Creador de Reportes (enfocado a archivos xls de una hoja)<i><b>
 *
 */
public class ReportGenerator {

	private final static String REPORT_SRC_N = "/reportes/";

	/**
	 * <b><i>La clase se encarga de armar el PATH, construir el reporte y devolver
	 * el archivo contenido en un vector de bytes.<i><b>
	 * 
	 * @param tipo
	 *            -> "E"(Excel) -> "P" (Pdf)
	 * @param ic
	 *            -> (InvocationContext)
	 * @param fileName
	 *            -> (Nombre del archivo con la extension .jasper)
	 * @param datos
	 *            -> (Lista del objecto a mostrar)
	 * @param map
	 *            -> (Parametros de los objetos a mostrar)
	 * @return byte[]
	 * @throws ServiceException
	 *             (Errores de validacion propias de la clase)
	 * @throws JRException
	 *             (Errores de la libreria jasper)
	 */
	public byte[] getByteReports(String tipo, InvocationContext ic, String fileName, List<?> datos,
			HashMap<String, Object> map) throws ServiceException, JRException {
		Boolean atr1 = false;
		byte[] archivo;
		if (isNull(tipo)) {
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null.type", "tipo", "E -> (Excel)",
					"P -> (Pdf)");
		} else if (isNull(fileName)) {
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null.type2", "fileName");
		} else if (isNull(datos)) {
			atr1 = true;
		}
		InputStream jasperStream = this.getClass().getResourceAsStream(REPORT_SRC_N.concat(fileName));
		JasperPrint jasperPrint;
		if (atr1) {
			JREmptyDataSource dato = new JREmptyDataSource();
			jasperPrint = JasperFillManager.fillReport(jasperStream, map, dato);
		}else {
			JRBeanCollectionDataSource dato = new JRBeanCollectionDataSource(datos);
			jasperPrint = JasperFillManager.fillReport(jasperStream, map, dato);
		}
		
		ArrayList<JasperPrint> sheets= new ArrayList<JasperPrint>();
		sheets.add(jasperPrint);
		JRXlsExporter exporter = new JRXlsExporter();
		ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
		exporter.setExporterInput(SimpleExporterInput.getInstance(sheets));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReport));
		SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
		configuration.setOnePagePerSheet(Boolean.FALSE);
		configuration.setDetectCellType(Boolean.FALSE);
		configuration.setCollapseRowSpan(Boolean.TRUE);
		configuration.setRemoveEmptySpaceBetweenRows(Boolean.TRUE);
		configuration.setRemoveEmptySpaceBetweenColumns(Boolean.FALSE);
		exporter.setConfiguration(configuration);
		exporter.exportReport();
		archivo = xlsReport.toByteArray();

		return archivo;
	}

	/**
	 * <b><i>Recibe un objeto y retorna VERDADERO de ser null o no contener nada
	 * cargado.<i><b> Caso contrario FALSO
	 * 
	 * @param obj
	 *            -> (Cualquier tipo de dato)
	 * @return true || false
	 */
	public boolean isNull(Object obj) {
		if (obj == null) {
			return true;
		} else if (obj.getClass().equals(String.class)) {
			if (((String) obj).trim().isEmpty()) {
				return true;
			}
		} else if (obj.getClass().equals(List.class)) {
			if (((List<?>) obj).isEmpty()) {
				return true;
			}
		} else if (obj.getClass().equals(ArrayList.class)) {
			if (((ArrayList<?>) obj).isEmpty()) {
				return true;
			}
		}
		return false;
	}

}