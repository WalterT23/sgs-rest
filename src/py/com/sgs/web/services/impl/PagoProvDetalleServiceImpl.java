package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.CabeceraCompraDAO;
import py.com.sgs.persistence.dao.CuotaDetalleDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.PagoProvDetalleDAO;
import py.com.sgs.persistence.dto.CuotaDetalleDTO;
import py.com.sgs.persistence.dto.CuotasDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.PagoProvDetalleDTO;
import py.com.sgs.persistence.dto.composite.CabeceraCompraCompositeDTO;
import py.com.sgs.persistence.dto.composite.CuotasCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.PagoProvDetalleService;

public class PagoProvDetalleServiceImpl implements PagoProvDetalleService{

private Logger log = LoggerFactory.getLogger(getClass());
  @Autowired
  private PagoProvDetalleDAO pagoProvDetalleDAO; 
  
  @Autowired
  private CabeceraCompraDAO compradao;
  
  @Autowired
  private CuotaDetalleDAO cuodao;
  
  @Autowired
  private InfoRefOpcDAO infodao;

  public void setPagoProvDetalleDAO(PagoProvDetalleDAO pagoProvDetalleDAO) {
    this.pagoProvDetalleDAO = pagoProvDetalleDAO;
  }

  @Override
  public List<PagoProvDetalleDTO> listPagoProvDetalle(InvocationContext ic) throws ServiceException {
    List<PagoProvDetalleDTO> pagoProvDetalles =  pagoProvDetalleDAO.listPagoProvDetalle();

    return pagoProvDetalles;
  }

  @Override
  public PagoProvDetalleDTO getPagoProvDetalle(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    PagoProvDetalleDTO pagoProvDetalle =  pagoProvDetalleDAO.getPagoProvDetalleById(id);
    if (pagoProvDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"pagoprovdetalle.notfound", id);

    return pagoProvDetalle;
  }

  @Override
  public void deletePagoProvDetalle(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    PagoProvDetalleDTO pagoProvDetalle =  pagoProvDetalleDAO.getPagoProvDetalleById(id);
    if(pagoProvDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"pagoprovdetalle.notfound", id);

    try {
      pagoProvDetalleDAO.deletePagoProvDetalle(pagoProvDetalle);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "pagoprovdetalle.constraint.violation", id);
    }

  }

  @Override
  public PagoProvDetalleDTO updatePagoProvDetalle(Long id, PagoProvDetalleDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    PagoProvDetalleDTO pagoProvDetalle = pagoProvDetalleDAO.getPagoProvDetalleById(id);
    if (pagoProvDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"pagoprovdetalle.notfound", id);

    pagoProvDetalle.setIdPago(dto.getIdPago());
    pagoProvDetalle.setIdCabecera(dto.getIdCabecera());
    pagoProvDetalle.setMonto(dto.getMonto());
    pagoProvDetalle.setIdMedio(dto.getIdMedio());
    pagoProvDetalle.setIdFuente(dto.getIdFuente());
 
   
   
      pagoProvDetalleDAO.updatePagoProvDetalle(pagoProvDetalle);
 
    return pagoProvDetalle;
  }

  @Override
  public PagoProvDetalleDTO createPagoProvDetalle(PagoProvDetalleDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
    	
		InfoRefOpcDTO conta= infodao.getInfoRefOpcByAbv("FCONT");
		InfoRefOpcDTO pag= infodao.getInfoRefOpcByAbv("FCPAG");
    	CabeceraCompraCompositeDTO factura = compradao.getCabeceraCompraById(dto.getIdCabecera());
    	BigDecimal saldo = factura.getSaldo();
    
    	
    	if(factura.getTipoFactura().equals(conta.getId())) {
    		
    		if(saldo.subtract(dto.getMonto()).compareTo(BigDecimal.ZERO)==0)    			
    			factura.setIdEstado(pag.getId());
    		
    		factura.setSaldo(saldo.subtract(dto.getMonto()));
    			
    		
    		
    	}else {
    		
    		CuotaDetalleDTO cuo = cuodao.getCuotaDetalleById(dto.getIdCuota());
    		cuo.setIdEstado(pag.getId());
    		
    		if(saldo.subtract(dto.getMonto()).compareTo(BigDecimal.ZERO)==0)    			
    			factura.setIdEstado(pag.getId());
    		
    		factura.setSaldo(saldo.subtract(dto.getMonto()));
    		
    		cuodao.updateCuotaDetalle(cuo);
    		
    	}
    	compradao.updateCabeceraCompra(factura);
    	pagoProvDetalleDAO.createPagoProvDetalle(dto);
    	
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "pagoprovdetalle.duplicate.key");
    }

    return dto;
  }

}