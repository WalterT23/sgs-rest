package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.CabeceraCompraDAO;
import py.com.sgs.persistence.dao.CuotaDetalleDAO;
import py.com.sgs.persistence.dao.CuotasDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.MovMercDetalleDAO;
import py.com.sgs.persistence.dao.MovimientoMercaderiaDAO;
import py.com.sgs.persistence.dao.NotaCreditoCompraDAO;
import py.com.sgs.persistence.dao.NotaCreditoCompraDetalleDAO;
import py.com.sgs.persistence.dao.ProveedoresDAO;
import py.com.sgs.persistence.dao.StockDAO;
import py.com.sgs.persistence.dto.CabeceraCompraDTO;

import py.com.sgs.persistence.dto.DetalleCompraDTO;
import py.com.sgs.persistence.dto.CuotaDetalleDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.MovMercDetalleDTO;
import py.com.sgs.persistence.dto.NotaCreditoCompraDTO;
import py.com.sgs.persistence.dto.NotaCreditoCompraDetalleDTO;
import py.com.sgs.persistence.dto.ProveedoresDTO;
import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.composite.CabeceraCompraCompositeDTO;
import py.com.sgs.persistence.dto.composite.MovimientoMercaderiaCompositeDTO;
import py.com.sgs.persistence.dto.composite.NotaCreditoCompraCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.MovimientoMercaderiaService;
import py.com.sgs.web.services.NotaCreditoCompraDetalleService;
import py.com.sgs.web.services.NotaCreditoCompraService;

public class NotaCreditoCompraServiceImpl implements NotaCreditoCompraService{

private Logger log = LoggerFactory.getLogger(getClass()); 

  @Autowired
  private NotaCreditoCompraDAO notaCreditoCompraDAO;
  @Autowired
  private NotaCreditoCompraDetalleDAO notaCreditoCompraDetalleDAO;
  @Autowired
  private CabeceraCompraDAO cabeceraCompraDAO;
  @Autowired
  private InfoRefOpcDAO infoRefOpcDAO;
  @Autowired 
  private MovimientoMercaderiaDAO movimientoMercaderiaDAO;
  @Autowired	
  private MovMercDetalleDAO movMercDetalleDAO;
  @Autowired	
  private StockDAO stockDAO;
  @Autowired ProveedoresDAO proveedoresDAO;
  @Autowired
  private CuotaDetalleDAO cuotaDetalleDAO;
  
  @Autowired
  private NotaCreditoCompraDetalleService detalleService;
  @Autowired
  private MovimientoMercaderiaService movdao;
  
  public void setNotaCreditoCompraDAO(NotaCreditoCompraDAO notaCreditoCompraDAO) {
    this.notaCreditoCompraDAO = notaCreditoCompraDAO;
  }

  @Override
  public List<NotaCreditoCompraCompositeDTO> listNotaCreditoCompra(InvocationContext ic) throws ServiceException {
    List<NotaCreditoCompraCompositeDTO> notaCreditoCompras =  notaCreditoCompraDAO.listNotaCreditoCompra(Long.parseLong(ic.getUserSucursal()));

    return notaCreditoCompras;
  }

  @Override
  public NotaCreditoCompraCompositeDTO getNotaCreditoCompra(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    NotaCreditoCompraCompositeDTO notaCreditoCompra =  notaCreditoCompraDAO.getNotaCreditoCompraById(id);
    if (notaCreditoCompra == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditocompra.notfound", id);

    return notaCreditoCompra;
  }

  @Override
  public void deleteNotaCreditoCompra(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    NotaCreditoCompraCompositeDTO notaCreditoCompra =  notaCreditoCompraDAO.getNotaCreditoCompraById(id);
    if(notaCreditoCompra == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditocompra.notfound", id);

    try {
    	//verifico si es con factura
    	
    	if(notaCreditoCompra.getIdCabeceraCompra()!=null) {
    		CabeceraCompraCompositeDTO fc = cabeceraCompraDAO.getCabeceraCompraById(notaCreditoCompra.getIdCabeceraCompra());
        	InfoRefOpcDTO estado= infoRefOpcDAO.getInfoRefOpcByAbv("FCPEN");
        	InfoRefOpcDTO ivalido= infoRefOpcDAO.getInfoRefOpcById(fc.getIdEstado());
        	
        	if(!fc.getIdEstado().equals(estado.getId())) 
        		throw new ServiceException(ErrorCode.ESTADO_INVALIDO,"facturacompra.estado.invalido", ivalido.getDescripcion());
        	
        	 //al saldo existente se le resta el total de la nota de credito
	  		  fc.setSaldo(fc.getSaldo().add(notaCreditoCompra.getMontoTotal()));
	  		  cabeceraCompraDAO.updateCabeceraCompraSingle(fc);
  		
	  		  //se obtiene el id para verificar si la factura es a credito
	  		  InfoRefOpcDTO tipoFC = new InfoRefOpcDTO();
	  		  tipoFC = infoRefOpcDAO.getInfoRefOpcByAbv("FCTO");


      	  
      	  if(fc.getTipoFactura().compareTo(tipoFC.getId()) == 0){
      		  
      		  List<CuotaDetalleDTO> cuotasFC = new ArrayList<CuotaDetalleDTO>();
      		  
      		  cuotasFC = cuotaDetalleDAO.cuotasPenXcabCompra(notaCreditoCompra.getIdCabeceraCompra());
      		  
      		  if (cuotasFC != null) {
      			  for (CuotaDetalleDTO det: cuotasFC){
      				  
          			  if(det.getIdEstado().compareTo(infoRefOpcDAO.getInfoRefOpcByAbv("FCPEN").getId())== 0){
          					  det.setMonto(det.getMonto().add(notaCreditoCompra.getMontoTotal()));
          					  break;
          				 
          			  }
          			  //actualizo
          			cuotaDetalleDAO.updateCuotaDetalle(det);
          		  }
      		  }
      	  }
      	  
      	  

        } 
    	
    	MovimientoMercaderiaCompositeDTO dtomov= new MovimientoMercaderiaCompositeDTO();
    	ArrayList<MovMercDetalleDTO> listadeta = new ArrayList<MovMercDetalleDTO>();
    	
    	if (notaCreditoCompra.getDetalles() != null && !notaCreditoCompra.getDetalles().isEmpty()){
	    	for(NotaCreditoCompraDetalleDTO detalle: notaCreditoCompra.getDetalles()) {
	    		
	    		MovMercDetalleDTO md=new MovMercDetalleDTO();
				md.setCantidad(detalle.getCantidad());
				md.setIdProducto(detalle.getIdProducto());
				listadeta.add(md);
	    		detalleService.deleteNotaCreditoCompraDetalle(detalle.getId(), ic);
	    	}
	    	dtomov.setDetalles(listadeta);
		}
    	
    	Date fecha= new Date(System.currentTimeMillis());
    	Long idusu=Long.parseLong(ic.getUserSucursal());
    	InfoRefOpcDTO timov= infoRefOpcDAO.getInfoRefOpcByAbv("ING_M");
		InfoRefOpcDTO tipop=infoRefOpcDAO.getInfoRefOpcByAbv("CTOCPRA");
		
		
		
		//dtomov.setIdOperacion(dto.getId());
		dtomov.setFechaMovimiento(fecha);
		dtomov.setIdTipoMovimiento(timov.getId());
		dtomov.setTipoOperacion(tipop.getId());
		dtomov.setIdSucursal(idusu);
		dtomov.setUsuarioRegistro(ic.getUserPrincipal());
		
		movdao.createMovimientoMercaderia(dtomov, ic);
    	notaCreditoCompraDAO.deleteNotaCreditoCompra(notaCreditoCompra);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "notacreditocompra.constraint.violation", id);
    }

  }

  @Override
  public NotaCreditoCompraDTO updateNotaCreditoCompra(Long id, NotaCreditoCompraDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    NotaCreditoCompraDTO notaCreditoCompra = notaCreditoCompraDAO.getNotaCreditoCompraById(id);
    if (notaCreditoCompra == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditocompra.notfound", id);
    
    InfoRefOpcDTO estadoFC = new InfoRefOpcDTO();
    estadoFC = infoRefOpcDAO.getInfoRefOpcByAbv("FCPEN");
    
    if (dto.getIdCabeceraCompra() != null) {
    	
    	if (notaCreditoCompra.getIdCabeceraCompra() != null) {

    		CabeceraCompraDTO nuevaFC = new CabeceraCompraDTO();
    		nuevaFC = cabeceraCompraDAO.getCabeceraCompraById(dto.getId());

    		CabeceraCompraDTO viejaFC = new CabeceraCompraDTO();
    		viejaFC = cabeceraCompraDAO.getCabeceraCompraById(notaCreditoCompra.getIdCabeceraCompra());

    		// se verifica que la factura asociada actualmente aun no haya sido pagada
    		if(viejaFC.getIdEstado().compareTo(estadoFC.getId()) == 0){

    			// se verifica que la nueva factura a asociar aun no haya sido pagada
    			if(nuevaFC.getIdEstado().compareTo(estadoFC.getId()) == 0){

    				//se asocia la nueva factura, se suma el saldo descontado a la factura anterior
    				notaCreditoCompra.setIdCabeceraCompra(dto.getIdCabeceraCompra());
    				viejaFC.setSaldo(viejaFC.getSaldo().add(notaCreditoCompra.getMontoTotal()));
    				cabeceraCompraDAO.updateCabeceraCompraSingle(viejaFC);

    			} else {
    				throw new ServiceException(ErrorCode.NOTA_CREDITO_COMPRA," nueva.FC.already.payed", notaCreditoCompra.getIdCabeceraCompra());
    			}
    		}else {
    			throw new ServiceException(ErrorCode.NOTA_CREDITO_COMPRA," actual.FC.already.payed", notaCreditoCompra.getIdCabeceraCompra());
    		}
    	} else {
    		notaCreditoCompra.setIdCabeceraCompra(dto.getIdCabeceraCompra());
    	}
    }
    
    if (dto.getNroNotaCredito() != null)
    	notaCreditoCompra.setNroNotaCredito(dto.getNroNotaCredito());
    
    if (dto.getMontoTotal() != null){
    	
    	// si se actualiza el monto se verifica si la NC ya tenia una FC asociada, si estaba asociada entonces tb se debe actualizar su saldo
    	if(notaCreditoCompra.getIdCabeceraCompra() != null) {
    		
    		CabeceraCompraDTO fc = new CabeceraCompraDTO();
    		fc = cabeceraCompraDAO.getCabeceraCompraById(notaCreditoCompra.getIdCabeceraCompra());
    		
    		
    		//al saldo actual se le suma el monto inicial de la nota de credito (se devuelve el saldo)
    		fc.setSaldo(fc.getSaldo().add(notaCreditoCompra.getMontoTotal()));
    		
    		//al saldo resultante se le descuenta el nuevo monto de la nota de credito
    		fc.setSaldo(fc.getSaldo().subtract(dto.getMontoTotal()));
    		cabeceraCompraDAO.updateCabeceraCompraSingle(fc);
    		
    	} else {
    		notaCreditoCompra.setMontoTotal(dto.getMontoTotal());
    	}
    }
    	
    if(dto.getIva5() != null)
    	notaCreditoCompra.setIva5(dto.getIva5());
    
    if(dto.getIva10() != null)
    	notaCreditoCompra.setIva10(dto.getIva10());
    
    if(dto.getExentas() != null)
    	notaCreditoCompra.setExentas(dto.getExentas());

    notaCreditoCompraDAO.updateNotaCreditoCompra(notaCreditoCompra);
  

    return notaCreditoCompra;
  }

  @Override
  public NotaCreditoCompraCompositeDTO createNotaCreditoCompra(NotaCreditoCompraCompositeDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
    
    if (dto.getIdProveedor() != null){
    	ProveedoresDTO pro = new ProveedoresDTO();
    	pro = proveedoresDAO.getProveedorSById(dto.getIdProveedor());
    	if (pro == null){
    		throw new ServiceException(ErrorCode.NOTA_CREDITO_COMPRA,"proveedor..required");
    	}
    	
    }else {
    	throw new ServiceException(ErrorCode.NOTA_CREDITO_COMPRA,"proveedor..required");
    }
    	
    
    if ("".equals(dto.getFecha()) || dto.getFecha() == null)
    	throw new ServiceException(ErrorCode.NOTA_CREDITO_COMPRA,"fecha.required");

    BigDecimal total=new BigDecimal(0);
    
    
    //se obtiene el total de los detalles 
    if (dto.getDetalles() != null && !dto.getDetalles().isEmpty()) {

    	for (NotaCreditoCompraDetalleDTO ncd:dto.getDetalles()){
    		BigDecimal cant = new BigDecimal(ncd.getCantidad());
    		total=total.add(ncd.getCostoUnitario().multiply(cant));
    	}
    	dto.setMontoTotal(total);
    }
  
    //se setea el total en la cabecera
    if (total == null || total.compareTo(BigDecimal.ZERO) == 0)
    	throw new ServiceException(ErrorCode.NOTA_CREDITO_COMPRA,"moto.total.invalid");
    
    
    InfoRefOpcDTO estadoFC = new InfoRefOpcDTO();
    estadoFC = infoRefOpcDAO.getInfoRefOpcByAbv("FCPEN");
    try {
    	
      dto.setUsuarioCreacion(ic.getUserPrincipal());
      dto.setFechaCreacion(new Date(System.currentTimeMillis()));
      dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
      dto.setIdEstado(infoRefOpcDAO.getInfoRefOpcByAbv("NCCREG").getId());

      notaCreditoCompraDAO.createNotaCreditoCompra(dto);
      
      if(dto.getIdCabeceraCompra() != null) {


    	  CabeceraCompraDTO facturaC = new CabeceraCompraDTO();
    	  facturaC = cabeceraCompraDAO.getCabeceraCompraById(dto.getIdCabeceraCompra());

		  //se controla que el saldo existente de la nota de credito no sea menor al monto total de la nota de credito
		  if(facturaC.getSaldo().compareTo(dto.getMontoTotal()) < 0 )
			  throw new ServiceException(ErrorCode.NOTA_CREDITO_COMPRA,"monto.total.invalid", facturaC.getSaldo());


		  //se verifica que la factura a asociar este "pendiente"
		  if(facturaC.getIdEstado().compareTo(estadoFC.getId()) != 0)
				throw new ServiceException(ErrorCode.NOTA_CREDITO_COMPRA," nueva.FC.already.payed", dto.getIdCabeceraCompra());
			
		  //al saldo existente se le resta el total de la nota de credito
		  facturaC.setSaldo(facturaC.getSaldo().subtract(dto.getMontoTotal()));
		  cabeceraCompraDAO.updateCabeceraCompraSingle(facturaC);
		
		  //se obtiene el id para verificar si la factura es a credito
		  InfoRefOpcDTO tipoFC = new InfoRefOpcDTO();
		  tipoFC = infoRefOpcDAO.getInfoRefOpcByAbv("FCTO");


    	  
    	  if(facturaC.getTipoFactura().compareTo(tipoFC.getId()) == 0){
    		  
    		  List<CuotaDetalleDTO> cuotasFC = new ArrayList<CuotaDetalleDTO>();
    		  cuotasFC = cuotaDetalleDAO.cuotasPenXcabCompra(dto.getIdCabeceraCompra());
    		  if (cuotasFC != null) {
    			  for (CuotaDetalleDTO det: cuotasFC){
        			  if(det.getIdEstado().compareTo(infoRefOpcDAO.getInfoRefOpcByAbv("FCPEN").getId())== 0){
        				  if(det.getMonto().compareTo(dto.getMontoTotal()) > 0 ){
        					  det.setMonto(det.getMonto().subtract(dto.getMontoTotal()));
        					  break;
        				  }
        			  }
        			  cuotaDetalleDAO.updateCuotaDetalle(det);
        		  }
    		  }
    		  
    	  }

      } 
      
      
      
      //se deben crear los detalles
      List<NotaCreditoCompraDetalleDTO> detalles = null;
      
      if(dto.getDetalles() != null && !dto.getDetalles().isEmpty()) {
    	  detalles = new ArrayList<NotaCreditoCompraDetalleDTO>();
    	  detalles.addAll(dto.getDetalles());
      }
      
      
      //crea el movimiento de mercaderia
      MovMercDetalleDTO detMerc = new MovMercDetalleDTO();
      
      if(detalles != null){
    	  
    	  for(NotaCreditoCompraDetalleDTO det: detalles){
    		  det.setIdNotaCreditoCompra(dto.getId());
    		  BigDecimal cant = new BigDecimal(det.getCantidad());
    		  BigDecimal tot = det.getCostoUnitario().multiply(cant);
    		  det.setTotal(tot);
    		  try {
    			  notaCreditoCompraDetalleDAO.createNotaCreditoCompraDetalle(det);
    		  }
    		  catch (DataIntegrityViolationException e){
    			  log.error(e.getMessage(), e);
    			  throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "notacreditoventa.duplicate.key");
    		  }

    	  }
    	  
    	  if(dto !=null && dto.getDetalles().size() > 0 ){

    		  if(dto.getDetalles().get(0).getCantidad() != null && dto.getDetalles().get(0).getCantidad() > 0){
    			  MovimientoMercaderiaCompositeDTO movMerc = new MovimientoMercaderiaCompositeDTO();
    			  movMerc.setIdTipoMovimiento(infoRefOpcDAO.getInfoRefOpcByAbv("EGR_M").getId());
    			  movMerc.setFechaMovimiento(dto.getFechaCreacion());
    			  movMerc.setTipoOperacion(infoRefOpcDAO.getInfoRefOpcByAbv("CTOCPRA").getId());
    			  movMerc.setUsuarioRegistro(dto.getUsuarioCreacion());
    			  movMerc.setIdSucursal(dto.getIdSucursal());
    			  movMerc.setIdOperacion(dto.getId());

    			  movimientoMercaderiaDAO.createMovimientoMercaderia(movMerc);
    			  
    			  
    	    	  for(NotaCreditoCompraDetalleDTO det: detalles) {
    	    		  
    	    		//se debe actualizar el stock restando la cantidad comprada del producto de la cantidad en stock
    	    		  if( det.getCantidad() != null && det.getCantidad() > 0){
    	    		      
    	    			  StockDTO st = stockDAO.stockByIdProducto(new StockDTO(det.getIdProducto(), dto.getIdSucursal()));
    	    			  if(st != null){
    	    				  st.setStock(st.getStock() - det.getCantidad());
    	    				  stockDAO.updateStockVenta(st);

    	    				  //se crean los detalles de movimientos
    	    				  detMerc.setIdMovimiento(movMerc.getId());
    	    				  detMerc.setIdProducto(det.getIdProducto());
    	    				  detMerc.setCantidad(det.getCantidad());

    	    				  try{
    	    					  movMercDetalleDAO.createMovMercDetalle(detMerc);
    	    				  }catch (DuplicateKeyException e){
    	    					  log.error(e.getMessage(), e);
    	    					  throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "detalleventa.duplicate.key");
    	    				  }
    	    				  detMerc = new MovMercDetalleDTO();

    	    			  }else{
    	    				  throw new ServiceException(ErrorCode.STOCK_NOT_FOUND, "stock.notfound.producto.sucursal", det.getIdProducto(), dto.getIdSucursal());
    	    			  }
    	    		  }
    	    	  }
    		  }
    	  }
      }
      
      
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "notacreditocompra.duplicate.key");
    }

    return dto;
  }

}