package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.AperturaCierreDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.IngresosEgresosCajaDAO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.IngresosEgresosCajaDTO;
import py.com.sgs.persistence.dto.MovimientoRecaudacionDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.composite.AperturaCierreCompositeDTO;
import py.com.sgs.persistence.dto.composite.EntregaCajaCompositeDTO;
import py.com.sgs.util.report.ReportComprobanteMovimiento;
import py.com.sgs.util.report.ReportEntregaCaja;
import py.com.sgs.persistence.dto.composite.IngresosEgresosCajaCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.IngresosEgresosCajaService;
import py.com.sgs.web.services.MovimientoRecaudacionService;

public class IngresosEgresosCajaServiceImpl implements IngresosEgresosCajaService{

private Logger log = LoggerFactory.getLogger(getClass());  

  @Autowired
  private IngresosEgresosCajaDAO ingresosEgresosCajaDAO;
  
  @Autowired
  private AperturaCierreDAO aperturaCierreDAO;
  
  @Autowired 
  private MovimientoRecaudacionService movirecadao;
  

  public void setAperturaCierreDAO(AperturaCierreDAO aperturaCierreDAO) {
	this.aperturaCierreDAO = aperturaCierreDAO;
}

@Autowired
  private InfoRefOpcDAO infoRefDAO;
	
  public void setIngresosEgresosCajaDAO(IngresosEgresosCajaDAO ingresosEgresosCajaDAO) {
    this.ingresosEgresosCajaDAO = ingresosEgresosCajaDAO;
  }
  
  

  public void setInfoRefDAO(InfoRefOpcDAO infoRefDAO) {
	this.infoRefDAO = infoRefDAO;
  }



@Override
  public List<IngresosEgresosCajaDTO> listIngresosEgresosCaja(InvocationContext ic) throws ServiceException {
    List<IngresosEgresosCajaDTO> ingresosEgresosCajas =  ingresosEgresosCajaDAO.listIngresosEgresosCaja();

    return ingresosEgresosCajas;
  }

  @Override
  public IngresosEgresosCajaDTO getIngresosEgresosCaja(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    IngresosEgresosCajaDTO ingresosEgresosCaja =  ingresosEgresosCajaDAO.getIngresosEgresosCajaById(id);
    if (ingresosEgresosCaja == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ingresosegresoscaja.notfound", id);

    return ingresosEgresosCaja;
  }

  @Override
  public void deleteIngresosEgresosCaja(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    IngresosEgresosCajaDTO ingresosEgresosCaja =  ingresosEgresosCajaDAO.getIngresosEgresosCajaById(id);
    if(ingresosEgresosCaja == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ingresosegresoscaja.notfound", id);

    try {
      ingresosEgresosCajaDAO.deleteIngresosEgresosCaja(ingresosEgresosCaja);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "ingresosegresoscaja.constraint.violation", id);
    }

  }

  @Override
  public IngresosEgresosCajaDTO updateIngresosEgresosCaja(Long id, IngresosEgresosCajaDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    IngresosEgresosCajaDTO ingresosEgresosCaja = ingresosEgresosCajaDAO.getIngresosEgresosCajaById(id);
    if (ingresosEgresosCaja == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ingresosegresoscaja.notfound", id);

    ingresosEgresosCaja.setMonto(dto.getMonto());
    ingresosEgresosCaja.setDescripcion(dto.getDescripcion());
    ingresosEgresosCaja.setIdAperturaCierre(dto.getIdAperturaCierre());
 
    ingresosEgresosCajaDAO.updateIngresosEgresosCaja(ingresosEgresosCaja);

    return ingresosEgresosCaja;
  }
  
  @Override
  public IngresosEgresosCajaDTO createIngresosEgresosCaja(IngresosEgresosCajaDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
    	
	    	InfoRefOpcDTO movEgr = infoRefDAO.getInfoRefOpcByAbv("EGR");
	    	InfoRefOpcDTO extraccion = infoRefDAO.getInfoRefOpcByAbv("EXT");
	    	InfoRefOpcDTO ingreso = infoRefDAO.getInfoRefOpcByAbv("ING");
	    	Date fecha =new Date(System.currentTimeMillis());
    	
    if (movEgr.getId().equals(dto.getIdTipoMovimiento())) {
    	
    
    	AperturaCierreCompositeDTO datosMov = aperturaCierreDAO.getAperturaCierreById(dto.getIdAperturaCierre());
    	BigDecimal saldo = datosMov.getSaldoInicial().add(datosMov.getVentaEfectivo()).add(datosMov.getVentaTarjeta()).add(datosMov.getOtrosIngresos());
	    		  
	    	if (saldo != null && saldo.compareTo(BigDecimal.ZERO) > 0)
    		
    			  saldo = saldo.subtract(ingresosEgresosCajaDAO.getEgresosByIdAperturaCierre(dto.getIdAperturaCierre()));
		      
		     if (saldo.compareTo(BigDecimal.ZERO) <= 0) 
		    	  throw new ServiceException(ErrorCode.CAJA_SIN_SALDO,"caja.sin.saldo");
		    
		      saldo= saldo.subtract(dto.getMonto());
		    	  
		      if (saldo.compareTo(BigDecimal.ZERO) < 0) 
		    	  throw new ServiceException(ErrorCode.CAJA_SIN_SALDO,"caja.sin.saldo");
		      

		      
		      dto.setFechaHora(fecha);
		      ingresosEgresosCajaDAO.createIngresosEgresosCaja(dto);
		      
	      
		      if (dto.getIdMovimiento().equals(extraccion.getId())) {

			      MovimientoRecaudacionDTO movidto= new MovimientoRecaudacionDTO();
			      movidto.setFechaHora(fecha);
			      movidto.setIdAperturaCierre(dto.getIdAperturaCierre());
			      movidto.setIdMovimiento(dto.getIdMovimiento());
			      movidto.setIdTipoMovimiento(ingreso.getId());
			      movidto.setMonto(dto.getMonto());
			    	
			      movirecadao.createMovimientoRecaudacion(movidto, ic);
		    	  
		    	  
		    	  
		    	  
		      }else {
		    	  //si ess pago o cualquier egreso de caja que no sea extraccion se realizan 2 movimientos
		    	  
		    	  
			      MovimientoRecaudacionDTO moviIngre= new MovimientoRecaudacionDTO();
			      moviIngre.setFechaHora(fecha);
			      moviIngre.setIdAperturaCierre(dto.getIdAperturaCierre());
			      moviIngre.setIdMovimiento(dto.getIdMovimiento());
			      moviIngre.setIdTipoMovimiento(ingreso.getId());
			      moviIngre.setMonto(dto.getMonto());
			    	
			      movirecadao.createMovimientoRecaudacion(moviIngre, ic);
			      
			      MovimientoRecaudacionDTO moviEgre= new MovimientoRecaudacionDTO();
			      moviEgre.setFechaHora(fecha);
			      moviEgre.setIdAperturaCierre(dto.getIdAperturaCierre());
			      moviEgre.setIdMovimiento(dto.getIdMovimiento());
			      moviEgre.setIdTipoMovimiento(movEgr.getId());
			      moviEgre.setMonto(dto.getMonto());
			    	
			      movirecadao.createMovimientoRecaudacion(moviEgre, ic);
		    	  
		      }
	      
    }else {
    	
	    dto.setFechaHora(fecha);
	    ingresosEgresosCajaDAO.createIngresosEgresosCaja(dto);
    }
    
    
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "ingresosegresoscaja.duplicate.key");
    }

    return dto;
  }
  
  @Override
	public PrintDTO imprimir(Long id, InvocationContext ic) throws ServiceException{
		
		PrintDTO retorno = new PrintDTO();
		//se obtiene la entrega caja
		IngresosEgresosCajaCompositeDTO inEg = ingresosEgresosCajaDAO.getIngresosEgresosCompositeById(id);
		if(inEg == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"entrega.caja.notfound", id);
		
		retorno = this.getReportMovimiento(inEg);
		
		return retorno;
		
	}
	
	private PrintDTO getReportMovimiento(IngresosEgresosCajaCompositeDTO param) throws ServiceException {
	      PrintDTO reporte = new PrintDTO();
	      try {
	    	  ReportComprobanteMovimiento rMov = new ReportComprobanteMovimiento(param);
	          reporte.setBytes(rMov.generarPDF());
	          reporte.setFileName("movimiento" + param); 
	          // reporte.setFileName("14002IRE1000001A");
	          // MagicMatch match = Magic.getMagicMatch(reporte.getBytes());
	          // reporte.setMimeType(match.getMimeType());
	          // reporte.setType(match.getExtension());
	      } catch (Exception e) {
	          log.error(e.getMessage(), e);
	          reporte.setMimeType(null);
	          reporte.setType(null);
	          throw new ServiceException(ErrorCode.IMPRIMIR_OC_ERROR, "sgs.print.oc.error", param.getId());
	      }
	      return reporte;
	}



@Override
public IngresosEgresosCajaCompositeDTO getIngresosEgresosCajaCompositeDTO(Long id, InvocationContext ic) throws ServiceException {
	if (id == null || id <= 0)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

	    IngresosEgresosCajaCompositeDTO ingresosEgresosCaja =  ingresosEgresosCajaDAO.getIngresosEgresosCompositeById(id);
	    if (ingresosEgresosCaja == null)
	      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ingresosegresoscaja.notfound", id);

	    return ingresosEgresosCaja;
}

}