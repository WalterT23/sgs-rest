package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.GrupoProductosDAO;
import py.com.sgs.persistence.dao.ListaPreciosDAO;
import py.com.sgs.persistence.dao.ListaPreciosDetalleDAO;
import py.com.sgs.persistence.dao.ProductosDAO;
import py.com.sgs.persistence.dao.StockDAO;
import py.com.sgs.persistence.dao.SucursalDAO;
import py.com.sgs.persistence.dto.GrupoProductosDTO;
import py.com.sgs.persistence.dto.ListaPreciosDTO;
import py.com.sgs.persistence.dto.ListaPreciosDetalleDTO;
import py.com.sgs.persistence.dto.ProductosDTO;
import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.SucursalDTO;
import py.com.sgs.persistence.dto.composite.ListaPreciosCompositeDTO;
import py.com.sgs.persistence.dto.composite.ProductosCompositeDTO;
import py.com.sgs.persistence.dto.composite.ProductosGroupCompositeDTO;
import py.com.sgs.persistence.dto.composite.StockCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ProductosService;

public class ProductosServiceImpl implements ProductosService {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private ProductosDAO productosDAO;
	@Autowired
	private StockDAO stockDAO;
	@Autowired
	private ListaPreciosDAO listaPreciosDAO;
	@Autowired
	private ListaPreciosDetalleDAO lpDetalleDAO;
	@Autowired
	private SucursalDAO sucursalDAO;
	@Autowired
	private GrupoProductosDAO gruposDAO;
	
	public void setProductosDAO(ProductosDAO productosDAO) {
		this.productosDAO = productosDAO;
	}

	public void setStockDAO(StockDAO stockDAO) {
		this.stockDAO = stockDAO;
	}
	
	public void setListaPreciosDAO(ListaPreciosDAO listaPreciosDAO) {
		this.listaPreciosDAO = listaPreciosDAO;
	}
	
	public void setLpDetalleDAO(ListaPreciosDetalleDAO lpDetalleDAO){
		this.lpDetalleDAO = lpDetalleDAO;
	}
	
	public void setSucursalDAO(SucursalDAO sucursalDAO){
		this.sucursalDAO = sucursalDAO;
	}
	
	public void setGruposDAO(GrupoProductosDAO gruposDAO){
		this.gruposDAO = gruposDAO;
	}
	
	@Override
	public List<ProductosDTO> listProductos(InvocationContext ic) throws ServiceException {
		
		List<ProductosDTO> productos = productosDAO.listProductos();

		return productos;
	}

	@Override
	public ProductosDTO getProductosById(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");
		
		ProductosDTO productos = productosDAO.getProductosById(id);
		if (productos == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"productos.notfound", id);

		return productos;
	}

	@Override
	public ProductosCompositeDTO getProductosByCod(String cod, InvocationContext ic) throws ServiceException {
		
		if (cod == null || cod == "")
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null", "cod");
		
		ProductosCompositeDTO dto = new ProductosCompositeDTO();
		dto.setCod(cod);
		dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
		
		ProductosCompositeDTO productos = productosDAO.getProductosByCod(dto);
		
		if (productos == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"productos.notexist", cod);

		return productos;
	}
	
	@Override
	public List<ProductosDTO> getProductosLikeCod(String cod,InvocationContext ic) throws ServiceException {
		
		if (cod.trim().isEmpty())
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null");
		
		List<ProductosDTO> productos = productosDAO.getProductosLikeCod(cod);
  		
		return productos;
	}
	
	
	@Override
	public List<ProductosDTO> listProductosByGroups(ProductosGroupCompositeDTO dto, InvocationContext ic) throws ServiceException {
		
		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null");
		
		if (dto.getIncSubgroups()){
			List<GrupoProductosDTO> subgrupos = new ArrayList<GrupoProductosDTO>();
			for (GrupoProductosDTO grupo : dto.getGrupos()){
				subgrupos.addAll(gruposDAO.subgrupos(grupo.getId()));
			}
			dto.getGrupos().addAll(subgrupos);
		}
			
		List<ProductosDTO> productos = productosDAO.listProductosByGroups(dto);

		return productos;
	}

	
	@Override
	public void deleteProductos(Long id, InvocationContext ic)
			throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");
		
		ProductosDTO productos = productosDAO.getProductosById(id);
		if (productos == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"productos.notfound", id);

		try {
			productosDAO.deleteProductos(productos);

		} catch (DataIntegrityViolationException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION,
					"productos.constraint.violation", id);
		}

	}

	@Override
	public ProductosDTO updateProductos(Long id, ProductosDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		ProductosDTO productos = productosDAO.getProductosById(id);
		
		if (productos == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND, "productos.notfound", id);

		if (dto.getCod() != null)
			productos.setCod(dto.getCod());

		if (dto.getNombre() != null)
			productos.setNombre(dto.getNombre().trim());

		if (dto.getIdUnidadMed() != null)
			productos.setIdUnidadMed(dto.getIdUnidadMed());

		if (dto.getStockMin() != null)
			productos.setStockMin(dto.getStockMin());
		
		if (dto.getDescripcion() != null)
			productos.setDescripcion(dto.getDescripcion().trim());
		
		if (dto.getIdGrupo() != null)
			productos.setIdGrupo(dto.getIdGrupo());

		if (dto.getIdMarca() != null)
			productos.setIdMarca(dto.getIdMarca());

		if (dto.getIdTipoTributo() != null)
			productos.setIdTipoTributo(dto.getIdTipoTributo());
		
		if (dto.getPorcRecarga() != null)
			productos.setPorcRecarga(dto.getPorcRecarga());
		
		productosDAO.updateProductos(productos);

		if (dto.getCod() != null && !dto.getCod().isEmpty()) {
			List<StockDTO>  stock = stockDAO.getStockByIdProducto(productos.getId());
			for (StockDTO s: stock){
				s.setCodProducto(productos.getCod());
				stockDAO.updateStock(s);
			}

		}

		return productos;
	}

	@Override
	public ProductosDTO createProductos(ProductosDTO dto, InvocationContext ic)
			throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		try {
			dto.setNombre(dto.getNombre().trim());
			dto.setDescripcion(dto.getDescripcion().trim());
			if (dto.getPorcRecarga() == null)
				dto.setPorcRecarga(BigDecimal.ZERO);
			
			if (dto.getCod() == null && dto.getCod().isEmpty())
				throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null", dto.getCod());
			
			productosDAO.createProductos(dto);
			
			List<SucursalDTO> sucursales = sucursalDAO.listSucursal();
			for (SucursalDTO sucursal: sucursales){
				StockDTO stock = new StockDTO();
				stock.setCodProducto(dto.getCod());
				stock.setIdProducto(dto.getId());
				stock.setStock(0D);
				stock.setIdSucursal(sucursal.getId());
				stockDAO.insertStock(stock);
			}
			
			
			List<ListaPreciosCompositeDTO> listas = listaPreciosDAO.listListaPreciosAllSucs();
			if (listas != null){
				ListaPreciosDetalleDTO lpDetalle = new ListaPreciosDetalleDTO();
				for ( ListaPreciosCompositeDTO lpd: listas){
					lpDetalle.setIdListaPrecios(lpd.getId());
					lpDetalle.setIdProducto(dto.getId());
					lpDetalleDAO.createListaPreciosDetalle(lpDetalle);
				}
			}

		} catch (DuplicateKeyException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION,
					"productos.duplicate.key");
		}

		return dto;
	}

	@Override
	public String codProGen() {
		Long retorno = productosDAO.codProGen();
		
//		ProductosCompositeDTO validar = productosDAO.getProductosByCod(retorno.toString());
//		
//		if (validar == null)
//			return retorno.toString();
		
		return retorno.toString();
	}


}