package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.ProductosMarcaDAO;
import py.com.sgs.persistence.dto.ProductosMarcaDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ProductosMarcaService;

public class ProductosMarcaServiceImpl implements ProductosMarcaService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private ProductosMarcaDAO productosMarcaDAO;

  public void setProductosMarcaDAO(ProductosMarcaDAO productosMarcaDAO) {
    this.productosMarcaDAO = productosMarcaDAO;
  }

  @Override
  public List<ProductosMarcaDTO> listProductosMarca(InvocationContext ic) throws ServiceException {
    List<ProductosMarcaDTO> productosMarcas =  productosMarcaDAO.listProductosMarca();

    return productosMarcas;
  }

  @Override
  public ProductosMarcaDTO getProductosMarca(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ProductosMarcaDTO productosMarca =  productosMarcaDAO.getProductosMarcaById(id);
    if (productosMarca == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"productosmarca.notfound", id);

    return productosMarca;
  }

  @Override
  public void deleteProductosMarca(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ProductosMarcaDTO productosMarca =  productosMarcaDAO.getProductosMarcaById(id);
    if(productosMarca == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"productosmarca.notfound", id);

    try {
      productosMarcaDAO.deleteProductosMarca(productosMarca);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "productosmarca.constraint.violation", id);
    }

  }

  @Override
  public ProductosMarcaDTO updateProductosMarca(Long id, ProductosMarcaDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    ProductosMarcaDTO productosMarca = productosMarcaDAO.getProductosMarcaById(id);
    if (productosMarca == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"productosmarca.notfound", id);

    productosMarca.setDescripcion(dto.getDescripcion().trim());
 
    productosMarcaDAO.updateProductosMarca(productosMarca);

    return productosMarca;
  }

  @Override
  public ProductosMarcaDTO createProductosMarca(ProductosMarcaDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
      dto.setDescripcion(dto.getDescripcion().trim());
      productosMarcaDAO.createProductosMarca(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "productosmarca.duplicate.key");
    }

    return dto;
  }

}