package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.FormaPagoDAO;
import py.com.sgs.persistence.dto.FormaPagoDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.FormaPagoService;

public class FormaPagoServiceImpl implements FormaPagoService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private FormaPagoDAO formaPagoDAO;

  public void setFormaPagoDAO(FormaPagoDAO formaPagoDAO) {
    this.formaPagoDAO = formaPagoDAO;
  }

  @Override
  public List<FormaPagoDTO> listFormaPago(InvocationContext ic) throws ServiceException {
    List<FormaPagoDTO> formaPagos =  formaPagoDAO.listFormaPago();

    return formaPagos;
  }

  @Override
  public FormaPagoDTO getFormaPago(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    FormaPagoDTO formaPago =  formaPagoDAO.getFormaPagoById(id);
    if (formaPago == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"formapago.notfound", id);

    return formaPago;
  }

  @Override
  public void deleteFormaPago(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    FormaPagoDTO formaPago =  formaPagoDAO.getFormaPagoById(id);
    if(formaPago == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"formapago.notfound", id);

    try {
      formaPagoDAO.deleteFormaPago(formaPago);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "formapago.constraint.violation", id);
    }

  }

  @Override
  public FormaPagoDTO updateFormaPago(Long id, FormaPagoDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    FormaPagoDTO formaPago = formaPagoDAO.getFormaPagoById(id);
    if (formaPago == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"formapago.notfound", id);

    formaPago.setIdCabeceraVenta(dto.getIdCabeceraVenta());
    formaPago.setIdMedioPago(dto.getIdMedioPago());
    formaPago.setMontoPago(dto.getMontoPago());
    
    formaPagoDAO.updateFormaPago(formaPago);

    return formaPago;
  }

  @Override
  public FormaPagoDTO createFormaPago(FormaPagoDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
      formaPagoDAO.createFormaPago(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "formapago.duplicate.key");
    }

    return dto;
  }

}