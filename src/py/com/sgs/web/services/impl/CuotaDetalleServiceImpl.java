package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.CuotaDetalleDAO;
import py.com.sgs.persistence.dto.CuotaDetalleDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.CuotaDetalleService;

public class CuotaDetalleServiceImpl implements CuotaDetalleService{

private Logger log = LoggerFactory.getLogger(getClass()); 
@Autowired
  private CuotaDetalleDAO cuotaDetalleDAO;



  public void setCuotaDetalleDAO(CuotaDetalleDAO cuotaDetalleDAO) {
    this.cuotaDetalleDAO = cuotaDetalleDAO;
  }

  @Override
  public List<CuotaDetalleDTO> listCuotaDetalle(InvocationContext ic) throws ServiceException {
    List<CuotaDetalleDTO> cuotaDetalles =  cuotaDetalleDAO.listCuotaDetalle();

    return cuotaDetalles;
  }

  @Override
  public CuotaDetalleDTO getCuotaDetalle(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    CuotaDetalleDTO cuotaDetalle =  cuotaDetalleDAO.getCuotaDetalleById(id);
    if (cuotaDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cuotadetalle.notfound", id);

    return cuotaDetalle;
  }

  @Override
  public void deleteCuotaDetalle(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    CuotaDetalleDTO cuotaDetalle =  cuotaDetalleDAO.getCuotaDetalleById(id);
    if(cuotaDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cuotadetalle.notfound", id);

    try {
      cuotaDetalleDAO.deleteCuotaDetalle(cuotaDetalle);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "cuotadetalle.constraint.violation", id);
    }

  }

  @Override
  public CuotaDetalleDTO updateCuotaDetalle(Long id, CuotaDetalleDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    CuotaDetalleDTO cuotaDetalle = cuotaDetalleDAO.getCuotaDetalleById(id);
    if (cuotaDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cuotadetalle.notfound", id);

    cuotaDetalle.setIdCuotas(dto.getIdCuotas());
    cuotaDetalle.setNroCuota(dto.getNroCuota());
    cuotaDetalle.setMonto(dto.getMonto());
    cuotaDetalle.setFechaVencimiento(dto.getFechaVencimiento());
    cuotaDetalle.setIdEstado(dto.getIdEstado());
 
   
      cuotaDetalleDAO.updateCuotaDetalle(cuotaDetalle);
    

    return cuotaDetalle;
  }

  @Override
  public CuotaDetalleDTO createCuotaDetalle(CuotaDetalleDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {

         cuotaDetalleDAO.createCuotaDetalle(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "cuotadetalle.duplicate.key");
    }

    return dto;
  }

}