package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.AperturaCierreDAO;
import py.com.sgs.persistence.dao.CabeceraVentaDAO;
import py.com.sgs.persistence.dao.ClientesListaPrecioDAO;
import py.com.sgs.persistence.dao.DescuentosDAO;
import py.com.sgs.persistence.dao.DescuentosListaPreciosDAO;
import py.com.sgs.persistence.dao.DetallePagoDAO;
import py.com.sgs.persistence.dao.DetalleVentaDAO;
import py.com.sgs.persistence.dao.FormaPagoDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.ListaPreciosDAO;
import py.com.sgs.persistence.dao.MovMercDetalleDAO;
import py.com.sgs.persistence.dao.MovimientoMercaderiaDAO;
import py.com.sgs.persistence.dao.NotaCreditoVentaDAO;
import py.com.sgs.persistence.dao.ProductosDAO;
import py.com.sgs.persistence.dao.PuntoExpedicionDAO;
import py.com.sgs.persistence.dao.StockDAO;
import py.com.sgs.persistence.dao.TimbradoPtoExpedicionDAO;
import py.com.sgs.persistence.dao.UsuariosDAO;
import py.com.sgs.persistence.dto.AperturaCierreDTO;
import py.com.sgs.persistence.dto.CabeceraVentaDTO;
import py.com.sgs.persistence.dto.ClientesListaPrecioDTO;
import py.com.sgs.persistence.dto.DescuentosDetallesDTO;
import py.com.sgs.persistence.dto.DescuentosListaPreciosDTO;
import py.com.sgs.persistence.dto.DetallePagoDTO;
import py.com.sgs.persistence.dto.DetalleProMasVenDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.ListaPreciosDTO;
import py.com.sgs.persistence.dto.ListaPreciosDetalleDTO;
import py.com.sgs.persistence.dto.MovMercDetalleDTO;
import py.com.sgs.persistence.dto.MovimientoMercaderiaDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.PuntoExpedicionDTO;
import py.com.sgs.persistence.dto.ReporteVentasRealizadasDTO;
import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.TimbradoPtoExpedicionDTO;
import py.com.sgs.persistence.dto.UsuariosDTO;
import py.com.sgs.persistence.dto.composite.CabeceraVentaCompositeDTO;
import py.com.sgs.persistence.dto.composite.DescuentosCompositeDTO;
import py.com.sgs.persistence.dto.composite.DetalleVentaCompositeDTO;
import py.com.sgs.persistence.dto.composite.FormaPagoCompositeDTO;
import py.com.sgs.persistence.dto.composite.ListaPreciosCompositeDTO;
import py.com.sgs.persistence.dto.composite.MovimientoMercaderiaCompositeDTO;
import py.com.sgs.persistence.dto.composite.NotaCreditoVentaCompositeDTO;
import py.com.sgs.persistence.dto.composite.ProductosCompositeDTO;
import py.com.sgs.persistence.dto.consulta.ConsultaHorariosDTO;
import py.com.sgs.persistence.dto.consulta.ConsultaVentaDTO;
import py.com.sgs.persistence.dto.consulta.HorarioVentasDTO;
import py.com.sgs.persistence.dto.consulta.InfoVentasDTO;
import py.com.sgs.persistence.dto.consulta.ResumenHorarioVentasDTO;
import py.com.sgs.util.HorariosComparator;
import py.com.sgs.util.report.ReportDetalleVenta;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.CabeceraVentaService;

public class CabeceraVentaServiceImpl implements CabeceraVentaService{

	private Logger log = LoggerFactory.getLogger(getClass()); 
	@Autowired private CabeceraVentaDAO cabeceraVentaDAO;
	@Autowired	private DetalleVentaDAO detalleVentaDAO;
	@Autowired	private FormaPagoDAO formaPagoDAO;
	@Autowired	private DetallePagoDAO detallePagoDAO;
	@Autowired	private ProductosDAO productosDAO;
	@Autowired	private StockDAO stockDAO;
	@Autowired	private TimbradoPtoExpedicionDAO timbradoPtoExpedicionDAO;
	@Autowired	private PuntoExpedicionDAO puntoExpedicionDAO;
	@Autowired	private AperturaCierreDAO aperturaCierreDAO;
	@Autowired	private ClientesListaPrecioDAO clientesListaPrecioDAO;
	@Autowired	private ListaPreciosDAO listaPreciosDAO;
	@Autowired	private DescuentosListaPreciosDAO descuentosListaPreciosDAO;
	@Autowired	private DescuentosDAO descuentosDAO;
	@Autowired	private InfoRefOpcDAO infoRefOpcDAO;
	@Autowired	private MovimientoMercaderiaDAO movimientoMercaderiaDAO;
	@Autowired	private MovMercDetalleDAO movMercDetalleDAO;
	@Autowired	private UsuariosDAO usuariosDAO;
	@Autowired	private NotaCreditoVentaDAO notaCreditoVentaDAO;
	
	private static final SimpleDateFormat xx = new SimpleDateFormat("ddMMyyyy");
	
	public void setCabeceraVentaDAO(CabeceraVentaDAO cabeceraVentaDAO) {
		this.cabeceraVentaDAO = cabeceraVentaDAO;
	}

	@Override
	public List<CabeceraVentaDTO> listCabeceraVenta(InvocationContext ic) throws ServiceException {
		List<CabeceraVentaDTO> cabeceraVentas =  cabeceraVentaDAO.listCabeceraVenta(Long.parseLong(ic.getUserSucursal()));

		return cabeceraVentas;
	}

	@Override
	public CabeceraVentaCompositeDTO getCabeceraVenta(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		CabeceraVentaCompositeDTO cabeceraVenta =  cabeceraVentaDAO.getCabeceraVentaById(id);
		if (cabeceraVenta == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cabeceraventa.notfound", id);

		return cabeceraVenta;
	}
	
	@Override
	public CabeceraVentaCompositeDTO getCabeceraVentaByFactura(String nroFactura, InvocationContext ic) throws ServiceException {
		if (nroFactura == null || nroFactura.isEmpty())
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","nroFactura");

		CabeceraVentaCompositeDTO cabeceraVenta =  cabeceraVentaDAO.getCabeceraVentaByFactura(nroFactura);
		if (cabeceraVenta == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cabeceraventa.notfound", nroFactura);

		return cabeceraVenta;
	}

	@Override
	public void deleteCabeceraVenta(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		CabeceraVentaDTO cabeceraVenta =  cabeceraVentaDAO.getCabeceraVentaById(id);
		if(cabeceraVenta == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cabeceraventa.notfound", id);

		try {
			cabeceraVentaDAO.deleteCabeceraVenta(cabeceraVenta);
		}
		catch (DataIntegrityViolationException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "cabeceraventa.constraint.violation", id);
		}

	}

	@Override
	public CabeceraVentaDTO updateCabeceraVenta(Long id, CabeceraVentaDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		CabeceraVentaDTO cabeceraVenta = cabeceraVentaDAO.getCabeceraVentaById(id);
		if (cabeceraVenta == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cabeceraventa.notfound", id);

		cabeceraVenta.setIdTimbradoPtoExp(dto.getIdTimbradoPtoExp());
		cabeceraVenta.setNroFactura(dto.getNroFactura());
		cabeceraVenta.setCajero(dto.getCajero());
		cabeceraVenta.setGravada10(dto.getGravada10());
		cabeceraVenta.setGravada5(dto.getGravada5());
		cabeceraVenta.setExenta(dto.getExenta());
		cabeceraVenta.setIva10(dto.getIva10());
		cabeceraVenta.setIva5(dto.getIva5());
		cabeceraVenta.setDescuento(dto.getDescuento());
		cabeceraVenta.setNeto(dto.getNeto());

		cabeceraVentaDAO.updateCabeceraVenta(cabeceraVenta);

		return cabeceraVenta;
	}

	@Override
	public CabeceraVentaCompositeDTO createCabeceraVenta(CabeceraVentaCompositeDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {
			dto.setFechaCreacion(new Date(System.currentTimeMillis()));
			MovimientoMercaderiaCompositeDTO movMerc = new MovimientoMercaderiaCompositeDTO();
			
			//se debe obtener el usuario por su nombre de usuario
			UsuariosDTO usuario = usuariosDAO.getUsuariosByUser(new UsuariosDTO(ic.getUserPrincipal()));
			
			if(usuario == null){
				throw new ServiceException(ErrorCode.USUARIO_NO_EXISTE,"usuario.not.found", ic.getUserPrincipal());
			}
			
			//el id timbrado se debe obtener ahora de apertura cierre, buscando por el usuario, un registro que este abierto y sin fecha cierre
			AperturaCierreDTO ap = aperturaCierreDAO.getAperturaCierreVigenteByCajero(usuario.getId());
			if(ap == null)
				throw new ServiceException(ErrorCode.APERTURA_ACTIVA,"apertura.cierre.usuario.not.found", ic.getUserPrincipal());
			
			//se debe buscar el que es tipo F
			TimbradoPtoExpedicionDTO tpe = timbradoPtoExpedicionDAO.getTimbradoPtoExpedicionByIdPtoExpedicionFact(ap.getIdPuntoExpedicion());
			String nroFact = "";
			String nroFactAUtilizar = "";
			if(tpe != null){
				Integer nro = Integer.parseInt(tpe.getUltimoNumero()) + 1;
				Integer siguienteNro = Integer.parseInt(tpe.getUltimoNumero());
				//TODO verificar que el numero de factura, no debe exceder el ultimo numero disponible para el punto de exp.
				//se debe completar con ceros 
				nroFact = String.format("%07d", nro);
				nroFactAUtilizar = String.format("%07d", siguienteNro);
				tpe.setUltimoNumero(nroFact);
				
				//se debe actualiza el ultimo numero en la tabla
				timbradoPtoExpedicionDAO.actualizarUltNro(tpe);
				//al numero de factura se le agrega el punto de expedicion
				PuntoExpedicionDTO pe = puntoExpedicionDAO.getPuntoExpedicionById(tpe.getIdPtoExpedicion());
				
				if(pe != null){
					String nroPtoExp = pe.getPuntoExp().trim();
//					String peStr = String.format("%03d", nroPtoExp);
//					
//					nroFact = "001-" + peStr + "-" + nroFact;
					
					nroFactAUtilizar = "001-" + nroPtoExp + "-" + nroFactAUtilizar;
					
				}else{
					throw new ServiceException(ErrorCode.PTO_EXPEDICION_NO_ENCONTRADO,"punto.expedicion.not.found", tpe.getIdPtoExpedicion());
				}
				
				dto.setIdAperturaCierre(ap.getId());
			}else{
				throw new ServiceException(ErrorCode.TIMBRADO_PTO_EXPEDICION_NO_ENCONTRADO,"timbra.pto.expedicion.not.found", dto.getIdTimbradoPtoExp());
			}
			dto.setNroFactura(nroFactAUtilizar);
			dto.setFechaCreacion(new Date(System.currentTimeMillis()));
			dto.setIdCliente(dto.getCliente().getId());
			dto.setIdTimbradoPtoExp(tpe.getId());
			dto.setCajero(ic.getUserPrincipal());
			dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

			InfoRefOpcDTO tipMovEgreso= infoRefOpcDAO.getInfoRefOpcByAbv("EGR_M");
			InfoRefOpcDTO tipOpVenta=infoRefOpcDAO.getInfoRefOpcByAbv("VNTA");

			try{
				cabeceraVentaDAO.createCabeceraVenta(dto);
				
				//despues de crear la cabecera venta se debe crear el movimiento
				movMerc.setIdOperacion(dto.getId());
				movMerc.setIdTipoMovimiento(tipMovEgreso.getId());
				movMerc.setFechaMovimiento(new Date(System.currentTimeMillis()));
				movMerc.setUsuarioRegistro(ic.getUserPrincipal());
				movMerc.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
				movMerc.setTipoOperacion(tipOpVenta.getId());
				
				try{
					movimientoMercaderiaDAO.createMovimientoMercaderia(movMerc);
				}catch (DuplicateKeyException e){
					log.error(e.getMessage(), e);
					throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "mov.merc.duplicate.key");
				}
				
			}catch (DuplicateKeyException e){
				log.error(e.getMessage(), e);
				throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "cabeceraventa.duplicate.key");
			}

			DetalleVentaCompositeDTO detVenta = new DetalleVentaCompositeDTO();
			MovMercDetalleDTO detMerc = new MovMercDetalleDTO();
			//se crean los detalles de venta
			if(dto.getInfoVenta().getProductos() != null && !dto.getInfoVenta().getProductos().isEmpty()){
				for(ProductosCompositeDTO prod: dto.getInfoVenta().getProductos()){
					
					prod.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
					detVenta.setIdCabVenta(dto.getId());
					detVenta.setIdProducto(productosDAO.getProductosByIdNew(prod).getId());
					detVenta.setTipoIdentificadorFiscal(prod.getAbvTributo());
					detVenta.setPrecioTotal(prod.getTotal());
					detVenta.setPrecioUnitario(prod.getPrecioProducto());
					detVenta.setCantidad(prod.getCantidad());
					detVenta.setMontoDescuento(prod.getTotalDescuento());
					
					try{
						detalleVentaDAO.createDetalleVenta(detVenta);
					}catch (DuplicateKeyException e){
						log.error(e.getMessage(), e);
						throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "detalleventa.duplicate.key");
					}
					
					detVenta = new DetalleVentaCompositeDTO();
					
					//se debe actualizar el stock restando la cantidad vendida del producto de la cantidad en sto
					StockDTO st = stockDAO.stockByIdProducto(new StockDTO(prod.getId(), prod.getIdSucursal()));
					if(st != null){
						st.setStock(st.getStock() - prod.getCantidad());
						stockDAO.updateStockVenta(st);
					}else{
						throw new ServiceException(ErrorCode.STOCK_NOT_FOUND, "stock.notfound.producto.sucursal", prod.getId(), prod.getIdSucursal());
					}
					
					//se crean los detalles de movimientos
					detMerc.setIdMovimiento(movMerc.getId());
					detMerc.setIdProducto(prod.getId());
					detMerc.setCantidad(prod.getCantidad());
					
					try{
						movMercDetalleDAO.createMovMercDetalle(detMerc);
					}catch (DuplicateKeyException e){
						log.error(e.getMessage(), e);
						throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "mov.merc.det.duplicate.key");
					}
					
					detMerc = new MovMercDetalleDTO();
				}
			}
			
			dto.setGravada10(dto.getInfoVenta().getGravada10());
			dto.setGravada5(dto.getInfoVenta().getGravada5());
			dto.setExenta(dto.getInfoVenta().getExenta());
			dto.setNeto(dto.getInfoVenta().getMontoTotal());
			dto.setIva10(dto.getInfoVenta().getIva10());
			dto.setIva5(dto.getInfoVenta().getIva5());
			dto.setDescuento(dto.getInfoVenta().getDescuentos());
			
			cabeceraVentaDAO.updateCabeceraVenta(dto);

			//se crean las formas de pago
			if(dto.getFormasPago() != null && !dto.getFormasPago().isEmpty()){
				for(FormaPagoCompositeDTO f: dto.getFormasPago()){
					f.setIdCabeceraVenta(dto.getId());
					try{
						this.formaPagoDAO.createFormaPago(f);
					}catch (DuplicateKeyException e){
						log.error(e.getMessage(), e);
						throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "formapago.duplicate.key");
					}
					//se crean los detalles de pago
					if(f.getDetallesPagos() != null && !f.getDetallesPagos().isEmpty()){
						for(DetallePagoDTO dp: f.getDetallesPagos()){
							dp.setIdFormaPago(f.getId());
							
							try{
								this.detallePagoDAO.createDetallePago(dp);
							}catch (DuplicateKeyException e){
								log.error(e.getMessage(), e);
								throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "detallepago.duplicate.key");
							}
						}
					}
				}
			}else{
				throw new ServiceException(ErrorCode.FORMAS_DE_PAGO_NOT_FOUND, "cabeceraventa.formas.pago.notfound");
			}

		} 
		catch (ServiceException e){
			log.error(e.getMessage(), e);
			throw e;
		}

		return dto;
	}
	
	@Override
	public PrintDTO imprimirFacturaVenta(Long id, InvocationContext ic) throws ServiceException{
		
		PrintDTO retorno = new PrintDTO();
		//se obtiene la cabecera venta
		CabeceraVentaCompositeDTO venta = cabeceraVentaDAO.getCabeceraVentaById(id);
		if(venta == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cabeceraventa.notfound", id);
		//se agrega la empresa
		venta.setEmpresa(cabeceraVentaDAO.getEmpresaById(1L));
		//a los detalles se les debe cargar el precio unitario, el mismo se obtiene del stock por medio del codproducto
//		if(venta.getDetalles() != null && !venta.getDetalles().isEmpty()){
//			StockDTO stock = null;
//			for(DetalleVentaCompositeDTO dv: venta.getDetalles()){
//				stock = stockDAO.getStockByIdProducto(dv.getIdProducto());
//				if(stock != null){
//					dv.setPrecioUnitario(stock.getPrecioVenta());
//				}
//			}
//		}
		
		retorno = this.getReportDetalleVenta(venta);
		
		return retorno;
		
	}
	
	private PrintDTO getReportDetalleVenta(CabeceraVentaCompositeDTO param) throws ServiceException {
	      PrintDTO reporte = new PrintDTO();
	      try {
	          ReportDetalleVenta rDV = new ReportDetalleVenta(param);
	          reporte.setBytes(rDV.generarPDF());
	          reporte.setFileName(param.getNroFactura()); // descomentar para tomar el
	                                                  // id del ds ddt como nombre
	                                                  // del reporte
	          // reporte.setFileName("14002IRE1000001A");
	          // MagicMatch match = Magic.getMagicMatch(reporte.getBytes());
	          // reporte.setMimeType(match.getMimeType());
	          // reporte.setType(match.getExtension());
	      } catch (Exception e) {
	          log.error(e.getMessage(), e);
	          reporte.setMimeType(null);
	          reporte.setType(null);
	          throw new ServiceException(ErrorCode.IMPRIMIR_OC_ERROR, "sgs.print.oc.error", param.getId());
	      }
	      return reporte;
	}
	
	@Override
	public  InfoVentasDTO getResumenVenta(ConsultaVentaDTO dto, InvocationContext ic) throws ServiceException {
		
		InfoVentasDTO result = new InfoVentasDTO();
		BigDecimal valorZero = BigDecimal.ZERO;
		BigDecimal valor100 = new BigDecimal(100D);
		BigDecimal montoTotal = valorZero;
		BigDecimal exenta = valorZero;
		BigDecimal gravada5 = valorZero;
		BigDecimal gravada10 = valorZero;
		BigDecimal descuentos = valorZero;
		BigDecimal totalSinDescuento = valorZero;
		//solo para el tipo de descuento pague x lleve y
		Integer cantPromos = 0;
		Integer descontarXPromo = 0;
		Integer cantDescontar = 0;
		Integer resultado = 0;
		
//		//se debe obtener la lista de precios vigente del cliente para esa sucursal si existe
		ClientesListaPrecioDTO params = new ClientesListaPrecioDTO();
		params.setIdCliente(dto.getIdCliente());
		params.setIdSucursalListaP(Long.parseLong(ic.getUserSucursal()));
		List<ClientesListaPrecioDTO> precios = this.clientesListaPrecioDAO.getClientesLPbyClienteVigentes(params);
		
		ListaPreciosCompositeDTO listaP = null;
		ListaPreciosCompositeDTO listaPBase = null;
		
		List<ListaPreciosCompositeDTO> preciosList = this.listaPreciosDAO.listaPreciosBaseVigentes(params.getIdSucursalListaP());
				
	    if(precios != null && !precios.isEmpty()){
	    	//se puede tener solo una lista de precios vigente para un cliente en una sucursal
	    	ClientesListaPrecioDTO dtoCliPre = precios.get(0);
	    	//se recupera la lista de precios
	    	listaP = this.listaPreciosDAO.getListaPreciosById(dtoCliPre.getIdListaPrecio());
	    	if(listaP == null){
	    		//si la lista del cliente no se encuentra se carga la lista base como lista de precios a ser utilizada
		    	if(preciosList != null && !preciosList.isEmpty())
		    		listaP = preciosList.get(0);
	    	}	
	    	//solo se carga la lista base en el atributo si el cliente tiene una lista especial
	    	if(preciosList != null && !preciosList.isEmpty())
	    		listaPBase = preciosList.get(0);
	    	
	    }else{
	    	//se toma la lista de precios por defecto
	    	if(preciosList != null && !preciosList.isEmpty()){
	    		listaP = preciosList.get(0);
	    		
	    	}else{
	    		throw new ServiceException(ErrorCode.LP_BASE_NOT_FOUND, "none.listaprecios.base");
	    	}
	    }
	    
	    //una vez obtenida la lista de precios se debe verificar si la misma posee descuentos asociados
	    DescuentosListaPreciosDTO dlp = this.descuentosListaPreciosDAO.getDescuentosListaPreciosByLista(listaP.getId());
	    
	    if(dlp != null){
	    	//la lista esta asociada a un descuento vigente, por lo tanto los precios se calculan en base al descuento
	    	//se debe obtener el descuento con sus detalles
	    	DescuentosCompositeDTO descuento = this.descuentosDAO.getDescuentosById(dlp.getIdDescuento());
	    	if(descuento != null){
	    		if(descuento.getDetalles() != null && !descuento.getDetalles().isEmpty()){
	    			for(ProductosCompositeDTO p: dto.getProductos()){
	    				for(DescuentosDetallesDTO dd: descuento.getDetalles()){
	    					if(p.getId().compareTo(dd.getIdProducto()) == 0){
	    						//quiere decir que este producto posee un descuento
	    						p.setDetalleDescuento(dd);
	    					}
	    				}
	    			}
	    		}else{
//	    			throw new ServiceException(ErrorCode.DESCUENTO_SIN_DETALLES, "descuento.detalles.not.found", descuento.getId());
	    			//se da error o se usa la politica de no trancar hasta donde se pueda?
	    		}
	    		
	    		//cargar monto total de descuentos antes de retornar
	    		for(ProductosCompositeDTO p: dto.getProductos()){
	    			for(ListaPreciosDetalleDTO lp: listaP.getDetalles()){
		    			if(p.getId().compareTo(lp.getIdProducto()) == 0){
		    				if(lp.getPrecioVenta() != null && lp.getPrecioVenta().compareTo(valorZero) > 0){
		    					p.setPrecioProducto(lp.getPrecioVenta());
		    				}
		    				break;
		    			}
	    			}
		    			//si no tiene precio se debe buscar en la lista base
		    			if(p.getPrecioProducto() == null || p.getPrecioProducto().compareTo(valorZero) == 0){
		    				//se debe verificar si no es lista base, si el producto tiene precio en esa lista
		    				if(listaPBase != null){
		    					//esta lista solo estara cargada si el cliente tiene una lista especial
		    					for(ListaPreciosDetalleDTO lpd: listaP.getDetalles()){
		    		    			if(p.getId().compareTo(lpd.getIdProducto()) == 0){
		    		    				if(lpd.getPrecioVenta() != null && lpd.getPrecioVenta().compareTo(valorZero) > 0){
		    		    					p.setPrecioProducto(lpd.getPrecioVenta());
		    		    				}
		    		    				break;
		    		    			}
		    		    		}
		    				}
		    			}
		    			//si el precio del producto sigue siendo nulo o cero se lanza un error
		    			if(p.getPrecioProducto() == null || p.getPrecioProducto().compareTo(valorZero) == 0){
		    				throw new ServiceException(ErrorCode.PRODUCTO_SIN_PRECIO, "producto.precio.not.found", p.getId());
		    			}
		    			
		    			//DESCUENTOS
		    			//se necesita saber la forma de calcular el precio del producto y verificar el tipo de tributo
		    			if(p.getDetalleDescuento() != null){
		    				//se debe tratar cada tipo de descuento
		    				if(p.getDetalleDescuento().getPorcentaje() != null){
		    					//se descuenta un porcentaje
		    					p.setTotal(p.getPrecioProducto().multiply(new BigDecimal(p.getCantidad())));
		    					totalSinDescuento = p.getTotal();
		    					p.setTotal(p.getTotal().multiply(new BigDecimal(p.getDetalleDescuento().getPorcentaje())));
		    					p.setTotal(p.getTotal().divide(valor100));
		    					p.setTotalDescuento(totalSinDescuento.subtract(p.getTotal()));
		    					
		    					descuentos = descuentos.add(totalSinDescuento.subtract(p.getTotal()));
		    					totalSinDescuento = valorZero;
		    					
		    				}else if(p.getDetalleDescuento().getMonto() != null){
		    					//se descuenta un monto fijo
		    					p.setTotal(p.getPrecioProducto().multiply(new BigDecimal(p.getCantidad())));
		    					totalSinDescuento = p.getTotal();
		    					p.setTotal(p.getTotal().subtract(p.getDetalleDescuento().getMonto().multiply(new BigDecimal(p.getCantidad()))));
		    					p.setTotalDescuento(totalSinDescuento.subtract(p.getTotal()));
		    					
		    					descuentos = descuentos.add(totalSinDescuento.subtract(p.getTotal()));
		    					totalSinDescuento = valorZero;
		    					
		    				}else if(p.getDetalleDescuento().getCantACobrar() != null){
		    					cantPromos = p.getCantidad().intValue() / p.getDetalleDescuento().getCantALlevar().intValue();
		    					descontarXPromo = p.getDetalleDescuento().getCantALlevar() - p.getDetalleDescuento().getCantACobrar();
		    					cantDescontar = cantPromos * descontarXPromo;
		    					resultado = p.getCantidad().intValue() - cantDescontar;
		    					
		    					p.setTotal(p.getPrecioProducto().multiply(new BigDecimal(resultado)));
		    					p.setTotalDescuento(p.getPrecioProducto().multiply(new BigDecimal(cantDescontar)));
		    					descuentos = descuentos.add(p.getPrecioProducto().multiply(new BigDecimal(cantDescontar)));
		    					totalSinDescuento = valorZero;
		    				}
		    			}else{
		    				p.setTotal(p.getPrecioProducto().multiply(new BigDecimal(p.getCantidad())));
		    			}
		    			
		    			if("TTIV5".equalsIgnoreCase(p.getAbvTributo())){
		    				gravada5 = gravada5.add(p.getTotal());
		    			}else if("TTI10".equalsIgnoreCase(p.getAbvTributo())){
		    				gravada10 = gravada10.add(p.getTotal());
		    			}else{
		    				exenta = exenta.add(p.getTotal());
		    			}

		    			montoTotal = montoTotal.add(p.getTotal());
		    		
	    		}
	    	}
	    	
	    }else{
	    	//la lista no posee descuentos, los precios se toman directamente de la lista de precios
	    	for(ProductosCompositeDTO p: dto.getProductos()){
	    		for(ListaPreciosDetalleDTO lp: listaP.getDetalles()){
	    			if(p.getId().compareTo(lp.getIdProducto()) == 0){
	    				if(lp.getPrecioVenta() != null && lp.getPrecioVenta().compareTo(valorZero) > 0){
	    					p.setPrecioProducto(lp.getPrecioVenta());
	    				}
	    				break;
	    			}
	    		}
	    		//si no tiene precio se debe buscar en la lista base
    			if(p.getPrecioProducto() == null || p.getPrecioProducto().compareTo(valorZero) == 0){
    				//se debe verificar si no es lista base, si el producto tiene precio en esa lista
    				if(listaPBase != null){
    					//esta lista solo estara cargada si el cliente tiene una lista especial
    					for(ListaPreciosDetalleDTO lp: listaP.getDetalles()){
    		    			if(p.getId().compareTo(lp.getIdProducto()) == 0){
    		    				if(lp.getPrecioVenta() != null && lp.getPrecioVenta().compareTo(valorZero) > 0){
    		    					p.setPrecioProducto(lp.getPrecioVenta());
    		    				}
    		    				break;
    		    			}
    		    		}
    				}
    			}
    			//si el precio del producto sigue siendo nulo o cero se lanza un error
    			if(p.getPrecioProducto() == null || p.getPrecioProducto().compareTo(valorZero) == 0){
    				throw new ServiceException(ErrorCode.PRODUCTO_SIN_PRECIO, "producto.precio.not.found", p.getId());
    			}

    			//se necesita saber la forma de calcular el precio del producto y verificar el tipo de tributo
    			p.setTotal(p.getPrecioProducto().multiply(new BigDecimal(p.getCantidad())));

    			if("TTIV5".equalsIgnoreCase(p.getAbvTributo())){
    				gravada5 = gravada5.add(p.getTotal());
    			}else if("TTI10".equalsIgnoreCase(p.getAbvTributo())){
    				gravada10 = gravada10.add(p.getTotal());
    			}else{
    				exenta = exenta.add(p.getTotal());
    			}

    			montoTotal = montoTotal.add(p.getTotal());
	    	}
	    }
	    
	    //antes de retornar se calculan los montos iva correspondientes
	    if(gravada5 != null && gravada5.compareTo(valorZero) > 0){
	    	result.setIva5(gravada5.divide(new BigDecimal(22), 0, RoundingMode.HALF_UP));
	    	result.setGravada5(gravada5);
	    }else{
	    	result.setIva5(valorZero);
	    	result.setGravada5(valorZero);
	    }
	    
    	if(gravada10 != null && gravada10.compareTo(valorZero) > 0){
	    	result.setIva10(gravada10.divide(new BigDecimal(11), 0, RoundingMode.HALF_UP));
	    	result.setGravada10(gravada10);
	    }else{
	    	result.setIva10(valorZero);
	    	result.setGravada10(valorZero);
	    }
    	
    	if(exenta != null && exenta.compareTo(valorZero) > 0){
    		result.setExenta(exenta);
    	}else{
    		result.setExenta(valorZero);
    	}
    	
    	result.setDescuentos(descuentos);
    	result.setMontoTotal(result.getGravada10().add(result.getGravada5()));
    	result.setMontoTotal(result.getMontoTotal().add(result.getExenta()));
//    	result.setMontoTotal(result.getMontoTotal().subtract(descuentos));
    	result.setProductos(dto.getProductos());
		
		return result;
		
	}
	
	@Override
	public  ConsultaHorariosDTO ventasPorHorarios(ConsultaHorariosDTO param, InvocationContext ic) throws ServiceException{
		
		if (param == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");
		
		if (param.getFechaDesde() == null){ 
	    	 Calendar fechaD = Calendar.getInstance();
	    	 int horas = fechaD.get(Calendar.HOUR_OF_DAY);
	    	 fechaD.add(Calendar.DATE, -7);
	    	 fechaD.add(Calendar.HOUR_OF_DAY, -horas);
	    	 param.setFechaDesde(fechaD.getTime());
	    }
	    
	    if (param.getFechaHasta() == null){ 
		   	 Calendar fechaH = Calendar.getInstance();
		   	param.setFechaHasta(fechaH.getTime());
	    }
		
		//recuperar la lista de ventas
		HorarioVentasDTO resumen = new HorarioVentasDTO();
		param.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
		Calendar fechaVenta = Calendar.getInstance();
		List<CabeceraVentaDTO> ventas = cabeceraVentaDAO.listByParams(param);
		Integer horaVenta = 0;
		
		if(ventas != null && !ventas.isEmpty()){
			
			for(CabeceraVentaDTO cb: ventas){
				fechaVenta.setTime(cb.getFechaCreacion());
				horaVenta = fechaVenta.get(Calendar.HOUR_OF_DAY);
				//preguntar por el horario para acumular monto y cantidad
				if(horaVenta == 7){
					resumen.setCantidad7a8(resumen.getCantidad7a8() + 1D);
					resumen.setMonto7a8(resumen.getMonto7a8().add(cb.getNeto()));
				}else if( horaVenta == 8){
					resumen.setCantidad8a9(resumen.getCantidad8a9() + 1D);
					resumen.setMonto8a9(resumen.getMonto8a9().add(cb.getNeto()));
				}else if( horaVenta == 9){
					resumen.setCantidad9a10(resumen.getCantidad9a10() + 1D);
					resumen.setMonto9a10(resumen.getMonto9a10().add(cb.getNeto()));
				}else if( horaVenta == 10){
					resumen.setCantidad10a11(resumen.getCantidad10a11() + 1D);
					resumen.setMonto10a11(resumen.getMonto10a11().add(cb.getNeto()));
				}else if( horaVenta == 11){
					resumen.setCantidad11a12(resumen.getCantidad11a12() + 1D);
					resumen.setMonto11a12(resumen.getMonto11a12().add(cb.getNeto()));
				}else if( horaVenta == 12){
					resumen.setCantidad12a13(resumen.getCantidad12a13() + 1D);
					resumen.setMonto12a13(resumen.getMonto12a13().add(cb.getNeto()));
				}else if( horaVenta == 13){
					resumen.setCantidad13a14(resumen.getCantidad13a14() + 1D);
					resumen.setMonto13a14(resumen.getMonto13a14().add(cb.getNeto()));
				}else if( horaVenta == 14){
					resumen.setCantidad14a15(resumen.getCantidad14a15() + 1D);
					resumen.setMonto14a15(resumen.getMonto14a15().add(cb.getNeto()));
				}else if( horaVenta == 15){
					resumen.setCantidad15a16(resumen.getCantidad15a16() + 1D);
					resumen.setMonto15a16(resumen.getMonto15a16().add(cb.getNeto()));
				}else if( horaVenta == 16){
					resumen.setCantidad16a17(resumen.getCantidad16a17() + 1D);
					resumen.setMonto16a17(resumen.getMonto16a17().add(cb.getNeto()));
				}else if( horaVenta == 17){
					resumen.setCantidad17a18(resumen.getCantidad17a18() + 1D);
					resumen.setMonto17a18(resumen.getMonto17a18().add(cb.getNeto()));
				}else if( horaVenta == 18){
					resumen.setCantidad18a19(resumen.getCantidad18a19() + 1D);
					resumen.setMonto18a19(resumen.getMonto18a19().add(cb.getNeto()));
				}else if( horaVenta == 19){
					resumen.setCantidad19a20(resumen.getCantidad19a20() + 1D);
					resumen.setMonto19a20(resumen.getMonto19a20().add(cb.getNeto()));
				}else if( horaVenta == 20){
					resumen.setCantidad20a21(resumen.getCantidad20a21() + 1D);
					resumen.setMonto20a21(resumen.getMonto20a21().add(cb.getNeto()));
				}else if( horaVenta == 21){
					resumen.setCantidad21a22(resumen.getCantidad21a22() + 1D);
					resumen.setMonto21a22(resumen.getMonto21a22().add(cb.getNeto()));
				}
			}
		}
		//recuperar las notas de credito venta
		List<NotaCreditoVentaCompositeDTO> notasCred = notaCreditoVentaDAO.listByParams(param);
		if(notasCred != null && !notasCred.isEmpty()){
			for(NotaCreditoVentaCompositeDTO nc: notasCred){
				fechaVenta.setTime(nc.getFechaCreacion());
				horaVenta = fechaVenta.get(Calendar.HOUR_OF_DAY);
				//preguntar por el horario para acumular monto y cantidad
				if(horaVenta == 7){
					resumen.setCantidad7a8(resumen.getCantidad7a8() + 1D);
					resumen.setMonto7a8(resumen.getMonto7a8().add(nc.getMontoTotal()));
				}else if( horaVenta == 8){
					resumen.setCantidad8a9(resumen.getCantidad8a9() + 1D);
					resumen.setMonto8a9(resumen.getMonto8a9().add(nc.getMontoTotal()));
				}else if( horaVenta == 9){
					resumen.setCantidad9a10(resumen.getCantidad9a10() + 1D);
					resumen.setMonto9a10(resumen.getMonto9a10().add(nc.getMontoTotal()));
				}else if( horaVenta == 10){
					resumen.setCantidad10a11(resumen.getCantidad10a11() + 1D);
					resumen.setMonto10a11(resumen.getMonto10a11().add(nc.getMontoTotal()));
				}else if( horaVenta == 11){
					resumen.setCantidad11a12(resumen.getCantidad11a12() + 1D);
					resumen.setMonto11a12(resumen.getMonto11a12().add(nc.getMontoTotal()));
				}else if( horaVenta == 12){
					resumen.setCantidad12a13(resumen.getCantidad12a13() + 1D);
					resumen.setMonto12a13(resumen.getMonto12a13().add(nc.getMontoTotal()));
				}else if( horaVenta == 13){
					resumen.setCantidad13a14(resumen.getCantidad13a14() + 1D);
					resumen.setMonto13a14(resumen.getMonto13a14().add(nc.getMontoTotal()));
				}else if( horaVenta == 14){
					resumen.setCantidad14a15(resumen.getCantidad14a15() + 1D);
					resumen.setMonto14a15(resumen.getMonto14a15().add(nc.getMontoTotal()));
				}else if( horaVenta == 15){
					resumen.setCantidad15a16(resumen.getCantidad15a16() + 1D);
					resumen.setMonto15a16(resumen.getMonto15a16().add(nc.getMontoTotal()));
				}else if( horaVenta == 16){
					resumen.setCantidad16a17(resumen.getCantidad16a17() + 1D);
					resumen.setMonto16a17(resumen.getMonto16a17().add(nc.getMontoTotal()));
				}else if( horaVenta == 17){
					resumen.setCantidad17a18(resumen.getCantidad17a18() + 1D);
					resumen.setMonto17a18(resumen.getMonto17a18().add(nc.getMontoTotal()));
				}else if( horaVenta == 18){
					resumen.setCantidad18a19(resumen.getCantidad18a19() + 1D);
					resumen.setMonto18a19(resumen.getMonto18a19().add(nc.getMontoTotal()));
				}else if( horaVenta == 19){
					resumen.setCantidad19a20(resumen.getCantidad19a20() + 1D);
					resumen.setMonto19a20(resumen.getMonto19a20().add(nc.getMontoTotal()));
				}else if( horaVenta == 20){
					resumen.setCantidad20a21(resumen.getCantidad20a21() + 1D);
					resumen.setMonto20a21(resumen.getMonto20a21().add(nc.getMontoTotal()));
				}else if( horaVenta == 21){
					resumen.setCantidad21a22(resumen.getCantidad21a22() + 1D);
					resumen.setMonto21a22(resumen.getMonto21a22().add(nc.getMontoTotal()));
				}
			}
		}
		
		int cantidadHoras = 15;
		List<ResumenHorarioVentasDTO> detalles = new ArrayList<ResumenHorarioVentasDTO>();
		ResumenHorarioVentasDTO resumenHorario = new ResumenHorarioVentasDTO();
		
		for(int i = 0; i<cantidadHoras; i++){
			resumenHorario.setId(new Long(i+1));
			if(i == 0){
				resumenHorario.setCantidad(resumen.getCantidad7a8());
				resumenHorario.setMonto(resumen.getMonto7a8());
				resumenHorario.setDescripcion("07:00 a 08:00");
			}else if(i == 1){
				resumenHorario.setCantidad(resumen.getCantidad8a9());
				resumenHorario.setMonto(resumen.getMonto8a9());
				resumenHorario.setDescripcion("08:00 a 09:00");
			}else if(i == 2){
				resumenHorario.setCantidad(resumen.getCantidad9a10());
				resumenHorario.setMonto(resumen.getMonto9a10());
				resumenHorario.setDescripcion("09:00 a 10:00");
			}else if(i == 3){
				resumenHorario.setCantidad(resumen.getCantidad10a11());
				resumenHorario.setMonto(resumen.getMonto10a11());
				resumenHorario.setDescripcion("10:00 a 11:00");
			}else if(i == 4){
				resumenHorario.setCantidad(resumen.getCantidad11a12());
				resumenHorario.setMonto(resumen.getMonto11a12());
				resumenHorario.setDescripcion("11:00 a 12:00");
			}else if(i == 5){
				resumenHorario.setCantidad(resumen.getCantidad12a13());
				resumenHorario.setMonto(resumen.getMonto12a13());
				resumenHorario.setDescripcion("12:00 a 13:00");
			}else if(i == 6){
				resumenHorario.setCantidad(resumen.getCantidad13a14());
				resumenHorario.setMonto(resumen.getMonto13a14());
				resumenHorario.setDescripcion("13:00 a 14:00");
			}else if(i == 7){
				resumenHorario.setCantidad(resumen.getCantidad14a15());
				resumenHorario.setMonto(resumen.getMonto14a15());
				resumenHorario.setDescripcion("14:00 a 15:00");
			}else if(i == 8){
				resumenHorario.setCantidad(resumen.getCantidad15a16());
				resumenHorario.setMonto(resumen.getMonto15a16());
				resumenHorario.setDescripcion("15:00 a 16:00");
			}else if(i == 9){
				resumenHorario.setCantidad(resumen.getCantidad16a17());
				resumenHorario.setMonto(resumen.getMonto16a17());
				resumenHorario.setDescripcion("16:00 a 17:00");
			}else if(i == 10){
				resumenHorario.setCantidad(resumen.getCantidad17a18());
				resumenHorario.setMonto(resumen.getMonto17a18());
				resumenHorario.setDescripcion("17:00 a 18:00");
			}else if(i == 11){
				resumenHorario.setCantidad(resumen.getCantidad18a19());
				resumenHorario.setMonto(resumen.getMonto18a19());
				resumenHorario.setDescripcion("18:00 a 19:00");
			}else if(i == 12){
				resumenHorario.setCantidad(resumen.getCantidad19a20());
				resumenHorario.setMonto(resumen.getMonto19a20());
				resumenHorario.setDescripcion("19:00 a 20:00");
			}else if(i == 13){
				resumenHorario.setCantidad(resumen.getCantidad20a21());
				resumenHorario.setMonto(resumen.getMonto20a21());
				resumenHorario.setDescripcion("20:00 a 21:00");
			}else if(i == 14){
				resumenHorario.setCantidad(resumen.getCantidad21a22());
				resumenHorario.setMonto(resumen.getMonto21a22());
				resumenHorario.setDescripcion("21:00 a 22:00");
			}
			
			detalles.add(resumenHorario);
			resumenHorario = new ResumenHorarioVentasDTO();
		}
		Collections.sort(detalles, new HorariosComparator());
		param.setDetalles(detalles);

		return param;
	}
	
	@Override
	public FileReportDTO ventasPorHorariosImpresion(ConsultaHorariosDTO dto, InvocationContext ic)
			throws Exception {
		FileReportDTO archivo = new FileReportDTO();
		if (dto != null) {
			ConsultaHorariosDTO datosRepo = this.ventasPorHorarios(dto, ic);
			ReportGenerator creador = new ReportGenerator();
			BigDecimal suma = new BigDecimal(0);
			for (ResumenHorarioVentasDTO da : datosRepo.getDetalles()) {
				suma = suma.add(da.getMonto());
			}
			HashMap<String, Object> map = new HashMap<>();
			map.put("fechaDesde", datosRepo.getFechaDesde());
			map.put("fechaHasta", datosRepo.getFechaHasta());
			map.put("suma", suma);
			try {
				archivo.setArchivo(
						creador.getByteReports("E", ic, "ventasHorarios.jasper", datosRepo.getDetalles(), map));
				archivo.setTipo(".xls");
				archivo.setNombre("ventas-por-horarios-" + xx.format(datosRepo.getFechaDesde()) + "-"
						+ xx.format(datosRepo.getFechaHasta()));
			} catch (Exception e) {
				log.info(e.getLocalizedMessage());
				throw e;
			}
		}

		return archivo;
	}


}