package py.com.sgs.web.services.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.ClientesDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dto.ClientesDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.composite.ClientesCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ClientesService;

public class ClientesServiceImpl implements ClientesService{

private Logger log = LoggerFactory.getLogger(getClass());  

@Autowired
private InfoRefOpcDAO infoRefDAO;

@Autowired
private ClientesDAO clientesDAO;


  public void setClientesDAO(ClientesDAO clientesDAO) {
    this.clientesDAO = clientesDAO;
  }

  @Override
  public List<ClientesCompositeDTO> listClientes(InvocationContext ic) throws ServiceException {
    List<ClientesCompositeDTO> clientes =  clientesDAO.listClientes();

    return clientes;
  }

  @Override
  public ClientesCompositeDTO getClientes(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ClientesCompositeDTO clientes =  clientesDAO.getClientesById(id);
    if (clientes == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"clientes.notfound", id);

    return clientes;
  }
  
  
  @Override
  public List<ClientesCompositeDTO> getClientesByRuc(String ruc, InvocationContext ic) throws ServiceException {
  	if (ruc.trim().isEmpty())
  		throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null");
  	
  	List<ClientesCompositeDTO> clientes = clientesDAO.getClientesByRuc(ruc.toUpperCase());
  	if (clientes == null)
  		throw new ServiceException(ErrorCode.NO_DATA_FOUND,"clientes.notfound", ruc);
  	
  	return clientes;
  }
  
  
  @Override
  public void deleteClientes(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ClientesDTO clientes =  clientesDAO.getClientesById(id);
    if(clientes == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"clientes.notfound", id);

    try {
      clientesDAO.deleteClientes(clientes);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "clientes.constraint.violation", id);
    }

  }

  @Override
  public ClientesDTO updateClientes(Long id, ClientesDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    ClientesDTO clientes = clientesDAO.getClientesById(id);
    if (clientes == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"clientes.notfound", id);

    	
    if (dto.getRuc().trim().isEmpty())
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"clientes.nullruc");
    
    if (dto.getRazonSocial().trim().isEmpty())
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"clientes.nullrazonsocial");
   
       	
    
    if (dto.getNombreFantasia() != null)
    	clientes.setNombreFantasia(dto.getNombreFantasia().trim());
    
    if(dto.getDireccion() != null)
    	clientes.setDireccion(dto.getDireccion().trim());
    
    if(dto.getTelefono() != null)
    	clientes.setTelefono(dto.getTelefono().trim());
    
    if(dto.getCorreo() != null)
    	clientes.setCorreo(dto.getCorreo().trim());
    
    if(dto.getIdEstado() != null)
    	clientes.setIdEstado(dto.getIdEstado());
    
    if(dto.getFechaRegistro() != null)
    	clientes.setFechaRegistro(dto.getFechaRegistro());
    
    
    clientes.setRuc(dto.getRuc().trim());
    clientes.setRazonSocial(dto.getRazonSocial().trim());
    InfoRefOpcDTO estConf = infoRefDAO.getInfoRefOpcByAbv("ESTAC");
    dto.setIdEstado(estConf.getId());
    clientes.setFechaModificacion(new Date(System.currentTimeMillis()));
    clientesDAO.updateClientes(clientes);

    return clientes;
  }

  @Override
  public ClientesDTO createClientes(ClientesDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
    	
     	
        if (dto.getRuc().trim().isEmpty())
        	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"clientes.nullruc");
        
        if (dto.getRazonSocial().trim().isEmpty())
        	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"clientes.nullrazonsocial");
        
        
        dto.setRuc(dto.getRuc().trim());
        dto.setRazonSocial(dto.getRazonSocial().trim());
    	dto.setFechaRegistro(new Date(System.currentTimeMillis()));
    	InfoRefOpcDTO estConf = infoRefDAO.getInfoRefOpcByAbv("ESTAC");
        dto.setIdEstado(estConf.getId());
    	clientesDAO.createClientes(dto);

    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "clientes.duplicate.key");
    }

    return dto;
  }

}