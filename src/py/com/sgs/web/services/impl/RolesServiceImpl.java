package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.RolesDAO;
import py.com.sgs.persistence.dto.RolesDTO;
import py.com.sgs.persistence.dto.UsuarioRolDTO;
import py.com.sgs.persistence.dto.composite.RolesCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.RolesService;

public class RolesServiceImpl implements RolesService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private RolesDAO rolesDAO;


  public void setRolesDAO(RolesDAO rolesDAO) {
    this.rolesDAO = rolesDAO;
  }

  @Override
  public List<RolesCompositeDTO> listRoles(InvocationContext ic) throws ServiceException {
    List<RolesCompositeDTO> roles =  rolesDAO.listRoles();

    return roles;
  }
  
  @Override
  public List<RolesCompositeDTO> getByUsuario(Long idUsuario, InvocationContext ic) throws ServiceException {
    List<RolesCompositeDTO> roles =  rolesDAO.getByUsuario(idUsuario);

    return roles;
  }
  
  @Override
  public List<RolesCompositeDTO> getDispByUsuario(Long idUsuario, InvocationContext ic) throws ServiceException {
    List<RolesCompositeDTO> roles =  rolesDAO.getDispByUsuario(idUsuario);

    return roles;
  }
  
  @Override
  public RolesDTO getRoles(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    RolesDTO roles =  rolesDAO.getRolesById(id);
    if (roles == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"roles.notfound", id);

    return roles;
  }

  @Override
  public void deleteRoles(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    RolesDTO roles =  rolesDAO.getRolesById(id);
    if(roles == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"roles.notfound", id);

    try {
      rolesDAO.deleteRoles(roles);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "roles.constraint.violation", id);
    }

  }

  @Override
  public RolesDTO updateRoles(Long id, RolesDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    RolesDTO roles = rolesDAO.getRolesById(id);
    if (roles == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"roles.notfound", id);
    
    if(dto.getRol() != null)
    	roles.setRol(dto.getRol());
    
    if (dto.getDescripcion() != null)
    	roles.setDescripcion(dto.getDescripcion());
    
    rolesDAO.updateRoles(roles);
 
   
    return roles;
  }

  @Override
  public RolesDTO createRoles(RolesDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {

      rolesDAO.createRoles(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "roles.duplicate.key");
    }

    return dto;
  }
  
  @Override
  public UsuarioRolDTO createUsuarioRol(UsuarioRolDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {

      rolesDAO.createUsuarioRol(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "roles.duplicate.key");
    }

    return dto;
  }
  
  @Override
  public void deleteUsuarioRol(UsuarioRolDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    try {
      rolesDAO.deleteUsuarioRol(dto);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "roles.constraint.violation");
    }

  }


}