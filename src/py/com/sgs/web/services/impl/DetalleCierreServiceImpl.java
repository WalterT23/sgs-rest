package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.AperturaCierreDAO;
import py.com.sgs.persistence.dao.DetalleCierreDAO;
import py.com.sgs.persistence.dto.DetalleCierreDTO;
import py.com.sgs.persistence.dto.composite.AperturaCierreCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.DetalleCierreService;

public class DetalleCierreServiceImpl implements DetalleCierreService{

private Logger log = LoggerFactory.getLogger(getClass());  

  @Autowired
  private DetalleCierreDAO detalleCierreDAO;
  
  @Autowired
  private AperturaCierreDAO aperturaCierreDAO;
  

  public void setAperturaCierreDAO(AperturaCierreDAO aperturaCierreDAO) {
	this.aperturaCierreDAO = aperturaCierreDAO;
}

public void setDetalleCierreDAO(DetalleCierreDAO detalleCierreDAO) {
    this.detalleCierreDAO = detalleCierreDAO;
  }

  @Override
  public List<DetalleCierreDTO> listDetalleCierre(InvocationContext ic) throws ServiceException {
    List<DetalleCierreDTO> detalleCierres =  detalleCierreDAO.listDetalleCierre();

    return detalleCierres;
  }

  @Override
  public DetalleCierreDTO getDetalleCierre(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    DetalleCierreDTO detalleCierre =  detalleCierreDAO.getDetalleCierreById(id);
    if (detalleCierre == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detallecierre.notfound", id);

    return detalleCierre;
  }

  @Override
  public void deleteDetalleCierre(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    DetalleCierreDTO detalleCierre =  detalleCierreDAO.getDetalleCierreById(id);
    if(detalleCierre == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detallecierre.notfound", id);

    try {
      detalleCierreDAO.deleteDetalleCierre(detalleCierre);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "detallecierre.constraint.violation", id);
    }

  }

  @Override
  public DetalleCierreDTO updateDetalleCierre(Long id, DetalleCierreDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    DetalleCierreDTO detalleCierre = detalleCierreDAO.getDetalleCierreById(id);
    if (detalleCierre == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detallecierre.notfound", id);

    detalleCierre.setIdAperturaCierre(dto.getIdAperturaCierre());
    detalleCierre.setMontoEfectivo(dto.getMontoEfectivo());
    detalleCierre.setMontoTarjetas(dto.getMontoTarjetas());
    detalleCierre.setMontoTotal(dto.getMontoTotal());
 
    detalleCierreDAO.updateDetalleCierre(detalleCierre);

    return detalleCierre;
  }

  @Override
  public DetalleCierreDTO createDetalleCierre(DetalleCierreDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {

    	dto.setUsuarioCreacion(ic.getUserPrincipal());
    	detalleCierreDAO.createDetalleCierre(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "detallecierre.duplicate.key");
    }

    return dto;
  }

}