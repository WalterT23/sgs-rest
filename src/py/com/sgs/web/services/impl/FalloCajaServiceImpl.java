package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.FalloCajaDAO;
import py.com.sgs.persistence.dto.ConsultaPagosDTO;
import py.com.sgs.persistence.dto.FalloCajaDTO;
import py.com.sgs.persistence.dto.FalloCajaGenDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.FiltroComprasDTO;
import py.com.sgs.persistence.dto.FiltroCuotasDTO;
import py.com.sgs.persistence.dto.FiltroFalloDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.FalloCajaService;

public class FalloCajaServiceImpl implements FalloCajaService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private FalloCajaDAO falloCajaDAO;



  public void setFalloCajaDAO(FalloCajaDAO falloCajaDAO) {
    this.falloCajaDAO = falloCajaDAO;
  }

  @Override
  public List<FalloCajaDTO> listFalloCaja(FiltroFalloDTO dto, InvocationContext ic) throws ServiceException {
	  
	  if (dto.getFechaDesde() == null){ 
	    	 Calendar fechaD = Calendar.getInstance();
	    	 fechaD.add(Calendar.DATE, -15);
	    	 dto.setFechaDesde(fechaD.getTime());
	    }
	    
	    if (dto.getFechaHasta() == null){ 
	  	   	 Calendar fechaH = Calendar.getInstance();
	  	   	 dto.setFechaHasta(fechaH.getTime());
	    }
	    dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
	    
	    List<FalloCajaDTO> falloCajas =  falloCajaDAO.listFalloCaja(dto);
	    if(falloCajas!=null && !falloCajas.isEmpty()) {
	    	return falloCajas;
	    }else {
	    	throw new ServiceException(ErrorCode.NO_DATA_FOUND,"fallocaja.notfound");
	    }
	    	

    
  }

  @Override
  public FalloCajaDTO getFalloCaja(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    FalloCajaDTO falloCaja =  falloCajaDAO.getFalloCajaById(id);
    if (falloCaja == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"fallocaja.notfound", id);

    return falloCaja;
  }

  @Override
  public void deleteFalloCaja(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    FalloCajaDTO falloCaja =  falloCajaDAO.getFalloCajaById(id);
    if(falloCaja == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"fallocaja.notfound", id);

    try {
      falloCajaDAO.deleteFalloCaja(falloCaja);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "fallocaja.constraint.violation", id);
    }

  }

  @Override
  public FalloCajaDTO updateFalloCaja(Long id, FalloCajaDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    FalloCajaDTO falloCaja = falloCajaDAO.getFalloCajaById(id);
    if (falloCaja == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"fallocaja.notfound", id);

    falloCaja.setIdAperturaCierre(dto.getIdAperturaCierre());
    falloCaja.setIdUsuario(dto.getIdUsuario());
    falloCaja.setMonto(dto.getMonto());
    falloCaja.setFecha(dto.getFecha());
 

      falloCajaDAO.updateFalloCaja(falloCaja);

    return falloCaja;
  }

  @Override
  public FalloCajaDTO createFalloCaja(FalloCajaDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
      dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
      falloCajaDAO.createFalloCaja(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "fallocaja.duplicate.key");
    }

    return dto;
  }
  
  
  
  @Override
  public FiltroFalloDTO getReportFallo(FiltroFalloDTO dto , InvocationContext ic) throws ServiceException  {
  	
  	if (dto == null)
  	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");
    
    if (dto.getFechaDesde() == null){ 
    	 Calendar fechaD = Calendar.getInstance();
    	 fechaD.add(Calendar.DATE, -15);
    	 dto.setFechaDesde(fechaD.getTime());
    }
    
    if (dto.getFechaHasta() == null){ 
  	   	 Calendar fechaH = Calendar.getInstance();
  	   	 dto.setFechaHasta(fechaH.getTime());
    }
    dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
    
    FiltroFalloDTO reporteFallo = new FiltroFalloDTO();
    BigDecimal total = new BigDecimal(0);
    
    reporteFallo.setFechaDesde(dto.getFechaDesde());
    reporteFallo.setFechaHasta(dto.getFechaHasta());
    reporteFallo.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

    
    if (falloCajaDAO.getRepoFallo(dto) != null && !falloCajaDAO.getRepoFallo(dto).isEmpty()){
  	  for(FalloCajaGenDTO fc : falloCajaDAO.getRepoFallo(dto)) {
  		  total=total.add(fc.getMonto());
  	  } 
    }
   
    reporteFallo.setDetalles(falloCajaDAO.getRepoFallo(dto));
    reporteFallo.setTotal(total);
   
    
    
    return reporteFallo;
  }

  

	@Override
	 public FileReportDTO getFallosImpresion(FiltroFalloDTO dto, InvocationContext ic) throws Exception {
		 FileReportDTO archivo = new FileReportDTO();
		 if(dto != null){
			 FiltroFalloDTO datosRepo = this.getReportFallo(dto, ic);
			 ReportGenerator creador = new ReportGenerator();
				HashMap<String, Object> map = new HashMap<>();
				map.put("fechaDesde", datosRepo.getFechaDesde());
				map.put("fechaHasta", datosRepo.getFechaHasta());
				//map.put("totalCompras", datosRepo.getTotal());
				try {
				SimpleDateFormat xx = new SimpleDateFormat("ddMMyyyy");
				archivo.setArchivo(creador.getByteReports("E", ic, "reporteFallo.jasper", datosRepo.getDetalles(), map));
				archivo.setTipo(".xls");
				archivo.setNombre("reporteFallo-"+xx.format(datosRepo.getFechaDesde())+"-"+xx.format(datosRepo.getFechaHasta()));
				}catch(Exception e) {
					log.info(e.getLocalizedMessage());
					throw e;
				}
		 }
		 
		 
	 	return archivo;
	 }
}