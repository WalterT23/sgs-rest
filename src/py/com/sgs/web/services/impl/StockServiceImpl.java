package py.com.sgs.web.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.StockDAO;
import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.composite.StockCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.StockService;

public class StockServiceImpl implements StockService{

	private Logger log = LoggerFactory.getLogger(getClass());  
	@Autowired
	private StockDAO stockDAO;

	public void setStockDAO(StockDAO stockDAO) {
		this.stockDAO = stockDAO;
	}

	@Override
	public List<StockCompositeDTO> listStock(InvocationContext ic) throws ServiceException {
		List<StockCompositeDTO> stocks =  stockDAO.listStock(Long.parseLong(ic.getUserSucursal()));

		return stocks;
	}

	@Override
	public StockCompositeDTO getStock(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		StockCompositeDTO stock =  stockDAO.getStockById(id);
		if (stock == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"stock.notfound", id);

		return stock;
	}
	
	@Override
	public StockDTO getStockByCodProducto(String codProducto, InvocationContext ic) throws ServiceException {
		if (codProducto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","codProducto");
	
		StockDTO dto = new StockDTO();
		dto.setCodProducto(codProducto);
		dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
		
		StockDTO stock =  stockDAO.getStockByCodProducto(dto);
		
		if (stock == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"stock.notfound", codProducto);

		return stock;
	}

	@Override
	public void deleteStock(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		StockDTO stock =  stockDAO.getStockById(id);
		if(stock == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"stock.notfound", id);

		try {
			stockDAO.deleteStock(stock);
		}
		catch (DataIntegrityViolationException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "stock.constraint.violation", id);
		}

	}

	@Override
	public StockDTO updateStock(Long id, StockDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		StockDTO stock = stockDAO.getStockById(id);
		
		if (stock == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"stock.notfound", id);
		
		if(dto.getStock() != null)
			stock.setStock(dto.getStock());
		
		if (dto.getPromPonderado() != null )
			stock.setPromPonderado(dto.getPromPonderado());
		
		if (dto.getUltCostoCompra() != null)
			stock.setUltCostoCompra(dto.getUltCostoCompra());
		
		stock.setFechaUltModif(new Date(System.currentTimeMillis()));
		
		stock.setUsuarioUltModif(ic.getUserPrincipal());

		
		stockDAO.updateStock(stock);

		return stock;
	}


	@Override
	public StockDTO insertStock(StockDTO cod, InvocationContext ic) throws ServiceException {
		
		if (cod == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
		
		cod.setIdSucursal(Long.parseLong(ic.getUserSucursal())); 
		
		try {
			
			stockDAO.insertStock(cod);
			
		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "productos.duplicate.key");
		}

		return cod;
	}

	@Override
	public StockDTO updateCodStock(StockDTO dto, InvocationContext ic) throws ServiceException {
		
		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		StockDTO stock = stockDAO.getStockById(dto.getId());
		
		if (stock == null)
			stockDAO.insertStock(dto);
		
		stockDAO.updateCodStock(dto);
	
		return dto;
	}
	
	
}

