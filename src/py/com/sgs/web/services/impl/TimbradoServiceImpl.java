package py.com.sgs.web.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.TimbradoDAO;
import py.com.sgs.persistence.dto.TimbradoDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.TimbradoService;

public class TimbradoServiceImpl implements TimbradoService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private TimbradoDAO timbradoDAO;

  public void setTimbradoDAO(TimbradoDAO timbradoDAO) {
    this.timbradoDAO = timbradoDAO;
  }

  @Override
  public List<TimbradoDTO> listTimbrado(InvocationContext ic) throws ServiceException {
    List<TimbradoDTO> timbrados =  timbradoDAO.listTimbrado(Long.parseLong(ic.getUserSucursal()));
    
    Calendar fecha = Calendar.getInstance();
    for (TimbradoDTO t : timbrados) {
    	t.setVigente(t.getFinVigencia().after(fecha.getTime()));
    }
    	
    return timbrados;
  }

  @Override
  public TimbradoDTO getTimbrado(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    TimbradoDTO timbrado =  timbradoDAO.getTimbradoById(id);
    if (timbrado == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"timbrado.notfound", id);
    
    Calendar fecha = Calendar.getInstance();
    timbrado.setVigente(timbrado.getFinVigencia().after(fecha.getTime()));
    
    return timbrado;
  }

  
  @Override
  public TimbradoDTO updateTimbrado(Long id, TimbradoDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    TimbradoDTO timbrado = timbradoDAO.getTimbradoById(id);
    
    if (timbrado == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"timbrado.notfound", id);
    
    if(dto.getNumero() != null)
    	timbrado.setNumero(dto.getNumero());
    
    if(dto.getInicioVigencia() != null)
    	timbrado.setInicioVigencia(dto.getInicioVigencia());
    
    if(dto.getFinVigencia() != null)
    	timbrado.setFinVigencia(dto.getFinVigencia());
    
    if(dto.getIdSucursal() != null)
    	timbrado.setIdSucursal(dto.getIdSucursal());
    
    timbradoDAO.updateTimbrado(timbrado);

    return timbrado;
  }

  
  @Override
  public TimbradoDTO createTimbrado(TimbradoDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
    
    if (dto.getNumero() == null)
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.null");
    if (dto.getInicioVigencia() == null)
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.null");
    if (dto.getFinVigencia() == null)
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.null");
    
    try {
      
      TimbradoDTO timbrado = timbradoDAO.getTimbradoByNro(dto.getNumero());
      if (timbrado != null)
    	  throw new ServiceException(ErrorCode.TIMB_EXIST,"timbrado.duplicate.key");
      
      dto.setUsuarioCreacion(ic.getUserPrincipal());
      dto.setFechaCreacion(new Date (System.currentTimeMillis()));
      dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
            
      if (dto.getIdSucursal() == null)
    	  throw new ServiceException(ErrorCode.SUCURSAL_NOT_FOUND,"sucursal.missing");
      
      timbradoDAO.createTimbrado(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "timbrado.duplicate.key");
    }

    return dto;
  }
  
  @Override
  public void vencerTimbrado(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    TimbradoDTO timbrado = timbradoDAO.getTimbradoById(id);
    
    if (timbrado == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"timbrado.notfound", id);
    
    Calendar fecha = Calendar.getInstance();
    
    if (timbrado.getFinVigencia().after(fecha.getTime())) {
    	timbradoDAO.vencerTimbrado(id);
    }else {
    	throw new ServiceException(ErrorCode.TIMB_VENCIDO,"timb.vencido");
    }
  }

}