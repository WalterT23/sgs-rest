package py.com.sgs.web.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import java.util.List;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.NotaCreditoVentaDetalleDAO;
import py.com.sgs.persistence.dto.NotaCreditoVentaDetalleDTO;
import py.com.sgs.util.Utils;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.NotaCreditoVentaDetalleService;

public class NotaCreditoVentaDetalleServiceImpl implements NotaCreditoVentaDetalleService{

	private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
	private NotaCreditoVentaDetalleDAO notaCreditoVentaDetalleDAO;

	public void setNotaCreditoVentaDetalleDAO(NotaCreditoVentaDetalleDAO notaCreditoVentaDetalleDAO) {
		this.notaCreditoVentaDetalleDAO = notaCreditoVentaDetalleDAO;
	}

	@Override
	public List<NotaCreditoVentaDetalleDTO> listNotaCreditoVentaDetalle(InvocationContext ic) throws ServiceException {
		List<NotaCreditoVentaDetalleDTO> notaCreditoVentaDetalles =  notaCreditoVentaDetalleDAO.listNotaCreditoVentaDetalle();

		return notaCreditoVentaDetalles;
	}

	@Override
	public NotaCreditoVentaDetalleDTO getNotaCreditoVentaDetalle(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		NotaCreditoVentaDetalleDTO notaCreditoVentaDetalle =  notaCreditoVentaDetalleDAO.getNotaCreditoVentaDetalleById(id);
		if (notaCreditoVentaDetalle == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditoventadetalle.notfound", id);

		return notaCreditoVentaDetalle;
	}

	@Override
	public void deleteNotaCreditoVentaDetalle(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		NotaCreditoVentaDetalleDTO notaCreditoVentaDetalle =  notaCreditoVentaDetalleDAO.getNotaCreditoVentaDetalleById(id);
		if(notaCreditoVentaDetalle == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditoventadetalle.notfound", id);

		try {
			notaCreditoVentaDetalleDAO.deleteNotaCreditoVentaDetalle(notaCreditoVentaDetalle);
		}
		catch (DataIntegrityViolationException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "notacreditoventadetalle.constraint.violation", id);
		}

	}

	@Override
	public NotaCreditoVentaDetalleDTO updateNotaCreditoVentaDetalle(Long id, NotaCreditoVentaDetalleDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		NotaCreditoVentaDetalleDTO notaCreditoVentaDetalle = notaCreditoVentaDetalleDAO.getNotaCreditoVentaDetalleById(id);
		if (notaCreditoVentaDetalle == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditoventadetalle.notfound", id);

		notaCreditoVentaDetalle.setIdNotaCreditoVenta(dto.getIdNotaCreditoVenta());
		notaCreditoVentaDetalle.setIdProducto(dto.getIdProducto());
		notaCreditoVentaDetalle.setIva5(dto.getIva5());
		notaCreditoVentaDetalle.setIva10(dto.getIva10());
		notaCreditoVentaDetalle.setTotal(dto.getTotal());
		notaCreditoVentaDetalle.setObservaciones(dto.getObservaciones());
		notaCreditoVentaDetalle.setCantidad(dto.getCantidad());

		notaCreditoVentaDetalleDAO.updateNotaCreditoVentaDetalle(notaCreditoVentaDetalle);

		return notaCreditoVentaDetalle;
	}

	@Override
	public NotaCreditoVentaDetalleDTO createNotaCreditoVentaDetalle(NotaCreditoVentaDetalleDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {
			notaCreditoVentaDetalleDAO.createNotaCreditoVentaDetalle(dto);
		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "notacreditoventadetalle.duplicate.key");
		}

		return dto;
	}

}