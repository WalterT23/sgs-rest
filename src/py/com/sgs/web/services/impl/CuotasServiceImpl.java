package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.CabeceraCompraDAO;
import py.com.sgs.persistence.dao.CuotaDetalleDAO;
import py.com.sgs.persistence.dao.CuotasDAO;
import py.com.sgs.persistence.dto.ConsultaPagosDTO;
import py.com.sgs.persistence.dto.CuotaDetalleDTO;
import py.com.sgs.persistence.dto.CuotasDTO;
import py.com.sgs.persistence.dto.CuotasPenDTO;
import py.com.sgs.persistence.dto.EstadoCuentaProveedorDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.FiltroCuotasDTO;
import py.com.sgs.persistence.dto.ReporteVentasRealizadasDTO;
import py.com.sgs.persistence.dto.composite.CabeceraCompraCompositeDTO;
import py.com.sgs.persistence.dto.composite.CuotasCompositeDTO;
import py.com.sgs.persistence.dto.composite.CuotasDetalleCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.CuotasService;

public class CuotasServiceImpl implements CuotasService{

private Logger log = LoggerFactory.getLogger(getClass());  

  @Autowired
  private CuotasDAO cuotasDAO;
  
  @Autowired
  private CabeceraCompraDAO compradao;

  @Autowired
  private CuotaDetalleDAO detalledao;
  
  public void setCuotasDAO(CuotasDAO cuotasDAO) {
    this.cuotasDAO = cuotasDAO;
  }
  
  

  public void setCompradao(CabeceraCompraDAO compradao) {
	this.compradao = compradao;
}



public void setDetalledao(CuotaDetalleDAO detalledao) {
	this.detalledao = detalledao;
}



@Override
  public List<CuotasDTO> listCuotas(InvocationContext ic) throws ServiceException {
    List<CuotasDTO> cuotass =  cuotasDAO.listCuotas(Long.parseLong(ic.getUserSucursal()));

    return cuotass;
  }

@Override
public List<ConsultaPagosDTO> listCuotasPendientes(InvocationContext ic) throws ServiceException {
  List<ConsultaPagosDTO> cuotassp =  cuotasDAO.listCuotasPendientes(Long.parseLong(ic.getUserSucursal()));

  return cuotassp;
}

@Override
public List<CuotasDetalleCompositeDTO> listCuotasporProv(CuotasDTO cd) throws ServiceException {
  List<CuotasDetalleCompositeDTO> cuotaspp =  cuotasDAO.listCuotasporProv(cd);

  return cuotaspp;
}

  @Override
  public CuotasDTO getCuotas(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    CuotasDTO cuotas =  cuotasDAO.getCuotasById(id);
    if (cuotas == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cuotas.notfound", id);

    return cuotas;
  }

  @Override
  public void deleteCuotas(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    CuotasDTO cuotas =  cuotasDAO.getCuotasById(id);
    if(cuotas == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cuotas.notfound", id);

    try {
      cuotasDAO.deleteCuotas(cuotas);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "cuotas.constraint.violation", id);
    }

  }

  @Override
  public CuotasDTO updateCuotas(Long id, CuotasDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    CuotasDTO cuotas = cuotasDAO.getCuotasById(id);
    if (cuotas == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cuotas.notfound", id);
    	if (dto.getIdCabeceraCompra()!=null)
    		cuotas.setIdCabeceraCompra(dto.getIdCabeceraCompra());
    	if (dto.getMontoEntregado()!=null)
    		cuotas.setMontoEntregado(dto.getMontoEntregado());
    	if (dto.getCantidadCuotas()!=null)
    		cuotas.setCantidadCuotas(dto.getCantidadCuotas());
    	if (dto.getIdProveedor()!=null)
    		cuotas.setIdProveedor(dto.getIdProveedor());
    	if (dto.getIdSucursal()!=null)
    		cuotas.setIdSucursal(dto.getIdSucursal());
 
    	//en el caso que se pueda modificar aqui debo hacer el get de los detalles
      cuotasDAO.updateCuotas(cuotas);
    
    return cuotas;
  }

  @Override
  public CuotasCompositeDTO createCuotas(CuotasCompositeDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
    	
		if (dto.getDetalles() != null && !dto.getDetalles().isEmpty()) {
			BigDecimal montof= new BigDecimal(0);
			for (CuotaDetalleDTO cd:dto.getDetalles()){
				//sumo las cuotas
				if (cd != null) {
						montof=montof.add(cd.getMonto());
					
				}
			}
			//comparo con el total de la factura
			montof=montof.add(dto.getMontoEntregado());
			CabeceraCompraCompositeDTO fc = compradao.getCabeceraCompraById(dto.getIdCabeceraCompra());
			if(fc.getTotal().compareTo(montof)> 0)
				 throw new ServiceException(ErrorCode.SUPERA_MONTO,"cuota.error.monto",montof);	

		}
		//aca inserto el registro de la cuota(dto);
		cuotasDAO.createCuotas(dto);
		
		if (dto.getDetalles() != null && !dto.getDetalles().isEmpty()){
			
			for (CuotaDetalleDTO d:dto.getDetalles()){
				d.setIdCuotas(dto.getId());
				
				detalledao.createCuotaDetalle(d);

			}
		}

    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "cuotas.duplicate.key");
    }

    return dto;
  }



@Override
public FiltroCuotasDTO getReporCuotas(FiltroCuotasDTO dto , InvocationContext ic) throws ServiceException  {
	
	if (dto == null)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");
  
  if (dto.getFechaDesde() == null){ 
  	 Calendar fechaD = Calendar.getInstance();
  	 fechaD.add(Calendar.DATE, -15);
  	 dto.setFechaDesde(fechaD.getTime());
  }
  
  if (dto.getFechaHasta() == null){ 
	   	 Calendar fechaH = Calendar.getInstance();
	   	 fechaH.add(Calendar.DATE, +15);
	   	 dto.setFechaHasta(fechaH.getTime());
  }
  dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
  
  FiltroCuotasDTO reporteCuotas = new FiltroCuotasDTO();
  BigDecimal total = new BigDecimal(0);
  reporteCuotas.setFechaDesde(dto.getFechaDesde());
  reporteCuotas.setFechaHasta(dto.getFechaHasta());
  reporteCuotas.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
  
  if (cuotasDAO.getReporCuotas(dto) != null && !cuotasDAO.getReporCuotas(dto).isEmpty()){
	  for(ConsultaPagosDTO cp : cuotasDAO.getReporCuotas(dto)) {
		  total=total.add(cp.getMontoaPagar());
	  } 
  }
  reporteCuotas.setTotalXfecha(cuotasDAO.getCuotasGraphic(dto));
  reporteCuotas.setDetalles(cuotasDAO.getReporCuotas(dto));
  reporteCuotas.setTotal(total);
 
  
  
  return reporteCuotas;
}



@Override
public FileReportDTO getCuotasaVencerImpresion(FiltroCuotasDTO dto, InvocationContext ic)throws Exception {
			
			 FileReportDTO archivo = new FileReportDTO();
			 if(dto != null){
				 FiltroCuotasDTO datosRepo = this.getReporCuotas(dto, ic);
				 ReportGenerator creador = new ReportGenerator();
					HashMap<String, Object> map = new HashMap<>();
					map.put("fechaDesde", datosRepo.getFechaDesde());
					map.put("fechaHasta", datosRepo.getFechaHasta());
					map.put("proveedor", datosRepo.getProveedor());
					map.put("totalCuotas", datosRepo.getTotal());
					try {
					SimpleDateFormat xx = new SimpleDateFormat("ddMMyyyy");
					archivo.setArchivo(creador.getByteReports("E", ic, "reporteCuotas.jasper", datosRepo.getDetalles(), map));
					archivo.setTipo(".xls");
					archivo.setNombre("cuotasaVencer-"+xx.format(datosRepo.getFechaDesde())+"-"+xx.format(datosRepo.getFechaHasta()));
					}catch(Exception e) {
						log.info(e.getLocalizedMessage());
						throw e;
					}
			 }
			 
			 
		 	return archivo;
		 }
}

