package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.CajaUserDAO;
import py.com.sgs.persistence.dto.CajaUserDTO;
import py.com.sgs.persistence.dto.UsuariosDTO;
import py.com.sgs.persistence.dto.composite.CajaUserCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.CajaUserService;

public class CajaUserServiceImpl implements CajaUserService{

private Logger log = LoggerFactory.getLogger(getClass()); 

@Autowired
 private CajaUserDAO cajaUserDAO;



  public void setCajaUserDAO(CajaUserDAO cajaUserDAO) {
    this.cajaUserDAO = cajaUserDAO;
  }

  @Override
  public List<CajaUserCompositeDTO> listCajaUser(InvocationContext ic) throws ServiceException {
    List<CajaUserCompositeDTO> cajaUsers =  cajaUserDAO.listCajaUser(Long.parseLong(ic.getUserSucursal()));

    return cajaUsers;
  }

  @Override
  public List<UsuariosDTO> listCajeros(InvocationContext ic) throws ServiceException {
    List<UsuariosDTO> cajeros =  cajaUserDAO.listCajeros(Long.parseLong(ic.getUserSucursal()));

    return cajeros;
  }
  @Override
  public CajaUserCompositeDTO getCajaUser(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    CajaUserCompositeDTO cajaUser =  cajaUserDAO.getCajaUserById(id);
    if (cajaUser == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cajauser.notfound", id);

    return cajaUser;
  }
  
  @Override
  public CajaUserCompositeDTO getCajaUserByIdPtoExp(Long idPuntoExpedicion, InvocationContext ic) throws ServiceException {
    if (idPuntoExpedicion == null || idPuntoExpedicion <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    CajaUserCompositeDTO cajaUser =  cajaUserDAO.getCajaUserByIdPtoExp(idPuntoExpedicion);
    if (cajaUser == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cajauser.notfound", idPuntoExpedicion);

    return cajaUser;
  }


  @Override
  public void deleteCajaUser(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    CajaUserDTO cajaUser =  cajaUserDAO.getCajaUserById(id);
    if(cajaUser == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cajauser.notfound", id);

    try {
      cajaUserDAO.deleteCajaUser(cajaUser);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "cajauser.constraint.violation", id);
    }

  }

  @Override
  public CajaUserDTO updateCajaUser(Long id, CajaUserDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    CajaUserDTO cajaUser = cajaUserDAO.getCajaUserById(id);
    if (cajaUser == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cajauser.notfound", id);

    cajaUser.setIdPuntoExpedicion(dto.getIdPuntoExpedicion());
    cajaUser.setIdUsuario(dto.getIdUsuario());
    cajaUser.setInicio(dto.getInicio());
    cajaUser.setFin(dto.getFin());
  


     cajaUserDAO.updateCajaUser(cajaUser);
    

    return cajaUser;
  }

  @Override
  public CajaUserDTO createCajaUser(CajaUserDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {	
    	//se debe verificar que el punto de expedicion no posea una asociacion vigente
    	CajaUserDTO ptoCaj = cajaUserDAO.getAsociacionVigenteByPtoExp(dto.getIdPuntoExpedicion());
		if(ptoCaj != null)
			throw new ServiceException(ErrorCode.PTO_ASOCIACION_ACTIVA,"caja.asociacion.punto.activo", dto.getIdPuntoExpedicion());
    	
    	//se debe verificar que el cajero no posea una asociacion vigente 
    	CajaUserDTO usCaj = cajaUserDAO.getAsociacionVigenteByCajero(dto.getIdUsuario());
		if(usCaj != null)	
			throw new ServiceException(ErrorCode.CAJERO_ASOCIACION_ACTIVA,"caja.asociacion.cajero.activo", dto.getIdUsuario());
		

			cajaUserDAO.createCajaUser(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "cajauser.duplicate.key");
    }

    return dto;
  }

}