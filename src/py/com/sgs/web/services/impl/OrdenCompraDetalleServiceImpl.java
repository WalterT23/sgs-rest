package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.OrdenCompraDetalleDAO;
import py.com.sgs.persistence.dto.OrdenCompraDetalleDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.OrdenCompraDetalleService;

public class OrdenCompraDetalleServiceImpl implements OrdenCompraDetalleService{

	private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
	private OrdenCompraDetalleDAO ordenCompraDetalleDAO;


	public void setOrdenCompraDetalleDAO(OrdenCompraDetalleDAO ordenCompraDetalleDAO) {
		this.ordenCompraDetalleDAO = ordenCompraDetalleDAO;
	}

	@Override
	public List<OrdenCompraDetalleDTO> listOrdenCompraDetalle(InvocationContext ic) throws ServiceException {
		List<OrdenCompraDetalleDTO> ordenCompraDetalles =  ordenCompraDetalleDAO.listOrdenCompraDetalle();

		if (ordenCompraDetalles == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedores.notfound");

		for(OrdenCompraDetalleDTO p: ordenCompraDetalles){
			p.setCostoTotal(new BigDecimal(p.getCantidad()).multiply(p.getUltCostoCompra()));
		}

		return ordenCompraDetalles;
	}

	@Override
	public List<OrdenCompraDetalleDTO> listDetalleByIdOC(Long idOC, InvocationContext ic) throws ServiceException {
		if (idOC == null || idOC <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		List<OrdenCompraDetalleDTO> detallesByOC =  ordenCompraDetalleDAO.listDetalleByIdOC(idOC);

		if (detallesByOC == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ordencompradetalle.notfound", idOC);

		for(OrdenCompraDetalleDTO p: detallesByOC){
			p.setCostoTotal(new BigDecimal(p.getCantidad()).multiply(p.getUltCostoCompra()));
		}

		return detallesByOC;
	}

	@Override
	public OrdenCompraDetalleDTO getOrdenCompraDetalle(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		OrdenCompraDetalleDTO ordenCompraDetalle =  ordenCompraDetalleDAO.getOrdenCompraDetalleById(id);
		if (ordenCompraDetalle == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ordencompradetalle.notfound", id);

		ordenCompraDetalle.setCostoTotal(new BigDecimal(ordenCompraDetalle.getCantidad()).multiply(ordenCompraDetalle.getUltCostoCompra()));

		return ordenCompraDetalle;
	}

	@Override
	public void deleteOrdenCompraDetalle(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		OrdenCompraDetalleDTO ordenCompraDetalle =  ordenCompraDetalleDAO.getOrdenCompraDetalleById(id);
		if(ordenCompraDetalle == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ordencompradetalle.notfound", id);

		try {
			ordenCompraDetalleDAO.deleteOrdenCompraDetalle(ordenCompraDetalle);
		}
		catch (DataIntegrityViolationException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "ordencompradetalle.constraint.violation", id);
		}

	}

	@Override
	public OrdenCompraDetalleDTO updateOrdenCompraDetalle(Long id, OrdenCompraDetalleDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		OrdenCompraDetalleDTO ordenCompraDetalle = ordenCompraDetalleDAO.getOrdenCompraDetalleById(id);
		if (ordenCompraDetalle == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ordencompradetalle.notfound", id);

		if (dto.getIdOrdenCompra() != null)
			ordenCompraDetalle.setIdOrdenCompra(dto.getIdOrdenCompra());

		if (dto.getIdProducto() != null)
			ordenCompraDetalle.setIdProducto(dto.getIdProducto());

		if (dto.getProducto() != null)
			ordenCompraDetalle.setProducto(dto.getProducto());

		if (dto.getCantidad() != null)
			ordenCompraDetalle.setCantidad(dto.getCantidad());

		if (dto.getUltCostoCompra() != null)
			ordenCompraDetalle.setUltCostoCompra(dto.getUltCostoCompra());

		ordenCompraDetalle.setFechaReg(new Date(System.currentTimeMillis()));

		ordenCompraDetalleDAO.updateOrdenCompraDetalle(ordenCompraDetalle);


		return ordenCompraDetalle;
	}

	@Override
	public OrdenCompraDetalleDTO createOrdenCompraDetalle(OrdenCompraDetalleDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {
			dto.setFechaReg(new Date(System.currentTimeMillis()));
			ordenCompraDetalleDAO.createOrdenCompraDetalle(dto);
		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "ordencompradetalle.duplicate.key");
		}

		return dto;
	}



}