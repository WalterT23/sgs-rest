package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.ReportesDAO;
import py.com.sgs.persistence.dto.CabeceraVentaDTO;
import py.com.sgs.persistence.dto.Det4GraphicProMasVenDTO;
import py.com.sgs.persistence.dto.DetalleProMasVenDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.RepoMovimientoProductosDTO;
import py.com.sgs.persistence.dto.ReporteProductosMasVendidosDTO;
import py.com.sgs.persistence.dto.ReporteStockDTO;
import py.com.sgs.persistence.dto.ReporteVentasRealizadasDTO;
import py.com.sgs.persistence.dto.composite.StockCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ReportesService;

public class ReportesServiceImpl implements ReportesService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ReportesDAO reportesDAO;
	private static final SimpleDateFormat xx = new SimpleDateFormat("ddMMyyyy");

	public void setReportesDAO(ReportesDAO reportesDAO) {
		this.reportesDAO = reportesDAO;
	}

	@Override
	public ReporteVentasRealizadasDTO getReporteVentasRealizadas(ReporteVentasRealizadasDTO dto, InvocationContext ic)
			throws ServiceException {

		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null", "id");

		if (dto.getFechaDesde() == null) {
			Calendar fechaD = Calendar.getInstance();
			fechaD.add(Calendar.DATE, -7);
			dto.setFechaDesde(fechaD.getTime());
		}

		if (dto.getFechaHasta() == null) {
			Calendar fechaH = Calendar.getInstance();
			dto.setFechaHasta(fechaH.getTime());
		}
		dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

		ReporteVentasRealizadasDTO reporteVenta = new ReporteVentasRealizadasDTO();
		reporteVenta.setFechaDesde(dto.getFechaDesde());
		reporteVenta.setFechaHasta(dto.getFechaHasta());
		reporteVenta.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
		reporteVenta.setDetalles(reportesDAO.getRepoVentas(dto));
		reporteVenta.setTotalXfecha(reportesDAO.getVentasGraphic(dto));

		BigDecimal total = BigDecimal.ZERO;
		if (reporteVenta.getDetalles() != null) {
			for (CabeceraVentaDTO det : reporteVenta.getDetalles()) {
				total = total.add(det.getNeto());
			}
		}

		reporteVenta.setTotalVendido(total);

		return reporteVenta;

	}

	@Override
	public FileReportDTO getReporteVentasRealizadasImpresion(ReporteVentasRealizadasDTO dto, InvocationContext ic)
			throws Exception {
		FileReportDTO archivo = new FileReportDTO();
		if (dto != null) {
			ReporteVentasRealizadasDTO datosRepo = this.getReporteVentasRealizadas(dto, ic);
			ReportGenerator creador = new ReportGenerator();
			HashMap<String, Object> map = new HashMap<>();
			map.put("fechaDesde", datosRepo.getFechaDesde());
			map.put("fechaHasta", datosRepo.getFechaHasta());
			map.put("recaudado", datosRepo.getTotalVendido());
			try {
				archivo.setArchivo(
						creador.getByteReports("E", ic, "ventasRealizadas.jasper", datosRepo.getDetalles(), map));
				archivo.setTipo(".xls");
				archivo.setNombre("Ventas_realizadas-" + xx.format(datosRepo.getFechaDesde()) + "-"
						+ xx.format(datosRepo.getFechaHasta()));
			} catch (Exception e) {
				log.info(e.getLocalizedMessage());
				throw e;
			}
		}

		return archivo;
	}

	@Override
	public ReporteProductosMasVendidosDTO getReporteProMasVendidos(ReporteProductosMasVendidosDTO dto,
			InvocationContext ic) throws ServiceException {

		if (dto.getFechaDesde() == null) {
			Calendar fechaD = Calendar.getInstance();
			int horas = fechaD.get(Calendar.HOUR_OF_DAY);
			fechaD.add(Calendar.DATE, -7);
			fechaD.add(Calendar.HOUR_OF_DAY, -horas);
			dto.setFechaDesde(fechaD.getTime());
		}

		if (dto.getFechaHasta() == null) {
			Calendar fechaH = Calendar.getInstance();
			dto.setFechaHasta(fechaH.getTime());
		}

		dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

		ReporteProductosMasVendidosDTO repoProductosVendidos = new ReporteProductosMasVendidosDTO();
		repoProductosVendidos.setFechaDesde(dto.getFechaDesde());
		repoProductosVendidos.setFechaHasta(dto.getFechaHasta());
		repoProductosVendidos.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
		repoProductosVendidos.setGrupo(dto.getGrupo());
		repoProductosVendidos.setSubGrupo(dto.getSubGrupo());

		if (dto.getGrupo()) {
			repoProductosVendidos.setDetalles(reportesDAO.getRepoProMasVendidosGrupo(dto));
		} else {
			repoProductosVendidos.setDetalles(reportesDAO.getRepoProMasVendidosFecha(dto));
		}

		if (repoProductosVendidos.getDetalles() != null) {

			BigDecimal totalMonto = BigDecimal.ZERO;
			Double totalCant = (double) 0;

			for (DetalleProMasVenDTO det : repoProductosVendidos.getDetalles()) {

				totalMonto = totalMonto.add(det.getMontoRecaudado());
				totalCant = totalCant + det.getCantVendida();

			}

			ArrayList<Det4GraphicProMasVenDTO> datos4grap = new ArrayList<Det4GraphicProMasVenDTO>();
			Det4GraphicProMasVenDTO aux = new Det4GraphicProMasVenDTO();

			for (DetalleProMasVenDTO det : repoProductosVendidos.getDetalles()) {

				aux.setCantidad(det.getCantVendida());
				aux.setMonto(det.getMontoRecaudado());
				if (dto.getGrupo()) {
					aux.setNombre(det.getGrupo());
				} else {
					aux.setNombre(det.getProducto().getNombre());
				}
				aux.setTotCant(totalCant);
				aux.setTotMonto(totalMonto);

				datos4grap.add(aux);
				aux = new Det4GraphicProMasVenDTO();

			}
			if (datos4grap != null)
				repoProductosVendidos.setDatosGraphics(datos4grap);
		}

		return repoProductosVendidos;
	}

	@Override
	public FileReportDTO getReporteProMasVendidosImpresion(ReporteProductosMasVendidosDTO dto, InvocationContext ic)
			throws Exception {
		FileReportDTO archivo = new FileReportDTO();
		String nameFile = "";
		if (dto != null) {
			ReporteProductosMasVendidosDTO datosRepo = this.getReporteProMasVendidos(dto, ic);
			BigDecimal suma = new BigDecimal(0);
			for (DetalleProMasVenDTO da : datosRepo.getDetalles()) {
				suma = suma.add(da.getMontoRecaudado());
			}
			ReportGenerator creador = new ReportGenerator();
			HashMap<String, Object> map = new HashMap<>();
			map.put("fechaDesde", datosRepo.getFechaDesde());
			map.put("fechaHasta", datosRepo.getFechaHasta());
			map.put("suma", suma);
			try {
				if(datosRepo.getGrupo()) {
					nameFile = "productosVendidos2.jasper";
				}else {
					nameFile = "productosVendidos.jasper";
				}
				archivo.setTipo(".xls");
				archivo.setNombre("Pro-mas-vendidos-" + xx.format(datosRepo.getFechaDesde()) + "-"
						+ xx.format(datosRepo.getFechaHasta()));
				archivo.setArchivo(
						creador.getByteReports("E", ic, nameFile, datosRepo.getDetalles(), map));
			} catch (Exception e) {
				log.info(e.getLocalizedMessage());
				throw e;
			}
		}

		return archivo;
	}

	@Override
	public ReporteStockDTO getReporteStock(ReporteStockDTO dto, InvocationContext ic) throws ServiceException {

		dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

		if (!dto.getStockMin() && !dto.getStock20() && !dto.getStock40())
			dto.setStockMin(true);

		if (dto.getStock40()) {

			dto.setStock20(false);
			dto.setStockMin(false);

		} else if (dto.getStock20()) {

			dto.setStockMin(false);

		} else {
			dto.setStockMin(true);
		}

		ReporteStockDTO repo = new ReporteStockDTO();
		repo.setStockMin(dto.getStockMin());
		repo.setStock20(dto.getStock20());
		repo.setStock40(dto.getStock40());
		repo.setIdSucursal(dto.getIdSucursal());

		List<StockCompositeDTO> lstStock = reportesDAO.getListaProductos(dto);

		if (lstStock != null)
			repo.setDetalles(lstStock);

		return repo;

	}

	@Override
	public FileReportDTO getReporteStockImpresion(ReporteStockDTO dto, InvocationContext ic)
			throws ServiceException, Exception {

		FileReportDTO archivo = new FileReportDTO();
		ReporteStockDTO datosRepo = this.getReporteStock(dto, ic);
		ReportGenerator creador = new ReportGenerator();
		Date fecha= new Date() ;
//		String fecha = xx.format(new Date());
		HashMap<String, Object> map = new HashMap<>();
		map.put("fecha", fecha);
		try {
			archivo.setTipo(".xls");
			archivo.setNombre("Proximos-agotarse-" + xx.format(fecha));
			archivo.setArchivo(creador.getByteReports("E", ic, "productosProAgotados.jasper", datosRepo.getDetalles(), map));
		} catch (Exception e) {
			log.info(e.getLocalizedMessage());
			throw e;
		}

		return archivo;
	}

	@Override
	public RepoMovimientoProductosDTO getReporteMovProductos(RepoMovimientoProductosDTO dto, InvocationContext ic)
			throws ServiceException {

		if (dto.getFechaDesde() == null) {
			Calendar fechaD = Calendar.getInstance();
			int horas = fechaD.get(Calendar.HOUR_OF_DAY);
			fechaD.add(Calendar.DATE, -7);
			fechaD.add(Calendar.HOUR_OF_DAY, -horas);
			dto.setFechaDesde(fechaD.getTime());
		}

		if (dto.getFechaHasta() == null) {
			Calendar fechaH = Calendar.getInstance();
			dto.setFechaHasta(fechaH.getTime());
		}

		dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

		RepoMovimientoProductosDTO repoMovimiento = new RepoMovimientoProductosDTO();
		repoMovimiento.setFechaDesde(dto.getFechaDesde());
		repoMovimiento.setFechaHasta(dto.getFechaHasta());
		repoMovimiento.setIdSucursal(dto.getIdSucursal());
		repoMovimiento.setDetalles(reportesDAO.getLstMovProductos(repoMovimiento));

		return repoMovimiento;
	}

	@Override
	public FileReportDTO getReporteMovProductosImpresion(RepoMovimientoProductosDTO dto, InvocationContext ic)
			throws ServiceException, Exception {

		FileReportDTO archivo = new FileReportDTO();
		RepoMovimientoProductosDTO datosRepo = this.getReporteMovProductos(dto, ic);
		ReportGenerator creador = new ReportGenerator();
		HashMap<String, Object> map = new HashMap<>();
		map.put("fechaDesde", datosRepo.getFechaDesde());
		map.put("fechaHasta", datosRepo.getFechaHasta());
		try {
			archivo.setTipo(".xls");
			archivo.setNombre("Movimiento-productos-" + xx.format(datosRepo.getFechaDesde()) + "-"
					+ xx.format(datosRepo.getFechaHasta()));
			archivo.setArchivo(creador.getByteReports("E", ic, "movimientoProductos.jasper", datosRepo.getDetalles(), map));
		} catch (Exception e) {
			log.info(e.getLocalizedMessage());
			throw e;
		}

		return archivo;
	}

}