package py.com.sgs.web.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.TimbradoPtoExpedicionDAO;
import py.com.sgs.persistence.dto.TimbradoPtoExpedicionDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.TimbradoPtoExpedicionService;

public class TimbradoPtoExpedicionServiceImpl implements TimbradoPtoExpedicionService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private TimbradoPtoExpedicionDAO timbradoPtoExpedicionDAO;
	
@Autowired
private InfoRefOpcDAO infoRefOpcDAO;

  public void setTimbradoPtoExpedicionDAO(TimbradoPtoExpedicionDAO timbradoPtoExpedicionDAO) {
    this.timbradoPtoExpedicionDAO = timbradoPtoExpedicionDAO;
  }

  @Override
  public List<TimbradoPtoExpedicionDTO> listTimbradoPtoExpedicion(InvocationContext ic) throws ServiceException {
    List<TimbradoPtoExpedicionDTO> timbradoPtoExpedicions =  timbradoPtoExpedicionDAO.listTimbradoPtoExpedicion();

    return timbradoPtoExpedicions;
  }

  @Override
  public TimbradoPtoExpedicionDTO getTimbradoPtoExpedicion(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    TimbradoPtoExpedicionDTO timbradoPtoExpedicion =  timbradoPtoExpedicionDAO.getTimbradoPtoExpedicionById(id);
    if (timbradoPtoExpedicion == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"timbradoptoexpedicion.notfound", id);

    return timbradoPtoExpedicion;
  }

  @Override
  public void deleteTimbradoPtoExpedicion(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    TimbradoPtoExpedicionDTO timbradoPtoExpedicion =  timbradoPtoExpedicionDAO.getTimbradoPtoExpedicionById(id);
    if(timbradoPtoExpedicion == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"timbradoptoexpedicion.notfound", id);

    try {
      timbradoPtoExpedicionDAO.deleteTimbradoPtoExpedicion(timbradoPtoExpedicion);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "timbradoptoexpedicion.constraint.violation", id);
    }

  }

  @Override
  public TimbradoPtoExpedicionDTO updateTimbradoPtoExpedicion(Long id, TimbradoPtoExpedicionDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    TimbradoPtoExpedicionDTO timbradoPtoExpedicion = timbradoPtoExpedicionDAO.getTimbradoPtoExpedicionById(id);
    if (timbradoPtoExpedicion == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"timbradoptoexpedicion.notfound", id);

    timbradoPtoExpedicion.setIdTimbrado(dto.getIdTimbrado());
    timbradoPtoExpedicion.setIdPtoExpedicion(dto.getIdPtoExpedicion());
    timbradoPtoExpedicion.setNumeroDesde(dto.getNumeroDesde());
    timbradoPtoExpedicion.setNumeroHasta(dto.getNumeroHasta());
    timbradoPtoExpedicion.setUltimoNumero(dto.getUltimoNumero());
    timbradoPtoExpedicion.setIdEstado(dto.getIdEstado());
    timbradoPtoExpedicion.setUsuarioModificacion(ic.getUserPrincipal());
    timbradoPtoExpedicion.setFechaModificacion(new Date(System.currentTimeMillis()));
    timbradoPtoExpedicion.setTipoComprobante(dto.getTipoComprobante());
    
    timbradoPtoExpedicionDAO.updateTimbradoPtoExpedicion(timbradoPtoExpedicion);

    return timbradoPtoExpedicion;
  }

  @Override
  public TimbradoPtoExpedicionDTO createTimbradoPtoExpedicion(TimbradoPtoExpedicionDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
    
    List<TimbradoPtoExpedicionDTO> aux = new ArrayList<TimbradoPtoExpedicionDTO>();
    aux = timbradoPtoExpedicionDAO.getTimbradoPtoExpedicionByDTO(dto);
    
    Boolean aux1 = false;
    
  //se verifica que no exista un registro para el timbrado y punto de expedicion
    if(aux != null && aux.size() > 0){
    	for(TimbradoPtoExpedicionDTO dato: aux){
    		if(dato.getIdEstado().compareTo(infoRefOpcDAO.getInfoRefOpcByAbv("ESTAC").getId())== 0){
    			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"timbrado.pto.already.exist",dto.getIdTimbrado(), dto.getIdPtoExpedicion());
    		}
    	}
    }
    
    try {
      dto.setUltimoNumero(dto.getNumeroDesde());
      dto.setUsuarioModificacion(ic.getUserPrincipal());
      dto.setFechaModificacion(new Date(System.currentTimeMillis()));
      
      
      timbradoPtoExpedicionDAO.createTimbradoPtoExpedicion(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "timbradoptoexpedicion.duplicate.key");
    }

    return dto;
  }

}