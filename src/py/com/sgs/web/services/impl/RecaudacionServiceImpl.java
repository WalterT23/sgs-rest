package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.RecaudacionDAO;
import py.com.sgs.persistence.dto.*;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.RecaudacionService;

public class RecaudacionServiceImpl implements RecaudacionService{

private Logger log = LoggerFactory.getLogger(getClass());
@Autowired private RecaudacionDAO recaudacionDAO;

@Autowired
private InfoRefOpcDAO infoRefDAO;

  public void setRecaudacionDAO(RecaudacionDAO recaudacionDAO) {
    this.recaudacionDAO = recaudacionDAO;
  }

  @Override
  public List<RecaudacionDTO> listRecaudacion(InvocationContext ic) throws ServiceException {
    List<RecaudacionDTO> recaudacions =  recaudacionDAO.listRecaudacion(Long.parseLong(ic.getUserSucursal()));

    return recaudacions;
  }

  @Override
  public RecaudacionDTO getRecaudacion(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    RecaudacionDTO recaudacion =  recaudacionDAO.getRecaudacionById(id);
    if (recaudacion == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"recaudacion.notfound", id);

    return recaudacion;
  }

  @Override
  public void deleteRecaudacion(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    RecaudacionDTO recaudacion =  recaudacionDAO.getRecaudacionById(id);
    if(recaudacion == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"recaudacion.notfound", id);

    try {
      recaudacionDAO.deleteRecaudacion(recaudacion);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "recaudacion.constraint.violation", id);
    }

  }

  @Override
  public RecaudacionDTO updateRecaudacion(Long id, RecaudacionDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    RecaudacionDTO recaudacion = recaudacionDAO.getRecaudacionById(id);
    if (recaudacion == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"recaudacion.notfound", id);

    if (dto.getFecha()!=null)
    	recaudacion.setFecha(dto.getFecha());
    if (dto.getSaldoInicial()!=null)
    	recaudacion.setSaldoInicial(dto.getSaldoInicial());
    if (dto.getIngresos()==null) 
    	dto.setIngresos(BigDecimal.ZERO);  
	   
    if (dto.getEgresos()!=null) {
  		
  		if (recaudacion.getSaldoInicial().compareTo(dto.getEgresos())<0)
  			throw new ServiceException(ErrorCode.RECAUDACION_INSUFICIENTE,"recaudacion.saldo.insuficiente"); 
  		
  		recaudacion.setEgresos(dto.getEgresos());
  		
  	}else {
			dto.setEgresos(BigDecimal.ZERO);
  	}
    
    
    	
   		recaudacion.setIngresos(dto.getIngresos());
   		//recaudacion.setEgresos(dto.getEgresos());
    
    	
		BigDecimal salfin = recaudacion.getSaldoInicial().add(dto.getIngresos()).subtract(dto.getEgresos());
		recaudacion.setSaldoFinal(salfin);
 

        recaudacionDAO.updateRecaudacion(recaudacion);
        

    return recaudacion;
  }

  @Override
  public RecaudacionDTO createRecaudacion(RecaudacionDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
        RecaudacionDTO redto=recaudacionDAO.getRecaudacionAnterior(Long.parseLong(ic.getUserSucursal()));
      Date fecha =new Date(System.currentTimeMillis());
      if (redto==null) {
    	//throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
          redto = new RecaudacionDTO();
          InfoRefOpcDTO saldoInicial = infoRefDAO.getInfoRefOpcByAbv("INIREC");
          redto.setFecha(fecha);
          redto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
          redto.setSaldoInicial(new BigDecimal(Long.parseLong(saldoInicial.getDescripcion())));
          redto.setSaldoFinal(new BigDecimal(Long.parseLong(saldoInicial.getDescripcion())));
          redto.setIngresos(BigDecimal.ZERO);
          redto.setEgresos(BigDecimal.ZERO);

          recaudacionDAO.createRecaudacion(redto);
      }


      	dto.setFecha(fecha);
      	dto.setSaldoInicial(redto.getSaldoFinal());
      	if (dto.getIngresos()==null)
      		dto.setIngresos(BigDecimal.ZERO);
      	if (dto.getEgresos()!=null) {
      		
      		if (redto.getSaldoFinal().compareTo(dto.getEgresos())<0)
      			throw new ServiceException(ErrorCode.RECAUDACION_INSUFICIENTE,"recaudacion.saldo.insuficiente"); 
      		
      		dto.setEgresos(dto.getEgresos());
      		
      	}else {
  			dto.setEgresos(BigDecimal.ZERO);
      	}
      	
      	dto.setIngresos(dto.getIngresos());
      	BigDecimal fin= redto.getSaldoFinal().add(dto.getIngresos()).subtract(dto.getEgresos());
      	dto.setSaldoFinal(fin);
      	recaudacionDAO.createRecaudacion(dto);
      	
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "recaudacion.duplicate.key");
    }

    return dto;
  }
  
  
  @Override
  public FiltroBalanceDTO getReportBalance(FiltroBalanceDTO dto , InvocationContext ic) throws ServiceException  {
  	
  	if (dto == null)
  	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");
    
    if (dto.getPeriodo() == null){ 
    	Calendar fechaD = Calendar.getInstance();
   	 //aca enviarle el actual si viene vacio
    	//se debe cargar en el dtto el periodo
    	
    }
    	 
    
    
    dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
    
    FiltroBalanceDTO reporteBalance = recaudacionDAO.getBalanceAnual(dto);
   
    reporteBalance.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
    reporteBalance.setMeses(recaudacionDAO.getBalanceMensual(dto));
    
    
    
    return reporteBalance;
  }
  

	@Override
	 public FileReportDTO getReporteRecaImpresion(FiltroBalanceDTO dto, InvocationContext ic)
	 		throws Exception {
		 FileReportDTO archivo = new FileReportDTO();
		 if(dto != null){
			// FiltroBalanceDTO datosRepo = new FiltroBalanceDTO(); 
			 FiltroBalanceDTO datosRepo = this.getReportBalance(dto, ic);
			 
			 ReportGenerator creador = new ReportGenerator();
				HashMap<String, Object> map = new HashMap<>();
				map.put("totalIngresos", datosRepo.getTotalIngreso());
				map.put("totalEgresos", datosRepo.getTotalEgreso());
				map.put("saldo", datosRepo.getSaldo());
				try {
				SimpleDateFormat xx = new SimpleDateFormat("ddMMyyyy");
				archivo.setArchivo(creador.getByteReports("E", ic, "reporteBalance.jasper", datosRepo.getMeses(), map));
				archivo.setTipo(".xls");
				archivo.setNombre("report-"+datosRepo.getPeriodo());
				}catch(Exception e) {
					log.info(e.getLocalizedMessage());
					throw e;
				}
		 }
		 
		 
	 	return archivo;
	 }

}