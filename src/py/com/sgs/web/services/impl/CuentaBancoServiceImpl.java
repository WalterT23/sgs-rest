package py.com.sgs.web.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import java.util.List;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.CuentaBancoDAO;
import py.com.sgs.persistence.dto.CuentaBancoDTO;
import py.com.sgs.util.Utils;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.CuentaBancoService;

public class CuentaBancoServiceImpl implements CuentaBancoService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private CuentaBancoDAO cuentaBancoDAO;


  public void setCuentaBancoDAO(CuentaBancoDAO cuentaBancoDAO) {
    this.cuentaBancoDAO = cuentaBancoDAO;
  }

  @Override
  public List<CuentaBancoDTO> listCuentaBanco(InvocationContext ic) throws ServiceException {
    List<CuentaBancoDTO> cuentaBancos =  cuentaBancoDAO.listCuentaBanco();

    return cuentaBancos;
  }
  
  @Override
  public List<CuentaBancoDTO> listCuentaBancoByIdBanco(Long idBanco, InvocationContext ic) throws ServiceException {
	 if (idBanco == null || idBanco <= 0)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

	 List<CuentaBancoDTO> cuentaBancos =  cuentaBancoDAO.listCuentaBancoByIdBanco(idBanco);
	 if (cuentaBancos == null)
	      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cuentabanco.notfound", idBanco);

    return cuentaBancos;
  	}

  @Override
  public CuentaBancoDTO getCuentaBanco(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    CuentaBancoDTO cuentaBanco =  cuentaBancoDAO.getCuentaBancoById(id);
    if (cuentaBanco == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cuentabanco.notfound", id);

    return cuentaBanco;
  }

  @Override
  public void deleteCuentaBanco(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    CuentaBancoDTO cuentaBanco =  cuentaBancoDAO.getCuentaBancoById(id);
    if(cuentaBanco == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cuentabanco.notfound", id);

    try {
      cuentaBancoDAO.deleteCuentaBanco(cuentaBanco);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "cuentabanco.constraint.violation", id);
    }

  }		


  @Override
  public CuentaBancoDTO updateCuentaBanco(Long id, CuentaBancoDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    CuentaBancoDTO cuentaBanco = cuentaBancoDAO.getCuentaBancoById(id);
    if (cuentaBanco == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cuentabanco.notfound", id);
    if (dto.getNombreCuenta().trim().isEmpty())
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"cuentaBanco.nullnombre");
    	
    if (dto.getTitular().trim().isEmpty())
    	 throw new ServiceException(ErrorCode.PARAMETER_ERROR,"cuentaBanco.nulltitular");
  
    cuentaBanco.setNombreCuenta(dto.getNombreCuenta());
    cuentaBanco.setIdTipoCuenta(dto.getIdTipoCuenta());
    cuentaBanco.setNroCuenta(dto.getNroCuenta());
    cuentaBanco.setTitular(dto.getTitular());
    cuentaBanco.setIdBanco(dto.getIdBanco());

    cuentaBancoDAO.updateCuentaBanco(cuentaBanco);
    

    return cuentaBanco;
  }

  @Override
  public CuentaBancoDTO createCuentaBanco(CuentaBancoDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
    
    	 if (dto.getNombreCuenta().trim().isEmpty())
    	    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"cuentaBanco.nullnombre");
    	    	
         if (dto.getTitular().trim().isEmpty())
    	    	 throw new ServiceException(ErrorCode.PARAMETER_ERROR,"cuentaBanco.nulltitular");
    	 
         dto.setNombreCuenta(dto.getNombreCuenta().trim());
         dto.setTitular(dto.getTitular().trim());

         cuentaBancoDAO.createCuentaBanco(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "cuentabanco.duplicate.key");
    }

    return dto;
  }

}