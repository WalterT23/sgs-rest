package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.BancosDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dto.BancosDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.BancosService;


public class BancosServiceImpl implements BancosService{

private Logger log = LoggerFactory.getLogger(getClass());  
@Autowired
private BancosDAO bancosDAO;
@Autowired
private InfoRefOpcDAO infoRefDAO;
  public void setBancosDAO(BancosDAO bancosDAO) {
    this.bancosDAO = bancosDAO;
  }

  @Override
  public List<BancosDTO> listBancos(InvocationContext ic) throws ServiceException {
    List<BancosDTO> bancoss =  bancosDAO.listBancos();

    return bancoss;
  }

  @Override
  public BancosDTO getBancos(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    BancosDTO bancos =  bancosDAO.getBancosById(id);
    if (bancos == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"bancos.notfound", id);

    return bancos;
  }

  @Override
  public void deleteBancos(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    BancosDTO bancos =  bancosDAO.getBancosById(id);
    if(bancos == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"bancos.notfound", id);

    try {
      bancosDAO.deleteBancos(bancos);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "bancos.constraint.violation", id);
    }

  }

  @Override
  public BancosDTO updateBancos(Long id, BancosDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    BancosDTO bancos = bancosDAO.getBancosById(id);
    if (bancos == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"bancos.notfound", id);
    if (dto.getAbreviatura().trim().isEmpty())
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"bancos.nullabreviatura");
    	
    if (dto.getNombre().trim().isEmpty())
    	 throw new ServiceException(ErrorCode.PARAMETER_ERROR,"bancos.nullnombre");
    
    
    bancos.setNombre(dto.getNombre().trim());
    bancos.setAbreviatura(dto.getAbreviatura().trim());
    InfoRefOpcDTO estConf = infoRefDAO.getInfoRefOpcByAbv("ESTAC");
    dto.setIdEstado(estConf.getId());
    bancos.setIdEstado(dto.getIdEstado());
 
    bancosDAO.updateBancos(bancos);

    return bancos;
  }

  @Override
  public BancosDTO createBancos(BancosDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
    	
      if (dto.getAbreviatura().trim().isEmpty())
        	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"bancos.nullabreviatura");
        	
      if (dto.getNombre().trim().isEmpty())
        	 throw new ServiceException(ErrorCode.PARAMETER_ERROR,"bancos.nullnombre");
      
      dto.setNombre(dto.getNombre().trim());
      dto.setAbreviatura(dto.getAbreviatura().trim());
      InfoRefOpcDTO estConf = infoRefDAO.getInfoRefOpcByAbv("ESTAC");
      dto.setIdEstado(estConf.getId());
      bancosDAO.createBancos(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "bancos.duplicate.key");
    }

    return dto;
  }

}