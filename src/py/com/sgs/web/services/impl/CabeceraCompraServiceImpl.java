package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.CabeceraCompraDAO;
import py.com.sgs.persistence.dao.CabeceraOrdenPagoDAO;
import py.com.sgs.persistence.dao.CuotaDetalleDAO;
import py.com.sgs.persistence.dao.CuotasDAO;
import py.com.sgs.persistence.dao.DetalleOrdenPagoDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.OrdenDeCompraDAO;
import py.com.sgs.persistence.dao.PagoProvDetalleDAO;
import py.com.sgs.persistence.dao.PagoProveedoresDAO;
import py.com.sgs.persistence.dao.ProductosDAO;
import py.com.sgs.persistence.dto.CabeceraCompraDTO;
import py.com.sgs.persistence.dto.CuotaDetalleDTO;
import py.com.sgs.persistence.dto.DetalleCompraDTO;
import py.com.sgs.persistence.dto.DetalleOrdenPagoDTO;
import py.com.sgs.persistence.dto.EstadoCuentaProveedorDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.FiltroComprasDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.MovMercDetalleDTO;
import py.com.sgs.persistence.dto.MovimientoRecaudacionDTO;
import py.com.sgs.persistence.dto.PagoProvDetalleDTO;
import py.com.sgs.persistence.dto.PagoProveedoresDTO;
import py.com.sgs.persistence.dto.composite.CabeceraCompraCompositeDTO;
import py.com.sgs.persistence.dto.composite.CabeceraOrdenPagoCompositeDTO;
import py.com.sgs.persistence.dto.composite.CuotasCompositeDTO;
import py.com.sgs.persistence.dto.composite.MovimientoMercaderiaCompositeDTO;
import py.com.sgs.persistence.dto.composite.OrdenDeCompraCompositeDTO;
import py.com.sgs.persistence.dto.composite.ProductosCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.CabeceraCompraService;
import py.com.sgs.web.services.DetalleCompraService;
import py.com.sgs.web.services.MovimientoMercaderiaService;
import py.com.sgs.web.services.MovimientoRecaudacionService;

public class CabeceraCompraServiceImpl implements CabeceraCompraService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private CabeceraCompraDAO cabeceraCompraDAO;
	@Autowired
	private ProductosDAO productosDao;
	@Autowired
	private DetalleCompraService detallecompradao;
	@Autowired
	private InfoRefOpcDAO infodao;
	@Autowired
	private MovimientoMercaderiaService movdao;
	@Autowired
	private OrdenDeCompraDAO ordendao;
	@Autowired
	private CabeceraOrdenPagoDAO ordenpagodao;
	@Autowired
	private DetalleOrdenPagoDAO ordenpagodetdao;
	@Autowired()
	private PagoProvDetalleDAO ppdetalledao;
	@Autowired()
	private MovimientoRecaudacionService movirecadao;
	@Autowired
	private PagoProveedoresDAO pagoProveedoresDAO;

	@Autowired
	private CuotasDAO cuotadao;

	@Autowired
	private CuotaDetalleDAO cuotadetdao;

	public void setProductosDao(ProductosDAO productosDao) {
		this.productosDao = productosDao;
	}

	public void setDetallecompradao(DetalleCompraService detallecompradao) {
		this.detallecompradao = detallecompradao;
	}

	public void setInfodao(InfoRefOpcDAO infodao) {
		this.infodao = infodao;
	}

	public void setCabeceraCompraDAO(CabeceraCompraDAO cabeceraCompraDAO) {
		this.cabeceraCompraDAO = cabeceraCompraDAO;
	}

	@Override
	public List<CabeceraCompraCompositeDTO> listCabeceraCompra(InvocationContext ic) throws ServiceException {
		List<CabeceraCompraCompositeDTO> cabeceraCompras = cabeceraCompraDAO
				.listCabeceraCompra(Long.parseLong(ic.getUserSucursal()));

		return cabeceraCompras;
	}

	@Override
	public CabeceraCompraDTO getCabeceraCompra(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null", "id");

		CabeceraCompraDTO cabeceraCompra = cabeceraCompraDAO.getCabeceraCompraById(id);
		if (cabeceraCompra == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND, "cabeceracompra.notfound", id);

		return cabeceraCompra;
	}

	@Override
	public List<CabeceraCompraCompositeDTO> getCabeceraCompraByIdProveedor(CabeceraCompraDTO cc)
			throws ServiceException {
		if (cc.getIdProveedor() == null || cc.getIdProveedor() <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null", "idProveedor");

		List<CabeceraCompraCompositeDTO> cabeceraCompras = cabeceraCompraDAO.getCabeceraCompraByIdProveedor(cc);
		return cabeceraCompras;
	}

	@Override
	public void deleteCabeceraCompra(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null", "id");

		CabeceraCompraCompositeDTO cabeceraCompra = cabeceraCompraDAO.getCabeceraCompraById(id);

		if (cabeceraCompra == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND, "cabeceracompra.notfound", id);

		try {

			InfoRefOpcDTO estado = infodao.getInfoRefOpcByAbv("FCPEN");
			InfoRefOpcDTO ivalido = infodao.getInfoRefOpcById(cabeceraCompra.getIdEstado());

			if (!cabeceraCompra.getIdEstado().equals(estado.getId()))
				throw new ServiceException(ErrorCode.ESTADO_INVALIDO, "facturacompra.estado.invalido",
						ivalido.getDescripcion());

			MovimientoMercaderiaCompositeDTO dtomov = new MovimientoMercaderiaCompositeDTO();
			ArrayList<MovMercDetalleDTO> listadeta = new ArrayList<MovMercDetalleDTO>();
			if (cabeceraCompra.getDetalles() != null && !cabeceraCompra.getDetalles().isEmpty()) {
				for (DetalleCompraDTO d : cabeceraCompra.getDetalles()) {
					MovMercDetalleDTO md = new MovMercDetalleDTO();
					md.setCantidad(d.getCantidad());
					md.setIdProducto(d.getIdProducto());
					listadeta.add(md);

					detallecompradao.deleteDetalleCompra(d.getId(), ic);

				}
				dtomov.setDetalles(listadeta);

			}
			Date fecha = new Date(System.currentTimeMillis());
			Long idusu = Long.parseLong(ic.getUserSucursal());
			InfoRefOpcDTO timov = infodao.getInfoRefOpcByAbv("EGR_M");
			InfoRefOpcDTO tipop = infodao.getInfoRefOpcByAbv("CPRA");

			// dtomov.setIdOperacion(dto.getId());
			dtomov.setFechaMovimiento(fecha);
			dtomov.setIdTipoMovimiento(timov.getId());
			dtomov.setTipoOperacion(tipop.getId());
			dtomov.setIdSucursal(idusu);
			dtomov.setUsuarioRegistro(ic.getUserPrincipal());

			movdao.createMovimientoMercaderia(dtomov, ic);

			cabeceraCompraDAO.deleteCabeceraCompra(cabeceraCompra);
		} catch (DataIntegrityViolationException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "cabeceracompra.constraint.violation", id);
		}

	}

	@Override
	public CabeceraCompraCompositeDTO updateCabeceraCompra(Long id, CabeceraCompraCompositeDTO dto,
			InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null", "id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null", "dto");

		CabeceraCompraCompositeDTO cabeceraCompra = cabeceraCompraDAO.getCabeceraCompraById(id);

		if (cabeceraCompra == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND, "cabeceracompra.notfound", dto.getNroFactura());

		InfoRefOpcDTO estado = infodao.getInfoRefOpcByAbv("FCPEN");

		if (!dto.getIdEstado().equals(estado.getId()))
			throw new ServiceException(ErrorCode.ESTADO_INVALIDO, "facturacompra.estado.invalido", id);

		if (dto.getTimbrado() != null)
			cabeceraCompra.setTimbrado(dto.getTimbrado());
		if (!dto.getNroFactura().isEmpty())
			cabeceraCompra.setNroFactura(dto.getNroFactura());
		if (dto.getTipoFactura() != null)
			cabeceraCompra.setTipoFactura(dto.getTipoFactura());
		if (dto.getOcAsociada() != null)
			cabeceraCompra.setOcAsociada(dto.getOcAsociada());
		if (dto.getIdOrden() != null)
			cabeceraCompra.setIdOrden(dto.getIdOrden());
		if (!dto.getMotivo().isEmpty())
			cabeceraCompra.setMotivo(dto.getMotivo());
		if (dto.getIdProveedor() != null)
			cabeceraCompra.setIdProveedor(dto.getIdProveedor());
		if (dto.getGravada10() != null)
			cabeceraCompra.setGravada10(dto.getGravada10());
		if (dto.getGravada5() != null)
			cabeceraCompra.setGravada5(dto.getGravada5());
		if (dto.getExenta() != null)
			cabeceraCompra.setExenta(dto.getExenta());
		if (dto.getIva10() != null)
			cabeceraCompra.setIva10(dto.getIva10());
		if (dto.getIva5() != null)
			cabeceraCompra.setIva5(dto.getIva5());
		if (dto.getTotal() != null)
			cabeceraCompra.setTotal(dto.getTotal());
		if (dto.getIdEstado() != null)
			cabeceraCompra.setIdEstado(dto.getIdEstado());
		if (dto.getTipoDocumento() != null)
			cabeceraCompra.setTipoDocumento(dto.getTipoDocumento());
		if (dto.getSaldo() != null)
			cabeceraCompra.setSaldo(dto.getSaldo());

		cabeceraCompraDAO.updateCabeceraCompra(cabeceraCompra);

		if (dto.getDetalles() != null && !dto.getDetalles().isEmpty()) {

			for (DetalleCompraDTO d : dto.getDetalles()) {

				detallecompradao.updateDetalleCompra(d.getId(), d, ic);
			}
		}

		if (dto.getDetallesAAgregar() != null && !dto.getDetallesAAgregar().isEmpty()) {

			for (DetalleCompraDTO d : dto.getDetallesAAgregar()) {
				d.setIdCabeceraCompra(dto.getId());
				detallecompradao.createDetalleCompra(d, ic);
			}
		}

		if (dto.getDetallesAEliminar() != null && !dto.getDetallesAEliminar().isEmpty()) {

			for (DetalleCompraDTO d : dto.getDetallesAEliminar()) {
				detallecompradao.deleteDetalleCompra(d.getId(), ic);
			}
		}

		return cabeceraCompra;
	}

	@Override
	public FiltroComprasDTO getReporCompras(FiltroComprasDTO dto, InvocationContext ic) throws ServiceException {

		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null", "id");

		if (dto.getFechaDesde() == null) {
			Calendar fechaD = Calendar.getInstance();
			fechaD.add(Calendar.DATE, -15);
			dto.setFechaDesde(fechaD.getTime());
		}

		if (dto.getFechaHasta() == null) {
			Calendar fechaH = Calendar.getInstance();
			dto.setFechaHasta(fechaH.getTime());
		}
		dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

		FiltroComprasDTO reporteCompras = new FiltroComprasDTO();
		BigDecimal total = new BigDecimal(0);
		reporteCompras.setFechaDesde(dto.getFechaDesde());
		reporteCompras.setFechaHasta(dto.getFechaHasta());
		reporteCompras.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

		if (cabeceraCompraDAO.getReporCompras(dto) != null && !cabeceraCompraDAO.getReporCompras(dto).isEmpty()) {
			for (CabeceraCompraDTO cc : cabeceraCompraDAO.getReporCompras(dto)) {
				total = total.add(cc.getTotal());
			}
		}
		reporteCompras.setTotalXfecha(cabeceraCompraDAO.getComprasGraphic(dto));
		reporteCompras.setDetalles(cabeceraCompraDAO.getReporCompras(dto));
		reporteCompras.setTotal(total);

		return reporteCompras;
	}

	@Override
	public FileReportDTO getComprasImpresion(FiltroComprasDTO dto, InvocationContext ic) throws Exception {
		FileReportDTO archivo = new FileReportDTO();
		if (dto != null) {
			FiltroComprasDTO datosRepo = this.getReporCompras(dto, ic);
			ReportGenerator creador = new ReportGenerator();
			HashMap<String, Object> map = new HashMap<>();
			map.put("fechaDesde", datosRepo.getFechaDesde());
			map.put("fechaHasta", datosRepo.getFechaHasta());
			map.put("proveedor", datosRepo.getProveedor());
			map.put("totalCompras", datosRepo.getTotal());
			try {
				SimpleDateFormat xx = new SimpleDateFormat("ddMMyyyy");
				archivo.setArchivo(
						creador.getByteReports("E", ic, "reporteCompras.jasper", datosRepo.getDetalles(), map));
				archivo.setTipo(".xls");
				archivo.setNombre("reporteCompra-" + xx.format(datosRepo.getFechaDesde()) + "-"
						+ xx.format(datosRepo.getFechaHasta()));
			} catch (Exception e) {
				log.info(e.getLocalizedMessage());
				throw e;
			}
		}

		return archivo;
	}

	@Override
	public CabeceraCompraCompositeDTO createCabeceraCompra(CabeceraCompraCompositeDTO dto, InvocationContext ic)
			throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null", "dto");

		try {

			MovimientoMercaderiaCompositeDTO dtomov = new MovimientoMercaderiaCompositeDTO();
			BigDecimal total = new BigDecimal(0);

			if (dto.getDetalles() != null && !dto.getDetalles().isEmpty()) {
				BigDecimal iva5 = new BigDecimal(0);
				BigDecimal iva10 = new BigDecimal(0);
				BigDecimal gravada5 = new BigDecimal(0);
				BigDecimal gravada10 = new BigDecimal(0);
				BigDecimal excenta = new BigDecimal(0);

				for (DetalleCompraDTO ccd : dto.getDetalles()) {

					if (ccd != null) {

						ProductosCompositeDTO di = new ProductosCompositeDTO();
						di.setId(ccd.getIdProducto());
						di.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

						ProductosCompositeDTO producto = productosDao.getProductosByIdNew(di);
						if (ccd.getImpuestoFiscal() == null)
							ccd.setImpuestoFiscal(producto.getIdTipoTributo());

						ccd.setPrecioTotal(new BigDecimal(ccd.getCantidad()).multiply(ccd.getPrecioUnitario()));
						if (producto.getAbvTributo().equals("TTIV5")) {
							iva5 = iva5.add(ccd.getPrecioTotal().divide(new BigDecimal(22), RoundingMode.HALF_UP));
							gravada5 = gravada5.add(ccd.getPrecioTotal());
						} else if (producto.getAbvTributo().equals("TTI10")) {
							iva10 = iva10.add(ccd.getPrecioTotal().divide(new BigDecimal(11), RoundingMode.HALF_UP));
							gravada10 = gravada10.add(ccd.getPrecioTotal());
						} else {
							excenta = excenta.add(ccd.getPrecioTotal());
						}
						total = total.add(ccd.getPrecioTotal());
					}
				}
				dto.setGravada5(gravada5);
				dto.setGravada10(gravada10);
				dto.setIva5(iva5);
				dto.setIva10(iva10);
				dto.setExenta(excenta);
				dto.setTotal(total);

			}

			Date fecha = new Date(System.currentTimeMillis());
			Long idusu = Long.parseLong(ic.getUserSucursal());
			InfoRefOpcDTO estfc = infodao.getInfoRefOpcByAbv("FCPEN");

			dto.setIdEstado(estfc.getId());
			dto.setFechaCreacion(fecha);
			dto.setUsuarioCreacion(ic.getUserPrincipal());
			dto.setIdSucursal(idusu);

			if (dto.getCuotas().getMontoEntregado() != null
					&& dto.getCuotas().getMontoEntregado().compareTo(BigDecimal.ZERO) > 0) {
				BigDecimal entre = dto.getCuotas().getMontoEntregado();
				dto.setSaldo(total.subtract(entre));
			} else {
				dto.setSaldo(total);
				dto.getCuotas().setMontoEntregado(BigDecimal.ZERO);
			}

			cabeceraCompraDAO.createCabeceraCompra(dto);

			ArrayList<MovMercDetalleDTO> listadeta = new ArrayList<MovMercDetalleDTO>();

			if (dto.getDetalles() != null && !dto.getDetalles().isEmpty()) {

				for (DetalleCompraDTO d : dto.getDetalles()) {

					d.setIdCabeceraCompra(dto.getId());

					MovMercDetalleDTO md = new MovMercDetalleDTO();
					md.setCantidad(d.getCantidad());
					md.setIdProducto(d.getIdProducto());
					listadeta.add(md);

					detallecompradao.createDetalleCompra(d, ic);
				}

				dtomov.setDetalles(listadeta);
			}

			InfoRefOpcDTO timov = infodao.getInfoRefOpcByAbv("ING_M");
			InfoRefOpcDTO tipop = infodao.getInfoRefOpcByAbv("CPRA");

			dtomov.setIdOperacion(dto.getId());
			dtomov.setFechaMovimiento(fecha);
			dtomov.setIdTipoMovimiento(timov.getId());
			dtomov.setTipoOperacion(tipop.getId());
			dtomov.setIdSucursal(idusu);
			dtomov.setUsuarioRegistro(ic.getUserPrincipal());

			movdao.createMovimientoMercaderia(dtomov, ic);

			if (dto.getIdOrden() != null) {

				OrdenDeCompraCompositeDTO odc = ordendao.getOrdenDeCompraById(dto.getIdOrden());
				InfoRefOpcDTO esta = infodao.getInfoRefOpcByAbv("EOAFE");
				odc.setIdEstado(esta.getId());
				ordendao.updateOrdenDeCompra(odc);
			}

			InfoRefOpcDTO tipoFC = infodao.getInfoRefOpcByAbv("FCTO");
			InfoRefOpcDTO chq = infodao.getInfoRefOpcByAbv("CQUE");
			InfoRefOpcDTO cta = infodao.getInfoRefOpcByAbv("CTA_CRIA");
			// InfoRefOpcDTO efe=infodao.getInfoRefOpcByAbv("EFE");

			if (dto.getTipoFactura().equals(tipoFC.getId())) {

				if (dto.getCuotas() == null)
					throw new ServiceException(ErrorCode.FACT_CTO_CTAS, "cuotas.null.param");

				CuotasCompositeDTO cuo = new CuotasCompositeDTO();

				cuo.setCantidadCuotas(dto.getCuotas().getCantidadCuotas());
				cuo.setIdCabeceraCompra(dto.getId());
				cuo.setIdProveedor(dto.getIdProveedor());
				cuo.setIdSucursal(idusu);
				cuo.setMontoEntregado(dto.getCuotas().getMontoEntregado());

				cuotadao.createCuotas(cuo);
				InfoRefOpcDTO pen = infodao.getInfoRefOpcByAbv("FCPEN");

				for (CuotaDetalleDTO cdd : dto.getCuotas().getDetalles()) {
					cdd.setIdCuotas(cuo.getId());
					cdd.setIdEstado(pen.getId());

					cuotadetdao.createCuotaDetalle(cdd);
				}

				if (dto.getCuotas().getMontoEntregado() != null
						&& dto.getCuotas().getMontoEntregado().compareTo(BigDecimal.ZERO) > 0) {

					PagoProveedoresDTO pp = new PagoProveedoresDTO();

					pp.setFecha(fecha);
					pp.setIdProveedor(dto.getIdProveedor());
					pp.setTotalPagado(dto.getCuotas().getMontoEntregado());
					pp.setIdSucursal(idusu);

					// creo mi cabecera pago
					pagoProveedoresDAO.createPagoProveedores(pp);

					if (dto.getPagos().getDetalles() != null && !dto.getPagos().getDetalles().isEmpty()) {
						// inserto los valores
						InfoRefOpcDTO medio = new InfoRefOpcDTO();
						for (PagoProvDetalleDTO pd : dto.getPagos().getDetalles()) {
							pd.setIdCabecera(dto.getId());
							pd.setIdPago(pp.getId());
							pd.setMonto(dto.getCuotas().getMontoEntregado());
							
							if (pd.getIdMedio() == null) {
								
								medio = infodao.getInfoRefOpcByAbv(pd.getAbreviaturaMedio());
								pd.setIdMedio(medio.getId());
							}
							ppdetalledao.createPagoProvDetalle(pd);

							if (pd.getIdMedio().equals(chq.getId()) || pd.getIdMedio().equals(cta.getId())) {

								// crear op en el caso de que sea cheque o cuenta bancaria

								Long nroOrdenPago = ordenpagodao.getNroOrdenPago(Long.parseLong(ic.getUserSucursal()));
								if (nroOrdenPago == null) {
									nroOrdenPago = 1L;
								} else {
									nroOrdenPago = nroOrdenPago + 1L;
								}

								CabeceraOrdenPagoCompositeDTO op = new CabeceraOrdenPagoCompositeDTO();
								op.setNroOrdenPago(String.format("%010d", nroOrdenPago));
								op.setIdProveedor(dto.getIdProveedor());
								op.setFechaCreacion(fecha);
								op.setFechaOrdenPago(fecha);
								op.setMontoTotal(dto.getCuotas().getMontoEntregado());
								op.setUsuarioCreacion(ic.getUserPrincipal());
								op.setIdFuente(pd.getIdFuente());
								op.setIdMedio(pd.getIdMedio());
								op.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
								
								if (!pd.getNroCheque().isEmpty())
									op.setCheque(pd.getNroCheque());

								ordenpagodao.createCabeceraOrdenPago(op);

								DetalleOrdenPagoDTO opd = new DetalleOrdenPagoDTO();

								opd.setIdCabeceraCompra(dto.getId());
								opd.setIdCabeceraOp(op.getId());
								opd.setMonto(pd.getMonto());

								// creo el detalle de mi op
								ordenpagodetdao.createDetalleOrdenPago(opd);

							} else {

								// creo movimiento de recaudacion
								// el control de suficiencia de saldos se realiza en el impl de reca

								InfoRefOpcDTO tmovi = infodao.getInfoRefOpcByAbv("EGR");
								InfoRefOpcDTO movi = infodao.getInfoRefOpcByAbv("PGPR");

								MovimientoRecaudacionDTO movidto = new MovimientoRecaudacionDTO();
								movidto.setFechaHora(fecha);
								movidto.setIdMovimiento(movi.getId());
								movidto.setIdTipoMovimiento(tmovi.getId());
								movidto.setMonto(pd.getMonto());

								movirecadao.createMovimientoRecaudacion(movidto, ic);
							}

						}

					}

				} else {
					dto.getCuotas().setMontoEntregado(BigDecimal.ZERO);
				}

			}

		} catch (DuplicateKeyException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "cabeceracompra.duplicate.key");
		}

		return dto;
	}

}