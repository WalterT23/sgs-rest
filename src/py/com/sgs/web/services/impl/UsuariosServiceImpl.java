package py.com.sgs.web.services.impl;

import java.util.Calendar;
import java.util.List;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.FuncionalidadesDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.UsuariosDAO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.UsuarioSucursalDTO;
//import py.com.sgs.persistence.dao.InfoRefDAO;
import py.com.sgs.persistence.dto.UsuariosDTO;
import py.com.sgs.persistence.dto.composite.UsuariosCompositeDTO;
import py.com.sgs.util.MD5;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.UsuariosService;

public class UsuariosServiceImpl implements UsuariosService {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private UsuariosDAO usuariosDAO;

	@Autowired
	private FuncionalidadesDAO funcionalidadesDAO;
	
	@Autowired
	private InfoRefOpcDAO infoRefOpcDAO;

	// @Autowired
	// private AccesoServiceImpl accesoServiceImpl;

	public void setUsuariosDAO(UsuariosDAO usuariosDAO) {
		this.usuariosDAO = usuariosDAO;
	}

	@Override
	public List<UsuariosCompositeDTO> listUsuarios(InvocationContext ic)
			throws ServiceException {
		
		List<UsuariosCompositeDTO> usuarios = usuariosDAO.listUsuarios();

		Calendar fecha = Calendar.getInstance();

		for (UsuariosCompositeDTO uc : usuarios) {
			if (uc.getSucursales() != null)
				for (UsuarioSucursalDTO us : uc.getSucursales()) {
					us.setVigente(us.getFinVigencia().after(fecha.getTime()));
				}
		}

		return usuarios;
	}

	@Override
	public List<UsuariosCompositeDTO> listRolesUsuario(InvocationContext ic)
			throws ServiceException {
		List<UsuariosCompositeDTO> usuarios = usuariosDAO.listRolesUsuario();

		return usuarios;
	}

	@Override
	public UsuariosCompositeDTO getUsuarios(Long id, InvocationContext ic)
			throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		UsuariosCompositeDTO usuarios = usuariosDAO.getUsuariosById(id);
		if (usuarios == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"usuarios.notfound", id);

		Calendar fecha = Calendar.getInstance();

		if (usuarios.getSucursales() != null)
			for (UsuarioSucursalDTO us : usuarios.getSucursales()) {
				us.setVigente(us.getFinVigencia().after(fecha.getTime()));
			}

		return usuarios;
	}

	@Override
	public UsuariosDTO updateUsuarios(Long id, UsuariosDTO dto,
			InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		UsuariosDTO usuarios = usuariosDAO.getUsuariosById(id);
		if (usuarios == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"usuarios.notfound", id);

		if (dto.getNombre() != null)
			usuarios.setNombre(dto.getNombre());

		if (dto.getApellido() != null)
			usuarios.setApellido(dto.getApellido());

		if (dto.getUsuario() != null)
			usuarios.setUsuario(dto.getUsuario());

		if (dto.getPass() != null)
			usuarios.setPass(MD5.getMD5(dto.getPass()));

		if (dto.getIdEstado() != null)
			usuarios.setIdEstado(dto.getIdEstado());

		usuariosDAO.updateUsuarios(usuarios);

		return usuarios;
	}

	@Override
	public UsuariosDTO createUsuarios(UsuariosDTO dto, InvocationContext ic)
			throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		try {
			dto.setPass(MD5.getMD5(dto.getPass()));

			usuariosDAO.createUsuarios(dto);
		} catch (DuplicateKeyException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION,
					"usuarios.duplicate.key");
		}

		return dto;
	}

	@Override
	public UsuariosCompositeDTO validarUsuario(UsuariosDTO dto,InvocationContext ic) throws ServiceException {

		UsuariosCompositeDTO retorno = new UsuariosCompositeDTO();

		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "null.param",
					"dto");

		if (dto.getUsuario() == null || dto.getUsuario().isEmpty())
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "null.param",
					"usuario");

		if (dto.getPass() == null || dto.getPass().isEmpty())
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "null.param",
					"pass");

		try {

			dto.setPass(MD5.getMD5(dto.getPass()));

			retorno = usuariosDAO.getUsuariosByUser(dto);

			if (retorno == null)
				throw new ServiceException(ErrorCode.USUARIO_NO_EXISTE,"usuario.not.found", dto.getUsuario());

			if (!retorno.getPass().equals(dto.getPass()))
				throw new ServiceException(ErrorCode.PASS_ERROR,"usuario.pass.error");
			
			if (retorno.getSucursales().size() == 0)
				throw new ServiceException(ErrorCode.USUARIO_NO_EXISTE,"usuario.not.found", dto.getUsuario());
			
			retorno.setFuncionalidades(funcionalidadesDAO.listFuncionalidadesByUsr(retorno.getId()));
			
			Calendar fecha = Calendar.getInstance();

			if (retorno.getSucursales().size() > 0) {
				for (UsuarioSucursalDTO us : retorno.getSucursales()) {
					us.setVigente(us.getFinVigencia().after(fecha.getTime()));
				}
			}
			
			try {

				retorno.setPass(null);
				OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(
						new MD5Generator());
				String accessToken = oauthIssuerImpl.accessToken();
				retorno.setAccesstoken(accessToken);

			} catch (Exception e) {
				log.error(e.getMessage(), e);
				throw new ServiceException(ErrorCode.TOKEN_NO_EXISTE,
						"token.not.found");
			}
		} catch (DuplicateKeyException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION,
					"usuarios.duplicate.key");
		}
		return retorno;
	}

	@Override
	public UsuariosDTO inactivarUsuario(UsuariosDTO dto, InvocationContext ic) throws ServiceException {
	
		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		UsuariosDTO usuarios = usuariosDAO.getUsuariosById(dto.getId());
		
		if (usuarios == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"usuarios.notfound", dto.getId());

		InfoRefOpcDTO estado = infoRefOpcDAO.getInfoRefOpcByAbv("ESTIN");
		
		dto.setIdEstado(estado.getId());

		usuariosDAO.inactivarUsuario(dto);

		return dto;
	}

}