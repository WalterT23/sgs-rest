package py.com.sgs.web.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import java.math.BigDecimal;
import java.util.List;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.ListaPreciosDAO;
import py.com.sgs.persistence.dao.ListaPreciosDetalleDAO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.ListaPreciosDTO;
import py.com.sgs.persistence.dto.ListaPreciosDetalleDTO;
import py.com.sgs.persistence.dto.composite.LPDetallesCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ListaPreciosDetalleService;

public class ListaPreciosDetalleServiceImpl implements
		ListaPreciosDetalleService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ListaPreciosDetalleDAO listaPreciosDetalleDAO;

	@Autowired
	private InfoRefOpcDAO infoRefOpcDAO;

	@Autowired
	private ListaPreciosDAO listaPreciosDAO;

	public void setListaPreciosDetalleDAO(
			ListaPreciosDetalleDAO listaPreciosDetalleDAO) {
		this.listaPreciosDetalleDAO = listaPreciosDetalleDAO;
	}

	@Override
	public List<LPDetallesCompositeDTO> listListaPreciosDetalle(
			InvocationContext ic) throws ServiceException {
		List<LPDetallesCompositeDTO> listaPreciosDetalles = listaPreciosDetalleDAO
				.listListaPreciosDetalle();

		return listaPreciosDetalles;
	}

	@Override
	public LPDetallesCompositeDTO getListaPreciosDetalle(Long id,
			InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", id);

		LPDetallesCompositeDTO listaPreciosDetalle = listaPreciosDetalleDAO
				.getListaPreciosDetalleById(id);
		if (listaPreciosDetalle == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"listapreciosdetalle.notfound", id);

		return listaPreciosDetalle;
	}

	@Override
	public LPDetallesCompositeDTO precioByLPcodPro(String codPro, InvocationContext ic) throws ServiceException {

		if (codPro.trim().isEmpty())
			throw new ServiceException(ErrorCode.VALIDATION_FAILED,"codigo.producto.notfound", codPro);
		
		if ( ic.getUserSucursal() == null || ic.getUserSucursal().trim().isEmpty())
			throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null", "sucursal");
			
		InfoRefOpcDTO idTipoLista = infoRefOpcDAO.getInfoRefOpcByAbv("LPBASE");
		if (idTipoLista == null)
			throw new ServiceException(ErrorCode.VALIDATION_FAILED,"tipo.lista.notfound");

		ListaPreciosDTO lista = listaPreciosDAO.listExist(Long.parseLong(ic.getUserSucursal()), idTipoLista.getId());
		if (lista == null)
			throw new ServiceException(ErrorCode.VALIDATION_FAILED,"valid.list.not.found");

		LPDetallesCompositeDTO detalle = listaPreciosDetalleDAO.precioByLPcodPro(lista.getId(), codPro);
		if (detalle == null)
			throw new ServiceException(ErrorCode.DETALLE_LP_NOT_FOUND,"detalle.not.found", codPro);
		
		if (detalle.getPrecioVenta() == null || detalle.getPrecioVenta().compareTo(BigDecimal.ZERO) == 0)
			throw new ServiceException(ErrorCode.DETALLE_LP_NOT_FOUND,"detalle.incomplete", codPro);

		return detalle;
	}

	@Override
	public List<LPDetallesCompositeDTO> getListaPreciosDetalleByIdLP(Long idLP,
			InvocationContext ic) throws ServiceException {
		if (idLP == null || idLP <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", idLP);

		List<LPDetallesCompositeDTO> listaPreciosDetalles = listaPreciosDetalleDAO
				.getListaPreciosDetalleByIdLP(idLP);
		// listaPreciosDetalles.sort(Comparator);
		// if (listaPreciosDetalles == null)
		// throw new
		// ServiceException(ErrorCode.NO_DATA_FOUND,"listapreciosdetalle.notfound",
		// idLP);

		return listaPreciosDetalles;
	}

	@Override
	public void deleteListaPreciosDetalle(Long id, InvocationContext ic)
			throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		ListaPreciosDetalleDTO listaPreciosDetalle = listaPreciosDetalleDAO
				.getListaPreciosDetalleById(id);
		if (listaPreciosDetalle == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"listapreciosdetalle.notfound", id);

		try {
			listaPreciosDetalleDAO
					.deleteListaPreciosDetalle(listaPreciosDetalle);
		} catch (DataIntegrityViolationException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION,
					"listapreciosdetalle.constraint.violation", id);
		}

	}

	@Override
	public ListaPreciosDetalleDTO updateListaPreciosDetalle(Long id,
			ListaPreciosDetalleDTO dto, InvocationContext ic)
			throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		ListaPreciosDetalleDTO listaPreciosDetalle = listaPreciosDetalleDAO
				.getListaPreciosDetalleById(id);

		if (listaPreciosDetalle == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"listapreciosdetalle.notfound", id);

		if (dto.getIdProducto() != null)
			listaPreciosDetalle.setIdProducto(dto.getIdProducto());

		if (dto.getCostoBase() != null)
			listaPreciosDetalle.setCostoBase(dto.getCostoBase());

		if (dto.getIdTipoRecarga() != null)
			listaPreciosDetalle.setIdTipoRecarga(dto.getIdTipoRecarga());

		if (dto.getMontoRecarga() != null)
			listaPreciosDetalle.setMontoRecarga(dto.getMontoRecarga());

		if (dto.getPrecioVenta() != null)
			listaPreciosDetalle.setPrecioVenta(dto.getPrecioVenta());

		listaPreciosDetalleDAO.updateListaPreciosDetalle(listaPreciosDetalle);

		return listaPreciosDetalle;
	}

	@Override
	public ListaPreciosDetalleDTO createListaPreciosDetalle(
			ListaPreciosDetalleDTO dto, InvocationContext ic)
			throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		try {

			listaPreciosDetalleDAO.createListaPreciosDetalle(dto);
		} catch (DuplicateKeyException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION,
					"listapreciosdetalle.duplicate.key");
		}

		return dto;
	}

}