package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.CabeceraOrdenPagoDAO;
import py.com.sgs.persistence.dto.CabeceraOrdenPagoDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.composite.CabeceraOrdenPagoCompositeDTO;
import py.com.sgs.persistence.dto.composite.CabeceraVentaCompositeDTO;
import py.com.sgs.util.report.ReportDetalleVenta;
import py.com.sgs.util.report.ReportOrdenPago;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.CabeceraOrdenPagoService;

public class CabeceraOrdenPagoServiceImpl implements CabeceraOrdenPagoService{

	private Logger log = LoggerFactory.getLogger(getClass());  
	@Autowired
	private CabeceraOrdenPagoDAO cabeceraOrdenPagoDAO;



	public void setCabeceraOrdenPagoDAO(CabeceraOrdenPagoDAO cabeceraOrdenPagoDAO) {
		this.cabeceraOrdenPagoDAO = cabeceraOrdenPagoDAO;
	}

	@Override
	public List<CabeceraOrdenPagoDTO> listCabeceraOrdenPago(InvocationContext ic) throws ServiceException {
		List<CabeceraOrdenPagoDTO> cabeceraOrdenPagos =  cabeceraOrdenPagoDAO.listCabeceraOrdenPago();

		return cabeceraOrdenPagos;
	}

	@Override
	public CabeceraOrdenPagoCompositeDTO getCabeceraOrdenPago(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		CabeceraOrdenPagoCompositeDTO cabeceraOrdenPago =  cabeceraOrdenPagoDAO.getCabeceraOrdenPagoById(id);
		if (cabeceraOrdenPago == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cabeceraordenpago.notfound", id);

		return cabeceraOrdenPago;
	}

	@Override
	public void deleteCabeceraOrdenPago(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		CabeceraOrdenPagoDTO cabeceraOrdenPago =  cabeceraOrdenPagoDAO.getCabeceraOrdenPagoById(id);
		if(cabeceraOrdenPago == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cabeceraordenpago.notfound", id);

		try {
			cabeceraOrdenPagoDAO.deleteCabeceraOrdenPago(cabeceraOrdenPago);
		}
		catch (DataIntegrityViolationException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "cabeceraordenpago.constraint.violation", id);
		}

	}

	@Override
	public CabeceraOrdenPagoDTO updateCabeceraOrdenPago(Long id, CabeceraOrdenPagoDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		CabeceraOrdenPagoDTO cabeceraOrdenPago = cabeceraOrdenPagoDAO.getCabeceraOrdenPagoById(id);
		if (cabeceraOrdenPago == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cabeceraordenpago.notfound", id);

		cabeceraOrdenPago.setIdProveedor(dto.getIdProveedor());
		cabeceraOrdenPago.setNroOrdenPago(dto.getNroOrdenPago());
		cabeceraOrdenPago.setFechaOrdenPago(dto.getFechaOrdenPago());
		cabeceraOrdenPago.setFechaImpresion(dto.getFechaImpresion());
		cabeceraOrdenPago.setUsuarioImpresion(dto.getUsuarioImpresion());
		cabeceraOrdenPago.setMontoTotal(dto.getMontoTotal());
		cabeceraOrdenPago.setIdEstado(dto.getIdEstado());
		cabeceraOrdenPago.setIdSucursal(dto.getIdSucursal());



		cabeceraOrdenPagoDAO.updateCabeceraOrdenPago(cabeceraOrdenPago);


		return cabeceraOrdenPago;
	}

	@Override
	public CabeceraOrdenPagoDTO createCabeceraOrdenPago(CabeceraOrdenPagoDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {
			dto.setUsuarioCreacion(ic.getUserPrincipal());
			cabeceraOrdenPagoDAO.createCabeceraOrdenPago(dto);
		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "cabeceraordenpago.duplicate.key");
		}

		return dto;
	}
	
	@Override
	public PrintDTO imprimir(Long id, InvocationContext ic) throws ServiceException{
		
		PrintDTO retorno = new PrintDTO();
		//se obtiene la cabecera venta
		CabeceraOrdenPagoCompositeDTO ordenPago = cabeceraOrdenPagoDAO.getCabeceraOrdenPagoById(id);
		if(ordenPago == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cabeceraordenpago.notfound", id);
		//se agrega la empresa
//		venta.setEmpresa(cabeceraVentaDAO.getEmpresaById(1L));
		retorno = this.getReport(ordenPago);
		
		return retorno;
		
	}
	
	private PrintDTO getReport(CabeceraOrdenPagoCompositeDTO param) throws ServiceException {
	      PrintDTO reporte = new PrintDTO();
	      try {
	    	  ReportOrdenPago rOP = new ReportOrdenPago(param);
	          reporte.setBytes(rOP.generarPDF());
	          reporte.setFileName(param.getNroOrdenPago());
	      } catch (Exception e) {
	          log.error(e.getMessage(), e);
	          reporte.setMimeType(null);
	          reporte.setType(null);
	          throw new ServiceException(ErrorCode.IMPRIMIR_OC_ERROR, "sgs.print.oc.error", param.getId());
	      }
	      return reporte;
	}

}