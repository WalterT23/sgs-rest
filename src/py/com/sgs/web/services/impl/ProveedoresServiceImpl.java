package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.CrossOrigin;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.ProveedoresDAO;
import py.com.sgs.persistence.dto.CuotasPenDTO;
import py.com.sgs.persistence.dto.EstadoCuentaProveedorDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.ProveedoresDTO;
import py.com.sgs.persistence.dto.ReporteVentasRealizadasDTO;
import py.com.sgs.persistence.dto.TransaccionesCompraDTO;
import py.com.sgs.persistence.dto.composite.ProveedoresCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ProveedoresService;

@CrossOrigin()

public class ProveedoresServiceImpl implements ProveedoresService{

private Logger log = LoggerFactory.getLogger(getClass());  
@Autowired
private ProveedoresDAO proveedoresDAO;
@Autowired
private InfoRefOpcDAO infoRefDAO;


  public void setProveedoresDAO(ProveedoresDAO proveedoresDAO) {
    this.proveedoresDAO = proveedoresDAO;
  }

  @Override
  public List<ProveedoresCompositeDTO> listProveedores(InvocationContext ic) throws ServiceException {
    List<ProveedoresCompositeDTO> proveedores =  proveedoresDAO.listProveedores();
    
    if (proveedores == null)
	      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedores.notfound");
	  
	  for(ProveedoresCompositeDTO p: proveedores){
		  if(p.getRuc() != null){
			  p.setDv(Integer.parseInt(p.getRuc().toString().substring(p.getRuc().toString().length() - 1, p.getRuc().toString().length())));
			  p.setRuc(p.getRuc().toString().substring(0, p.getRuc().toString().length() - 1));
		  }
	  }
    
    return proveedores;
  }
  
  @Override
  public List<ProveedoresCompositeDTO> getProveedorById(Long id, InvocationContext ic) throws ServiceException {
	  if (id == null || id <= 0)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

	  List<ProveedoresCompositeDTO> proveedor =  proveedoresDAO.getProveedorById(id);
	    
	  if (proveedor == null)
	      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedores.notfound", id);
	  
	  for(ProveedoresCompositeDTO p: proveedor){
		  if(p.getRuc() != null){
			  p.setDv(Integer.parseInt(p.getRuc().toString().substring(p.getRuc().toString().length() - 1, p.getRuc().toString().length())));
			  p.setRuc(p.getRuc().toString().substring(0, p.getRuc().toString().length() - 1));
		  }
	  }

	    return proveedor;
  }
  
  @Override
  public List<ProveedoresDTO> getProveedoresByRuc(String ruc, InvocationContext ic) throws ServiceException {
	  if (ruc.isEmpty())
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

	  List<ProveedoresDTO> lproveedor =  proveedoresDAO.getProveedoresByRuc(ruc);
	    
	  if (lproveedor == null)
	      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedores.notfound", ruc);
	  
	  return lproveedor;
  }
  
  @Override
  public ProveedoresCompositeDTO getProveedores(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ProveedoresCompositeDTO proveedores =  proveedoresDAO.getProveedoresById(id);
    if (proveedores == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedores.notfound", id);
    
	
	if(proveedores.getRuc() != null){
	  proveedores.setDv(Integer.parseInt(proveedores.getRuc().toString().substring(proveedores.getRuc().toString().length() - 1, proveedores.getRuc().toString().length())));
	  proveedores.setRuc(proveedores.getRuc().toString().substring(0, proveedores.getRuc().toString().length() - 1));
	}
	
	
    return proveedores;
  }
  
  
  @Override
  public List<CuotasPenDTO> listSaldoProveedor(InvocationContext ic) throws ServiceException {
    List<CuotasPenDTO> cuotassp =  proveedoresDAO.listSaldoProveedor(Long.parseLong(ic.getUserSucursal()));

    return cuotassp;
  }


  @Override
  public void deleteProveedores(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ProveedoresDTO proveedores =  proveedoresDAO.getProveedoresById(id);
    if(proveedores == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedores.notfound", id);

    try {
      proveedoresDAO.deleteProveedores(proveedores);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "proveedores.constraint.violation", id);
    }

  }

  @Override
  public ProveedoresDTO updateProveedores(Long id, ProveedoresDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    ProveedoresDTO proveedores = proveedoresDAO.getProveedorSById(id);
    if (proveedores == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedores.notfound", id);
    
    if (dto.getRazonSocial().trim().isEmpty())
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"proveedores.nullrazonsocial");
    
    if (dto.getDireccion().trim().isEmpty())
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"proveedores.nulldireccion");
    
    if (dto.getRuc()!= null)
    	proveedores.setRuc(dto.getRuc());
    
    
    if (dto.getDv() != null)
    	proveedores.setDv(dto.getDv());
    
    
    if (dto.getIdEstado() != null)
    	proveedores.setIdEstado(dto.getIdEstado());
    
    if (dto.getNotas() != null)
    	proveedores.setNotas(dto.getNotas());
    
    if (dto.getNombreFantasia()!= null && !dto.getNombreFantasia().trim().isEmpty())
    	
    	proveedores.setNombreFantasia(dto.getNombreFantasia());
   
	proveedores.setRazonSocial(dto.getRazonSocial().trim());
	proveedores.setDireccion(dto.getDireccion().trim());
    proveedores.setFechaModificacion(new Date(System.currentTimeMillis()));
 
    
    proveedoresDAO.updateProveedores(proveedores);
    
    return proveedores;
  }

  @Override
  public ProveedoresDTO createProveedores(ProveedoresDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
    	
    	 if (dto.getRazonSocial().trim().isEmpty())
    	    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"proveedores.nullrazonsocial");
    	    
    	 if (dto.getDireccion().trim().isEmpty())
    	    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"proveedores.nulldireccion");
      
    	dto.setFechaRegistro(new Date(System.currentTimeMillis()));
    	dto.setRazonSocial(dto.getRazonSocial().trim());
    	dto.setDireccion(dto.getDireccion().trim());
        InfoRefOpcDTO estConf = infoRefDAO.getInfoRefOpcByAbv("ESTAC");
        dto.setIdEstado(estConf.getId());
    	proveedoresDAO.createProveedores(dto);
    	
    	
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "proveedores.duplicate.key");
    }

    return dto;
  }

@Override
public ProveedoresDTO getProveedorSById(Long id, InvocationContext ic) throws ServiceException {
	if (id == null || id <= 0)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

	  ProveedoresDTO proveedor =  proveedoresDAO.getProveedorSById(id);
	    
	  if (proveedor == null)
	      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedores.notfound", id);

	    return proveedor;

}



		@Override
		public EstadoCuentaProveedorDTO getEstadoCuenta(EstadoCuentaProveedorDTO dto , InvocationContext ic) throws ServiceException  {
			
		 if (dto == null)
			      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");
		 
		 if(dto.getMes()!=null) {
			 Calendar fecha = Calendar.getInstance();
			 fecha.set(Calendar.MONTH,dto.getMes());
			 fecha.set(Calendar.DAY_OF_MONTH, 1);
			 dto.setFechaDesde(fecha.getTime());
			 
			 fecha.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
			 dto.setFechaHasta(fecha.getTime());
		 }
		  
		  if (dto.getFechaDesde() == null){ 
		  	 Calendar fechaD = Calendar.getInstance();
		  	 fechaD.add(Calendar.DATE, -15);
		  	 dto.setFechaDesde(fechaD.getTime());
		  }
		  
		  if (dto.getFechaHasta() == null){ 
			   	 Calendar fechaH = Calendar.getInstance();
			   	 fechaH.add(Calendar.DATE, +15);
			   	 dto.setFechaHasta(fechaH.getTime());
		  }
		  
		  dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
		  
		  EstadoCuentaProveedorDTO estadoCuenta = new EstadoCuentaProveedorDTO();
		  BigDecimal saldo = new BigDecimal(0);
		  BigDecimal credito = new BigDecimal(0);
		  BigDecimal debito = new BigDecimal(0);
		  
		  estadoCuenta.setFechaDesde(dto.getFechaDesde());
		  estadoCuenta.setFechaHasta(dto.getFechaHasta());
		  estadoCuenta.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
		  
		  
		  EstadoCuentaProveedorDTO retorno=proveedoresDAO.getSaldoAnterior(dto);
		  
		  if (retorno==null) {
			  estadoCuenta.setSaldoAnterior(BigDecimal.ZERO);
			  ProveedoresCompositeDTO proveedor=proveedoresDAO.getProveedoresById(dto.getIdProveedor());
			  estadoCuenta.setProveedor(proveedor.getRazonSocial());;
			  estadoCuenta.setIdProveedor(dto.getIdProveedor());
		  }else {
			  estadoCuenta.setIdProveedor(proveedoresDAO.getSaldoAnterior(dto).getIdProveedor());
			  estadoCuenta.setProveedor(proveedoresDAO.getSaldoAnterior(dto).getProveedor());
			  estadoCuenta.setSaldoAnterior(proveedoresDAO.getSaldoAnterior(dto).getSaldoAnterior());
		  }
		  
		  
		  if (proveedoresDAO.getEstadoCuenta(dto) != null && !proveedoresDAO.getEstadoCuenta(dto).isEmpty()){
			  
			  estadoCuenta.setTransacciones(proveedoresDAO.getEstadoCuenta(dto));
		    
			 if (estadoCuenta.getTransacciones()!=null && !estadoCuenta.getTransacciones().isEmpty()) {
				 for(TransaccionesCompraDTO cp : estadoCuenta.getTransacciones()) {
					  credito=credito.add(cp.getCredito());
					  debito=debito.add(cp.getDebito());
				  } 
				  saldo=credito.subtract(debito);
				 
			 }else {
				 
				 saldo=dto.getSaldoAnterior();
			 }
				  
		  }
		  
		 
		  estadoCuenta.setSaldo(saldo);
		  
		  
		  return estadoCuenta;
		}
		
		
		@Override
		 public FileReportDTO getEstadoCuentaImpresion(EstadoCuentaProveedorDTO dto, InvocationContext ic)
		 		throws Exception {
			 FileReportDTO archivo = new FileReportDTO();
			 if(dto != null){
				 EstadoCuentaProveedorDTO datosRepo = dto;
				 ReportGenerator creador = new ReportGenerator();
					HashMap<String, Object> map = new HashMap<>();
					map.put("fechaDesde", datosRepo.getFechaDesde());
					map.put("fechaHasta", datosRepo.getFechaHasta());
					map.put("proveedor", datosRepo.getProveedor());
					map.put("saldo", datosRepo.getSaldo());
					try {
					SimpleDateFormat xx = new SimpleDateFormat("ddMMyyyy");
					archivo.setArchivo(creador.getByteReports("E", ic, "reporteCompra.jasper", datosRepo.getTransacciones(), map));
					archivo.setTipo(".xls");
					archivo.setNombre("estadoCuenta-"+xx.format(datosRepo.getFechaDesde())+"-"+xx.format(datosRepo.getFechaHasta()));
					}catch(Exception e) {
						log.info(e.getLocalizedMessage());
						throw e;
					}
			 }
			 
			 
		 	return archivo;
		 }
		 




}