package py.com.sgs.web.services.impl;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.GrupoProductosDAO;
import py.com.sgs.persistence.dto.GrupoProductosDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.GrupoProductosService;
public class GrupoProductosServiceImpl implements GrupoProductosService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private GrupoProductosDAO grupoProductosDAO;

  public void setGrupoProductosDAO(GrupoProductosDAO grupoProductosDAO) {
    this.grupoProductosDAO = grupoProductosDAO;
  }

  @Override
  public List<GrupoProductosDTO> listGrupoProductos(InvocationContext ic) throws ServiceException {
    List<GrupoProductosDTO> grupoProductoss =  grupoProductosDAO.listGrupoProductos();

    return grupoProductoss;
  }

  @Override
  public GrupoProductosDTO getGrupoProductos(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    GrupoProductosDTO grupoProductos =  grupoProductosDAO.getGrupoProductosById(id);
    if (grupoProductos == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"grupoproductos.notfound", id);

    return grupoProductos;
  }

  @Override
  public void deleteGrupoProductos(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    GrupoProductosDTO grupoProductos =  grupoProductosDAO.getGrupoProductosById(id);
    if(grupoProductos == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"grupoproductos.notfound", id);

    try {
      grupoProductosDAO.deleteGrupoProductos(grupoProductos);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "grupoproductos.constraint.violation", id);
    }

  }

  @Override
  public GrupoProductosDTO updateGrupoProductos(Long id, GrupoProductosDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    GrupoProductosDTO grupoProductos = grupoProductosDAO.getGrupoProductosById(id);
    if (grupoProductos == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"grupoproductos.notfound", id);

    grupoProductos.setAbreviatura(dto.getAbreviatura());
    grupoProductos.setNombre(dto.getNombre());
    grupoProductos.setIdPadre(dto.getIdPadre());
 
    
      grupoProductosDAO.updateGrupoProductos(grupoProductos);
      return grupoProductos;
    }

    
  

  @Override
  public GrupoProductosDTO createGrupoProductos(GrupoProductosDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {

      grupoProductosDAO.createGrupoProductos(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "grupoproductos.duplicate.key");
    }

    return dto;
  }

}