package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.MovMercDetalleDAO;
import py.com.sgs.persistence.dto.MovMercDetalleDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.MovMercDetalleService;

public class MovMercDetalleServiceImpl implements MovMercDetalleService{

private Logger log = LoggerFactory.getLogger(getClass());  

  @Autowired
  private MovMercDetalleDAO movMercDetalleDAO;

 
  public void setMovMercDetalleDAO(MovMercDetalleDAO movMercDetalleDAO) {
    this.movMercDetalleDAO = movMercDetalleDAO;
  }

  @Override
  public List<MovMercDetalleDTO> listMovMercDetalle(InvocationContext ic) throws ServiceException {
    List<MovMercDetalleDTO> movMercDetalles =  movMercDetalleDAO.listMovMercDetalle();

    return movMercDetalles;
  }

  @Override
  public MovMercDetalleDTO getMovMercDetalle(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    MovMercDetalleDTO movMercDetalle =  movMercDetalleDAO.getMovMercDetalleById(id);
    if (movMercDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"movmercdetalle.notfound", id);

    return movMercDetalle;
  }
  
  @Override
  public List<MovMercDetalleDTO> getMovMercDetByIdMov(Long idMov, InvocationContext ic) throws ServiceException {
	  if (idMov == null || idMov <= 0)
		  throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","idMov");

	  List<MovMercDetalleDTO> movMercDetalle =  movMercDetalleDAO.getMovMercDetByIdMov(idMov);
	  if (movMercDetalle == null)
		  throw new ServiceException(ErrorCode.NO_DATA_FOUND,"movmercdetalle.notfound", idMov);

	  return movMercDetalle;
  }

  @Override
  public void deleteMovMercDetalle(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    MovMercDetalleDTO movMercDetalle =  movMercDetalleDAO.getMovMercDetalleById(id);
    if(movMercDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"movmercdetalle.notfound", id);

    try {
      movMercDetalleDAO.deleteMovMercDetalle(movMercDetalle);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
 

      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "movmercdetalle.constraint.violation", id);
    }

  }

  @Override
  public MovMercDetalleDTO updateMovMercDetalle(Long id, MovMercDetalleDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    MovMercDetalleDTO movMercDetalle = movMercDetalleDAO.getMovMercDetalleById(id);
    if (movMercDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"movmercdetalle.notfound", id);

    movMercDetalle.setIdMovimiento(dto.getIdMovimiento());
    movMercDetalle.setIdProducto(dto.getIdProducto());
    movMercDetalle.setCantidad(dto.getCantidad());
 
   
      movMercDetalleDAO.updateMovMercDetalle(movMercDetalle);
   

    return movMercDetalle;
  }

  @Override
  public MovMercDetalleDTO createMovMercDetalle(MovMercDetalleDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {

      movMercDetalleDAO.createMovMercDetalle(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "movmercdetalle.duplicate.key");
    }

    return dto;
  }

}