package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.BancosDAO;
import py.com.sgs.persistence.dao.ChequesDAO;
import py.com.sgs.persistence.dao.CuentaBancoDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dto.BancosDTO;
import py.com.sgs.persistence.dto.ChequesDTO;
import py.com.sgs.persistence.dto.CuentaBancoDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.util.Utils;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ChequesService;

public class ChequesServiceImpl implements ChequesService{

private Logger log = LoggerFactory.getLogger(getClass());  

  @Autowired
  private ChequesDAO chequesDAO;
  @Override
  public List<ChequesDTO> listCheques(InvocationContext ic) throws ServiceException {
    List<ChequesDTO> cheques =  chequesDAO.listCheques();

    return cheques;
  }

  @Override
  public List<ChequesDTO> listChequesActivos(InvocationContext ic) throws ServiceException {
    List<ChequesDTO> cheques =  chequesDAO.listCheques();

    return cheques;
  }
  
  @Override
  public List<ChequesDTO> listChequesByIdBanco(Integer idBanco, InvocationContext ic) throws ServiceException {
    if (idBanco == null || idBanco <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    List<ChequesDTO> cheques =  chequesDAO.listChequesByIdCuenta (idBanco);
    if (cheques == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cheques.notfound", idBanco);

    return cheques;
  }
  
  @Override
  public List<ChequesDTO> listChequesByIdCuenta(Integer idCuentaBanco, InvocationContext ic) throws ServiceException {
    if (idCuentaBanco == null || idCuentaBanco <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    List<ChequesDTO> cheques =  chequesDAO.listChequesByIdCuenta (idCuentaBanco);
    if (cheques == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cheques.notfound", idCuentaBanco);

    return cheques;
  }
  
  
  @Override
  public ChequesDTO getCheques(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ChequesDTO cheques =  chequesDAO.getChequesById(id);
    if (cheques == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cheques.notfound", id);

    return cheques;
  }

  @Override
  public void deleteCheques(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ChequesDTO cheques =  chequesDAO.getChequesById(id);
    if(cheques == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cheques.notfound", id);

    try {
      chequesDAO.deleteCheques(cheques);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "cheques.constraint.violation", id);
    }

  }

  @Override
  public ChequesDTO updateCheques(Long id, ChequesDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    ChequesDTO cheques = chequesDAO.getChequesById(id);
    if (cheques == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"cheques.notfound", id);
    if (dto.getSerie().trim().isEmpty())
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"cheques.nullserie");
    if (dto.getNumeroInicial().trim().isEmpty())
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"cheques.nullnroinicial");
    if (dto.getNumeroFinal().trim().isEmpty())
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"cheques.nullnrofinal");
    
    String niString = dto.getNumeroInicial().trim();
    int nientero = Integer.parseInt(niString); 
    
    String nfString = dto.getNumeroFinal().trim();
    int nfentero = Integer.parseInt(nfString); 
    
    if (nientero >  nfentero)
    	throw new ServiceException(ErrorCode.VALIDATION_FAILED,"cheques.nrofinal");
     														
    cheques.setIdCuentaBanco(dto.getIdCuentaBanco());
    cheques.setSerie(dto.getSerie());
    cheques.setNumeroInicial(dto.getNumeroInicial());
    cheques.setNumeroFinal(dto.getNumeroFinal());
    cheques.setIdEstado(dto.getIdEstado());
    cheques.setIdTipoCheque(dto.getIdTipoCheque());
    cheques.setUltNumero(dto.getUltNumero());
    cheques.setIdBanco(dto.getIdBanco());
 

      chequesDAO.updateCheques(cheques);

    return cheques;
  }

  @Override
  public ChequesDTO createCheques(ChequesDTO dto, InvocationContext ic) throws ServiceException {
	  
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
    	 
    	 if (dto.getSerie().trim().isEmpty())
    	    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"cheques.nullserie");
    	 if (dto.getNumeroInicial().trim().isEmpty())
    	    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"cheques.nullnroinicial");
    	 if (dto.getNumeroFinal().trim().isEmpty())
    	    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"cheques.nullnrofinal");
    	 
    	 String niString = dto.getNumeroInicial().trim();
    	 int nientero = Integer.parseInt(niString); 
    	    
	     String nfString = dto.getNumeroFinal().trim();
	     int nfentero = Integer.parseInt(nfString); 
	    
	     if (nientero >  nfentero)
	    	throw new ServiceException(ErrorCode.VALIDATION_FAILED,"cheques.nrofinal");
    	 
    	
    	 dto.setSerie(dto.getSerie().trim());
    	 dto.setNumeroInicial(dto.getNumeroInicial().trim());
    	 dto.setNumeroFinal(dto.getNumeroFinal().trim());
    	 
    	 
    	 
      chequesDAO.createCheques(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "cheques.duplicate.key");
    }

    return dto;
  }
  
	public void setChequesDAO(ChequesDAO chequesDAO) {
	    this.chequesDAO = chequesDAO;
	  }


}