package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.ArqueoCajaDAO;
import py.com.sgs.persistence.dto.AperturaCierreDTO;
import py.com.sgs.persistence.dto.ArqueoCajaDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.AperturaCierreService;
import py.com.sgs.web.services.ArqueoCajaService;

public class ArqueoCajaServiceImpl implements ArqueoCajaService{

private Logger log = LoggerFactory.getLogger(getClass());  

   @Autowired
  private ArqueoCajaDAO arqueoCajaDAO;
   

   
   @Autowired
   private AperturaCierreService cierredao;
   
   
  public void setArqueoCajaDAO(ArqueoCajaDAO arqueoCajaDAO) {
    this.arqueoCajaDAO = arqueoCajaDAO;
  }

  @Override
  public List<ArqueoCajaDTO> listArqueoCaja(InvocationContext ic) throws ServiceException {
    List<ArqueoCajaDTO> arqueoCajas =  arqueoCajaDAO.listArqueoCaja();

    return arqueoCajas;
  }

  @Override
  public ArqueoCajaDTO getArqueoCaja(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ArqueoCajaDTO arqueoCaja =  arqueoCajaDAO.getArqueoCajaById(id);
    if (arqueoCaja == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"arqueocaja.notfound", id);

    return arqueoCaja;
  }

  @Override
  public void deleteArqueoCaja(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ArqueoCajaDTO arqueoCaja =  arqueoCajaDAO.getArqueoCajaById(id);
    if(arqueoCaja == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"arqueocaja.notfound", id);

    try {
      arqueoCajaDAO.deleteArqueoCaja(arqueoCaja);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "arqueocaja.constraint.violation", id);
    }

  }

  @Override
  public ArqueoCajaDTO updateArqueoCaja(Long id, ArqueoCajaDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    ArqueoCajaDTO arqueoCaja = arqueoCajaDAO.getArqueoCajaById(id);
    if (arqueoCaja == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"arqueocaja.notfound", id);

    arqueoCaja.setIdAperturaCierre(dto.getIdAperturaCierre());
    arqueoCaja.setSaldoAlCierre(dto.getSaldoAlCierre());
    arqueoCaja.setTotalEntregado(dto.getTotalEntregado());
    arqueoCaja.setDiferencias(dto.getDiferencias());
    arqueoCaja.setFaltanteSobrante(dto.getFaltanteSobrante());
 
    arqueoCajaDAO.updateArqueoCaja(arqueoCaja);

    return arqueoCaja;
  }

  @Override
  public ArqueoCajaDTO createArqueoCaja(ArqueoCajaDTO dto, InvocationContext ic) throws ServiceException {
    
	if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
    	
      AperturaCierreDTO apertura= new AperturaCierreDTO();
      
      //apertura.setSaldoCierre(dto.getTotalEntregado());
      apertura.setId(dto.getIdAperturaCierre());
      apertura.setSaldoCierre(dto.getTotalEntregado());
      //aperturaCierreDAO.cerrarAperturaCierre(apertura);
      //voy a sacar de aca   cierredao.cerrarAperturaCierre(apertura.getId(),apertura, ic); 
      //y llevar a entrega usandole como composite
     
      arqueoCajaDAO.createArqueoCaja(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "arqueocaja.duplicate.key");
    }

    return dto;
  }

}