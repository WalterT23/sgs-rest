package py.com.sgs.web.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.support.DaoSupport;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.ListaPreciosDAO;
import py.com.sgs.persistence.dao.ListaPreciosDetalleDAO;
import py.com.sgs.persistence.dao.StockDAO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.ListaPreciosDTO;
import py.com.sgs.persistence.dto.ListaPreciosDetalleDTO;
import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.composite.ClientesCompositeDTO;
import py.com.sgs.persistence.dto.composite.ListaPreciosCompositeDTO;
import py.com.sgs.persistence.dto.composite.StockCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ListaPreciosService;

public class ListaPreciosServiceImpl implements ListaPreciosService{

private Logger log = LoggerFactory.getLogger(getClass());  
  @Autowired
  private ListaPreciosDAO listaPreciosDAO;
  @Autowired
  private StockDAO stockDAO;
  @Autowired
  private ListaPreciosDetalleDAO listaPreciosDetalleDAO;
  @Autowired
	private InfoRefOpcDAO infoRefDAO;
  
  public void setListaPreciosDAO(ListaPreciosDAO listaPreciosDAO) {
    this.listaPreciosDAO = listaPreciosDAO;
  }

  @Override
  public List<ListaPreciosCompositeDTO> listListaPrecios(InvocationContext ic) throws ServiceException {
    
	List<ListaPreciosCompositeDTO> listaPrecios =  listaPreciosDAO.listListaPrecios(Long.parseLong(ic.getUserSucursal()));
	if (listaPrecios == null)
		 throw new ServiceException(ErrorCode.NO_DATA_FOUND,"none.listaprecios");
	
	for (ListaPreciosCompositeDTO lp: listaPrecios){
		lp.setLpIncompleta(false);
		for (ListaPreciosDetalleDTO lpd: lp.getDetalles()){
			if (lpd.getPrecioVenta() == null){
				lp.setLpIncompleta(true);
			}	
		}
	}
		
    return listaPrecios;
  }
  
  @Override
  public List<ListaPreciosCompositeDTO> listaPreciosVigentes(InvocationContext ic) throws ServiceException {
    
	List<ListaPreciosCompositeDTO> listaPrecios =  listaPreciosDAO.listaPreciosVigentes(Long.parseLong(ic.getUserSucursal()));
	
	if (listaPrecios == null)
		 throw new ServiceException(ErrorCode.NO_DATA_FOUND,"none.listaprecios");
	
    return listaPrecios;
  }

  @Override
  public ListaPreciosCompositeDTO getListaPrecios(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");
    
    ListaPreciosCompositeDTO listaPrecios =  listaPreciosDAO.getListaPreciosById(id);
    if (listaPrecios == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"listaprecios.notfound", id);

    return listaPrecios;
  }

  @Override
  public void deleteListaPrecios(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ListaPreciosDTO listaPrecios =  listaPreciosDAO.getListaPreciosById(id);
    if(listaPrecios == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"listaprecios.notfound", id);

    try {
      listaPreciosDAO.deleteListaPrecios(listaPrecios);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "listaprecios.constraint.violation", id);
    }

  }

  @Override
  public ListaPreciosDTO updateListaPrecios(Long id, ListaPreciosDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    ListaPreciosCompositeDTO listaPrecios = listaPreciosDAO.getListaPreciosById(id);
    if (listaPrecios == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"listaprecios.notfound", id);
    
    if (dto.getIdTipoLista() != null){
    	listaPrecios.setIdTipoLista(dto.getIdTipoLista());
    	InfoRefOpcDTO indListaBase = infoRefDAO.getInfoRefOpcByAbv("LPBASE");
    	if (dto.getIdTipoLista() == indListaBase.getId())
    		listaPrecios.setListaBase("S");
    }
    if (dto.getInicioVigencia() != null)
    	listaPrecios.setInicioVigencia(dto.getInicioVigencia());
    
    if (dto.getFinVigencia() != null)
    	listaPrecios.setFinVigencia(dto.getFinVigencia());
    
    if (listaPrecios.getIdSucursal() != (Long.parseLong(ic.getUserSucursal())))
    	throw new ServiceException(ErrorCode.LISTA_PRECIOS_ERROR,"invalid_lp","id");
    
		
    ListaPreciosDTO lpbaseexist = listaPreciosDAO.validarNewLPbase(dto);
	if (lpbaseexist != null) 
		throw new ServiceException(ErrorCode.LISTA_PRECIOS_ERROR,"lista_base_already_exist"); 
	
	
	if (dto.getCostoBase() != null) {
		StockDTO s = new StockDTO();
		s.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
		
		for (ListaPreciosDetalleDTO lpd: listaPrecios.getDetalles()){
			
			if (lpd.getCostoBase() == null){
				
				s.setIdProducto(lpd.getIdProducto());
				StockCompositeDTO stock = stockDAO.stockByIdProducto(s);

				if(dto.getCostoBase().equals("PP") && stock.getPromPonderado() != null){
					lpd.setCostoBase(stock.getPromPonderado());
				}else if(dto.getCostoBase().equals("UC") && stock.getUltCostoCompra() != null){
					lpd.setCostoBase(stock.getUltCostoCompra());
				}

				listaPreciosDetalleDAO.updateListaPreciosDetalle(lpd);
			}
		}
	}
	
	if (dto.getCalcPrecioVenta()) {
	    InfoRefOpcDTO tipoRecarga = infoRefDAO.getInfoRefOpcByAbv("CVPOR");
	    StockDTO s = new StockDTO();
	    s.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
	    
	    for (ListaPreciosDetalleDTO lpde: listaPrecios.getDetalles()){
	    	
	    	if (lpde.getPrecioVenta() == null ){
		    	s.setIdProducto(lpde.getIdProducto());
		    	StockCompositeDTO stock = stockDAO.stockByIdProducto(s);
		    	
	    		if (lpde.getCostoBase() == null && stock.getUltCostoCompra() != null){
	    			lpde.setCostoBase(stock.getUltCostoCompra());
	    		} 
	    		
	    		if (lpde.getCostoBase() != null) {
	    			lpde.setIdTipoRecarga(tipoRecarga.getId());
	    			lpde.setMontoRecarga(stock.getProductoDTO().getPorcRecarga());
	    			lpde.setPrecioVenta(lpde.getCostoBase().add((lpde.getCostoBase().multiply(lpde.getMontoRecarga())).divide(new BigDecimal(100),RoundingMode.HALF_UP)));
	    		}
	    	} 
	    	
	    	listaPreciosDetalleDAO.updateListaPreciosDetalle(lpde);
	    }
	}
	
    listaPrecios.setFechaUltimaActualizacion(new Date(System.currentTimeMillis()));
    listaPrecios.setUsuarioUltActualizacion(dto.getUsuarioUltActualizacion());
    
    listaPreciosDAO.updateListaPrecios(listaPrecios);

    return listaPrecios;
  }

  @Override
  public ListaPreciosDTO createListaPrecios(ListaPreciosDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
    
    Calendar fecha = Calendar.getInstance();
    
    if (dto.getInicioVigencia() == null)
		dto.setInicioVigencia(fecha.getTime());
	
	if (dto.getFinVigencia() == null) {
		Calendar fechaF = Calendar.getInstance();
		fechaF.set(Calendar.YEAR, 3000);
		dto.setFinVigencia(fechaF.getTime());
	}
	
	if (dto.getInicioVigencia().before(fecha.getTime()))
		throw new ServiceException(ErrorCode.PARAMETER_ERROR,"lista.precios.fecha.inicio");
	
	
	dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
	
	
	InfoRefOpcDTO indListaBase = infoRefDAO.getInfoRefOpcByAbv("LPBASE");
	if (dto.getIdTipoLista() == indListaBase.getId()){
		dto.setListaBase("S");
	}else {
		dto.setListaBase("N");
	}
	
	ListaPreciosDTO lpexist = listaPreciosDAO.validarNewLPbase(dto);
	if (lpexist != null) 
		throw new ServiceException(ErrorCode.LISTA_PRECIOS_ERROR,"tipo.lista.already.exist");
	
    dto.setUsuarioUltActualizacion(ic.getUserPrincipal());
    dto.setFechaUltimaActualizacion(new Date(System.currentTimeMillis()));

    try {
    	
    	listaPreciosDAO.createListaPrecios(dto);
    	
    	List<StockCompositeDTO> listaProductos = stockDAO.listStock(Long.parseLong(ic.getUserSucursal()));
    	
    	InfoRefOpcDTO tipoRecarga = infoRefDAO.getInfoRefOpcByAbv("CVPOR");
    	
    	for (StockCompositeDTO lpd: listaProductos){
    		
    		ListaPreciosDetalleDTO detalle = new ListaPreciosDetalleDTO();
    		detalle.setIdListaPrecios(dto.getId());
    		detalle.setIdProducto(lpd.getIdProducto());
    		if (dto.getCostoBase() != null){
	    		if(dto.getCostoBase().equals("PP") && lpd.getPromPonderado() != null){
	    			detalle.setCostoBase(lpd.getPromPonderado());
	    		}else if(dto.getCostoBase().equals("UC") && lpd.getUltCostoCompra() != null){
	    			detalle.setCostoBase(lpd.getUltCostoCompra());
	    		}
    		}
    		if(dto.getCalcPrecioVenta() != null && dto.getCalcPrecioVenta() && detalle.getCostoBase() != null){
    			detalle.setIdTipoRecarga(tipoRecarga.getId());
    			detalle.setMontoRecarga(lpd.getProductoDTO().getPorcRecarga());
    			detalle.setPrecioVenta(detalle.getCostoBase().add((detalle.getCostoBase().multiply(detalle.getMontoRecarga())).divide(new BigDecimal(100),RoundingMode.HALF_UP)));
    		}
    		
    		listaPreciosDetalleDAO.createListaPreciosDetalle(detalle);
    	}
    	
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "listaprecios.duplicate.key");
    }

    return dto;
  }

}