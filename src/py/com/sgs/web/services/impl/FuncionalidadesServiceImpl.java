package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.FuncionalidadesDAO;
import py.com.sgs.persistence.dto.FuncionalidadesDTO;
import py.com.sgs.persistence.dto.RolFuncionalidadDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.FuncionalidadesService;

public class FuncionalidadesServiceImpl implements FuncionalidadesService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private FuncionalidadesDAO funcionalidadesDAO;


  public void setFuncionalidadesDAO(FuncionalidadesDAO funcionalidadesDAO) {
    this.funcionalidadesDAO = funcionalidadesDAO;
  }

  @Override
  public List<FuncionalidadesDTO> listFuncionalidades(InvocationContext ic) throws ServiceException {
    List<FuncionalidadesDTO> funcionalidadess =  funcionalidadesDAO.listFuncionalidades();

    return funcionalidadess;
  }

  @Override
  public FuncionalidadesDTO getFuncionalidades(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    FuncionalidadesDTO funcionalidades =  funcionalidadesDAO.getFuncionalidadesById(id);
    if (funcionalidades == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"funcionalidades.notfound", id);

    return funcionalidades;
  }

  @Override
  public void deleteFuncionalidades(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    FuncionalidadesDTO funcionalidades =  funcionalidadesDAO.getFuncionalidadesById(id);
    if(funcionalidades == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"funcionalidades.notfound", id);

    try {
      funcionalidadesDAO.deleteFuncionalidades(funcionalidades);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "funcionalidades.constraint.violation", id);
    }

  }

  @Override
  public FuncionalidadesDTO updateFuncionalidades(Long id, FuncionalidadesDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    FuncionalidadesDTO funcionalidades = funcionalidadesDAO.getFuncionalidadesById(id);
    if (funcionalidades == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"funcionalidades.notfound", id);

    funcionalidades.setFuncionalidad(dto.getFuncionalidad());
 

    funcionalidadesDAO.updateFuncionalidades(funcionalidades);


    return funcionalidades;
  }

  @Override
  public FuncionalidadesDTO createFuncionalidades(FuncionalidadesDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {

      funcionalidadesDAO.createFuncionalidades(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "funcionalidades.duplicate.key");
    }

    return dto;
  }

  @Override
  public RolFuncionalidadDTO createRolFuncionalidad(RolFuncionalidadDTO dto, InvocationContext ic) throws ServiceException {
	  if (dto == null)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

	    try {

	    	funcionalidadesDAO.createRolFuncionalidad(dto);
	  }
	  catch (DuplicateKeyException e){
	      log.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "funcionalidades.duplicate.key");
	  }
	  return dto;
  }

  @Override
  public void deleteRolFuncionalidad(RolFuncionalidadDTO dto, InvocationContext ic) {
  	if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","idFuncionalidad");

    try {
    	funcionalidadesDAO.deleteRolFuncionalidad(dto);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "funcionalidades.constraint.violation");
    }

	
  }

}