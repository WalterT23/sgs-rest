package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.InfoRefOpcService;

public class InfoRefOpcServiceImpl implements InfoRefOpcService {
  private Logger log = LoggerFactory.getLogger(getClass());
  private InfoRefOpcDAO infoRefOpcDAO;
  
  public void setInfoRefOpcDAO(InfoRefOpcDAO infoRefOpcDAO) {
    this.infoRefOpcDAO = infoRefOpcDAO;
  }

  @Override
  public List<InfoRefOpcDTO> listInfoRefOpc(InvocationContext ic) throws ServiceException {
    List<InfoRefOpcDTO> retorno = infoRefOpcDAO.listInfoRefOpc();
    
    return retorno;
  }

  @Override
  public List<InfoRefOpcDTO> getInfoRefOpcByIdRef(Integer idInfoRef, InvocationContext ic) {
	  if (idInfoRef == null || idInfoRef<= 0) {
		  throw new ServiceException(ErrorCode.PARAMETER_ERROR, "null.param");
	  }
	  
	  List<InfoRefOpcDTO> listaRetorno = infoRefOpcDAO.getInfoRefOpcByIdRef(idInfoRef);
	  
	  if (listaRetorno == null){
		  throw new ServiceException(ErrorCode.NO_DATA_FOUND, "inforefopc.not.found", idInfoRef);
	  }
	  
	  return listaRetorno;
  }
  
  @Override
  public InfoRefOpcDTO getInfoRefOpcById(Long idInfoRef, InvocationContext ic) {
    if (idInfoRef == null || idInfoRef <= 0) {
      throw new ServiceException(ErrorCode.PARAMETER_ERROR, "null.param");
    }
    InfoRefOpcDTO result = infoRefOpcDAO.getInfoRefOpcById(idInfoRef);
    
    if (result == null){
      throw new ServiceException(ErrorCode.NO_DATA_FOUND, "inforefopc.not.found", idInfoRef);
    }
    
    return result;
  }

  @Override
  public List<InfoRefOpcDTO> getInfoRefOpcByCodRef(String codInfoRef, InvocationContext ic) {
  	if (codInfoRef.isEmpty()) {
  		throw new ServiceException(ErrorCode.PARAMETER_ERROR, "null.param");
  	}
  	
  	List<InfoRefOpcDTO> result = infoRefOpcDAO.getInfoRefOpcByCodRef(codInfoRef);
  	
  	if (result == null) {
  		throw new ServiceException(ErrorCode.NO_DATA_FOUND, "inforefopc.not.found", codInfoRef);
  	}
  	
  	return result;
  }

  @Override
  public InfoRefOpcDTO getInfoRefOpcByAbv(String abv, InvocationContext ic) {
  	if (abv.isEmpty())
  		throw new ServiceException(ErrorCode.PARAMETER_ERROR, "null.param");
  	
  	InfoRefOpcDTO result = infoRefOpcDAO.getInfoRefOpcByAbv(abv);
  	
  	return result;
  }
  
  @Override
  public InfoRefOpcDTO createInfoRefOpc(InfoRefOpcDTO dto, InvocationContext ic) {
	  if (dto == null)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

	    try {

	      infoRefOpcDAO.createInfoRefOpc(dto);
	      
	    }
	    catch (DuplicateKeyException e){
	      log.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "inforefopc.duplicate.key");
	    }

	    return dto;
    
  }

  @Override
  public InfoRefOpcDTO updateInfoRefOpc(Long id, InfoRefOpcDTO dto, InvocationContext ic) throws ServiceException {
	  if (id == null || id <= 0)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

	    if (null == dto)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

	    InfoRefOpcDTO referencia = infoRefOpcDAO.getInfoRefOpcById(id);
	    
	    if (referencia == null)
	      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"inforefopc.notfound", id);
	    
	    if (dto.getIdInfoRef()!= null)
	    	referencia.setIdInfoRef(dto.getIdInfoRef());
	    
	    if (dto.getDescripcion() != null)
	    	referencia.setDescripcion(dto.getDescripcion());

  	return referencia;
  }
  
  @Override
  public void deleteInfoRefOpc(Long id, InvocationContext ic) {
   
	  if (id == null || id == 0) 
      throw new ServiceException(ErrorCode.PARAMETER_ERROR, "parameter.error.null","id");
    
      InfoRefOpcDTO dto = infoRefOpcDAO.getInfoRefOpcById(id);
      
      if (dto == null)
    	  throw new ServiceException(ErrorCode.NO_DATA_FOUND,"inforefopc.notfound",id);
      
      try {
    	  infoRefOpcDAO.deleteInfoRefOpc(dto);
      }
      catch (DataIntegrityViolationException e){
	      log.error(e.getMessage(), e);
	      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "inforefopc.constraint.violation", id);
	  }
  }

  
}
