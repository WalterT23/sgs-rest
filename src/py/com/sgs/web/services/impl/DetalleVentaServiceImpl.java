package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.DetalleVentaDAO;
import py.com.sgs.persistence.dto.DetalleVentaDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.DetalleVentaService;

public class DetalleVentaServiceImpl implements DetalleVentaService{

	private Logger log = LoggerFactory.getLogger(getClass());  @Autowired

	private DetalleVentaDAO detalleVentaDAO;

	public void setDetalleVentaDAO(DetalleVentaDAO detalleVentaDAO) {
		this.detalleVentaDAO = detalleVentaDAO;
	}

	@Override
	public List<DetalleVentaDTO> listDetalleVenta(InvocationContext ic) throws ServiceException {
		List<DetalleVentaDTO> detalleVentas =  detalleVentaDAO.listDetalleVenta();

		return detalleVentas;
	}

	@Override
	public DetalleVentaDTO getDetalleVenta(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		DetalleVentaDTO detalleVenta =  detalleVentaDAO.getDetalleVentaById(id);
		if (detalleVenta == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detalleventa.notfound", id);

		return detalleVenta;
	}

	@Override
	public void deleteDetalleVenta(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		DetalleVentaDTO detalleVenta =  detalleVentaDAO.getDetalleVentaById(id);
		if(detalleVenta == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detalleventa.notfound", id);

		try {
			detalleVentaDAO.deleteDetalleVenta(detalleVenta);
		}
		catch (DataIntegrityViolationException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "detalleventa.constraint.violation", id);
		}

	}

	@Override
	public DetalleVentaDTO updateDetalleVenta(Long id, DetalleVentaDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		DetalleVentaDTO detalleVenta = detalleVentaDAO.getDetalleVentaById(id);
		if (detalleVenta == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detalleventa.notfound", id);

		detalleVenta.setIdCabVenta(dto.getIdCabVenta());
		detalleVenta.setIdProducto(dto.getIdProducto());
		detalleVenta.setCantidad(dto.getCantidad());
		detalleVenta.setPrecioUnitario(dto.getPrecioUnitario());
		detalleVenta.setPrecioTotal(dto.getPrecioTotal());
		detalleVenta.setTipoIdentificadorFiscal(dto.getTipoIdentificadorFiscal());
		detalleVenta.setPorcentajeDescuento(dto.getPorcentajeDescuento());
		detalleVenta.setMontoDescuento(dto.getMontoDescuento());
		
		detalleVentaDAO.updateDetalleVenta(detalleVenta);

		return detalleVenta;
	}

	@Override
	public DetalleVentaDTO createDetalleVenta(DetalleVentaDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {
			detalleVentaDAO.createDetalleVenta(dto);
		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "detalleventa.duplicate.key");
		}

		return dto;
	}

}