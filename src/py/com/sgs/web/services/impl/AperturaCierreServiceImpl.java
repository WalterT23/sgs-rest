package py.com.sgs.web.services.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.AperturaCierreDAO;
import py.com.sgs.persistence.dao.CajaUserDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dto.AperturaCierreDTO;
import py.com.sgs.persistence.dto.CajaUserDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.MovimientoRecaudacionDTO;
import py.com.sgs.persistence.dto.composite.AperturaCierreCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.AperturaCierreService;
import py.com.sgs.web.services.MovimientoRecaudacionService;

public class AperturaCierreServiceImpl implements AperturaCierreService{

	private Logger log = LoggerFactory.getLogger(getClass());  
	@Autowired
	private AperturaCierreDAO aperturaCierreDAO;
	
	
	@Autowired
	private CajaUserDAO cajaUserDAO;
	
	@Autowired
	private InfoRefOpcDAO infoRefDAO;
	

	
	@Autowired
	private MovimientoRecaudacionService movidao;
	
	

	public void setInfoRefDAO(InfoRefOpcDAO infoRefDAO) {
		this.infoRefDAO = infoRefDAO;
	}

	public void setCajaUserDAO(CajaUserDAO cajaUserDAO) {
		this.cajaUserDAO = cajaUserDAO;
	}

	public void setAperturaCierreDAO(AperturaCierreDAO aperturaCierreDAO) {
		this.aperturaCierreDAO = aperturaCierreDAO;
	}

	@Override
	public List<AperturaCierreDTO> listAperturaCierre(InvocationContext ic) throws ServiceException {
		List<AperturaCierreDTO> aperturaCierres =  aperturaCierreDAO.listAperturaCierre();

		return aperturaCierres;
	}

	@Override
	public AperturaCierreCompositeDTO getAperturaCierre(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		AperturaCierreCompositeDTO aperturaCierre =  aperturaCierreDAO.getAperturaCierreById(id);
		if (aperturaCierre == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"aperturacierre.notfound", id);

		return aperturaCierre;
	}
	
	@Override
	public AperturaCierreCompositeDTO getAperturaCierreVigenteByIdPuntoExp(Long idPuntoExpedicion, InvocationContext ic) throws ServiceException {
		if (idPuntoExpedicion== null || idPuntoExpedicion <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","idPuntoExpedicion");

		AperturaCierreCompositeDTO aperturaCierre =  aperturaCierreDAO.getAperturaCierreVigenteByIdPuntoExp(idPuntoExpedicion);
		if (aperturaCierre == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"aperturacierre.notfound", idPuntoExpedicion);

		return aperturaCierre;
	}


	@Override
	public void deleteAperturaCierre(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		AperturaCierreDTO aperturaCierre =  aperturaCierreDAO.getAperturaCierreById(id);
		if(aperturaCierre == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"aperturacierre.notfound", id);

		try {
			aperturaCierreDAO.deleteAperturaCierre(aperturaCierre);
		}
		catch (DataIntegrityViolationException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "aperturacierre.constraint.violation", id);
		}

	}

	@Override
	public AperturaCierreDTO updateAperturaCierre(Long id, AperturaCierreDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		AperturaCierreDTO aperturaCierre = aperturaCierreDAO.getAperturaCierreById(id);
		if (aperturaCierre == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"aperturacierre.notfound", id);

		aperturaCierre.setIdPuntoExpedicion(dto.getIdPuntoExpedicion());
		aperturaCierre.setIdUsuarioCaja(dto.getIdUsuarioCaja());;
		aperturaCierre.setFechaApertura(dto.getFechaApertura());
		aperturaCierre.setSaldoInicial(dto.getSaldoInicial());
		aperturaCierre.setSaldoCierre(dto.getSaldoCierre());
		aperturaCierre.setFechaCierre(dto.getFechaCierre());

		aperturaCierreDAO.updateAperturaCierre(aperturaCierre);

		return aperturaCierre;
	}

	@Override
	public AperturaCierreDTO createAperturaCierre(AperturaCierreDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {

			/*//cuando se va a crear un registro en apertura_cierre se debe verificar que no exista un registro vigente activo para la fecha y el punto de expedicion
			//se considera registro activo a uno en el que se tiene fecha de apertura y no fecha de cierre
			AperturaCierreDTO apVig = aperturaCierreDAO.getAperturaCierreVigenteByIdPuntoExp(dto.getIdPuntoExpedicion());
			if (apVig != null)
				throw new ServiceException(ErrorCode.APERTURA_CIERRE_ALREADY_EXIST,"apertura.cierre.existe.vigente",dto.getIdPuntoExpedicion());

			
			Date fecha =new Date(System.currentTimeMillis());
			dto.setUsuarioCreacion(ic.getUserPrincipal());
			dto.setFechaApertura(fecha);
			aperturaCierreDAO.createAperturaCierre(dto);
			
			
			CajaUserDTO toUpdate = null;
			toUpdate = cajaUserDAO.getCajaUserByIdPtoExp(dto.getIdPuntoExpedicion());
			if (toUpdate == null)
					throw new ServiceException(ErrorCode.NO_DATA_FOUND,"punto.expedicion.not.found", dto.getIdPuntoExpedicion());
						
					toUpdate.setInicio(fecha);
					toUpdate.setIdPuntoExpedicion(dto.getIdPuntoExpedicion());
					toUpdate.setIdUsuario(dto.getIdPuntoExpedicion());

			
			cajaUserDAO.updateCajaUser(toUpdate);

			IngresosEgresosCajaDTO dtoIni = new IngresosEgresosCajaDTO();
			InfoRefOpcDTO tipoMov = infoRefDAO.getInfoRefOpcByAbv("ING");
			InfoRefOpcDTO Mov = infoRefDAO.getInfoRefOpcByAbv("APT");
			dtoIni.setIdMovimiento(Mov.getId());
			dtoIni.setIdTipoMovimiento(tipoMov.getId());
			dtoIni.setFechaHora(fecha);
			dtoIni.setMonto(dto.getSaldoInicial());
			dtoIni.setIdAperturaCierre(dto.getId());
			ingresosEgresosCajaDAO.createIngresosEgresosCaja(dtoIni);*/
			
		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "aperturacierre.duplicate.key");
		}

		return dto;
	}
	
	@Override
	public AperturaCierreDTO generarAperturaCierre(AperturaCierreDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {

			//cuando se va a crear un registro en apertura_cierre se debe verificar que no exista un registro vigente activo para la fecha y el punto de expedicion
			//se considera registro activo a uno en el que se tiene fecha de apertura y no fecha de cierre
			AperturaCierreDTO apVig = aperturaCierreDAO.getAperturaCierreVigenteByIdPuntoExp(dto.getIdPuntoExpedicion());
			if (apVig != null)
				throw new ServiceException(ErrorCode.APERTURA_CIERRE_ALREADY_EXIST,"apertura.cierre.existe.vigente",dto.getIdPuntoExpedicion());
			
			//se debe verificar que el cajero no posea una apertura sin cierre
			AperturaCierreDTO apCaj = aperturaCierreDAO.getAperturaCierreVigenteByCajero(dto.getIdUsuarioCaja());
			if(apCaj != null)
				throw new ServiceException(ErrorCode.CAJERO_APERTURA_ACTIVA,"apertura.cierre.cajero.activo", dto.getIdUsuarioCaja());
			
			Date fecha =new Date(System.currentTimeMillis());
			dto.setUsuarioCreacion(ic.getUserPrincipal());
			dto.setFechaApertura(fecha);
			aperturaCierreDAO.createAperturaCierre(dto);

			InfoRefOpcDTO tipoMov = infoRefDAO.getInfoRefOpcByAbv("EGR");
			InfoRefOpcDTO movi = infoRefDAO.getInfoRefOpcByAbv("APT");
			
			MovimientoRecaudacionDTO mov= new MovimientoRecaudacionDTO();
			mov.setFechaHora(fecha);
			mov.setIdAperturaCierre(dto.getId());
			mov.setIdMovimiento(movi.getId());
			mov.setIdTipoMovimiento(tipoMov.getId());
			mov.setMonto(dto.getSaldoInicial());
			
			movidao.createMovimientoRecaudacion(mov, ic);
			
			
			Long idexp=dto.getIdPuntoExpedicion();
			CajaUserDTO cajaDTO=cajaUserDAO.getCajaUserByIdPtoExp(idexp);
			cajaDTO.setInicio(fecha);
			
			cajaUserDAO.updateCajaUser(cajaDTO);
			
			
		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "aperturacierre.duplicate.key");
		}

		return dto;
	}
	
	@Override
	public AperturaCierreDTO cerrarAperturaCierre(Long id, AperturaCierreDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		AperturaCierreDTO aperturaCierre = aperturaCierreDAO.getAperturaCierreById(id);
		if (aperturaCierre == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"aperturacierre.notfound", id);
		/*se modifica 11/05 se recibe como saldo cierre lo entregado en caja por el cajero
		//para cerrar una apertura se carga la fecha de cierre y se debe obtener el saldo al cierre
		//el saldo al cierre se obtiene sumando los registros en cabecera venta que posean el id de apertura cierre

		*/
		Date fecha =new Date(System.currentTimeMillis());
		aperturaCierre.setFechaCierre(fecha);
		aperturaCierre.setUsuarioCierre(ic.getUserPrincipal());
		aperturaCierre.setSaldoCierre(dto.getSaldoCierre());
		
		
		Long idexp=aperturaCierre.getIdPuntoExpedicion();
		CajaUserDTO cajaDTO=cajaUserDAO.getCajaUserByIdPtoExp(idexp);
		cajaDTO.setFin(fecha);
		
		
	
		
		
		
		aperturaCierreDAO.cerrarAperturaCierre(aperturaCierre);
		cajaUserDAO.updateCajaUser(cajaDTO);
		
		
		/*registra el movimiento de cierre a recaudacion*/
		InfoRefOpcDTO tipoMov = infoRefDAO.getInfoRefOpcByAbv("ING");
		InfoRefOpcDTO movi = infoRefDAO.getInfoRefOpcByAbv("CIE");
		
		MovimientoRecaudacionDTO movidto= new MovimientoRecaudacionDTO();
	    movidto.setFechaHora(fecha);
	    movidto.setIdAperturaCierre(dto.getId());
	    movidto.setIdMovimiento(movi.getId());
	    movidto.setIdTipoMovimiento(tipoMov.getId());
	    movidto.setMonto(dto.getSaldoCierre());
	    
	    movidao.createMovimientoRecaudacion(movidto, ic);
		
		return aperturaCierre;
	}

}