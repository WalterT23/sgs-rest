package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.support.DaoSupport;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.SucursalDAO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.SucursalDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.SucursalService;

public class SucursalServiceImpl implements SucursalService{

  private Logger log = LoggerFactory.getLogger(getClass());  
  @Autowired
  private SucursalDAO sucursalDAO;
  @Autowired
  private InfoRefOpcDAO infoRefDAO;


  public void setSucursalDAO(SucursalDAO sucursalDAO) {
    this.sucursalDAO = sucursalDAO;
  }

  @Override
  public List<SucursalDTO> listSucursal(InvocationContext ic) throws ServiceException {
    List<SucursalDTO> sucursals =  sucursalDAO.listSucursal();

    return sucursals;
  }

  @Override
  public SucursalDTO getSucursal(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    SucursalDTO sucursal =  sucursalDAO.getSucursalById(id);
    if (sucursal == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"sucursal.notfound", id);

    return sucursal;
  }
  
  @Override
  public List<SucursalDTO> sucDispByUsuario(Long id, InvocationContext ic) throws ServiceException {
	  List<SucursalDTO> sucursales = sucursalDAO.sucDispByUsuario(id);
	  
  	return sucursales;
  }

  @Override
  public void deleteSucursal(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    SucursalDTO sucursal =  sucursalDAO.getSucursalById(id);
    if(sucursal == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"sucursal.notfound", id);

    try {
      sucursalDAO.deleteSucursal(sucursal);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "sucursal.constraint.violation", id);
    }

  }

  @Override
  public SucursalDTO updateSucursal(Long id, SucursalDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    SucursalDTO sucursal = sucursalDAO.getSucursalById(id);
    
    if (sucursal == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"sucursal.not.found", id);
    
    
    
    if (dto.getCodigoSucursal() != null) {
    	dto.setId(id);
    	String checkCodSuc = sucursalDAO.checkSucursal(dto);
    	if (checkCodSuc != null)
    		throw new ServiceException(ErrorCode.CODIGO_YA_EXISTE,"cod.already.exist", dto.getCodigoSucursal());
    	if (checkCodSuc == null)
    		sucursal.setCodigoSucursal(dto.getCodigoSucursal());
    }
    
    if (dto.getDescripcion() != null)
    	sucursal.setDescripcion(dto.getDescripcion());
    
    if (dto.getDireccion() != null)
    	sucursal.setDireccion(dto.getDireccion());
    
    if (dto.getTelefono() != null)
    	sucursal.setTelefono(dto.getTelefono());
    
    if (dto.getIdEstado() != null)
    	sucursal.setIdEstado(dto.getIdEstado());
 
    
    sucursalDAO.updateSucursal(sucursal);
   

    return sucursal;
  }

  @Override
  public SucursalDTO createSucursal(SucursalDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
    
    try {
    	
    	Long codSucursal = sucursalDAO.getCodSucursal();
		if (codSucursal == null){
			codSucursal = 1L;
		}else {
			codSucursal = codSucursal + 1L;
		}
		
		dto.setCodigoSucursal(String.format("%03d", codSucursal));
		
		InfoRefOpcDTO estConf = infoRefDAO.getInfoRefOpcByAbv("ESTAC");
		dto.setIdEstado(estConf.getId());
		
		sucursalDAO.createSucursal(dto);
    
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "sucursal.duplicate.key");
    }

    return dto;
  }

}