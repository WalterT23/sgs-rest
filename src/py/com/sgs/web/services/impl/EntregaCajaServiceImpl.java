package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.AperturaCierreDAO;
import py.com.sgs.persistence.dao.ArqueoCajaDAO;
import py.com.sgs.persistence.dao.EntregaCajaDAO;
import py.com.sgs.persistence.dao.NotaCreditoVentaDAO;
import py.com.sgs.persistence.dto.ArqueoCajaDTO;
import py.com.sgs.persistence.dto.DetalleCierreDTO;
import py.com.sgs.persistence.dto.EntregaCajaDTO;
import py.com.sgs.persistence.dto.FalloCajaDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.composite.AperturaCierreCompositeDTO;
import py.com.sgs.persistence.dto.composite.EntregaCajaCompositeDTO;
import py.com.sgs.util.report.ReportComprobanteMovimiento;
import py.com.sgs.util.report.ReportEntregaCaja;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.AperturaCierreService;
import py.com.sgs.web.services.ArqueoCajaService;
import py.com.sgs.web.services.DetalleCierreService;
import py.com.sgs.web.services.EntregaCajaService;
import py.com.sgs.web.services.FalloCajaService;

public class EntregaCajaServiceImpl implements EntregaCajaService{

	private Logger log = LoggerFactory.getLogger(getClass()); 
	
	@Autowired
	private EntregaCajaDAO entregaCajaDAO;
	
	@Autowired
	private AperturaCierreDAO aperturaCierreDAO;
	
	
	@Autowired
	private NotaCreditoVentaDAO notaCreditoVentaDAO;
	
	@Autowired
	private ArqueoCajaDAO arqueoCajaDAO;
	
	@Autowired
	private ArqueoCajaService arqueodao;
	
	@Autowired
	private EntregaCajaService entreservice;
	
   @Autowired
   private AperturaCierreService cierredao;
   
   @Autowired
   private FalloCajaService fallodao;
   
   
	@Autowired 
	private DetalleCierreService cierredetdao;
	
	public void setArqueodao(ArqueoCajaService arqueodao) {
		this.arqueodao = arqueodao;
	}

	public void setNotaCreditoVentaDAO(NotaCreditoVentaDAO notaCreditoVentaDAO) {
		this.notaCreditoVentaDAO = notaCreditoVentaDAO;
	}

	public void setArqueoCajaDAO(ArqueoCajaDAO arqueoCajaDAO) {
		this.arqueoCajaDAO = arqueoCajaDAO;
	}

	public void setEntregaCajaDAO(EntregaCajaDAO entregaCajaDAO) {
		this.entregaCajaDAO = entregaCajaDAO;
	}

	@Override
	public List<EntregaCajaDTO> listEntregaCaja(InvocationContext ic) throws ServiceException {
		List<EntregaCajaDTO> entregaCajas =  entregaCajaDAO.listEntregaCaja();

		return entregaCajas;
	}

	@Override
	public EntregaCajaDTO getEntregaCaja(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		EntregaCajaDTO entregaCaja =  entregaCajaDAO.getEntregaCajaById(id);
		if (entregaCaja == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"entregacaja.notfound", id);

		return entregaCaja;
	}

	@Override
	public void deleteEntregaCaja(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		EntregaCajaDTO entregaCaja =  entregaCajaDAO.getEntregaCajaById(id);
		if(entregaCaja == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"entregacaja.notfound", id);

		try {
			entregaCajaDAO.deleteEntregaCaja(entregaCaja);
		}
		catch (DataIntegrityViolationException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "entregacaja.constraint.violation", id);
		}

	}

	@Override
	public EntregaCajaDTO updateEntregaCaja(Long id, EntregaCajaDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		EntregaCajaDTO entregaCaja = entregaCajaDAO.getEntregaCajaById(id);
		if (entregaCaja == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"entregacaja.notfound", id);

		entregaCaja.setIdAperturaCierre(dto.getIdAperturaCierre());
		entregaCaja.setMontoEfectivo(dto.getMontoEfectivo());
		entregaCaja.setMontoTarjeta(dto.getMontoTarjeta());
		entregaCaja.setTotal(dto.getTotal());

		entregaCajaDAO.updateEntregaCaja(entregaCaja);

		return entregaCaja;
	}

	@Override
	public EntregaCajaDTO createEntregaCaja(EntregaCajaDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {
			entregaCajaDAO.createEntregaCaja(dto);
			
		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "entregacaja.duplicate.key");
		}

		return dto;
	}
	
	@Override
	public EntregaCajaDTO generarEntregaCaja(EntregaCajaDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {	
				AperturaCierreCompositeDTO datosCie= aperturaCierreDAO.getAperturaCierreById(dto.getIdAperturaCierre());
				
				Date fecha =new Date(System.currentTimeMillis());
				BigDecimal efe =dto.getMontoEfectivo();
				BigDecimal tarjeta= dto.getMontoTarjeta();
				dto.setMontoEfectivo(efe);
				dto.setMontoTarjeta(tarjeta);	
				dto.setFechaCierre(fecha);
				BigDecimal totalentre =dto.getMontoEfectivo().add(dto.getMontoTarjeta());
				dto.setTotal(totalentre);
				
				
				BigDecimal montoNC =notaCreditoVentaDAO.getNcByIdAperturaCierre(dto.getIdAperturaCierre());
				BigDecimal saldoCie = datosCie.getVentaEfectivo().add(datosCie.getVentaTarjeta()).add(datosCie.getSaldoInicial()).add(datosCie.getOtrosIngresos());
				saldoCie = saldoCie.subtract(montoNC).subtract(datosCie.getEgresos());
				
				ArqueoCajaDTO arqueo= new ArqueoCajaDTO();
				BigDecimal dif = new BigDecimal(0);
				
				if(saldoCie != null && saldoCie.compareTo(BigDecimal.ZERO) > 0) {
						
					dif= totalentre.subtract(saldoCie);
				
					if(saldoCie.compareTo(totalentre) > 0) {
						arqueo.setDiferencias("F");
					}else if (saldoCie.compareTo(totalentre)<0) {
						arqueo.setDiferencias("S");		
					}else{
						arqueo.setDiferencias("N");
					}
					arqueo.setSaldoAlCierre(saldoCie);
					arqueo.setTotalEntregado(totalentre);
					arqueo.setFaltanteSobrante(dif);
					arqueo.setIdAperturaCierre(dto.getIdAperturaCierre());
				
				}
				
				datosCie.setSaldoCierre(totalentre);
				
				if (!arqueo.getDiferencias().equals("N")) {
					FalloCajaDTO fallo = new FalloCajaDTO();
					fallo.setFecha(fecha);
					fallo.setIdAperturaCierre(datosCie.getId());
					fallo.setMonto(dif);
					fallo.setIdUsuario(datosCie.getIdUsuarioCaja());

					fallodao.createFalloCaja(fallo, ic);
				}
				
				
				
				
				
				entregaCajaDAO.createEntregaCaja(dto);
				arqueodao.createArqueoCaja(arqueo, ic);
				
				cierredao.cerrarAperturaCierre(datosCie.getId(),datosCie, ic);
				DetalleCierreDTO dcdto= new DetalleCierreDTO();
				
				dcdto.setIdAperturaCierre(datosCie.getId());
				dcdto.setFechaCreacion(fecha);
				dcdto.setMontoEfectivo(efe);
				dcdto.setMontoTarjetas(tarjeta);
				dcdto.setMontoTotal(totalentre);
				cierredetdao.createDetalleCierre(dcdto,ic);
				
				
				
				
				
		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "entregacaja.duplicate.key");
		}

		return dto;
	}
	

	/*if(ac == null)
		throw new ServiceException(ErrorCode.CIERRE_NOT_FOUND,"entrega.caja.apertura.cierre.not.found", ic.getUserPrincipal());
	
	dto.setIdAperturaCierre(ac.getId());
	
	//se deben calcular los montos en efectivo y en tarjeta
	//se deben obtener todas las cabeceras venta con ese id apertura, se recorren las formas de pago
	//y se suman los montos efectivos y en tarjeta
	List<CabeceraVentaCompositeDTO> ventas = cabeceraVentaDAO.listCabeceraVentaByApertura(ac.getId());
	BigDecimal efectivo = BigDecimal.ZERO;
	BigDecimal tarjeta = BigDecimal.ZERO;
	if(ventas != null && !ventas.isEmpty()){
		for(CabeceraVentaCompositeDTO cb: ventas){
			if(cb.getFormasPago() != null && !cb.getFormasPago().isEmpty()){
				for(FormaPagoCompositeDTO fp: cb.getFormasPago()){
					if(fp.getMedioPago().getAbreviatura().equalsIgnoreCase("EFEC")){
						efectivo = efectivo.add(fp.getMontoPago());
					}else{
						tarjeta = tarjeta.add(fp.getMontoPago());
					}
				}
			}
		}*/
	
	@Override
	public PrintDTO imprimirEntrega(Long id, InvocationContext ic) throws ServiceException{
		
		PrintDTO retorno = new PrintDTO();
		//se obtiene la entrega caja
		EntregaCajaCompositeDTO ec = entregaCajaDAO.getEntregaCajaById(id);
		if(ec == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"entrega.caja.notfound", id);
		
		retorno = this.getReportEntrega(ec);
		
		return retorno;
		
	}
	
	private PrintDTO getReportEntrega(EntregaCajaCompositeDTO param) throws ServiceException {
	      PrintDTO reporte = new PrintDTO();
	      try {
	    	  ReportEntregaCaja rEnt = new ReportEntregaCaja(param);
	          reporte.setBytes(rEnt.generarPDF());
	          reporte.setFileName("entrega" + param); 
	      } catch (Exception e) {
	          log.error(e.getMessage(), e);
	          reporte.setMimeType(null);
	          reporte.setType(null);
	          throw new ServiceException(ErrorCode.IMPRIMIR_OC_ERROR, "sgs.print.oc.error", param.getId());
	      }
	      return reporte;
	}

}