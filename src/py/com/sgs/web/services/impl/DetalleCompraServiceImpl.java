package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.DetalleCompraDAO;
import py.com.sgs.persistence.dao.StockDAO;
import py.com.sgs.persistence.dto.DetalleCompraDTO;
import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.composite.StockCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.DetalleCompraService;

public class DetalleCompraServiceImpl implements DetalleCompraService{

private Logger log = LoggerFactory.getLogger(getClass());  

  @Autowired
  private DetalleCompraDAO detalleCompraDAO;
  
  @Autowired
  private StockDAO stockDAO;


  public void setDetalleCompraDAO(DetalleCompraDAO detalleCompraDAO) {
    this.detalleCompraDAO = detalleCompraDAO;
  }

  @Override
  public List<DetalleCompraDTO> listDetalleCompra(InvocationContext ic) throws ServiceException {
    List<DetalleCompraDTO> detalleCompras =  detalleCompraDAO.listDetalleCompra();

    return detalleCompras;
  }

  @Override
  public DetalleCompraDTO getDetalleCompra(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    DetalleCompraDTO detalleCompra =  detalleCompraDAO.getDetalleCompraById(id);
    if (detalleCompra == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detallecompra.notfound", id);

    return detalleCompra;
  }

  @Override
  public void deleteDetalleCompra(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    DetalleCompraDTO detalleCompra =  detalleCompraDAO.getDetalleCompraById(id);
    if(detalleCompra == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detallecompra.notfound", id);

    try {
    	StockCompositeDTO toUpdate= new StockCompositeDTO();
    	StockDTO di=new StockDTO();
		di.setIdProducto(detalleCompra.getIdProducto());
		di.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
		
		toUpdate = stockDAO.stockByIdProducto(di);
		
		if (toUpdate == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"stock.notfound", detalleCompra.getIdProducto());
		
		Double ns = new Double(0);
		
		if(toUpdate.getStock()==null) {
			ns=ns-detalleCompra.getCantidad();
		}else {
			ns= toUpdate.getStock() - detalleCompra.getCantidad();
		}
		
		toUpdate.setStock(ns);
		toUpdate.setUsuarioUltModif(ic.getUserPrincipal());
		toUpdate.setFechaUltModif(new Date(System.currentTimeMillis()));
		
		
		stockDAO.updateStock(toUpdate);
      
		detalleCompraDAO.deleteDetalleCompra(detalleCompra);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "detallecompra.constraint.violation", id);
    }

  }

  @Override
  public DetalleCompraDTO updateDetalleCompra(Long id, DetalleCompraDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    DetalleCompraDTO detalleCompra = detalleCompraDAO.getDetalleCompraById(id);
    if (detalleCompra == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detallecompra.notfound", id);

    detalleCompra.setIdCabeceraCompra(dto.getIdCabeceraCompra());
    detalleCompra.setIdProducto(dto.getIdProducto());
    detalleCompra.setCantidad(dto.getCantidad());
    detalleCompra.setPrecioUnitario(dto.getPrecioUnitario());
    detalleCompra.setPrecioTotal(dto.getPrecioTotal());
    detalleCompra.setImpuestoFiscal(dto.getImpuestoFiscal());
 

   

     detalleCompraDAO.updateDetalleCompra(detalleCompra);
   

    return detalleCompra;
  }

  @Override
  public DetalleCompraDTO createDetalleCompra(DetalleCompraDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
    		StockCompositeDTO toUpdate= new StockCompositeDTO();
    		//debe ser el dto para tener en cuenta el id_sucursal
    		
    		StockDTO di=new StockDTO();
			di.setIdProducto(dto.getIdProducto());
			di.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
			
    		toUpdate = stockDAO.stockByIdProducto(di); 
    		BigDecimal pp =new BigDecimal(0);
    		Double ns = new Double(0);
    		
			if (toUpdate == null)
				throw new ServiceException(ErrorCode.NO_DATA_FOUND,"stock.notfound", dto.getIdProducto());
			
			if( toUpdate.getUltCostoCompra()==null ) {
				pp=dto.getPrecioUnitario();
				
			}else {
				BigDecimal divisor = new BigDecimal("2");
				//se halla el promedio ponderado sumando el costo actual al pp anterior y dividiendo por 2
				pp= (toUpdate.getPromPonderado().add(dto.getPrecioUnitario())).divide(divisor);
			}
			 
			if(toUpdate.getStock()==null) {
				ns=dto.getCantidad();
			}else {
				ns= toUpdate.getStock() + dto.getCantidad();
			}
			
			toUpdate.setUltCostoCompra(dto.getPrecioUnitario());
			toUpdate.setStock(ns);
			toUpdate.setPromPonderado(pp);
			toUpdate.setUsuarioUltModif(ic.getUserPrincipal());
			toUpdate.setFechaUltModif(new Date(System.currentTimeMillis()));
			
			
			stockDAO.updateStock(toUpdate);
			detalleCompraDAO.createDetalleCompra(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "detallecompra.duplicate.key");
    }

    return dto;
  }

}