package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.DescuentosDetallesDAO;
import py.com.sgs.persistence.dto.DescuentosDetallesDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.DescuentosDetallesService;

public class DescuentosDetallesServiceImpl implements DescuentosDetallesService{

	private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
	private DescuentosDetallesDAO descuentosDetallesDAO;

	public void setDescuentosDetallesDAO(DescuentosDetallesDAO descuentosDetallesDAO) {
		this.descuentosDetallesDAO = descuentosDetallesDAO;
	}

	@Override
	public List<DescuentosDetallesDTO> listDescuentosDetalles(InvocationContext ic) throws ServiceException {
		List<DescuentosDetallesDTO> descuentosDetalless =  descuentosDetallesDAO.listDescuentosDetalles();

		return descuentosDetalless;
	}

	@Override
	public DescuentosDetallesDTO getDescuentosDetalles(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		DescuentosDetallesDTO descuentosDetalles =  descuentosDetallesDAO.getDescuentosDetallesById(id);
		if (descuentosDetalles == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"descuentosdetalles.notfound", id);

		return descuentosDetalles;
	}
	
	@Override
	public List<DescuentosDetallesDTO> getDetallesByIdDescuento(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		List<DescuentosDetallesDTO> descuentosDetalles =  descuentosDetallesDAO.getDetallesByIdDescuento(id);
		if (descuentosDetalles == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"descuentosdetalles.notfound", id);

		return descuentosDetalles;
	}

	@Override
	public void deleteDescuentosDetalles(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		DescuentosDetallesDTO descuentosDetalles =  descuentosDetallesDAO.getDescuentosDetallesById(id);
		if(descuentosDetalles == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"descuentosdetalles.notfound", id);

		try {
			descuentosDetallesDAO.deleteDescuentosDetalles(descuentosDetalles);
		}
		catch (DataIntegrityViolationException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "descuentosdetalles.constraint.violation", id);
		}

	}

	@Override
	public DescuentosDetallesDTO updateDescuentosDetalles(Long id, DescuentosDetallesDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		DescuentosDetallesDTO descuentosDetalles = descuentosDetallesDAO.getDescuentosDetallesById(id);
		if (descuentosDetalles == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"descuentosdetalles.notfound", id);

		descuentosDetalles.setIdDescuento(dto.getIdDescuento());
		descuentosDetalles.setIdProducto(dto.getIdProducto());
		descuentosDetalles.setPorcentaje(dto.getPorcentaje());
		descuentosDetalles.setMonto(dto.getMonto());
		descuentosDetalles.setCantACobrar(dto.getCantACobrar());
		descuentosDetalles.setCantALlevar(dto.getCantALlevar());

		descuentosDetallesDAO.updateDescuentosDetalles(descuentosDetalles);

		return descuentosDetalles;
	}

	@Override
	public DescuentosDetallesDTO createDescuentosDetalles(DescuentosDetallesDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {
			descuentosDetallesDAO.createDescuentosDetalles(dto);
		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "descuentosdetalles.duplicate.key");
		}

		return dto;
	}

}