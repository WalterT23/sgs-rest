package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.config.SgsRestApplicationContextProvider;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.OrdenCompraDetalleDAO;
import py.com.sgs.persistence.dao.OrdenDeCompraDAO;
import py.com.sgs.persistence.dao.ProductosDAO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.OrdenCompraDetalleDTO;
import py.com.sgs.persistence.dto.OrdenDeCompraDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.composite.OrdenDeCompraCompositeDTO;
import py.com.sgs.persistence.dto.composite.ProductosCompositeDTO;
import py.com.sgs.util.report.ReportOrdenDeCompra;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.OrdenDeCompraService;

public class OrdenDeCompraServiceImpl implements OrdenDeCompraService{
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private OrdenDeCompraDAO ordenDeCompraDAO;
	@Autowired
	private OrdenCompraDetalleDAO ordenCompraDetalleDAO;
	@Autowired
	private ProductosDAO productosDao;
	@Autowired
	private InfoRefOpcDAO infoRefDAO;
	
	private ApplicationContext ctx = null;
	
	public OrdenDeCompraServiceImpl() {
		this.ctx = SgsRestApplicationContextProvider.getContext();
		
	}
	
	@Override
	public List<OrdenDeCompraCompositeDTO> listOrdenDeCompra(InvocationContext ic) throws ServiceException {
		List<OrdenDeCompraCompositeDTO> ordenDeCompras =  ordenDeCompraDAO.listOrdenDeCompra(Long.parseLong(ic.getUserSucursal()));
		
		for(OrdenDeCompraCompositeDTO oc: ordenDeCompras){
			
			if (oc.getDetalles() != null)
				
			  for(OrdenCompraDetalleDTO p: oc.getDetalles()){
				  p.setCostoTotal(new BigDecimal(p.getCantidad()).multiply(p.getUltCostoCompra()));
			  }
		 }
		
		return ordenDeCompras;
	}
	
	@Override
	public List<OrdenDeCompraCompositeDTO> listConfirmadas(InvocationContext ic) throws ServiceException {
		
		List<OrdenDeCompraCompositeDTO> ordenDeCompras =  ordenDeCompraDAO.listConfirmadas(Long.parseLong(ic.getUserSucursal()));
		
		for(OrdenDeCompraCompositeDTO oc: ordenDeCompras){
			
			if (oc.getDetalles() != null)
				
			  for(OrdenCompraDetalleDTO p: oc.getDetalles()){
				  p.setCostoTotal(new BigDecimal(p.getCantidad()).multiply(p.getUltCostoCompra()));
			  }
		 }
		
		return ordenDeCompras;
	}
	
	@Override
	public OrdenDeCompraCompositeDTO getOrdenDeCompraById(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		OrdenDeCompraCompositeDTO ordenDeCompra =  ordenDeCompraDAO.getOrdenDeCompraById(id);
		if (ordenDeCompra == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ordendecompra.notfound", id);
		
		if (ordenDeCompra.getDetalles() != null){
			 for(OrdenCompraDetalleDTO p: ordenDeCompra.getDetalles()){
				  p.setCostoTotal(new BigDecimal(p.getCantidad()).multiply(p.getUltCostoCompra()));
			  }
		}
		  
		return ordenDeCompra;
	}
	
	@Override
	public OrdenDeCompraCompositeDTO getOrdenDeCompraByNroOC(String nroOrdenCompra, InvocationContext ic) throws ServiceException {
		if (nroOrdenCompra.isEmpty())
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		OrdenDeCompraCompositeDTO ordenDeCompra = ordenDeCompraDAO.getOrdenDeCompraByNroOC(nroOrdenCompra);
		if (ordenDeCompra == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ordendecompra.notfound", nroOrdenCompra);
		
		if (ordenDeCompra.getDetalles() != null){
			 for(OrdenCompraDetalleDTO p: ordenDeCompra.getDetalles()){
				  p.setCostoTotal(new BigDecimal(p.getCantidad()).multiply(p.getUltCostoCompra()));
			  }
		}
		  
		return ordenDeCompra;
	}


	@Override
	public void deleteOrdenDeCompra(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		OrdenDeCompraDTO ordenDeCompra =  ordenDeCompraDAO.getOrdenDeCompraById(id);
		if(ordenDeCompra == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ordendecompra.notfound", id);

		try {
			ordenDeCompraDAO.deleteOrdenDeCompra(ordenDeCompra);
		}
		catch (DataIntegrityViolationException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "ordendecompra.constraint.violation", id);
		}

	}

	@Override
	public OrdenDeCompraCompositeDTO updateOrdenDeCompra(Long id, OrdenDeCompraCompositeDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		OrdenDeCompraCompositeDTO ordenDeCompra = ordenDeCompraDAO.getOrdenDeCompraById(id);
		
		if (ordenDeCompra == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ordendecompra.notfound", id);
		
		InfoRefOpcDTO estado= infoRefDAO.getInfoRefOpcByAbv("EOCUR");
		
		if(!dto.getIdEstado().equals(estado.getId()))
			throw new ServiceException(ErrorCode.ESTADO_INVALIDO,"ordendecompra.estado.invalido", id);

		if (dto.getIdProveedor() != null)
			ordenDeCompra.setIdProveedor(dto.getIdProveedor());

		BigDecimal totalIva5 = ordenDeCompra.getTotalIva5();
		BigDecimal totalIva10 = ordenDeCompra.getTotalIva10();
		BigDecimal totalExenta = ordenDeCompra.getTotalExcenta();
		BigDecimal totalMonto = ordenDeCompra.getMontoTotal();

		//Obtengo totales de detalles a agregar
		if (dto.getDetallesAAgregar() != null && !dto.getDetallesAAgregar().isEmpty()) {
			BigDecimal iva5 = new BigDecimal(0);
			BigDecimal iva10 = new BigDecimal(0);
			BigDecimal excenta = new BigDecimal(0);

			for (OrdenCompraDetalleDTO ocd:dto.getDetallesAAgregar()){
				if (ocd != null) {
					ProductosCompositeDTO di=new ProductosCompositeDTO();
					di.setId(ocd.getIdProducto());
					di.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

					ProductosCompositeDTO producto = productosDao.getProductosByIdNew(di);
					ocd.setCostoTotal(new BigDecimal(ocd.getCantidad()).multiply(ocd.getUltCostoCompra()));

					if (producto.getAbvTributo().equals("TTIV5") ){
						iva5 = iva5.add(ocd.getCostoTotal().divide(new BigDecimal(22), RoundingMode.HALF_UP));
					}else if (producto.getAbvTributo() .equals("TTI10")){
						iva10 = iva10.add(ocd.getCostoTotal().divide(new BigDecimal(11),RoundingMode.HALF_UP));
					}else {
						excenta = excenta.add(ocd.getCostoTotal());
					}
				}
				totalIva5 = totalIva5.add(iva5);
				totalIva10 = totalIva10.add(iva10);
				totalExenta = totalExenta.add(excenta);
				totalMonto = totalMonto.add(ocd.getCostoTotal());
			}
		}

		//Obtengo totales de detalles a eliminar
		if (dto.getDetallesAEliminar() != null && !dto.getDetallesAEliminar().isEmpty()) {
			BigDecimal iva5 = new BigDecimal(0);
			BigDecimal iva10 = new BigDecimal(0);
			BigDecimal excenta = new BigDecimal(0);

			for (OrdenCompraDetalleDTO ocd:dto.getDetallesAEliminar()){
				if (ocd != null) {
					ProductosCompositeDTO di=new ProductosCompositeDTO();
					di.setId(ocd.getIdProducto());
					di.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

					ProductosCompositeDTO producto = productosDao.getProductosByIdNew(di);
					ocd.setCostoTotal(new BigDecimal(ocd.getCantidad()).multiply(ocd.getUltCostoCompra()));

					if (producto.getAbvTributo().equals("TTIV5") ){
						iva5 = iva5.add(ocd.getCostoTotal().divide(new BigDecimal(22), RoundingMode.HALF_UP));
					}else if (producto.getAbvTributo() .equals("TTI10")){
						iva10 = iva10.add(ocd.getCostoTotal().divide(new BigDecimal(11),RoundingMode.HALF_UP));
					}else {
						excenta = excenta.add(ocd.getCostoTotal());
					}
				}
				totalIva5 = totalIva5.subtract(iva5);
				totalIva10 = totalIva10.subtract(iva10);
				totalExenta = totalExenta.subtract(excenta);
				totalMonto = totalMonto.subtract(ocd.getCostoTotal());
			}
		}

		if (dto.getMontoTotal() != null)
			ordenDeCompra.setMontoTotal(totalMonto);
		
		if (dto.getTotalIva5() != null)
			ordenDeCompra.setTotalIva5(totalIva5);
		
		if (dto.getTotalIva10() != null)
			ordenDeCompra.setTotalIva10(totalIva10);
		
		if (dto.getTotalExcenta() != null)
			ordenDeCompra.setTotalExcenta(totalExenta);

		ordenDeCompra.setFechaModificacion(new Date(System.currentTimeMillis()));
		ordenDeCompra.setUsuarioModificacion(ic.getUserPrincipal());

		ordenDeCompraDAO.updateOrdenDeCompra(ordenDeCompra);
		
		if (dto.getDetalles() != null && !dto.getDetalles().isEmpty()){
			
			for (OrdenCompraDetalleDTO d:dto.getDetalles()){
				
				ordenCompraDetalleDAO.updateOrdenCompraDetalle(d);
			}
		}
		
		if (dto.getDetallesAAgregar() != null && !dto.getDetallesAAgregar().isEmpty()){
			
			for (OrdenCompraDetalleDTO d:dto.getDetallesAAgregar()){
				d.setIdOrdenCompra(dto.getId());
				d.setFechaReg(new Date(System.currentTimeMillis()));
				ordenCompraDetalleDAO.createOrdenCompraDetalle(d);
			}
		}
		
		if (dto.getDetallesAEliminar() != null && !dto.getDetallesAEliminar().isEmpty()){
			
			for (OrdenCompraDetalleDTO d:dto.getDetallesAEliminar()){
				ordenCompraDetalleDAO.deleteOrdenCompraDetalle(d);
			}
		}
		
		return ordenDeCompra;
	}

	@Override
	public OrdenDeCompraCompositeDTO createOrdenDeCompra(OrdenDeCompraCompositeDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {
			
			Long nroOrdenCompra = ordenDeCompraDAO.getNroOrdenCompra(Long.parseLong(ic.getUserSucursal()));
			if (nroOrdenCompra == null){
				nroOrdenCompra = 1L;
			}else {
				nroOrdenCompra = nroOrdenCompra + 1L;
			}
			
			dto.setNroOrdenCompra(String.format("%010d", nroOrdenCompra));
			dto.setFechaRegistro(new Date(System.currentTimeMillis()));
			dto.setUsuarioRegistro(ic.getUserPrincipal());
			if(ic.getUserSucursal() != null)
			dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
			
			InfoRefOpcDTO estado= infoRefDAO.getInfoRefOpcByAbv("EOCUR");
			
			dto.setIdEstado(estado.getId());
			dto.setFechaEstado(new Date(System.currentTimeMillis()));

			if (dto.getDetalles() != null && !dto.getDetalles().isEmpty()) {
				BigDecimal iva5 = new BigDecimal(0);
				BigDecimal iva10 = new BigDecimal(0);
				BigDecimal excenta = new BigDecimal(0);
				BigDecimal total = new BigDecimal(0);
				
				for (OrdenCompraDetalleDTO ocd:dto.getDetalles()){

					
					
					if (ocd != null) {
						
						ProductosCompositeDTO di=new ProductosCompositeDTO();
						di.setId(ocd.getIdProducto());
						di.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
						
						ProductosCompositeDTO producto = productosDao.getProductosByIdNew(di);
						ocd.setCostoTotal(new BigDecimal(ocd.getCantidad()).multiply(ocd.getUltCostoCompra()));

						if (producto.getAbvTributo().equals("TTIV5") ){
							iva5 = iva5.add(ocd.getCostoTotal().divide(new BigDecimal(22), RoundingMode.HALF_UP));
						}else if (producto.getAbvTributo() .equals("TTI10")){
							iva10 = iva10.add(ocd.getCostoTotal().divide(new BigDecimal(11),RoundingMode.HALF_UP));
						}else {
							excenta = excenta.add(ocd.getCostoTotal());
						}
						total = total.add(ocd.getCostoTotal());
				}
				dto.setTotalIva5(iva5);
				dto.setTotalIva10(iva10);
				dto.setTotalExcenta(excenta);
				dto.setMontoTotal(total);
			}
			ordenDeCompraDAO.createOrdenDeCompra(dto);
			
			if (dto.getDetalles() != null && !dto.getDetalles().isEmpty()){
				
				for (OrdenCompraDetalleDTO d:dto.getDetalles()){
					d.setIdOrdenCompra(dto.getId());
					d.setFechaReg(new Date(System.currentTimeMillis()));
					ordenCompraDetalleDAO.createOrdenCompraDetalle(d);
				}
			}
		}
		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "ordendecompra.duplicate.key");
		}

		return dto;
	}

	@Override
	public OrdenDeCompraCompositeDTO confirmarOrdenDeCompra(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		OrdenDeCompraCompositeDTO ordenDeCompra = ordenDeCompraDAO.getOrdenDeCompraById(id);

		if (ordenDeCompra == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ordenDeCompra.notfound", id);
		
		ordenDeCompra.setDetalles(ordenCompraDetalleDAO.listDetalleByIdOC(id));
		
		//StockDTO toUpdate = null;
		if (ordenDeCompra.getDetalles() != null && !ordenDeCompra.getDetalles().isEmpty()){
		
		//	for (OrdenCompraDetalleDTO d:ordenDeCompra.getDetalles()){
		//
		//		toUpdate = stockDAO.getStockByIdProducto(d.getIdProducto());
		//		if (toUpdate == null)
		//			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"stock.notfound", d.getIdProducto());
		//		
		//		toUpdate.setUltCostoCompra(d.getCostoCompra());
		//		toUpdate.setUsuarioUltModif(ic.getUserPrincipal());
		//		toUpdate.setFechaUltModif(new Date(System.currentTimeMillis()));
		//		stockDAO.updateStockOc(toUpdate);
		//	}
		//	
			InfoRefOpcDTO estConf = infoRefDAO.getInfoRefOpcByAbv("EOCON");
			ordenDeCompra.setIdEstado(estConf.getId());
			ordenDeCompraDAO.updateOrdenDeCompra(ordenDeCompra);
		}
		
		return ordenDeCompra;
	}
	
	@Override
	public OrdenDeCompraCompositeDTO anularOrdenDeCompra(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		OrdenDeCompraCompositeDTO ordenDeCompra = ordenDeCompraDAO.getOrdenDeCompraById(id);

		if (ordenDeCompra == null)
			throw new ServiceException(ErrorCode.ESTADO_INVALIDO,"ordenDeCompra.notfound", id);
		
		InfoRefOpcDTO estadoOC = infoRefDAO.getInfoRefOpcByAbv("EOCON");
		if (ordenDeCompra.getIdEstado() == estadoOC.getId()) {
			throw new ServiceException(ErrorCode.ESTADO_INVALIDO,"La Orden de Compra ya fue confirmada", id);
		}
		
		ordenDeCompra.setDetalles(ordenCompraDetalleDAO.listDetalleByIdOC(id));
		
		if (ordenDeCompra.getDetalles() != null && !ordenDeCompra.getDetalles().isEmpty()){
			InfoRefOpcDTO estConf = infoRefDAO.getInfoRefOpcByAbv("EOANU");
			ordenDeCompra.setIdEstado(estConf.getId());
			ordenDeCompraDAO.updateOrdenDeCompra(ordenDeCompra);
		}
		
		return ordenDeCompra;
	}
	
	@Override
	public PrintDTO imprimirOrdenDeCompra(Long id, InvocationContext ic) throws ServiceException{
		
		PrintDTO retorno = new PrintDTO();
		//se obtiene la orden de compra
		OrdenDeCompraCompositeDTO oc = ordenDeCompraDAO.getOrdenDeCompraById(id);
		
		if(oc == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"ordenDeCompra.notfound", id);
		
		if (oc.getDetalles() != null){
			 for(OrdenCompraDetalleDTO p: oc.getDetalles()){
				  p.setCostoTotal(new BigDecimal(p.getCantidad()).multiply(p.getUltCostoCompra()));
			  }
		}
		
		retorno = this.getReportOrdenDeCompra(oc);
		
		return retorno;
		
	}
	
	private PrintDTO getReportOrdenDeCompra(OrdenDeCompraCompositeDTO param) throws ServiceException {
	      PrintDTO reporte = new PrintDTO();
	      try {
	          ReportOrdenDeCompra rOC = new ReportOrdenDeCompra(param);
	          reporte.setBytes(rOC.generarPDF());
	          reporte.setFileName(param.getNroOrdenCompra()); 
	          // descomentar para tomar el
	          // id del ds ddt como nombre
	          // del reporte
	          // reporte.setFileName("14002IRE1000001A");
	          // MagicMatch match = Magic.getMagicMatch(reporte.getBytes());
	          // reporte.setMimeType(match.getMimeType());
	          // reporte.setType(match.getExtension());
	      } catch (Exception e) {
	          log.error(e.getMessage(), e);
	          reporte.setMimeType(null);
	          reporte.setType(null);
	          throw new ServiceException(ErrorCode.IMPRIMIR_OC_ERROR, "sgs.print.oc.error", param.getId());
	      }
	      return reporte;
	}
	
	public void setOrdenDeCompraDAO(OrdenDeCompraDAO ordenDeCompraDAO) {
		this.ordenDeCompraDAO = ordenDeCompraDAO;
	}
	public void setOrdenCompraDetalleDAO(OrdenCompraDetalleDAO ordenCompraDetalleDAO) {
		this.ordenCompraDetalleDAO = ordenCompraDetalleDAO;
	}
	public void setProductosDao(ProductosDAO productosDao) {
		this.productosDao = productosDao;
	}
	public void setInfoRefOpcDAO(InfoRefOpcDAO infoRefDAO) {
		this.infoRefDAO = infoRefDAO;
	}
	
	  
}