package py.com.sgs.web.services.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.DescuentosListaPreciosDAO;
import py.com.sgs.persistence.dto.DescuentosListaPreciosDTO;
import py.com.sgs.persistence.dto.TimbradoDTO;
import py.com.sgs.persistence.dto.UsuarioSucursalDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.DescuentosListaPreciosService;

public class DescuentosListaPreciosServiceImpl implements
		DescuentosListaPreciosService {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private DescuentosListaPreciosDAO descuentosListaPreciosDAO;

	public void setDescuentosListaPreciosDAO(
			DescuentosListaPreciosDAO descuentosListaPreciosDAO) {
		this.descuentosListaPreciosDAO = descuentosListaPreciosDAO;
	}

	@Override
	public List<DescuentosListaPreciosDTO> listDescuentosListaPrecios(
			InvocationContext ic) throws ServiceException {
		List<DescuentosListaPreciosDTO> descuentosListaPrecios = descuentosListaPreciosDAO
				.listDescuentosListaPrecios();

		Calendar fecha = Calendar.getInstance();
		for (DescuentosListaPreciosDTO aso : descuentosListaPrecios) {
			aso.setVigente(aso.getFinVigencia().after(fecha.getTime())
					&& aso.getInicioVigencia().before(fecha.getTime()));
		}

		return descuentosListaPrecios;
	}

	@Override
	public List<DescuentosListaPreciosDTO> descuentosListaPreciosByLista(
			Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		List<DescuentosListaPreciosDTO> descuentosListaPrecios = descuentosListaPreciosDAO
				.descuentosLPbyLista(id);

		Calendar fecha = Calendar.getInstance();
		for (DescuentosListaPreciosDTO aso : descuentosListaPrecios) {
			aso.setVigente(aso.getInicioVigencia().before(fecha.getTime())
					&& aso.getFinVigencia().after(fecha.getTime()));
		}

		return descuentosListaPrecios;
	}

	@Override
	public DescuentosListaPreciosDTO getDescuentosListaPrecios(Long id,
			InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		DescuentosListaPreciosDTO dsctoListaPrecios = descuentosListaPreciosDAO
				.getDescuentosListaPreciosById(id);
		if (dsctoListaPrecios == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"descuentoslistaprecios.notfound", id);

		Calendar fecha = Calendar.getInstance();
		dsctoListaPrecios.setVigente(dsctoListaPrecios.getFinVigencia().after(
				fecha.getTime())
				&& dsctoListaPrecios.getInicioVigencia()
						.before(fecha.getTime()));

		return dsctoListaPrecios;
	}

	@Override
	public void deleteDescuentosListaPrecios(Long id, InvocationContext ic)
			throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		DescuentosListaPreciosDTO descuentosListaPrecios = descuentosListaPreciosDAO
				.getDescuentosListaPreciosById(id);
		if (descuentosListaPrecios == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"descuentoslistaprecios.notfound", id);

		try {
			descuentosListaPreciosDAO
					.deleteDescuentosListaPrecios(descuentosListaPrecios);
		} catch (DataIntegrityViolationException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION,
					"descuentoslistaprecios.constraint.violation", id);
		}

	}

	@Override
	public DescuentosListaPreciosDTO updateDescuentosListaPrecios(Long id,
			DescuentosListaPreciosDTO dto, InvocationContext ic)
			throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		DescuentosListaPreciosDTO descuentosListaPrecios = descuentosListaPreciosDAO
				.getDescuentosListaPreciosById(id);
		if (descuentosListaPrecios == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"descuentoslistaprecios.notfound", id);

		descuentosListaPrecios.setIdDescuento(dto.getIdDescuento());
		descuentosListaPrecios.setIdListaPrecio(dto.getIdListaPrecio());
		descuentosListaPrecios.setInicioVigencia(dto.getInicioVigencia());
		descuentosListaPrecios.setFinVigencia(dto.getFinVigencia());

		descuentosListaPreciosDAO
				.updateDescuentosListaPrecios(descuentosListaPrecios);

		return descuentosListaPrecios;
	}

	@Override
	public DescuentosListaPreciosDTO createDescuentosListaPrecios(
			DescuentosListaPreciosDTO dto, InvocationContext ic)
			throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		if (dto.getInicioVigencia() == null)
			dto.setInicioVigencia(new Date(System.currentTimeMillis()));

		if (dto.getFinVigencia() == null) {
			Calendar fecha = Calendar.getInstance();
			fecha.set(Calendar.YEAR, 3000);
			dto.setFinVigencia(fecha.getTime());
			
		}else {
			
			Calendar fin = Calendar.getInstance();
			fin.setTime(dto.getFinVigencia());
			
			int value = fin.get(Calendar.DAY_OF_MONTH);
			fin.set(Calendar.DAY_OF_MONTH, value + 1);
			
			dto.setFinVigencia(fin.getTime());
		}

		List<DescuentosListaPreciosDTO> asos = descuentosListaPreciosDAO.descuentosLPvigYprox(dto.getIdListaPrecio());

		for (DescuentosListaPreciosDTO a : asos) {

			if ((dto.getInicioVigencia().after(a.getInicioVigencia()) && dto.getInicioVigencia().before(a.getFinVigencia()))
					|| (dto.getFinVigencia().after(a.getInicioVigencia()) && dto.getInicioVigencia().before(a.getFinVigencia()))) {

				throw new ServiceException(ErrorCode.DESCTO_LP,"vigencia.solapada");

			}
		}

		try {
			
			descuentosListaPreciosDAO.createDescuentosListaPrecios(dto);
			
		} catch (DuplicateKeyException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION,"descuentoslistaprecios.duplicate.key");
		}

		return dto;
	}

	@Override
	public void vencerAso(Long id, InvocationContext ic)
			throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		DescuentosListaPreciosDTO asociacion = descuentosListaPreciosDAO
				.getDescuentosListaPreciosById(id);

		if (asociacion == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"timbrado.notfound", id);

		Calendar fecha = Calendar.getInstance();

		if (asociacion.getFinVigencia().after(fecha.getTime())) {
			descuentosListaPreciosDAO.vencerAsociacion(id);
		} else {
			throw new ServiceException(ErrorCode.DESCTO_LP, "aso.vencida");
		}
	}

}