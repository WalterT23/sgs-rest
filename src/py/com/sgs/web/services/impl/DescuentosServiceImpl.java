package py.com.sgs.web.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.DescuentosDAO;
import py.com.sgs.persistence.dao.DescuentosDetallesDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dto.DescuentosDTO;
import py.com.sgs.persistence.dto.DescuentosDetallesDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.composite.DescuentosCompositeDTO;
import py.com.sgs.persistence.dto.composite.StockCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.DescuentosService;

public class DescuentosServiceImpl implements DescuentosService{

private Logger log = LoggerFactory.getLogger(getClass());  
  @Autowired
  private DescuentosDAO descuentosDAO;
  @Autowired
  private InfoRefOpcDAO infoRefDAO;
  @Autowired 
  private DescuentosDetallesDAO dsctoDetalleDAO;
  
	  
  public void setDescuentosDAO(DescuentosDAO descuentosDAO) {
    this.descuentosDAO = descuentosDAO;
  }


  @Override
  public List<DescuentosCompositeDTO> listDescuentos(InvocationContext ic) throws ServiceException {
    List<DescuentosCompositeDTO> descuentos =  descuentosDAO.listDescuentos();

    return descuentos;
  }

  @Override
  public DescuentosDTO getDescuentos(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    DescuentosDTO descuentos =  descuentosDAO.getDescuentosById(id);
    if (descuentos == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"descuentos.notfound", id);

    return descuentos;
  }

  @Override
  public void deleteDescuentos(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    DescuentosDTO descuentos =  descuentosDAO.getDescuentosById(id);
    if(descuentos == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"descuentos.notfound", id);

    try {
      descuentosDAO.deleteDescuentos(descuentos);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "descuentos.constraint.violation", id);
    }

  }

  @Override
  public DescuentosDTO updateDescuentos(Long id, DescuentosCompositeDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    DescuentosDTO descuentos = descuentosDAO.getDescuentosById(id);
    if (descuentos == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"descuentos.notfound", id);
    
    if (dto.getIdTipoDescuento() == null)
    	descuentos.setIdTipoDescuento(dto.getIdTipoDescuento());
    
    if (dto.getDescripcion().trim().isEmpty())
    	descuentos.setDescripcion(dto.getDescripcion());
 
    descuentosDAO.updateDescuentos(descuentos);
    
    
    //se obtiene los detalles que tenia inicialmente el descuento
    List<DescuentosDetallesDTO> lisDet = dsctoDetalleDAO.getDetallesByIdDescuento(dto.getId());
    
    if (dto.getDetalles() != null){
    	
    	Boolean existe = false;
    	for ( DescuentosDetallesDTO detOrig: lisDet){
    		
    		for ( DescuentosDetallesDTO detalle: dto.getDetalles()){
    			
    			if(detalle.getId() != null && detalle.getId().compareTo(detOrig.getId()) == 0){
        			existe = true;
        		}
    		}
	    	
	    	if (!existe){
	    		dsctoDetalleDAO.deleteDescuentosDetalles(detOrig);
	    	}
    	}
    	
    	for ( DescuentosDetallesDTO detalle: dto.getDetalles()){
    		
    		if(detalle.getId() == null){
    		
    			dsctoDetalleDAO.createDescuentosDetalles(detalle);
    			
    		}else {
    		
	    		DescuentosDetallesDTO toUpd = dsctoDetalleDAO.getDescuentosDetallesById(detalle.getId());
	    		
	    		if(detalle.getPorcentaje() != null)
	    			toUpd.setPorcentaje(detalle.getPorcentaje());
	    		
	    		if(detalle.getMonto() != null)
	    			toUpd.setMonto(detalle.getMonto());
	    		
	    		if(detalle.getCantALlevar() != null)
	    			toUpd.setCantALlevar(detalle.getCantALlevar());
	    		
	    		if(detalle.getCantACobrar() != null)
	    			toUpd.setCantACobrar(detalle.getCantACobrar());
	    		
	    		dsctoDetalleDAO.updateDescuentosDetalles(toUpd);
    		}
    	}
    }

    return descuentos;
  }

  @Override
  public DescuentosDTO createDescuentos(DescuentosCompositeDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
    
    if (dto.getIdTipoDescuento() == null)
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null");
    
    if (dto.getDescripcion().trim().isEmpty())
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null");
    
    try {
      dto.setUsuarioCreacion(ic.getUserPrincipal());
      dto.setFechaCreacion(new Date(System.currentTimeMillis()));

      descuentosDAO.createDescuentos(dto);
      
//      InfoRefOpcDTO porcentaje = infoRefDAO.getInfoRefOpcByAbv("PORC");
//      InfoRefOpcDTO monto = infoRefDAO.getInfoRefOpcByAbv("MFIJO");
//      InfoRefOpcDTO pxy = infoRefDAO.getInfoRefOpcByAbv("PXY");
//      
      List<DescuentosDetallesDTO> detalles = dto.getDetalles();
      for( DescuentosDetallesDTO d: detalles){
    	  d.setIdDescuento(dto.getId());
//    	  if(d.getPorcentaje() == null)
//    		  d.setPorcentaje());
    	  dsctoDetalleDAO.createDescuentosDetalles(d);
      }
      
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "descuentos.duplicate.key");
    }

    return dto;
  }

}