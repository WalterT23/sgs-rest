package py.com.sgs.web.services.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.CrossOrigin;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.AccesoDAO;
import py.com.sgs.persistence.dao.FuncionalidadesDAO;
import py.com.sgs.persistence.dto.AccesoDTO;
import py.com.sgs.persistence.dto.composite.AccesoCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.AccesoService;

@CrossOrigin()
public class AccesoServiceImpl implements AccesoService{

  private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private AccesoDAO accesoDAO;

  @Autowired
  private FuncionalidadesDAO funcionalidadesDAO;
  
  public void setAccesoDAO(AccesoDAO accesoDAO) {
    this.accesoDAO = accesoDAO;
  }

  @Override
  public List<AccesoDTO> listAcceso(InvocationContext ic) throws ServiceException {
    List<AccesoDTO> accesos =  accesoDAO.listAcceso();

    return accesos;
  }

  @Override
  public AccesoDTO getAcceso(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    AccesoDTO acceso =  accesoDAO.getAccesoById(id);
    if (acceso == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"acceso.notfound", id);

    return acceso;
  }


  @Override
  public AccesoDTO updateAcceso(Long id, AccesoDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    AccesoDTO acceso = accesoDAO.getAccesoById(id);
    if (acceso == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"acceso.notfound", id);

    acceso.setToken(dto.getToken());
    acceso.setIdUsuario(dto.getIdUsuario());
    
    accesoDAO.updateAcceso(acceso);
    
    return acceso;
  }

  @Override
  public AccesoDTO createAcceso(AccesoDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
      
      dto.setFechaCreacion(new Date(System.currentTimeMillis()));
      dto.setFechaVencimiento(new Date(System.currentTimeMillis()+1800000));
      accesoDAO.createAcceso(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "acceso.duplicate.key");
    }

    return dto;
  }

  @Override
  public AccesoCompositeDTO getFuncionalidadesByToken(AccesoDTO dto, InvocationContext ic) throws ServiceException {
	  
	  AccesoCompositeDTO retorno = new AccesoCompositeDTO();
	  
	  if (dto == null)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
	  
	  if (dto.getToken() == null  || dto.getToken().isEmpty())
		  throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","token");
	  
	  try {
		  retorno = accesoDAO.getAccesoByToken(dto.getToken());
		  
		  if(retorno == null)
			  throw new ServiceException(ErrorCode.TOKEN_NO_EXISTE,"token.not.found");
		  
		  retorno.setFuncionalidades(funcionalidadesDAO.listFuncionalidadesByToken(retorno.getToken()));
	  }
	  catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "token.duplicate.key");
	}

	return retorno;
  }





}