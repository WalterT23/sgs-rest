package py.com.sgs.web.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.support.DaoSupport;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.AperturaCierreDAO;
import py.com.sgs.persistence.dao.CabeceraVentaDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.MovMercDetalleDAO;
import py.com.sgs.persistence.dao.MovimientoMercaderiaDAO;
import py.com.sgs.persistence.dao.NotaCreditoVentaDAO;
import py.com.sgs.persistence.dao.NotaCreditoVentaDetalleDAO;
import py.com.sgs.persistence.dao.PuntoExpedicionDAO;
import py.com.sgs.persistence.dao.StockDAO;
import py.com.sgs.persistence.dao.TimbradoPtoExpedicionDAO;
import py.com.sgs.persistence.dao.UsuariosDAO;
import py.com.sgs.persistence.dto.AperturaCierreDTO;
import py.com.sgs.persistence.dto.MovMercDetalleDTO;
import py.com.sgs.persistence.dto.NotaCreditoVentaDTO;
import py.com.sgs.persistence.dto.NotaCreditoVentaDetalleDTO;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.PuntoExpedicionDTO;
import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.TimbradoPtoExpedicionDTO;
import py.com.sgs.persistence.dto.UsuariosDTO;
import py.com.sgs.persistence.dto.composite.CabeceraVentaCompositeDTO;
import py.com.sgs.persistence.dto.composite.MovimientoMercaderiaCompositeDTO;
import py.com.sgs.persistence.dto.composite.NotaCreditoVentaCompositeDTO;
import py.com.sgs.util.report.ReportDetalleVenta;
import py.com.sgs.util.report.ReportNotaCredito;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.NotaCreditoVentaService;

public class NotaCreditoVentaServiceImpl implements NotaCreditoVentaService{

	private Logger log = LoggerFactory.getLogger(getClass()); 

	@Autowired	private NotaCreditoVentaDAO notaCreditoVentaDAO;
	@Autowired	private NotaCreditoVentaDetalleDAO notaCreditoVentaDetalleDAO;
	@Autowired	private UsuariosDAO usuariosDAO;
	@Autowired	private AperturaCierreDAO aperturaCierreDAO;
	@Autowired	private TimbradoPtoExpedicionDAO timbradoPtoExpedicionDAO;
	@Autowired	private PuntoExpedicionDAO puntoExpedicionDAO;
	@Autowired	private CabeceraVentaDAO cabeceraVentaDAO;
	@Autowired  private InfoRefOpcDAO infoRefOpcDAO;
	@Autowired 	private MovimientoMercaderiaDAO movimientoMercaderiaDAO;
	@Autowired	private MovMercDetalleDAO movMercDetalleDAO;
	@Autowired	private StockDAO stockDAO;


	public void setNotaCreditoVentaDAO(NotaCreditoVentaDAO notaCreditoVentaDAO) {
		this.notaCreditoVentaDAO = notaCreditoVentaDAO;
	}

	@Override
	public List<NotaCreditoVentaCompositeDTO> listNotaCreditoVenta(InvocationContext ic) throws ServiceException {
		List<NotaCreditoVentaCompositeDTO> notaCreditoVentas =  notaCreditoVentaDAO.listNotaCreditoVenta(Long.parseLong(ic.getUserSucursal()));

		return notaCreditoVentas;
	}

	@Override
	public NotaCreditoVentaDTO getNotaCreditoVenta(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		NotaCreditoVentaDTO notaCreditoVenta =  notaCreditoVentaDAO.getNotaCreditoVentaById(id);
		if (notaCreditoVenta == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditoventa.notfound", id);

		return notaCreditoVenta;
	}

	@Override
	public void deleteNotaCreditoVenta(Long id, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		NotaCreditoVentaDTO notaCreditoVenta =  notaCreditoVentaDAO.getNotaCreditoVentaById(id);
		if(notaCreditoVenta == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditoventa.notfound", id);

		try {
			notaCreditoVentaDAO.deleteNotaCreditoVenta(notaCreditoVenta);
		}
		catch (DataIntegrityViolationException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "notacreditoventa.constraint.violation", id);
		}

	}

	@Override
	public NotaCreditoVentaDTO updateNotaCreditoVenta(Long id, NotaCreditoVentaDTO dto, InvocationContext ic) throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		NotaCreditoVentaDTO notaCreditoVenta = notaCreditoVentaDAO.getNotaCreditoVentaById(id);
		if (notaCreditoVenta == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditoventa.notfound", id);

		notaCreditoVenta.setIdCabeceraVenta(dto.getIdCabeceraVenta());
		notaCreditoVenta.setNroNotaCredito(dto.getNroNotaCredito());
		notaCreditoVenta.setMontoTotal(dto.getMontoTotal());
		notaCreditoVenta.setIva5(dto.getIva5());
		notaCreditoVenta.setIva10(dto.getIva10());
		notaCreditoVenta.setExentas(dto.getExentas());

		notaCreditoVentaDAO.updateNotaCreditoVenta(notaCreditoVenta);


		return notaCreditoVenta;
	}

	@Override
	public NotaCreditoVentaCompositeDTO createNotaCreditoVenta(NotaCreditoVentaCompositeDTO dto, InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

		try {
			List<NotaCreditoVentaDetalleDTO> detalles = null;
			if(dto.getDetalles() != null && !dto.getDetalles().isEmpty()){
				detalles = new ArrayList<NotaCreditoVentaDetalleDTO>();
				detalles.addAll(dto.getDetalles());
			}
			//se debe obtener el usuario por su nombre de usuario
			UsuariosDTO usuario = usuariosDAO.getUsuariosByUser(new UsuariosDTO(ic.getUserPrincipal()));
			
			if(usuario == null){
				throw new ServiceException(ErrorCode.USUARIO_NO_EXISTE,"punto.expedicion.not.found", ic.getUserPrincipal());
			}
			
			//TODO se debe generar el numero de factura	
			//el id timbrado se debe obtener ahora de apertura cierre, buscando por el usuario, un registro que este abierto y sin fecha cierre
			AperturaCierreDTO ap = aperturaCierreDAO.getAperturaCierreVigenteByCajero(usuario.getId());
			if(ap == null)
				throw new ServiceException(ErrorCode.APERTURA_ACTIVA,"apertura.cierre.usuario.not.found", ic.getUserPrincipal());
			
			//se debe buscar el que es tipo N
			TimbradoPtoExpedicionDTO tpe = timbradoPtoExpedicionDAO.getTimbradoPtoExpedicionByIdPtoExpedicionNota(ap.getIdPuntoExpedicion());
			String nroFact = "";
			String nroFactAUtilizar = "";
			
			if(tpe != null){
				Integer nro = Integer.parseInt(tpe.getUltimoNumero()) + 1;
				Integer siguienteNro = Integer.parseInt(tpe.getUltimoNumero());
				//TODO verificar que el numero de factura, no debe exceder el ultimo numero disponible para el punto de exp.
				//se debe completar con ceros 
				nroFact = String.format("%07d", nro);
				nroFactAUtilizar = String.format("%07d", siguienteNro);
				tpe.setUltimoNumero(nroFact);
				
				//se debe actualiza el ultimo numero en la tabla
				timbradoPtoExpedicionDAO.actualizarUltNro(tpe);
				//al numero de factura se le agrega el punto de expedicion
				PuntoExpedicionDTO pe = puntoExpedicionDAO.getPuntoExpedicionById(tpe.getIdPtoExpedicion());
				
				if(pe != null){
					String nroPtoExp = pe.getPuntoExp().trim();
//					String peStr = String.format("%03d", nroPtoExp);
//					
//					nroFact = "001-" + peStr + "-" + nroFact;
					
					nroFactAUtilizar = "001-" + nroPtoExp + "-" + nroFactAUtilizar;
					
				}else{
					throw new ServiceException(ErrorCode.PTO_EXPEDICION_NO_ENCONTRADO,"punto.expedicion.not.found", tpe.getIdPtoExpedicion());
				}
				
			}else{
				throw new ServiceException(ErrorCode.TIMBRADO_PTO_EXPEDICION_NO_ENCONTRADO,"timbra.pto.expedicion.not.found", ap.getIdPuntoExpedicion());
			}
			dto.setNroNotaCredito(nroFactAUtilizar);
			dto.setUsuarioCreacion(ic.getUserPrincipal());
			dto.setIdTimbradoPtoExp(tpe.getId());
			dto.setIdCliente(dto.getCliente().getId());
			dto.setFechaCreacion(new Date(System.currentTimeMillis()));
			dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
			notaCreditoVentaDAO.createNotaCreditoVenta(dto);
			
			
			//se debe crear el registro en movimiento de mercaderias
			MovimientoMercaderiaCompositeDTO movMerc = new MovimientoMercaderiaCompositeDTO();
			movMerc.setIdTipoMovimiento(infoRefOpcDAO.getInfoRefOpcByAbv("ING_M").getId());
			movMerc.setFechaMovimiento(dto.getFechaCreacion());
			movMerc.setTipoOperacion(infoRefOpcDAO.getInfoRefOpcByAbv("CTOVTA").getId());
			movMerc.setUsuarioRegistro(dto.getUsuarioCreacion());
			movMerc.setIdSucursal(dto.getIdSucursal());
			movMerc.setIdOperacion(dto.getId());

			movimientoMercaderiaDAO.createMovimientoMercaderia(movMerc);

			MovMercDetalleDTO detMerc = new MovMercDetalleDTO();

			//se deben crear los detalles
			if(detalles != null){
				for(NotaCreditoVentaDetalleDTO det: detalles){
					
					det.setIdNotaCreditoVenta(dto.getId());
					try {
						notaCreditoVentaDetalleDAO.createNotaCreditoVentaDetalle(det);
					}
					catch (DataIntegrityViolationException e){
						log.error(e.getMessage(), e);
						throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "notacreditoventa.duplicate.key");
					}
					
					
					//se debe actualizar el stock sumando la cantidad vendida del producto de la cantidad en stock
					StockDTO st = stockDAO.stockByIdProducto(new StockDTO(det.getIdProducto(), dto.getIdSucursal()));
					if(st != null){
						st.setStock(st.getStock() + det.getCantidad());
						stockDAO.updateStockVenta(st);
					}else{
						throw new ServiceException(ErrorCode.STOCK_NOT_FOUND, "stock.notfound.producto.sucursal", det.getIdProducto(), dto.getIdSucursal());
					}
					
					//se crean los detalles de movimientos
					detMerc.setIdMovimiento(movMerc.getId());
					detMerc.setIdProducto(det.getIdProducto());
					detMerc.setCantidad(det.getCantidad());

					try{
						movMercDetalleDAO.createMovMercDetalle(detMerc);
					}catch (DuplicateKeyException e){
						log.error(e.getMessage(), e);
						throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "detalleventa.duplicate.key");
					}
					detMerc = new MovMercDetalleDTO();
				}
			}

		}
		catch (DuplicateKeyException e){
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "notacreditoventa.duplicate.key");
		}

		return dto;
	}
	
	@Override
	public PrintDTO imprimirNotaCredito(Long id, InvocationContext ic) throws ServiceException{
		
		PrintDTO retorno = new PrintDTO();
		//se obtiene la cabecera venta
		NotaCreditoVentaCompositeDTO nota = notaCreditoVentaDAO.getNotaCreditoVentaById(id);
		if(nota == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"nota.credito.venta.not.found", id);
		//se agrega la empresa
		nota.setEmpresa(cabeceraVentaDAO.getEmpresaById(1L));
		
		retorno = this.getReportNotaCredito(nota);
		
		return retorno;
		
	}
	
	private PrintDTO getReportNotaCredito(NotaCreditoVentaCompositeDTO param) throws ServiceException {
	      PrintDTO reporte = new PrintDTO();
	      try {
	          ReportNotaCredito rNC = new ReportNotaCredito(param);
	          reporte.setBytes(rNC.generarPDF());
	          reporte.setFileName(param.getNroNotaCredito()); // descomentar para tomar el
	                                                  // id del ds ddt como nombre
	                                                  // del reporte
	          // reporte.setFileName("14002IRE1000001A");
	          // MagicMatch match = Magic.getMagicMatch(reporte.getBytes());
	          // reporte.setMimeType(match.getMimeType());
	          // reporte.setType(match.getExtension());
	      } catch (Exception e) {
	          log.error(e.getMessage(), e);
	          reporte.setMimeType(null);
	          reporte.setType(null);
	          throw new ServiceException(ErrorCode.IMPRIMIR_OC_ERROR, "sgs.print.oc.error", param.getId());
	      }
	      return reporte;
	}

}