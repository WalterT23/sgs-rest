package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.DetalleOrdenPagoDAO;
import py.com.sgs.persistence.dto.DetalleOrdenPagoDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.DetalleOrdenPagoService;

public class DetalleOrdenPagoServiceImpl implements DetalleOrdenPagoService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private DetalleOrdenPagoDAO detalleOrdenPagoDAO;


  public void setDetalleOrdenPagoDAO(DetalleOrdenPagoDAO detalleOrdenPagoDAO) {
    this.detalleOrdenPagoDAO = detalleOrdenPagoDAO;
  }

  @Override
  public List<DetalleOrdenPagoDTO> listDetalleOrdenPago(InvocationContext ic) throws ServiceException {
    List<DetalleOrdenPagoDTO> detalleOrdenPagos =  detalleOrdenPagoDAO.listDetalleOrdenPago();

    return detalleOrdenPagos;
  }

  @Override
  public DetalleOrdenPagoDTO getDetalleOrdenPago(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    DetalleOrdenPagoDTO detalleOrdenPago =  detalleOrdenPagoDAO.getDetalleOrdenPagoById(id);
    if (detalleOrdenPago == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detalleordenpago.notfound", id);

    return detalleOrdenPago;
  }

  @Override
  public void deleteDetalleOrdenPago(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    DetalleOrdenPagoDTO detalleOrdenPago =  detalleOrdenPagoDAO.getDetalleOrdenPagoById(id);
    if(detalleOrdenPago == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detalleordenpago.notfound", id);

    try {
      detalleOrdenPagoDAO.deleteDetalleOrdenPago(detalleOrdenPago);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "detalleordenpago.constraint.violation", id);
    }

  }

  @Override
  public DetalleOrdenPagoDTO updateDetalleOrdenPago(Long id, DetalleOrdenPagoDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    DetalleOrdenPagoDTO detalleOrdenPago = detalleOrdenPagoDAO.getDetalleOrdenPagoById(id);
    if (detalleOrdenPago == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"detalleordenpago.notfound", id);

    detalleOrdenPago.setIdCabeceraOp(dto.getIdCabeceraOp());
    detalleOrdenPago.setIdCabeceraCompra(dto.getIdCabeceraCompra());
    detalleOrdenPago.setMonto(dto.getMonto());
    detalleOrdenPago.setNroRecibo(dto.getNroRecibo());
 
      detalleOrdenPagoDAO.updateDetalleOrdenPago(detalleOrdenPago);
    
    return detalleOrdenPago;
  }

  @Override
  public DetalleOrdenPagoDTO createDetalleOrdenPago(DetalleOrdenPagoDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {

      

      detalleOrdenPagoDAO.createDetalleOrdenPago(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "detalleordenpago.duplicate.key");
    }

    return dto;
  }

}