package py.com.sgs.web.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.UsuarioSucursalDAO;
import py.com.sgs.persistence.dto.UsuarioSucursalDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.UsuarioSucursalService;

public class UsuarioSucursalServiceImpl implements UsuarioSucursalService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private UsuarioSucursalDAO usuarioSucursalDAO;

  @Autowired

  public void setUsuarioSucursalDAO(UsuarioSucursalDAO usuarioSucursalDAO) {
    this.usuarioSucursalDAO = usuarioSucursalDAO;
  }

  @Override
  public List<UsuarioSucursalDTO> listUsuarioSucursal(InvocationContext ic) throws ServiceException {
    List<UsuarioSucursalDTO> usuarioSucursals =  usuarioSucursalDAO.listUsuarioSucursal();
    
    Calendar fecha = Calendar.getInstance();
    for (UsuarioSucursalDTO us : usuarioSucursals) {
    	us.setVigente(us.getFinVigencia().after(fecha.getTime()));
	}
    
    return usuarioSucursals;
  }

  @Override
  public UsuarioSucursalDTO getUsuarioSucursal(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    UsuarioSucursalDTO usuarioSucursal =  usuarioSucursalDAO.getUsuarioSucursalById(id);
    if (usuarioSucursal == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"usuariosucursal.notfound", id);
    
    Calendar fecha = Calendar.getInstance();
    usuarioSucursal.setVigente(usuarioSucursal.getFinVigencia().after(fecha.getTime()));
    
    return usuarioSucursal;
  }

  @Override
  public List<UsuarioSucursalDTO> listSucursalesByUser(Long idUsuario, InvocationContext ic) throws ServiceException {
    List<UsuarioSucursalDTO> usuarioSucursals =  usuarioSucursalDAO.listSucursalesByUser(idUsuario);
    
    if (usuarioSucursals == null)
    	throw new ServiceException(ErrorCode.NO_DATA_FOUND,"usuariosucursal.notfound",idUsuario);
    	
    Calendar fecha = Calendar.getInstance();
    for (UsuarioSucursalDTO us : usuarioSucursals) {
    	us.setVigente(us.getFinVigencia().after(fecha.getTime()));
	}
    
    return usuarioSucursals;
  }
  
  @Override
  public List<UsuarioSucursalDTO> listSucsVigentesByUser(Long idUsuario, InvocationContext ic) throws ServiceException {
    List<UsuarioSucursalDTO> usuarioSucursals =  usuarioSucursalDAO.listSucsVigentesByUser(idUsuario);
    
    if (usuarioSucursals == null)
    	throw new ServiceException(ErrorCode.NO_DATA_FOUND,"usuariosucursal.notfound",idUsuario);
    
    Calendar fecha = Calendar.getInstance();
    for (UsuarioSucursalDTO us : usuarioSucursals) {
    	us.setVigente(us.getFinVigencia().after(fecha.getTime()));
	}
    
    return usuarioSucursals;
  }
  
  @Override
  public void deleteUsuarioSucursal(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    UsuarioSucursalDTO usuarioSucursal =  usuarioSucursalDAO.getUsuarioSucursalById(id);
    if(usuarioSucursal == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"usuariosucursal.notfound", id);

    try {
      usuarioSucursalDAO.cerrarVigenciaUsuarioSucursal(id);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "usuariosucursal.constraint.violation", id);
    }

  }

  @Override
  public UsuarioSucursalDTO updateUsuarioSucursal(Long id, UsuarioSucursalDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    UsuarioSucursalDTO usuarioSucursal = usuarioSucursalDAO.getUsuarioSucursalById(id);
    if (usuarioSucursal == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"usuariosucursal.notfound", id);
    
    if (dto.getIdUsuario() != null)
    	usuarioSucursal.setIdUsuario(dto.getIdUsuario());
    
    if (dto.getIdSucursal() != null)
    	usuarioSucursal.setIdSucursal(dto.getIdSucursal());
    
    if (dto.getInicioVigencia() != null)
    	usuarioSucursal.setInicioVigencia(dto.getInicioVigencia());
    
    if (dto.getFinVigencia() != null)
    	usuarioSucursal.setFinVigencia(dto.getFinVigencia());
    
    if (dto.getFechaRegistro() != null)
    	usuarioSucursal.setFechaRegistro(dto.getFechaRegistro());
    
    usuarioSucursalDAO.updateUsuarioSucursal(usuarioSucursal);

    return usuarioSucursal;
  }
  

  @Override
  public UsuarioSucursalDTO createUsuarioSucursal(UsuarioSucursalDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
    
    if(dto.getIdUsuario() == null || dto.getIdUsuario() < 0)
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
    
    UsuarioSucursalDTO existeAso = usuarioSucursalDAO.controlUsrSuc(dto);
    	
    if (existeAso != null)
    	throw new ServiceException(ErrorCode.USUARIO_SUCURSAL_ALREADY_EXIST,"usuario.sucursal.existe.vigente",dto.getIdSucursal());
    
    if(dto.getIdSucursal() == null || dto.getIdSucursal() < 0)
    	throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
    
    if(dto.getInicioVigencia() == null)
    	dto.setInicioVigencia(new Date(System.currentTimeMillis()));
    
    if(dto.getFinVigencia() == null) {
    	Calendar fecha = Calendar.getInstance();
    	fecha.set(Calendar.YEAR, 3000);
    	dto.setFinVigencia(fecha.getTime());
    }
    
    dto.setFechaRegistro(new Date (System.currentTimeMillis()));
    
    
    try {
      usuarioSucursalDAO.createUsuarioSucursal(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "usuariosucursal.duplicate.key");
    }

    return dto;
  }

}