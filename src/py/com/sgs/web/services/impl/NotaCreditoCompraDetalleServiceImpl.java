package py.com.sgs.web.services.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.NotaCreditoCompraDetalleDAO;
import py.com.sgs.persistence.dao.StockDAO;
import py.com.sgs.persistence.dto.NotaCreditoCompraDetalleDTO;
import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.composite.StockCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.NotaCreditoCompraDetalleService;

public class NotaCreditoCompraDetalleServiceImpl implements NotaCreditoCompraDetalleService{

private Logger log = LoggerFactory.getLogger(getClass()); 

  @Autowired
  private NotaCreditoCompraDetalleDAO notaCreditoCompraDetalleDAO;

  @Autowired
  private StockDAO stockDAO;
  
  public void setNotaCreditoCompraDetalleDAO(NotaCreditoCompraDetalleDAO notaCreditoCompraDetalleDAO) {
    this.notaCreditoCompraDetalleDAO = notaCreditoCompraDetalleDAO;
  }

  @Override
  public List<NotaCreditoCompraDetalleDTO> listNotaCreditoCompraDetalle(InvocationContext ic) throws ServiceException {
    List<NotaCreditoCompraDetalleDTO> notaCreditoCompraDetalles =  notaCreditoCompraDetalleDAO.listNotaCreditoCompraDetalle();

    return notaCreditoCompraDetalles;
  }

  @Override
  public NotaCreditoCompraDetalleDTO getNotaCreditoCompraDetalle(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    NotaCreditoCompraDetalleDTO notaCreditoCompraDetalle =  notaCreditoCompraDetalleDAO.getNotaCreditoCompraDetalleById(id);
    if (notaCreditoCompraDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditocompradetalle.notfound", id);

    return notaCreditoCompraDetalle;
  }

  @Override
  public void deleteNotaCreditoCompraDetalle(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    NotaCreditoCompraDetalleDTO notaCreditoCompraDetalle =  notaCreditoCompraDetalleDAO.getNotaCreditoCompraDetalleById(id);
   
    if(notaCreditoCompraDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditocompradetalle.notfound", id);

    try {
        /*obtengo el stock del detalle que estoy recorriendo*/
    	StockCompositeDTO toUpdate= new StockCompositeDTO();
    	StockDTO di=new StockDTO();
		di.setIdProducto(notaCreditoCompraDetalle.getIdProducto());
		di.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
		
		toUpdate = stockDAO.stockByIdProducto(di);
		
		if (toUpdate == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"stock.notfound", notaCreditoCompraDetalle.getIdProducto());
		
		Double ns = new Double(0);
		/*actualizo stock*/
		if(toUpdate.getStock()==null) {
			ns=ns+notaCreditoCompraDetalle.getCantidad();
		}else {
			ns= toUpdate.getStock() + notaCreditoCompraDetalle.getCantidad();
		}
		
		toUpdate.setStock(ns);
		toUpdate.setUsuarioUltModif(ic.getUserPrincipal());
		toUpdate.setFechaUltModif(new Date(System.currentTimeMillis()));
		
		
		
		stockDAO.updateStock(toUpdate);
        notaCreditoCompraDetalleDAO.deleteNotaCreditoCompraDetalle(notaCreditoCompraDetalle);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "notacreditocompradetalle.constraint.violation", id);
    }

  }

  @Override
  public NotaCreditoCompraDetalleDTO updateNotaCreditoCompraDetalle(Long id, NotaCreditoCompraDetalleDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    NotaCreditoCompraDetalleDTO notaCreditoCompraDetalle = notaCreditoCompraDetalleDAO.getNotaCreditoCompraDetalleById(id);
    if (notaCreditoCompraDetalle == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"notacreditocompradetalle.notfound", id);

    notaCreditoCompraDetalle.setIdNotaCreditoCompra(dto.getIdNotaCreditoCompra());
    notaCreditoCompraDetalle.setIdProducto(dto.getIdProducto());
    notaCreditoCompraDetalle.setTotal(dto.getTotal());
    notaCreditoCompraDetalle.setObservaciones(dto.getObservaciones());
 


      notaCreditoCompraDetalleDAO.updateNotaCreditoCompraDetalle(notaCreditoCompraDetalle);


    return notaCreditoCompraDetalle;
  }

  @Override
  public NotaCreditoCompraDetalleDTO createNotaCreditoCompraDetalle(NotaCreditoCompraDetalleDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {



      notaCreditoCompraDetalleDAO.createNotaCreditoCompraDetalle(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "notacreditocompradetalle.duplicate.key");
    }

    return dto;
  }

}