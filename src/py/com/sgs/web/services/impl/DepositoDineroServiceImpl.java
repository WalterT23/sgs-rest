package py.com.sgs.web.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.DepositoDineroDAO;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.MovimientoRecaudacionDAO;
import py.com.sgs.persistence.dto.DepositoDineroDTO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.MovimientoRecaudacionDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.DepositoDineroService;
import py.com.sgs.web.services.MovimientoRecaudacionService;

public class DepositoDineroServiceImpl implements DepositoDineroService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private DepositoDineroDAO depositoDineroDAO;
	@Autowired
	private MovimientoRecaudacionService movirecasimpl;
	@Autowired
	private MovimientoRecaudacionDAO movirecaDAO;
	@Autowired
	private InfoRefOpcDAO infoRefDAO;

	public void setDepositoDineroDAO(DepositoDineroDAO depositoDineroDAO) {
		this.depositoDineroDAO = depositoDineroDAO;
	}

	@Override
	public List<DepositoDineroDTO> listDepositoDinero(InvocationContext ic)
			throws ServiceException {
		List<DepositoDineroDTO> depositoDineros = depositoDineroDAO
				.listDepositoDinero();

		return depositoDineros;
	}

	@Override
	public DepositoDineroDTO getDepositoDinero(Long id, InvocationContext ic)
			throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		DepositoDineroDTO depositoDinero = depositoDineroDAO
				.getDepositoDineroById(id);
		if (depositoDinero == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"depositodinero.notfound", id);

		return depositoDinero;
	}

	@Override
	public void deleteDepositoDinero(Long id, InvocationContext ic)
			throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		DepositoDineroDTO depositoDinero = depositoDineroDAO
				.getDepositoDineroById(id);
		if (depositoDinero == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,
					"depositodinero.notfound", id);

		try {
			depositoDineroDAO.deleteDepositoDinero(depositoDinero);
		} catch (DataIntegrityViolationException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION,
					"depositodinero.constraint.violation", id);
		}

	}

	@Override
	public DepositoDineroDTO updateDepositoDinero(Long id,
			DepositoDineroDTO dto, InvocationContext ic)
			throws ServiceException {
		if (id == null || id <= 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "id");

		if (null == dto)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		DepositoDineroDTO depositoDinero = depositoDineroDAO.getDepositoDineroById(id);
		
		if (depositoDinero == null)
			throw new ServiceException(ErrorCode.NO_DATA_FOUND,"depositodinero.notfound", id);

		if (dto.getIdBanco() != null)
			depositoDinero.setIdBanco(dto.getIdBanco());

		if (dto.getNroCuenta() != null && !dto.getNroCuenta().trim().isEmpty())
			depositoDinero.setNroCuenta(dto.getNroCuenta());

		if (dto.getFechaDeposito() != null)
			depositoDinero.setFechaDeposito(dto.getFechaDeposito());
		
		if (dto.getMonto().compareTo(BigDecimal.ZERO) < 0)
			throw new ServiceException(ErrorCode.DEPOSITO_DINERO,"invalid.amount ");
			
			
		
		if (dto.getMonto() != null && depositoDinero.getMonto().compareTo(dto.getMonto()) != 0) {
			
			MovimientoRecaudacionDTO movidto = new MovimientoRecaudacionDTO();
			movidto = movirecaDAO.getMovimientoRecaudacionByIdDeposito(id);
			
			if (movidto != null) {

				InfoRefOpcDTO movi = infoRefDAO.getInfoRefOpcByAbv("ADD");
				movidto.setIdMovimiento(movi.getId());
				movidto.setFechaHora(new Date(System.currentTimeMillis()));
				movidto.setIdDepDinero(id);
				
				if(depositoDinero.getMonto().compareTo(dto.getMonto()) > 0) {
					
					InfoRefOpcDTO tipoMov = infoRefDAO.getInfoRefOpcByAbv("ING");

					movidto.setIdTipoMovimiento(tipoMov.getId());
					movidto.setMonto(depositoDinero.getMonto().subtract(dto.getMonto()));
					
					movirecasimpl.createMovimientoRecaudacion(movidto, ic);
					
				} else if (depositoDinero.getMonto().compareTo(dto.getMonto()) < 0) {
					
					InfoRefOpcDTO tipoMov = infoRefDAO.getInfoRefOpcByAbv("EGR");

					movidto.setIdTipoMovimiento(tipoMov.getId());
					movidto.setMonto(dto.getMonto().subtract(depositoDinero.getMonto()));
					
					movirecasimpl.createMovimientoRecaudacion(movidto, ic);
					
				}				
			}
			depositoDinero.setMonto(dto.getMonto());
		}
		
		depositoDineroDAO.updateDepositoDinero(depositoDinero);
		
		return depositoDinero;
	}

	@Override
	public DepositoDineroDTO createDepositoDinero(DepositoDineroDTO dto,
			InvocationContext ic) throws ServiceException {
		if (dto == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		if (dto.getMonto() == null
				|| dto.getMonto().compareTo(BigDecimal.ZERO) == 0)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		if (dto.getFechaDeposito() == null)
			throw new ServiceException(ErrorCode.PARAMETER_ERROR,
					"parameter.error.null", "dto");

		try {
			dto.setUsuarioCreacion(ic.getUserPrincipal());
			dto.setFechaCreacion(new Date(System.currentTimeMillis()));
			dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));

			depositoDineroDAO.createDepositoDinero(dto);

			InfoRefOpcDTO tipoMov = infoRefDAO.getInfoRefOpcByAbv("EGR");
			InfoRefOpcDTO movi = infoRefDAO.getInfoRefOpcByAbv("DDD");

			MovimientoRecaudacionDTO movidto = new MovimientoRecaudacionDTO();
			movidto.setFechaHora(dto.getFechaCreacion());
			movidto.setIdDepDinero(dto.getId());
			movidto.setIdTipoMovimiento(tipoMov.getId());
			movidto.setIdMovimiento(movi.getId());
			movidto.setMonto(dto.getMonto());

			movirecasimpl.createMovimientoRecaudacion(movidto, ic);

		} catch (DuplicateKeyException e) {
			log.error(e.getMessage(), e);
			throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION,
					"depositodinero.duplicate.key");
		}

		return dto;
	}

}