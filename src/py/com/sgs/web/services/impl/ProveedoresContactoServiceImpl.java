package py.com.sgs.web.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.CrossOrigin;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.ProveedoresContactoDAO;
import py.com.sgs.persistence.dto.ProveedoresContactoDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.ProveedoresContactoService;

@CrossOrigin()
public class ProveedoresContactoServiceImpl implements ProveedoresContactoService{

private Logger log = LoggerFactory.getLogger(getClass());  @Autowired
  private ProveedoresContactoDAO proveedoresContactoDAO;


  public void setProveedoresContactoDAO(ProveedoresContactoDAO proveedoresContactoDAO) {
    this.proveedoresContactoDAO = proveedoresContactoDAO;
  }

  @Override
  public List<ProveedoresContactoDTO> listContactosByProveedor(Long idProveedor, InvocationContext ic) throws ServiceException {
   
    if (idProveedor == null || idProveedor <= 0)
	      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","idProveedor");
    	
    	List<ProveedoresContactoDTO> proveedoresContactos =  proveedoresContactoDAO.listContactosByProveedor(idProveedor);
	  
	if (proveedoresContactos == null)
	      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedorescontacto.notfound", idProveedor);
   
    return proveedoresContactos;
  }
  

  @Override
  public ProveedoresContactoDTO getProveedoresContacto(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ProveedoresContactoDTO proveedoresContacto =  proveedoresContactoDAO.getProveedoresContactoById(id);
    
    if (proveedoresContacto == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedorescontacto.notfound", id);

    return proveedoresContacto;
  }

  @Override
  public void deleteProveedoresContacto(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    ProveedoresContactoDTO proveedoresContacto =  proveedoresContactoDAO.getProveedoresContactoById(id);
    if(proveedoresContacto == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedorescontacto.notfound", id);

    try {
      proveedoresContactoDAO.deleteProveedoresContacto(proveedoresContacto);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "proveedorescontacto.constraint.violation", id);
    }

  }

  @Override
  public ProveedoresContactoDTO updateProveedoresContacto(Long id, ProveedoresContactoDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    ProveedoresContactoDTO proveedoresContacto = proveedoresContactoDAO.getProveedoresContactoById(id);
    if (proveedoresContacto == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"proveedorescontacto.notfound", id);

    proveedoresContacto.setNombre(dto.getNombre());
    proveedoresContacto.setTelefono(dto.getTelefono());
    proveedoresContacto.setCorreo(dto.getCorreo());
    
    proveedoresContactoDAO.updateProveedoresContacto(proveedoresContacto);
    
    return proveedoresContacto;
  }

  @Override
  public ProveedoresContactoDTO createProveedoresContacto(ProveedoresContactoDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
      proveedoresContactoDAO.createProveedoresContacto(dto);
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "proveedorescontacto.duplicate.key");
    }

    return dto;
  }


}