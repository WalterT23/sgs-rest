package py.com.sgs.web.services.impl;

import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.MovMercDetalleDAO;
import py.com.sgs.persistence.dao.MovimientoMercaderiaDAO;
import py.com.sgs.persistence.dao.StockDAO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.MovMercDetalleDTO;
import py.com.sgs.persistence.dto.MovimientoMercaderiaDTO;
import py.com.sgs.persistence.dto.StockDTO;
import py.com.sgs.persistence.dto.composite.MovimientoMercaderiaCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.MovimientoMercaderiaService;

public class MovimientoMercaderiaServiceImpl implements MovimientoMercaderiaService{

private Logger log = LoggerFactory.getLogger(getClass());  

  @Autowired
  private MovimientoMercaderiaDAO movimientoMercaderiaDAO;
  
  @Autowired
  private MovMercDetalleDAO movmercdao;
  
  @Autowired
  private InfoRefOpcDAO infoRefDAO;
  
  @Autowired
	private StockDAO stockDAO;

  public void setMovimientoMercaderiaDAO(MovimientoMercaderiaDAO movimientoMercaderiaDAO) {
    this.movimientoMercaderiaDAO = movimientoMercaderiaDAO;
  }
  
  

  public void setMovmercdao(MovMercDetalleDAO movmercdao) {
	this.movmercdao = movmercdao;
}



@Override
  public List<MovimientoMercaderiaDTO> listMovimientoMercaderia(InvocationContext ic) throws ServiceException {
    List<MovimientoMercaderiaDTO> movimientoMercaderias =  movimientoMercaderiaDAO.listMovimientoMercaderia(Long.parseLong(ic.getUserSucursal()));

    return movimientoMercaderias;
  }

  @Override
  public MovimientoMercaderiaDTO getMovimientoMercaderia(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    MovimientoMercaderiaDTO movimientoMercaderia =  movimientoMercaderiaDAO.getMovimientoMercaderiaById(id);
    if (movimientoMercaderia == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"movimientomercaderia.notfound", id);

    return movimientoMercaderia;
  }

  @Override
  public void deleteMovimientoMercaderia(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    MovimientoMercaderiaDTO movimientoMercaderia =  movimientoMercaderiaDAO.getMovimientoMercaderiaById(id);
    if(movimientoMercaderia == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"movimientomercaderia.notfound", id);

    try {
      movimientoMercaderiaDAO.deleteMovimientoMercaderia(movimientoMercaderia);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "movimientomercaderia.constraint.violation", id);
    }

  }

  @Override
  public MovimientoMercaderiaDTO updateMovimientoMercaderia(Long id, MovimientoMercaderiaDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    MovimientoMercaderiaDTO movimientoMercaderia = movimientoMercaderiaDAO.getMovimientoMercaderiaById(id);
    if (movimientoMercaderia == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"movimientomercaderia.notfound", id);

    if(dto.getIdTipoMovimiento() != null)
    	movimientoMercaderia.setIdTipoMovimiento(dto.getIdTipoMovimiento());
    
    if (dto.getFechaMovimiento() != null)
    	movimientoMercaderia.setFechaMovimiento(dto.getFechaMovimiento());
    
    if(dto.getTipoOperacion() != null)
    	movimientoMercaderia.setTipoOperacion(dto.getTipoOperacion());
    
    if(dto.getIdOperacion() != null)
	   movimientoMercaderia.setIdOperacion(dto.getIdOperacion());
 
   
    movimientoMercaderiaDAO.updateMovimientoMercaderia(movimientoMercaderia);
  

    return movimientoMercaderia;
  }

  @Override
  public MovimientoMercaderiaCompositeDTO createMovimientoMercaderia(MovimientoMercaderiaCompositeDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");
    
    if(dto.getIdTipoMovimiento() == null)
    	throw new ServiceException(ErrorCode.MOV_MERC_ERROR,"tipo.mov.not.found");
    
    if(dto.getTipoOperacion() == null)
    	throw new ServiceException(ErrorCode.MOV_MERC_ERROR,"tipo.operacion.not.found");
    	
    Calendar fecha = Calendar.getInstance();
    if(dto.getFechaMovimiento() == null){
    	dto.setFechaMovimiento(fecha.getTime());
    }
    
    if(dto.getUsuarioRegistro().trim().isEmpty())
    	dto.setUsuarioRegistro(ic.getUserPrincipal());
    
    if(dto.getIdSucursal() == null)
    	dto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
    
    try {

    	movimientoMercaderiaDAO.createMovimientoMercaderia(dto);
    	
    	InfoRefOpcDTO tipoOpe = infoRefDAO.getInfoRefOpcByAbv("AJT");
    	InfoRefOpcDTO tipoMovI = infoRefDAO.getInfoRefOpcByAbv("ING_M");
    	InfoRefOpcDTO tipoMovE = infoRefDAO.getInfoRefOpcByAbv("EGR_M");
    	
		if (dto.getDetalles() != null && !dto.getDetalles().isEmpty()){
			
			for (MovMercDetalleDTO d:dto.getDetalles()){
				d.setIdMovimiento(dto.getId());
//				d.setIdProducto(d.getIdProducto());
//				d.setCantidad(d.getCantidad());
				movmercdao.createMovMercDetalle (d);
				if(dto.getTipoOperacion().compareTo(tipoOpe.getId())==0){
					StockDTO st = stockDAO.stockByIdProducto(new StockDTO(d.getIdProducto(), Long.parseLong(ic.getUserSucursal())));
					if(st != null){
						if(dto.getIdTipoMovimiento().compareTo(tipoMovE.getId())==0){
							st.setStock(st.getStock() - d.getCantidad());
						}else if (dto.getIdTipoMovimiento().compareTo(tipoMovI.getId())==0){
							st.setStock(st.getStock() + d.getCantidad());
						}
						stockDAO.updateStockVenta(st);
					}else{
						throw new ServiceException(ErrorCode.STOCK_NOT_FOUND, "stock.notfound.producto.sucursal", d.getIdProducto(), Long.parseLong(ic.getUserSucursal()));
					}
				}
			}
			
			
		}
			

      
      
    }
    catch (DuplicateKeyException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "movimientomercaderia.duplicate.key");
    }

    return dto;
  }

}