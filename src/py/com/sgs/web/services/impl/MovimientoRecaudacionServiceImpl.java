package py.com.sgs.web.services.impl;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import py.com.sgs.exception.ErrorCode;
import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dao.InfoRefOpcDAO;
import py.com.sgs.persistence.dao.MovimientoRecaudacionDAO;
import py.com.sgs.persistence.dao.RecaudacionDAO;
import py.com.sgs.persistence.dto.InfoRefOpcDTO;
import py.com.sgs.persistence.dto.MovimientoRecaudacionDTO;
import py.com.sgs.persistence.dto.RecaudacionDTO;
import py.com.sgs.web.interceptor.InvocationContext;
import py.com.sgs.web.services.MovimientoRecaudacionService;
import py.com.sgs.web.services.RecaudacionService;

public class MovimientoRecaudacionServiceImpl implements MovimientoRecaudacionService{

private Logger log = LoggerFactory.getLogger(getClass());  

  @Autowired
  private MovimientoRecaudacionDAO movimientoRecaudacionDAO;
  @Autowired 
  private RecaudacionDAO recaudacionDAO;
  @Autowired
  private InfoRefOpcDAO infoRefDAO;
  
  @Autowired
  private RecaudacionService recadao;

  public void setMovimientoRecaudacionDAO(MovimientoRecaudacionDAO movimientoRecaudacionDAO) {
    this.movimientoRecaudacionDAO = movimientoRecaudacionDAO;
  }

  @Override
  public List<MovimientoRecaudacionDTO> listMovimientoRecaudacion(InvocationContext ic) throws ServiceException {
    List<MovimientoRecaudacionDTO> movimientoRecaudacions =  movimientoRecaudacionDAO.listMovimientoRecaudacion();

    return movimientoRecaudacions;
  }

  @Override
  public MovimientoRecaudacionDTO getMovimientoRecaudacion(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    MovimientoRecaudacionDTO movimientoRecaudacion =  movimientoRecaudacionDAO.getMovimientoRecaudacionById(id);
    if (movimientoRecaudacion == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"movimientorecaudacion.notfound", id);

    return movimientoRecaudacion;
  }

  @Override
  public void deleteMovimientoRecaudacion(Long id, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    MovimientoRecaudacionDTO movimientoRecaudacion =  movimientoRecaudacionDAO.getMovimientoRecaudacionById(id);
    if(movimientoRecaudacion == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"movimientorecaudacion.notfound", id);

    try {
      movimientoRecaudacionDAO.deleteMovimientoRecaudacion(movimientoRecaudacion);
    }
    catch (DataIntegrityViolationException e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DATA_INTEGRITY_VIOLATION, "movimientorecaudacion.constraint.violation", id);
    }

  }

  @Override
  public MovimientoRecaudacionDTO updateMovimientoRecaudacion(Long id, MovimientoRecaudacionDTO dto, InvocationContext ic) throws ServiceException {
    if (id == null || id <= 0)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","id");

    if (null == dto)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    MovimientoRecaudacionDTO movimientoRecaudacion = movimientoRecaudacionDAO.getMovimientoRecaudacionById(id);
    if (movimientoRecaudacion == null)
      throw new ServiceException(ErrorCode.NO_DATA_FOUND,"movimientorecaudacion.notfound", id);
    
    if(dto.getIdTipoMovimiento() != null)
    	movimientoRecaudacion.setIdTipoMovimiento(dto.getIdTipoMovimiento());
    
    if(dto.getIdMovimiento() != null)
    	movimientoRecaudacion.setIdMovimiento(dto.getIdMovimiento());
    
    if(dto.getMonto() != null)
    	movimientoRecaudacion.setMonto(dto.getMonto());
    
    if(dto.getFechaHora() != null)
    	movimientoRecaudacion.setFechaHora(dto.getFechaHora());
    
    if(dto.getIdAperturaCierre() != null)
    	movimientoRecaudacion.setIdAperturaCierre(dto.getIdAperturaCierre());
    
    if(dto.getObs() != null)
    	movimientoRecaudacion.setObs(dto.getObs());
    
    if(dto.getIdDepDinero() != null)
    	movimientoRecaudacion.setIdDepDinero(dto.getIdDepDinero());
    
    movimientoRecaudacionDAO.updateMovimientoRecaudacion(movimientoRecaudacion);

    return movimientoRecaudacion;
  }

  @Override
  public MovimientoRecaudacionDTO createMovimientoRecaudacion(MovimientoRecaudacionDTO dto, InvocationContext ic) throws ServiceException {
    if (dto == null)
      throw new ServiceException(ErrorCode.PARAMETER_ERROR,"parameter.error.null","dto");

    try {
    	
    	RecaudacionDTO rdto = recaudacionDAO.getRecaudacionVigente(Long.parseLong(ic.getUserSucursal()));
    	InfoRefOpcDTO ingreso = infoRefDAO.getInfoRefOpcByAbv("ING");
    	
    	if(rdto==null) {
    		
    		RecaudacionDTO rDto = new RecaudacionDTO();
    		
	    		if(dto.getIdTipoMovimiento().equals(ingreso.getId())) {
	 
	    			//cargo el ingreso
	        			rDto.setIngresos(dto.getMonto());
	   
	    		}else {
	    			//cargo el egreso
	    			rDto.setEgresos(dto.getMonto());
	    		}

	    	//inserto el primer registro en recaudacion
            rDto.setFecha(dto.getFechaHora());
            rDto.setIdSucursal(Long.parseLong(ic.getUserSucursal()));
		    recadao.createRecaudacion(rDto, ic);

		    
    	}else {
    		
    		if(dto.getIdTipoMovimiento().equals(ingreso.getId())) {
    			
        			//sumo los ingresos
        			BigDecimal monto= rdto.getIngresos().add(dto.getMonto());
        			rdto.setIngresos(monto);
        	}else {
        			//sumo los egresos
	    			BigDecimal monto= rdto.getEgresos().add(dto.getMonto());
	    			rdto.setEgresos(monto);
            }
    		
    		recadao.updateRecaudacion(rdto.getId(), rdto, ic);
    	}
      
    	//creo el movimiento de la recaudacion
      movimientoRecaudacionDAO.createMovimientoRecaudacion(dto);
      
      
    }
    catch (Exception e){
      log.error(e.getMessage(), e);
      throw new ServiceException(ErrorCode.DUPLICATE_KEY_EXCEPTION, "movimientorecaudacion.duplicate.key");
    }

    return dto;
  }

}