package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.ConsultaPagosDTO;
import py.com.sgs.persistence.dto.CuotasDTO;
import py.com.sgs.persistence.dto.CuotasPenDTO;
import py.com.sgs.persistence.dto.EstadoCuentaProveedorDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.FiltroCuotasDTO;
import py.com.sgs.persistence.dto.composite.CuotasCompositeDTO;
import py.com.sgs.persistence.dto.composite.CuotasDetalleCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface CuotasService {
  List<CuotasDTO> listCuotas(InvocationContext ic) throws ServiceException;
  List<ConsultaPagosDTO> listCuotasPendientes(InvocationContext ic) throws ServiceException;
  List<CuotasDetalleCompositeDTO> listCuotasporProv(CuotasDTO cd) throws ServiceException;
  CuotasDTO getCuotas(Long id, InvocationContext ic) throws ServiceException;
  FiltroCuotasDTO getReporCuotas (FiltroCuotasDTO dto , InvocationContext ic)throws ServiceException;
  
  FileReportDTO getCuotasaVencerImpresion(FiltroCuotasDTO dto, InvocationContext ic) throws ServiceException, Exception;
  

  void deleteCuotas(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  CuotasDTO updateCuotas(Long id, CuotasDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  CuotasCompositeDTO createCuotas( CuotasCompositeDTO dto, InvocationContext ic) throws ServiceException;

}