package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.CabeceraCompraDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.FiltroComprasDTO;
import py.com.sgs.persistence.dto.FiltroCuotasDTO;
import py.com.sgs.persistence.dto.composite.CabeceraCompraCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface CabeceraCompraService {
  List<CabeceraCompraCompositeDTO> listCabeceraCompra(InvocationContext ic) throws ServiceException;
  List<CabeceraCompraCompositeDTO> getCabeceraCompraByIdProveedor(CabeceraCompraDTO cc) throws ServiceException;
  CabeceraCompraDTO getCabeceraCompra(Long id, InvocationContext ic) throws ServiceException;
  FiltroComprasDTO getReporCompras (FiltroComprasDTO dto , InvocationContext ic)throws ServiceException;
  FileReportDTO getComprasImpresion (FiltroComprasDTO dto, InvocationContext ic) throws ServiceException, Exception;
  
  void deleteCabeceraCompra(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  CabeceraCompraCompositeDTO updateCabeceraCompra(Long id, CabeceraCompraCompositeDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  CabeceraCompraCompositeDTO createCabeceraCompra(CabeceraCompraCompositeDTO dto, InvocationContext ic) throws ServiceException;

}