package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.FormaPagoDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface FormaPagoService {
  List<FormaPagoDTO> listFormaPago(InvocationContext ic) throws ServiceException;

  FormaPagoDTO getFormaPago(Long id, InvocationContext ic) throws ServiceException;

  void deleteFormaPago(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  FormaPagoDTO updateFormaPago(Long id, FormaPagoDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  FormaPagoDTO createFormaPago(FormaPagoDTO dto, InvocationContext ic) throws ServiceException;

}