package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.OrdenCompraDetalleDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface OrdenCompraDetalleService {
  List<OrdenCompraDetalleDTO> listOrdenCompraDetalle(InvocationContext ic) throws ServiceException;
  
  List<OrdenCompraDetalleDTO> listDetalleByIdOC(Long idOC, InvocationContext ic) throws ServiceException;

  OrdenCompraDetalleDTO getOrdenCompraDetalle(Long id, InvocationContext ic) throws ServiceException;

  void deleteOrdenCompraDetalle(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  OrdenCompraDetalleDTO updateOrdenCompraDetalle(Long id, OrdenCompraDetalleDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  OrdenCompraDetalleDTO createOrdenCompraDetalle(OrdenCompraDetalleDTO dto, InvocationContext ic) throws ServiceException;

}