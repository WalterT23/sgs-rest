package py.com.sgs.web.services;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.CuotasPenDTO;
import py.com.sgs.persistence.dto.EstadoCuentaProveedorDTO;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.FiltroCuotasDTO;
import py.com.sgs.persistence.dto.ProveedoresDTO;
import py.com.sgs.persistence.dto.ReporteVentasRealizadasDTO;
import py.com.sgs.persistence.dto.TransaccionesCompraDTO;
import py.com.sgs.persistence.dto.composite.ProveedoresCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ProveedoresService {
  List<ProveedoresCompositeDTO> listProveedores(InvocationContext ic) throws ServiceException;

  //List<ProveedoresContactoDTO> getContactosByProveedor(Long idProveedor, InvocationContext ic) throws ServiceException;
  
  ProveedoresCompositeDTO getProveedores(Long id, InvocationContext ic) throws ServiceException;
  
  List<ProveedoresCompositeDTO> getProveedorById(Long id, InvocationContext ic) throws ServiceException;
  
  List<CuotasPenDTO> listSaldoProveedor( InvocationContext ic) throws ServiceException;
  
  List<ProveedoresDTO> getProveedoresByRuc(String ruc, InvocationContext ic) throws ServiceException;
  
  ProveedoresDTO getProveedorSById(Long id, InvocationContext ic) throws ServiceException;
  
  EstadoCuentaProveedorDTO getEstadoCuenta (EstadoCuentaProveedorDTO dto , InvocationContext ic)throws ServiceException;
  
  FileReportDTO getEstadoCuentaImpresion(EstadoCuentaProveedorDTO dto, InvocationContext ic) throws ServiceException, Exception;
  
  //List<EstadoCuentaProveedorDTO> getProveedoresSaldo (EstadoCuentaProveedorDTO dto , InvocationContext ic)throws ServiceException;
  
  void deleteProveedores(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ProveedoresDTO updateProveedores(Long id, ProveedoresDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  ProveedoresDTO createProveedores(ProveedoresDTO dto, InvocationContext ic) throws ServiceException;

  

}