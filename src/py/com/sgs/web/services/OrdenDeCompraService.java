package py.com.sgs.web.services;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.PrintDTO;
import py.com.sgs.persistence.dto.composite.OrdenDeCompraCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface OrdenDeCompraService {
  List<OrdenDeCompraCompositeDTO> listOrdenDeCompra(InvocationContext ic) throws ServiceException;
  
  List<OrdenDeCompraCompositeDTO> listConfirmadas(InvocationContext ic) throws ServiceException;
  
  OrdenDeCompraCompositeDTO getOrdenDeCompraByNroOC(String nroOrdenCompra ,InvocationContext ic) throws ServiceException;

  OrdenDeCompraCompositeDTO getOrdenDeCompraById(Long id, InvocationContext ic) throws ServiceException;

  void deleteOrdenDeCompra(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  OrdenDeCompraCompositeDTO updateOrdenDeCompra(Long id, OrdenDeCompraCompositeDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  OrdenDeCompraCompositeDTO createOrdenDeCompra(OrdenDeCompraCompositeDTO dto, InvocationContext ic) throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  OrdenDeCompraCompositeDTO confirmarOrdenDeCompra(Long id, InvocationContext ic) throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  OrdenDeCompraCompositeDTO anularOrdenDeCompra(Long id, InvocationContext ic) throws ServiceException;
  
//  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  PrintDTO imprimirOrdenDeCompra(Long id, InvocationContext ic) throws ServiceException;

}