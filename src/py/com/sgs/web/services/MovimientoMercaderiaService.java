package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.MovimientoMercaderiaDTO;
import py.com.sgs.persistence.dto.composite.MovimientoMercaderiaCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface MovimientoMercaderiaService {
  List<MovimientoMercaderiaDTO> listMovimientoMercaderia(InvocationContext ic) throws ServiceException;

  MovimientoMercaderiaDTO getMovimientoMercaderia(Long id, InvocationContext ic) throws ServiceException;

  void deleteMovimientoMercaderia(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  MovimientoMercaderiaDTO updateMovimientoMercaderia(Long id, MovimientoMercaderiaDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  MovimientoMercaderiaCompositeDTO createMovimientoMercaderia(MovimientoMercaderiaCompositeDTO dto, InvocationContext ic) throws ServiceException;

}