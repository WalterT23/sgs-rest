package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.PagoProvDetalleDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface PagoProvDetalleService {
  List<PagoProvDetalleDTO> listPagoProvDetalle(InvocationContext ic) throws ServiceException;

  PagoProvDetalleDTO getPagoProvDetalle(Long id, InvocationContext ic) throws ServiceException;

  void deletePagoProvDetalle(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  PagoProvDetalleDTO updatePagoProvDetalle(Long id, PagoProvDetalleDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  PagoProvDetalleDTO createPagoProvDetalle(PagoProvDetalleDTO dto, InvocationContext ic) throws ServiceException;

}