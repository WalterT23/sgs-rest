package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.CuotaDetalleDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface CuotaDetalleService {
  List<CuotaDetalleDTO> listCuotaDetalle(InvocationContext ic) throws ServiceException;

  CuotaDetalleDTO getCuotaDetalle(Long id, InvocationContext ic) throws ServiceException;

  void deleteCuotaDetalle(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  CuotaDetalleDTO updateCuotaDetalle(Long id, CuotaDetalleDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  CuotaDetalleDTO createCuotaDetalle(CuotaDetalleDTO dto, InvocationContext ic) throws ServiceException;

}