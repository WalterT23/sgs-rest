package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.FileReportDTO;
import py.com.sgs.persistence.dto.RepoMovimientoProductosDTO;
import py.com.sgs.persistence.dto.ReporteProductosMasVendidosDTO;
import py.com.sgs.persistence.dto.ReporteStockDTO;
import py.com.sgs.persistence.dto.ReporteVentasRealizadasDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface ReportesService {
	

  ReporteVentasRealizadasDTO getReporteVentasRealizadas(ReporteVentasRealizadasDTO dto, InvocationContext ic) throws ServiceException;
  
  FileReportDTO getReporteVentasRealizadasImpresion(ReporteVentasRealizadasDTO dto, InvocationContext ic) throws ServiceException, Exception;
  
  ReporteProductosMasVendidosDTO getReporteProMasVendidos(ReporteProductosMasVendidosDTO dto, InvocationContext ic) throws ServiceException;
  
  FileReportDTO getReporteProMasVendidosImpresion(ReporteProductosMasVendidosDTO dto, InvocationContext ic) throws ServiceException, Exception;

  ReporteStockDTO getReporteStock(ReporteStockDTO dto, InvocationContext ic) throws ServiceException;
  
  FileReportDTO getReporteStockImpresion(ReporteStockDTO dto, InvocationContext ic) throws ServiceException, Exception;
  
  RepoMovimientoProductosDTO getReporteMovProductos(RepoMovimientoProductosDTO dto, InvocationContext ic) throws ServiceException;
  
  FileReportDTO getReporteMovProductosImpresion(RepoMovimientoProductosDTO dto, InvocationContext ic) throws ServiceException, Exception;
  
}