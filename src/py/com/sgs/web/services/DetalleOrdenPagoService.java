package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.DetalleOrdenPagoDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface DetalleOrdenPagoService {
  List<DetalleOrdenPagoDTO> listDetalleOrdenPago(InvocationContext ic) throws ServiceException;

  DetalleOrdenPagoDTO getDetalleOrdenPago(Long id, InvocationContext ic) throws ServiceException;

  void deleteDetalleOrdenPago(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DetalleOrdenPagoDTO updateDetalleOrdenPago(Long id, DetalleOrdenPagoDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DetalleOrdenPagoDTO createDetalleOrdenPago(DetalleOrdenPagoDTO dto, InvocationContext ic) throws ServiceException;

}