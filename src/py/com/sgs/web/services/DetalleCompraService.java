package py.com.sgs.web.services;

import py.com.sgs.exception.ServiceException;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.persistence.dto.DetalleCompraDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface DetalleCompraService {
  List<DetalleCompraDTO> listDetalleCompra(InvocationContext ic) throws ServiceException;

  DetalleCompraDTO getDetalleCompra(Long id, InvocationContext ic) throws ServiceException;

  void deleteDetalleCompra(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DetalleCompraDTO updateDetalleCompra(Long id, DetalleCompraDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  DetalleCompraDTO createDetalleCompra(DetalleCompraDTO dto, InvocationContext ic) throws ServiceException;

}