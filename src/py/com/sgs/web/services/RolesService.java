package py.com.sgs.web.services;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import py.com.sgs.exception.ServiceException;
import py.com.sgs.persistence.dto.RolesDTO;
import py.com.sgs.persistence.dto.UsuarioRolDTO;
import py.com.sgs.persistence.dto.composite.RolesCompositeDTO;
import py.com.sgs.web.interceptor.InvocationContext;

public interface RolesService {
  List<RolesCompositeDTO> listRoles(InvocationContext ic) throws ServiceException;
  
  List<RolesCompositeDTO> getByUsuario(Long idUsuario, InvocationContext ic) throws ServiceException;

  List<RolesCompositeDTO> getDispByUsuario(Long idUsuario, InvocationContext ic) throws ServiceException;
  
  RolesDTO getRoles(Long id, InvocationContext ic) throws ServiceException;

  void deleteRoles(Long id, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  RolesDTO updateRoles(Long id, RolesDTO dto, InvocationContext ic) throws ServiceException;

  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  RolesDTO createRoles(RolesDTO dto, InvocationContext ic) throws ServiceException;
  
  @Transactional(value="transactionManager", rollbackFor={ServiceException.class},propagation=Propagation.REQUIRED)
  UsuarioRolDTO createUsuarioRol(UsuarioRolDTO dto, InvocationContext ic) throws ServiceException;

  void deleteUsuarioRol(UsuarioRolDTO dto, InvocationContext ic) throws ServiceException;

}