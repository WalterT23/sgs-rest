package py.com.sgs.web.interceptor;


/**
 * Define los datos obligatorios de contexto de la invocacion actual
 *
 */
public interface InvocationContext {
	
	/**
	 * Obtiene el nombre del usuario conectado remotamente
	 */
	String getUserPrincipal();	
	
	/**
	 * Obtiene el nombre de la aplicacion que invoca el servicio
	 */
	String getApplicationNameHeader();
	
	/**
	 * Obtiene la url de la pagina utilizada remotamente para acceder al servicio
	 */
	String getApplicationPageHeader();
	
	String getUserSucursal();
	
}
