package py.com.sgs.web.interceptor;


public class DefaultInvocationContext implements InvocationContext {

	private String userPrincipal;
	private String applicationNameHeader;
	private String applicationPageHeader;
	private String userSucursal;

	public DefaultInvocationContext(String userPrincipal, String applicationNameHeader, String applicationPageHeader, String userSucursal) {
		this.userPrincipal = userPrincipal;
		this.applicationNameHeader = applicationNameHeader;
		this.applicationPageHeader = applicationPageHeader;
		this.userSucursal = userSucursal;
	}

	@Override
	public String getUserPrincipal() {
		return userPrincipal;
	}

	@Override
	public String getApplicationNameHeader() {
		return applicationNameHeader;
	}

	@Override
	public String getApplicationPageHeader() {
		return applicationPageHeader;
	}
	
	@Override
	public String getUserSucursal() {
		return userSucursal;
	}
}
