package py.com.sgs.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import py.com.sgs.config.HttpHeaderConfig;
import py.com.sgs.config.SgsRestApplicationContextProvider;


public class RequestInterceptor extends HandlerInterceptorAdapter {

  private Logger log = LoggerFactory.getLogger(getClass());
    
  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    
    try{
      if (log.isTraceEnabled())
        log.trace("executing context interceptor");
      
      ApplicationContext ctx = SgsRestApplicationContextProvider.getContext();
      
      HttpHeaderConfig config = (HttpHeaderConfig) ctx.getBean(BeanNames.HTTP_HEADERS_BEAN);
      
      String userPrincipal = request.getHeader(config.getUserPrincipalHeader());
      String applicationName = request.getHeader(config.getApplicationNameHeader());
      String applicationPage = request.getHeader(config.getApplicationPageHeader());
      String userSucursal = request.getHeader(config.getUserSucursalHeader());
      
      DefaultInvocationContext ic = new DefaultInvocationContext(userPrincipal, applicationName, applicationPage, userSucursal);
      
      request.setAttribute(config.getContextAttributeName(), ic);
      
      if (log.isTraceEnabled())
        log.trace("invocation context set to {}",ic);
      
    } catch(Exception e ) {
      log.error(e.getMessage(), e);
      log.error("invalid request. cannot get header parameters for invocation context");
      return true;
    }

    return true;
  }
}
